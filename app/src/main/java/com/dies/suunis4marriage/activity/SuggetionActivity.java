package com.dies.suunis4marriage.activity;

import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import com.dies.suunis4marriage.R;
import com.dies.suunis4marriage.adapter.FragmentAdapter;
import com.dies.suunis4marriage.fragment.CompatibilitySearch.SuggestionFragment;
import com.dies.suunis4marriage.fragment.NearByFragment;
import com.dies.suunis4marriage.fragment.NearMeFragment;

public class SuggetionActivity extends AppCompatActivity {


    ViewPager viewPager;
    TabLayout tabs;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suggetion);

        viewPager = (ViewPager)findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        // Set Tabs inside Toolbar
        tabs = (TabLayout)findViewById(R.id.result_tabs);
        tabs.setupWithViewPager(viewPager);
        }

    private void setupViewPager(ViewPager viewPager) {
        FragmentAdapter adapter = new FragmentAdapter(getSupportFragmentManager());
        adapter.addFragment(new SuggestionFragment(), "Suggestion");
        adapter.addFragment(new NearMeFragment(), "Near Me");
        adapter.addFragment(new NearByFragment(), "NearBy Profile");
        viewPager.setAdapter(adapter);
    }
}
