package com.dies.suunis4marriage.activity;

import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;

import android.widget.ImageView;

import com.dies.suunis4marriage.R;
import com.dies.suunis4marriage.adapter.FragmentAdapter;
import com.dies.suunis4marriage.application.Utility;
import com.dies.suunis4marriage.fragment.ActivatePlanFragment;
import com.dies.suunis4marriage.fragment.AllPlanFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PlanActivity extends AppCompatActivity {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    @BindView(R.id.back_icon)
    ImageView back_icon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plan);
        ButterKnife.bind(this);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        back_icon.setOnClickListener(view -> {
            finish();
        });

        tabLayout = (TabLayout) findViewById(R.id.result_tabs);
        tabLayout.setupWithViewPager(viewPager);
        if(Utility.getAppcon().getSession().screen_name.equals("upgrade")){
            viewPager.setCurrentItem(1);
        }


    }

    private void setupViewPager(ViewPager viewPager) {
        FragmentAdapter adapter = new FragmentAdapter(getSupportFragmentManager());
        adapter.addFragment(new ActivatePlanFragment(), "Activated Plan");
        adapter.addFragment(new AllPlanFragment(), "All Plan");
        viewPager.setAdapter(adapter);
    }
}
