package com.dies.suunis4marriage.activity.profile;

import android.app.ProgressDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.dies.suunis4marriage.R;
import com.dies.suunis4marriage.adapter.RecyclerViewAdapter;
import com.dies.suunis4marriage.apiservice.ApiService;
import com.dies.suunis4marriage.apiservice.ApiServiceCreator;
import com.dies.suunis4marriage.application.SessionManager;
import com.dies.suunis4marriage.application.Utility;
import com.dies.suunis4marriage.model.QuestionsAnswerModel;

import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.Observer;
import rx.schedulers.Schedulers;

public class PrimaryQuestion extends AppCompatActivity {


    List<String> initItemList = new ArrayList<>();
    RecyclerViewAdapter chkadapter;
    Button btn_que;

    SessionManager sessionManager;
    ApiService apiservice;
    ProgressDialog pDialog;
    RecyclerView.LayoutManager layoutManager;
    int statusCode;
    RecyclerView recyclerViewWithCheckbox;
    List<QuestionsAnswerModel.Data> ret;

    EditText et_que1;
   // @BindView(R.id.et_que2)
    EditText et_que2;
   // @BindView(R.id.et_que3)
    EditText et_que3;

    private Integer[] friend_id = new Integer[]{};

    String friend_id_str = "";
    String[] Questions;
    String[] selected_Questions;
    String str="";
    Button btn_delete;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_primary_question);
        //ButterKnife.bind(this);
        et_que1=findViewById(R.id.et_que1);
        et_que2=findViewById(R.id.et_que2);
        et_que3=findViewById(R.id.et_que3);

        btn_delete=findViewById(R.id.btn_delete);
        recyclerViewWithCheckbox = findViewById(R.id.recycler_view);
        btn_que = findViewById(R.id.btn_que);
        sessionManager = new SessionManager(this);
        apiservice = ApiServiceCreator.createService("latest");

        getQuestions();

        btn_delete.setOnClickListener(view -> {
            DeleteQuestions();
        });

        btn_que.setOnClickListener(view -> {

//            SparseBooleanArray selectedRows = chkadapter.getSelectedIds();
//            if (selectedRows.size() > 0) {
//                StringBuilder stringBuilder = new StringBuilder();
//                StringBuilder string = new StringBuilder();
//                for (int i = 0; i < selectedRows.size(); i++) {
//                    if (selectedRows.valueAt(i)) {
//                        String selectedRowLabel = initItemList.get(selectedRows.keyAt(i));
////                        friend_id_str=string.append(ret.get(i).getQ_id()).append(",");
////                        friend_id_str += friend_id[(int) ret.get(i).getQ_id()] + ",";
////                        friend_id_str= ret.get(i).getQ_id();
//                        friend_id_str = stringBuilder.append(selectedRowLabel + ",").toString();
//                    }
//                }
//                Toast.makeText(this, "Selected Rows\n" + friend_id_str, Toast.LENGTH_SHORT).show();
//            }

            if (et_que1.getText().toString().equals("") || et_que2.getText().toString().equals("") || et_que3.getText().toString().equals("")) {
                Utility.displayToast(this, "Please Write all Questions");
            } else {
                setQuestions(friend_id_str);
            }
        });
    }


    public void getQuestions() {


        recyclerViewWithCheckbox.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerViewWithCheckbox.setLayoutManager(linearLayoutManager);
        pDialog = new ProgressDialog(this);
        pDialog.setTitle("Checking Data");
        pDialog.setMessage("Please Wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        Observable<QuestionsAnswerModel> responseObservable = apiservice.getQuestions(sessionManager.getKeyId());

        responseObservable.subscribeOn(Schedulers.newThread())
                .observeOn(rx.android.schedulers.AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> {
                    if (throwable instanceof retrofit2.HttpException) {
                        retrofit2.HttpException ex = (retrofit2.HttpException) throwable;
                        statusCode = ex.code();
                        Log.e("error", ex.getLocalizedMessage());
                    } else if (throwable instanceof SocketTimeoutException) {
                        statusCode = 1000;
                    }
                    return Observable.empty();
                })
                .subscribe(new Observer<QuestionsAnswerModel>() {
                    @Override
                    public void onCompleted() {
                        pDialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("error", "" + e.getMessage());
                    }

                    @Override
                    public void onNext(QuestionsAnswerModel questionsAnswerModel) {
                        statusCode = questionsAnswerModel.getStatusCode();
                        if (statusCode == 200) {
                            ArrayList<String> id=new ArrayList<>();
                            ArrayList<String> questions= new ArrayList<>();
                            if (questionsAnswerModel.getData().size()>0){
                                btn_delete.setVisibility(View.VISIBLE);
                                for(int i=0;i<questionsAnswerModel.getData().size();i++){
                                    id.add(questionsAnswerModel.getData().get(i).getQ_id());
                                }


                               // questions.add(questionsAnswerModel.getData().get(0).getQuestions());
                            }else {

                            }

                            for (int i=0;i<questionsAnswerModel.getData().size();i++){
                                questions.add(questionsAnswerModel.getData().get(i).getQuestions());
                            }
                            Object[] mStringArray = questions.toArray();
                            str = id.toString();
                            str = str.replaceAll("\\[", "").replaceAll("\\]","");

                                et_que1.setText((String)mStringArray[0]);
                                et_que2.setText((String)mStringArray[1]);
                                et_que3.setText((String)mStringArray[2]);
                               // Log.d("string is",(String)mStringArray[i]);






//                            ret = questionsAnswerModel.getData();
//                            friend_id = new Integer[ret.size()];
//                            for (int i = 0; i < ret.size(); i++) {
//                                initItemList.add(ret.get(i).getQ_id());
//                                friend_id[i] = Integer.parseInt(ret.get(i).getQ_id());
//                            }
//                            List<String> que=new ArrayList<>();
//                            for (int i=0;i<questionsAnswerModel.getData().size();i++){
//                                que.add(questionsAnswerModel.getData().get(i).getQuestions());
//                            }
//                            Questions=que.toArray(new String[questionsAnswerModel.getData().size()]);
//
//                            chkadapter = new RecyclerViewAdapter(PrimaryQuestion.this, questionsAnswerModel.getData());
//                            recyclerViewWithCheckbox.setAdapter(chkadapter);

                        }else {
                            btn_delete.setVisibility(View.GONE);
                        }
                    }
                });


    }


    public void setQuestions(String questions) {

        pDialog = new ProgressDialog(this);
        pDialog.setTitle("Checking Data");
        pDialog.setMessage("Please Wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        Observable<QuestionsAnswerModel> responseObservable = apiservice.setQuestions(sessionManager.getKeyId(),et_que1.getText().toString()
                ,et_que2.getText().toString()
                ,et_que3.getText().toString(),str);

        responseObservable.subscribeOn(Schedulers.newThread())
                .observeOn(rx.android.schedulers.AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> {
                    if (throwable instanceof retrofit2.HttpException) {
                        retrofit2.HttpException ex = (retrofit2.HttpException) throwable;
                        statusCode = ex.code();
                        Log.e("error", ex.getLocalizedMessage());
                    } else if (throwable instanceof SocketTimeoutException) {
                        statusCode = 1000;
                    }
                    return Observable.empty();
                })
                .subscribe(new Observer<QuestionsAnswerModel>() {
                    @Override
                    public void onCompleted() {
                        pDialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("error", "" + e.getMessage());
                    }

                    @Override
                    public void onNext(QuestionsAnswerModel questionsAnswerModel) {
                        statusCode = questionsAnswerModel.getStatusCode();
                        if (statusCode == 200) {
                            Utility.displayToast(PrimaryQuestion.this,questionsAnswerModel.getMessage());
                        }
                    }
                });


    }



    public void DeleteQuestions(){

            pDialog = new ProgressDialog(this);
            pDialog.setTitle("Checking Data");
            pDialog.setMessage("Please Wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();

            Observable<QuestionsAnswerModel> responseObservable = apiservice.deleteQuestions(sessionManager.getKeyId());

            responseObservable.subscribeOn(Schedulers.newThread())
                    .observeOn(rx.android.schedulers.AndroidSchedulers.mainThread())
                    .onErrorResumeNext(throwable -> {
                        if (throwable instanceof retrofit2.HttpException) {
                            retrofit2.HttpException ex = (retrofit2.HttpException) throwable;
                            statusCode = ex.code();
                            Log.e("error", ex.getLocalizedMessage());
                        } else if (throwable instanceof SocketTimeoutException) {
                            statusCode = 1000;
                        }
                        return Observable.empty();
                    })
                    .subscribe(new Observer<QuestionsAnswerModel>() {
                        @Override
                        public void onCompleted() {
                            pDialog.dismiss();
                        }

                        @Override
                        public void onError(Throwable e) {
                            Log.e("error", "" + e.getMessage());
                        }

                        @Override
                        public void onNext(QuestionsAnswerModel questionsAnswerModel) {
                            statusCode = questionsAnswerModel.getStatusCode();
                            if (statusCode == 200) {
                                Utility.displayToast(PrimaryQuestion.this,questionsAnswerModel.getMessage());

                                et_que1.setText("");
                                et_que2.setText("");
                                et_que3.setText("");

                            }else {
                                Utility.displayToast(PrimaryQuestion.this,questionsAnswerModel.getMessage());
                            }
                        }
                    });

    }


    public void getMarkedQuestions(){

        pDialog = new ProgressDialog(this);
        pDialog.setTitle("Checking Data");
        pDialog.setMessage("Please Wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        Observable<QuestionsAnswerModel> responseObservable = apiservice.setMarkQuestions(sessionManager.getKeyId());

        responseObservable.subscribeOn(Schedulers.newThread())
                .observeOn(rx.android.schedulers.AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> {
                    if (throwable instanceof retrofit2.HttpException) {
                        retrofit2.HttpException ex = (retrofit2.HttpException) throwable;
                        statusCode = ex.code();
                        Log.e("error", ex.getLocalizedMessage());
                    } else if (throwable instanceof SocketTimeoutException) {
                        statusCode = 1000;
                    }
                    return Observable.empty();
                })
                .subscribe(new Observer<QuestionsAnswerModel>() {
                    @Override
                    public void onCompleted() {
                        pDialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("error", "" + e.getMessage());
                    }

                    @Override
                    public void onNext(QuestionsAnswerModel questionsAnswerModel) {
                        statusCode = questionsAnswerModel.getStatusCode();
                        if (statusCode == 200) {

                            List<String> question =new ArrayList<>();
                            Integer array_length = questionsAnswerModel.getData().size();
                            for (int i=0;i<questionsAnswerModel.getData().size();i++){
                                question.add(questionsAnswerModel.getData().get(i).getQuestions());
                            }

                            selected_Questions=question.toArray(new String[question.size()]);

                            chkadapter.setChecked(selected_Questions);
//                            for (int i = 0; i < selected_Questions.length; i++) {
//
//                                for (int j = 0; j < Questions.length; j++) {
//                                    if ((selected_Questions[i].equals(Questions[j]))) {
//
//                                        break;
//                                    }
//                                }
//                            }


                            Utility.displayToast(PrimaryQuestion.this,questionsAnswerModel.getMessage());
                        }
                    }
                });



    }



}
