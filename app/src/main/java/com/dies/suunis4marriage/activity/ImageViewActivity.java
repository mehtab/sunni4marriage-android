package com.dies.suunis4marriage.activity;

import android.app.ProgressDialog;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import com.dies.suunis4marriage.R;
import com.dies.suunis4marriage.adapter.ImageAdapter;
import com.dies.suunis4marriage.apiservice.ApiService;
import com.dies.suunis4marriage.application.SessionManager;
import com.dies.suunis4marriage.application.Utility;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ImageViewActivity extends AppCompatActivity {

    SessionManager sessionManager;
    ApiService apiservice;
    ProgressDialog pDialog;
    int statusCode;
    @BindView(R.id.back_icon)
    ImageView back_icon;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_view);
        ButterKnife.bind(this);
        back_icon.setOnClickListener(view -> {
            finish();
        });


//        sessionManager = new SessionManager(this);
//        apiservice = ApiServiceCreator.createService("latest");

        String[] images = new String[Utility.getAppcon().getSession().arrayListImages.size()];
        images = Utility.getAppcon().getSession().arrayListImages.toArray(images);



        String image=Utility.getAppcon().getSession().Image;
        String[] separated = image.split(",");
        ViewPager viewPager = findViewById(R.id.viewPager);
        ImageAdapter adapter = new ImageAdapter(this,images);
        viewPager.setAdapter(adapter);
    }




}


