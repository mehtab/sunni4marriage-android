package com.dies.suunis4marriage.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.activity.ComponentActivity;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;
import com.dies.suunis4marriage.R;
import com.dies.suunis4marriage.apiservice.ApiService;
import com.dies.suunis4marriage.apiservice.ApiServiceCreator;
import com.dies.suunis4marriage.apiservice.ApiServiceCreatornew;
import com.dies.suunis4marriage.application.Root;
import com.dies.suunis4marriage.application.SessionManager;
import com.dies.suunis4marriage.application.Utility;
import com.dies.suunis4marriage.model.PaymentModel;

import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observable;
import rx.Observer;
import rx.schedulers.Schedulers;

import com.stripe.android.PaymentConfiguration;
import com.stripe.android.paymentsheet.PaymentSheet;
import com.stripe.android.paymentsheet.PaymentSheetResult;


public class UpgradeAccountActivity extends AppCompatActivity {


    final String stripePK = "pk_live_51KUFpRJgVt65aXUkSpIUfeRR7j7JWIbFEYr6BPIn8wnAVXjKVBwJ9Ag2706KBf05Bbf8Ah9AIkhp4d4UEPhwTCGG00dRCMpSsy";

    SessionManager sessionManager;
    PaymentSheet paymentSheet;
    String transactionId = "";

    @BindView(R.id.btn_pay)
    Button btn_pay;
    String amount;
    HashMap<String, String> paramHash;
    ApiService apiservice,apiservice2;
    ProgressDialog pDialog;
    int statusCode;


    @BindView(R.id.txt_title)
    TextView txt_title;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upgrade_account);
        ButterKnife.bind(this);

        String rawAmount = Utility.getAppcon().getSession().amount;

        amount = rawAmount.contains(".") ? rawAmount.split("\\.")[0] : rawAmount ;;

        txt_title.setText("Annual membership fee of £"+amount+" is due");
        sessionManager = new SessionManager(this);
        apiservice = ApiServiceCreatornew.createService("latest");
        apiservice2= ApiServiceCreator.createService("latest");

        PaymentConfiguration.init(UpgradeAccountActivity.this, stripePK);
        paymentSheet = new PaymentSheet((ComponentActivity) this, this::onPaymentSheetResult);

        btn_pay.setOnClickListener(view -> {
            callStripePaymentIntent();
        });

    }


    private void callStripePaymentIntent() {
        pDialog = new ProgressDialog(this);
        pDialog.setTitle("Checking Data");
        pDialog.setMessage("Please Wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        int int_amount = Integer.parseInt(amount) * 100 ; // String charge in smallest unit i.e cents for Dollars and pens for Pounds
        Observable<Root> responseObservable = apiservice2.paymentIntent(sessionManager.getCustomerId(), int_amount,"GBP","card");
        responseObservable.subscribeOn(Schedulers.newThread())
                .observeOn(rx.android.schedulers.AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> {
                    if (throwable instanceof retrofit2.HttpException) {
                        retrofit2.HttpException ex = (retrofit2.HttpException) throwable;
                        statusCode = ex.code();
                        Log.e("error", ex.getLocalizedMessage());
                    } else if (throwable instanceof SocketTimeoutException) {
                        statusCode = 1000;
                    }
                    return Observable.empty();
                })
                .subscribe(new Observer<Root>() {
                    @Override
                    public void onCompleted() {
                        pDialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("error", "" + e.getMessage());
                    }

                    @Override
                    public void onNext(Root root) {
                        String cs = root.getData().getClient_secret();
                        transactionId = root.getData().getTransactionID();
                        PaymentSheet.Configuration configuration = new PaymentSheet.Configuration("Sunni4Marriage");
                        paymentSheet.presentWithPaymentIntent(cs, configuration);
                        pDialog.dismiss();
                    }
                });

    }

    private void onPaymentSheetResult(
            final PaymentSheetResult paymentSheetResult
    ) {
        if (paymentSheetResult instanceof PaymentSheetResult.Completed) {
            Log.e("Payment complete!",amount);
            SavePaymentDetail();

        } else if (paymentSheetResult instanceof PaymentSheetResult.Canceled) {
            Log.i("Canceled", "Payment canceled!");
        } else if (paymentSheetResult instanceof PaymentSheetResult.Failed) {
            Throwable error = ((PaymentSheetResult.Failed) paymentSheetResult).getError();
            Log.i("Payment failed", error.getLocalizedMessage());
        }
    }


    public void SavePaymentDetail() {

        pDialog = new ProgressDialog(this);
        pDialog.setTitle("Updating payment status");
        pDialog.setMessage("Please Wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        String date = df.format(new Date());

        Observable<PaymentModel> responseObservable = apiservice2.SavePaymentDetail(
                transactionId,
                "GBP",
                amount,
                sessionManager.getKeyId(),
                sessionManager.getKeyId(),
                "133",
                "133",
                "133",
                "1",
                "1",
                date,
                "upgrade",
                "membership");

        responseObservable.subscribeOn(Schedulers.newThread())
                .observeOn(rx.android.schedulers.AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> {
                    if (throwable instanceof retrofit2.HttpException) {
                        retrofit2.HttpException ex = (retrofit2.HttpException) throwable;
                        statusCode = ex.code();
                        Log.e("error", ex.getLocalizedMessage());
                    } else if (throwable instanceof SocketTimeoutException) {
                        statusCode = 1000;
                    }
                    return Observable.empty();
                })
                .subscribe(new Observer<PaymentModel>() {
                    @Override
                    public void onCompleted() {
//                        pDialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("error", "" + e.getMessage());
                    }

                    @Override
                    public void onNext(PaymentModel obj) {

                        if (obj.getStatusCode() == 200) {
                            Intent intent=new Intent(UpgradeAccountActivity.this,DashBoardActivity.class);
                            startActivity(intent);
                            finish();
                        } else
                            Utility.displayToast(UpgradeAccountActivity.this,"Something went wrong while updating the payment. Please talk to help center");
                    }
                });

    }
}
