package com.dies.suunis4marriage.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dies.suunis4marriage.R;
import com.dies.suunis4marriage.apiservice.ApiConstants;
import com.dies.suunis4marriage.apiservice.ApiService;
import com.dies.suunis4marriage.apiservice.ApiServiceCreator;
import com.dies.suunis4marriage.application.SessionManager;
import com.dies.suunis4marriage.application.Utility;
import com.dies.suunis4marriage.model.FriendModel;
import com.dies.suunis4marriage.model.ProfileSearch;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.lang.reflect.Type;
import java.net.SocketTimeoutException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observable;
import rx.Observer;
import rx.schedulers.Schedulers;

public class Full_Profile extends AppCompatActivity {


    @BindView(R.id.profile_image)
    ImageView profile_image;
    @BindView(R.id.txt_name)
    TextView txt_name;
    @BindView(R.id.txt_location)
    TextView txt_location;
    @BindView(R.id.txt_nationality)
    TextView txt_nationality;
    @BindView(R.id.txt_ethnicity)
    TextView txt_ethnicity;
    @BindView(R.id.txt_islamic_heritage)
    TextView txt_islamic_heritage;
    @BindView(R.id.txt_height)
    TextView txt_height;
    @BindView(R.id.txt_body_type)
    TextView txt_body_type;
    @BindView(R.id.txt_language)
    TextView txt_language;
    @BindView(R.id.txt_occupation)
    TextView txt_occupation;
    @BindView(R.id.txt_income)
    TextView txt_income;
    @BindView(R.id.txt_aboutme)
    TextView txt_aboutme;
    @BindView(R.id.txt_email)
    TextView txt_email;
    @BindView(R.id.tv_contactno)
    TextView tv_contactno;
    @BindView(R.id.txt_askquestion)
    TextView txt_askquestion;
    @BindView(R.id.txt_uniq_id)
    TextView txt_uniq_id;
    @BindView(R.id.txt_age)
    TextView txt_age;


    @BindView(R.id.lnr_shortlist)
    LinearLayout lnr_shortlist;
    @BindView(R.id.card_contact)
    CardView card_contact;
    //    @BindView(R.id.btn_short)
//    Button btn_short;
    @BindView(R.id.card_data)
    CardView card_data;
    @BindView(R.id.btn_connect)
    Button btn_connect;
    @BindView(R.id.img_gallery)
    ImageView img_gallery;
    @BindView(R.id.img_short)
    ImageView img_short;
    @BindView(R.id.img_block)
    ImageView img_block;
    @BindView(R.id.txt_merital_status)
    TextView txt_merital_status;
    @BindView(R.id.txt_children)
    TextView txt_children;

    @BindView(R.id.txt_rep_email)
    TextView txt_rep_email;

    @BindView(R.id.txt_rep_name)
    TextView txt_rep_name;

    @BindView(R.id.txt_first_name)
    TextView txt_first_name;

    @BindView(R.id.txt_questions)
    TextView txt_questions;
    @BindView(R.id.txt_questions1)
    TextView txt_questions1;
    @BindView(R.id.txt_questions2)
    TextView txt_questions2;
    @BindView(R.id.lnr_child)
    LinearLayout lnr_child;


    @BindView(R.id.lnr_first_name)
    LinearLayout lnr_first_name;

    @BindView(R.id.lnr_flag)
    LinearLayout lnr_flag;


    private Dialog dialog;
    EditText etText;
    TextView txt_que, txt_que1, txt_que2;

    Button btn_submit;
    String status = "0";
    String flag = "search_friend";
    int position;


//0 mins User Login with Send Request
//2 mins Send request user login
//3 no data


    SessionManager sessionManager;
    ApiService apiservice;
    ProgressDialog pDialog;
    RecyclerView.LayoutManager layoutManager;
    int statusCode;
    List<ProfileSearch.Data> arrayList;
    List<ProfileSearch.Data> arrayListall;
    int pos;
    String fragment_name;
    SharedPreferences sharedPreferences;
    static SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full__profile);
        ButterKnife.bind(this);
        sharedPreferences = getSharedPreferences("STORE_FILE_NAME", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();

        sessionManager = new SessionManager(this);
        apiservice = ApiServiceCreator.createService("latest");
        arrayList = Utility.getAppcon().getSession().arrayListProfile;
        arrayListall = Utility.getAppcon().getSession().arrayListProfileall;
        ViewProfile();

        fragment_name = getIntent().getStringExtra("fr_name");
        position = getIntent().getIntExtra("pos", 0);

        if (arrayList.get(0).getRequest_status().equals("1")) {
            btn_connect.setText("Remove");
            btn_connect.setEnabled(true);
        }


        if (arrayList.get(0).getBlock_status().equals("1")) {
            img_block.setImageResource(R.drawable.ic_block_fill);
        } else {
            img_block.setImageResource(R.drawable.ic_block);
        }

        if (arrayList.get(0).getMultiple_image() != null) {
            img_gallery.setVisibility(View.VISIBLE);
        }


        if (arrayList.get(0).getShortlist_status().equals("1")) {
            img_short.setImageResource(R.drawable.ic_like_fill);
        } else {
            img_short.setImageResource(R.drawable.ic_like);
        }

        img_short.setOnClickListener(view -> {
            if (arrayList.get(0).getShortlist_status().equals("1")) {
                RemoveShortlist(arrayList.get(0).getUserId(), sessionManager.getKeyId());
            } else {
                if (arrayList.get(0).getBlock_status().equals("1")) {
                    Utility.displayToast(this, "Please remove from blocklist first");
                } else {
                    ShortList();
                }
            }
        });

        img_block.setOnClickListener(view -> {
            if (arrayList.get(0).getBlock_status().equals("1")) {
                RemoveBlocklist(arrayList.get(0).getUserId(), sessionManager.getKeyId());
            } else {
                if (arrayList.get(0).getRequest_status().equals("1")) {
                    Utility.displayToast(this, "Please remove from friendlist first");
                } else {
                    if (arrayList.get(0).getShortlist_status().equals("1")) {
                        Utility.displayToast(this, "Please remove from favourite first");
                    } else {
                        BlockList();
                    }
                }


            }
        });

        lnr_flag.setOnClickListener(view -> {

            AlertDialog diaBox = AskOption();
            diaBox.show();


        });


//        if (arrayList.get(0).getRequest_status().equals("0")){
//            btn_connect.setText("invitation sent");
//            btn_connect.setEnabled(false);
//          //  btn_short.setVisibility(View.GONE);
//        }

//        if (Utility.getAppcon().getSession().fragment_name.equals("shortlist_data")){
//            btn_short.setVisibility(View.GONE);
//        }

        if (arrayList.get(0).getRequest_status().equals("1")) {
            btn_connect.setVisibility(View.VISIBLE);
            card_contact.setVisibility(View.VISIBLE);
            lnr_first_name.setVisibility(View.VISIBLE);
            card_data.setVisibility(View.GONE);
        } else {
            card_data.setVisibility(View.VISIBLE);
            lnr_first_name.setVisibility(View.GONE);
            card_contact.setVisibility(View.GONE);
        }

        if (Utility.getAppcon().getSession().fragment_name.equals("search_friend")) {
            card_data.setVisibility(View.VISIBLE);
        }

        if (Utility.getAppcon().getSession().fragment_name.equals("pending_request")) {

            //btn_short.setVisibility(View.GONE);
            btn_connect.setVisibility(View.GONE);
        }

        if (arrayList.get(0).getRequest_status().equals("0")) {

            btn_connect.setVisibility(View.VISIBLE);
            btn_connect.setText("Cancel Request");
            btn_connect.setEnabled(true);
        } else if (arrayList.get(0).getRequest_status().equals("2")) {
            btn_connect.setVisibility(View.VISIBLE);
            btn_connect.setText("Pending Request");
            btn_connect.setEnabled(false);
        }
        if (arrayList.get(0).getRequest_status().equals("0") && fragment_name.equals("sent_request")) {
            btn_connect.setText("Cancel Request");
            btn_connect.setEnabled(true);
        }

        if (fragment_name.equals("receive_request")) {
            btn_connect.setVisibility(View.GONE);
        }


        if (sessionManager.getUserData().get(0).getGender().equals("MALE")) {
            Picasso.with(this).load(ApiConstants.IMAGE_URL + arrayList.get(0).getProfilepic())
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .networkPolicy(NetworkPolicy.NO_CACHE)
                    .fit()
                    .placeholder(R.drawable.ic_female_avatar)
                    .into(profile_image);
        } else {
            Picasso.with(this).load(ApiConstants.IMAGE_URL + arrayList.get(0).getProfilepic())
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .networkPolicy(NetworkPolicy.NO_CACHE)
                    .fit()
                    .placeholder(R.drawable.ic_male_avtar)
                    .into(profile_image);
        }


        ArrayList<String> list = new ArrayList<>();
        if (arrayList.get(0).getMultiple_image() != null) {
            if (!arrayList.get(0).getMultiple_image().equals("")) {
                list.add(arrayList.get(0).getMultiple_image());
            }
        }
        if (arrayList.get(0).getImage12() != null) {
            if (!arrayList.get(0).getImage12().equals("")) {
                list.add(arrayList.get(0).getImage12());
            }
        }
        if (arrayList.get(0).getImage13() != null) {
            if (!arrayList.get(0).getImage13().equals("")) {
                list.add(arrayList.get(0).getImage13());
            }
        }
        if (arrayList.get(0).getImage14() != null) {
            if (!arrayList.get(0).getImage14().equals("")) {
                list.add(arrayList.get(0).getImage14());
            }
        }


        img_gallery.setOnClickListener(view -> {
            Utility.getAppcon().getSession().arrayListImages = list;
            Intent intent = new Intent(this, ImageViewActivity.class);
            startActivity(intent);
        });


        txt_name.setText(arrayList.get(0).getName());

        if (arrayList.get(0).getTown_city() != null) {
            txt_location.setText(arrayList.get(0).getTown_city());
        } else {
            txt_location.setText("N/A");
        }
        if (arrayList.get(0).getNationality() != null) {
            txt_nationality.setText(arrayList.get(0).getNationality());
        } else {
            txt_nationality.setText("N/A");
        }
        if (arrayList.get(0).getEthnicity() != null) {
            txt_ethnicity.setText(arrayList.get(0).getEthnicity());
        } else {
            txt_ethnicity.setText("N/A");
        }
        if (arrayList.get(0).getIslamic() != null) {
            txt_islamic_heritage.setText(arrayList.get(0).getIslamic());
        } else {
            txt_islamic_heritage.setText("N/A");
        }
        if (arrayList.get(0).getHeight() != null) {
            txt_height.setText(arrayList.get(0).getHeight());
        } else {
            txt_body_type.setText("N/A");
        }
        if (arrayList.get(0).getBodytype() != null) {
            txt_body_type.setText(arrayList.get(0).getBodytype());
        } else {
            txt_body_type.setText("N/A");
        }
        if (arrayList.get(0).getLanguagesspeak() != null) {
            txt_language.setText(arrayList.get(0).getLanguagesspeak());
        } else {
            txt_language.setText("N/A");
        }
        if (arrayList.get(0).getOccupation() != null) {
            txt_occupation.setText(arrayList.get(0).getOccupation());
        } else {
            txt_occupation.setText("N/A");
        }
        if (arrayList.get(0).getAnnualincome() != null) {
            txt_income.setText(arrayList.get(0).getAnnualincome());
        } else {
            txt_income.setText("N/A");
        }
        if (arrayList.get(0).getAboutyourself() != null) {
            txt_aboutme.setText(arrayList.get(0).getAboutyourself());
        } else {
            txt_aboutme.setText("N/A");
        }
        if (arrayList.get(0).getThreequestionspotential() != null) {
            txt_askquestion.setText(arrayList.get(0).getThreequestionspotential());
        } else {
            txt_askquestion.setText("N/A");
        }
        if (arrayList.get(0).getUnique_user_id() != null) {
            txt_uniq_id.setText(arrayList.get(0).getUnique_user_id());
        } else {
            txt_uniq_id.setText("N/A");
        }
        if (arrayList.get(0).getContactnumber() != null) {
            tv_contactno.setText(arrayList.get(0).getContactnumber());
        } else {
            tv_contactno.setText("N/A");
        }
        if (arrayList.get(0).getEmail() != null) {
            txt_email.setText(arrayList.get(0).getEmail());
        } else {
            txt_email.setText("N/A");
        }
        if (arrayList.get(0).getMaritalstatus() != null) {
            txt_merital_status.setText(arrayList.get(0).getMaritalstatus());
        } else {
            txt_email.setText("N/A");
        }
        if (arrayList.get(0).getChildren() != null) {
            if (!arrayList.get(0).getChildren().equals("")) {
                txt_children.setVisibility(View.VISIBLE);
                lnr_child.setVisibility(View.VISIBLE);
                txt_children.setText(arrayList.get(0).getChildren());
            } else {
                lnr_child.setVisibility(View.GONE);
                txt_children.setVisibility(View.GONE);
            }
        } else {
            lnr_child.setVisibility(View.GONE);
            txt_children.setVisibility(View.GONE);
        }
        if (arrayList.get(0).getEmail_rep() != null) {
            txt_rep_email.setText(arrayList.get(0).getEmail_rep());
        } else {
            txt_rep_email.setText("N/A");
        }
        if (arrayList.get(0).getLeagalname() != null) {
            txt_rep_name.setText(arrayList.get(0).getLeagalname());
        } else {
            txt_rep_name.setText("N/A");
        }
        if (arrayList.get(0).getLeagalname() != null) {
            txt_first_name.setText(arrayList.get(0).getFirst_name());
        } else {
            txt_first_name.setText("");
        }
        if (arrayList.get(0).getDateofbirth() != null) {
            String year = arrayList.get(0).getDateofbirth();
            String age = String.valueOf(calculateAge(year));
            txt_age.setText(age + " Year");
        } else {
            txt_age.setText("");
        }


        String yourString = arrayList.get(0).getQuestion();
        if (!yourString.equals("")) {


            List<String> items = Arrays.asList(yourString.split("\\s*,\\s*"));
            String formattedString = yourString.replace(',', '\n');
            String data[] = new String[items.size()];
            String text = "<font color=#cc0029>Question </font>";
            for (int i = 0; i < items.size(); i++) {
                String q = getColoredSpanned("Question " + (i + 1), "#000000");
                data[i] = q + ": " + items.get(i) + "\n";
            }

//        String yourString=arrayList.get(0).getQuestion();
//        String formattedString=yourString.replace(',', '\n');
            txt_questions.setText(Html.fromHtml(data[0]));
            txt_questions1.setText(Html.fromHtml(data[1]));
            txt_questions2.setText(Html.fromHtml(data[2]));

        } else {
            txt_questions.setText("No questions available");
        }

        btn_connect.setOnClickListener(view -> {

            if (fragment_name.equals("search_friend") && arrayList.get(0).getRequest_status().equals("3") || fragment_name.equals("search_friend") && arrayList.get(0).getRequest_status().equals("")) {
//                if (arrayList.get(0).getQuestion_status().equals("1")) {
//                    btn_connect.setText("Invitation Sent");
//                    btn_connect.setEnabled(false);
//                    Intent intent1 = new Intent(this, Answer_QuestionsActivity.class);
//                    intent1.putExtra("id", arrayList.get(0).getUserId());
//                    startActivity(intent1);
//                } else {
                displayDialog();
//                }
            } else if (arrayList.get(0).getRequest_status().equals("1")) {
                status = "1";
                flag = "friend_list";
                UpdateConnectRequest(status, flag, "");
            } else if (arrayList.get(0).getRequest_status().equals("0")) {
                flag = "sent_request";
                status = "";
                UpdateConnectRequest(status, flag, "");
            }

        });


    }


    public void ShortList() {
        pDialog = new ProgressDialog(this);
        pDialog.setTitle("Checking Data");
        pDialog.setMessage("Please Wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        Observable<ProfileSearch> responseObservable = apiservice.shortlist(sessionManager.getKeyId(),
                arrayList.get(0).getUserId());

        responseObservable.subscribeOn(Schedulers.newThread())
                .observeOn(rx.android.schedulers.AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> {
                    if (throwable instanceof retrofit2.HttpException) {
                        retrofit2.HttpException ex = (retrofit2.HttpException) throwable;
                        statusCode = ex.code();
                        Log.e("error", ex.getLocalizedMessage());
                    } else if (throwable instanceof SocketTimeoutException) {
                        statusCode = 1000;
                    }
                    return Observable.empty();
                })
                .subscribe(new Observer<ProfileSearch>() {
                    @Override
                    public void onCompleted() {
                        pDialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("error", "" + e.getMessage());
                    }

                    @Override
                    public void onNext(ProfileSearch profileSearch) {
                        statusCode = profileSearch.getStatusCode();
                        if (statusCode == 200) {
                            Utility.displayToast(Full_Profile.this, profileSearch.getMessage());
                            img_short.setImageResource(R.drawable.ic_like_fill);
                            arrayList.get(0).setShortlist_status("1");
                        } else {
                            Utility.displayToast(Full_Profile.this, profileSearch.getMessage());
                        }
                    }
                });
    }


    public void UpdateConnectRequest(String status, String flag, String message) {

        pDialog = new ProgressDialog(this);
        pDialog.setTitle("Checking Data");
        pDialog.setMessage("Please Wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        Observable<FriendModel> responseObservable = apiservice.ChangeFriendStatus(sessionManager.getKeyId(),
                arrayList.get(0).getUserId(),
                status,
                flag,
                message,
                arrayList.get(0).getConnect_request_id());

        responseObservable.subscribeOn(Schedulers.newThread())
                .observeOn(rx.android.schedulers.AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> {
                    if (throwable instanceof retrofit2.HttpException) {
                        retrofit2.HttpException ex = (retrofit2.HttpException) throwable;
                        statusCode = ex.code();
                        Log.e("error", ex.getLocalizedMessage());
                    } else if (throwable instanceof SocketTimeoutException) {
                        statusCode = 1000;
                    }
                    return Observable.empty();
                })
                .subscribe(new Observer<FriendModel>() {
                    @Override
                    public void onCompleted() {
                        pDialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("error", "" + e.getMessage());
                    }

                    @Override
                    public void onNext(FriendModel friendModel) {
                        statusCode = friendModel.getStatusCode();
                        if (statusCode == 200) {
                            if (fragment_name.equals("friend_list")) {
                                btn_connect.setText("Send Request");
                            } else if (fragment_name.equals("sent_request")) {
                                btn_connect.setText("Send Request");
                            } else if (fragment_name.equals("search_friend") && arrayList.get(0).getRequest_status().equals("3")) {
                                arrayListall.get(position).setRequest_status("0");
                                //arrayListall.get(0).setRequest_status("0");
                                btn_connect.setText("Cancel Request");
                            } else {
                                btn_connect.setEnabled(false);
                            }
                            Utility.displayToast(Full_Profile.this, friendModel.getMessage());

                            //arrayList.get(pos).getR

                        } else {
                            Utility.displayToast(Full_Profile.this, friendModel.getMessage());
                        }

                    }
                });

    }

    private void displayDialog() {

        dialog = new Dialog(this);
        dialog.getWindow();
        dialog.setTitle("Address");
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.add_message_layout);
        dialog.setCancelable(true);

        // spinner = dialog.findViewById(R.id.spin_year);
        etText = dialog.findViewById(R.id.etText);
        btn_submit = dialog.findViewById(R.id.btn_submit);
        txt_que = dialog.findViewById(R.id.txt_que);
        txt_que1 = dialog.findViewById(R.id.txt_que1);
        txt_que2 = dialog.findViewById(R.id.txt_que2);

        String yourString = arrayList.get(0).getQuestion();
        if (!yourString.equals("")) {
            List<String> items = Arrays.asList(yourString.split("\\s*,\\s*"));
            String formattedString = yourString.replace(',', '\n');
            String data[] = new String[items.size()];
            String text = "<font color=#cc0029>Question </font>";
            for (int i = 0; i < items.size(); i++) {
                String q = getColoredSpanned("Question " + (i + 1), "#000000");
                data[i] = q + ": " + items.get(i) + "\n";
            }

            txt_que.setText(Html.fromHtml(data[0]));
            txt_que1.setText(Html.fromHtml(data[1]));
            txt_que2.setText(Html.fromHtml(data[2]));
            String que = Arrays.toString(data);
        }


        btn_submit.setOnClickListener(view -> {

            if (etText.getText().toString().equals("")) {
                Utility.displayToast(this, "Enter Your Message");
            } else {
                UpdateConnectRequest(status, flag, etText.getText().toString());
                dialog.dismiss();
            }

        });


        dialog.show();
        Window window = dialog.getWindow();
        assert window != null;
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);


    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    public void ViewProfile() {


//        http://diestechnology.com.au/projects/sunnis4marriage/app_controller/UserData/viewProfile


        Observable<ProfileSearch> responseObservable = apiservice.ViewProfile(sessionManager.getKeyId(),
                arrayList.get(0).getUserId());

        responseObservable.subscribeOn(Schedulers.newThread())
                .observeOn(rx.android.schedulers.AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> {
                    if (throwable instanceof retrofit2.HttpException) {
                        retrofit2.HttpException ex = (retrofit2.HttpException) throwable;
                        statusCode = ex.code();
                        Log.e("error", ex.getLocalizedMessage());
                    } else if (throwable instanceof SocketTimeoutException) {
                        statusCode = 1000;
                    }
                    return Observable.empty();
                })
                .subscribe(new Observer<ProfileSearch>() {
                    @Override
                    public void onCompleted() {
                        // pDialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("error", "" + e.getMessage());
                    }

                    @Override
                    public void onNext(ProfileSearch friendModel) {
                        statusCode = friendModel.getStatusCode();
                        if (statusCode == 200) {

                        }
                    }
                });

    }

    public void RemoveBlocklist(String profile_id, String user_id) {

        pDialog = new ProgressDialog(this);
        pDialog.setTitle("Checking Data");
        pDialog.setMessage("Please Wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        Observable<FriendModel> responseObservable = apiservice.RemoveBlocklist(profile_id, user_id);

        responseObservable.subscribeOn(Schedulers.newThread())
                .observeOn(rx.android.schedulers.AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> {
                    if (throwable instanceof retrofit2.HttpException) {
                        retrofit2.HttpException ex = (retrofit2.HttpException) throwable;
                        statusCode = ex.code();
                        Log.e("error", ex.getLocalizedMessage());
                    } else if (throwable instanceof SocketTimeoutException) {
                        statusCode = 1000;
                    }
                    return Observable.empty();
                })
                .subscribe(new Observer<FriendModel>() {
                    @Override
                    public void onCompleted() {
                        pDialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("error", "" + e.getMessage());
                    }

                    @Override
                    public void onNext(FriendModel friendModel) {
                        statusCode = friendModel.getStatusCode();
                        if (statusCode == 200) {
                            Utility.displayToast(Full_Profile.this, friendModel.getMessage());
                            img_block.setImageResource(R.drawable.ic_block);
                            arrayList.get(0).setBlock_status("0");
                            // holder.img_block.setImageResource(R.drawable.ic_block);
                        } else {
                            Utility.displayToast(Full_Profile.this, friendModel.getMessage());
                        }

                    }
                });
    }


    public void BlockList() {
        pDialog = new ProgressDialog(this);
        pDialog.setTitle("Checking Data");
        pDialog.setMessage("Please Wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        Observable<ProfileSearch> responseObservable = apiservice.blocklist(sessionManager.getKeyId(),
                arrayList.get(pos).getUserId());

        responseObservable.subscribeOn(Schedulers.newThread())
                .observeOn(rx.android.schedulers.AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> {
                    if (throwable instanceof retrofit2.HttpException) {
                        retrofit2.HttpException ex = (retrofit2.HttpException) throwable;
                        statusCode = ex.code();
                        Log.e("error", ex.getLocalizedMessage());
                    } else if (throwable instanceof SocketTimeoutException) {
                        statusCode = 1000;
                    }
                    return Observable.empty();
                })
                .subscribe(new Observer<ProfileSearch>() {
                    @Override
                    public void onCompleted() {
                        pDialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("error", "" + e.getMessage());
                    }

                    @Override
                    public void onNext(ProfileSearch profileSearch) {
                        statusCode = profileSearch.getStatusCode();
                        if (statusCode == 200) {
                            Utility.displayToast(Full_Profile.this, profileSearch.getMessage());
                            img_block.setImageResource(R.drawable.ic_block_fill);
                            arrayList.get(0).setBlock_status("1");
                        } else {
                            Utility.displayToast(Full_Profile.this, profileSearch.getMessage());
                        }

//                            btn_short.setText("ShortListed");
//                            btn_short.setEnabled(false);

                    }
                });
    }

    public void RemoveShortlist(String profile_id, String user_id) {

        pDialog = new ProgressDialog(this);
        pDialog.setTitle("Checking Data");
        pDialog.setMessage("Please Wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        Observable<FriendModel> responseObservable = apiservice.RemoveShortlist(profile_id, user_id);

        responseObservable.subscribeOn(Schedulers.newThread())
                .observeOn(rx.android.schedulers.AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> {
                    if (throwable instanceof retrofit2.HttpException) {
                        retrofit2.HttpException ex = (retrofit2.HttpException) throwable;
                        statusCode = ex.code();
                        Log.e("error", ex.getLocalizedMessage());
                    } else if (throwable instanceof SocketTimeoutException) {
                        statusCode = 1000;
                    }
                    return Observable.empty();
                })
                .subscribe(new Observer<FriendModel>() {
                    @Override
                    public void onCompleted() {
                        pDialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("error", "" + e.getMessage());
                    }

                    @Override
                    public void onNext(FriendModel friendModel) {
                        statusCode = friendModel.getStatusCode();
                        if (statusCode == 200) {
                            Utility.displayToast(Full_Profile.this, friendModel.getMessage());
                            img_short.setImageResource(R.drawable.ic_like);
                            arrayList.get(0).setShortlist_status("0");
//                            notifyDataSetChanged();

                        } else {
                            Utility.displayToast(Full_Profile.this, friendModel.getMessage());
                        }

                    }
                });


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


    @Override
    protected void onRestart() {
        super.onRestart();
        finish();
        startActivity(getIntent());
    }


    private int getAge(String dobString) {

        Date date = null;
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        try {
            date = sdf.parse(dobString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (date == null) return 0;

        Calendar dob = Calendar.getInstance();
        Calendar today = Calendar.getInstance();

        dob.setTime(date);

        int year = dob.get(Calendar.YEAR);
        int month = dob.get(Calendar.MONTH);
        int day = dob.get(Calendar.DAY_OF_MONTH);

        dob.set(year, month + 1, day);

        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);

        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)) {
            age--;
        }


        return age;
    }

    private static int calculateAge(String sbirthDate) {
        int years = 0;
        int months = 0;
        int days = 0;

        Date birthDate = null;
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        try {
            birthDate = sdf.parse(sbirthDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        //create calendar object for birth day
        Calendar birthDay = Calendar.getInstance();
        birthDay.setTimeInMillis(birthDate.getTime());

        //create calendar object for current day
        long currentTime = System.currentTimeMillis();
        Calendar now = Calendar.getInstance();
        now.setTimeInMillis(currentTime);

        //Get difference between years
        years = now.get(Calendar.YEAR) - birthDay.get(Calendar.YEAR);
        int currMonth = now.get(Calendar.MONTH) + 1;
        int birthMonth = birthDay.get(Calendar.MONTH) + 1;

        //Get difference between months
        months = currMonth - birthMonth;

        //if month difference is in negative then reduce years by one
        //and calculate the number of months.
        if (months < 0) {
            years--;
            months = 12 - birthMonth + currMonth;
            if (now.get(Calendar.DATE) < birthDay.get(Calendar.DATE))
                months--;
        } else if (months == 0 && now.get(Calendar.DATE) < birthDay.get(Calendar.DATE)) {
            years--;
            months = 11;
        }

        //Calculate the days
        if (now.get(Calendar.DATE) > birthDay.get(Calendar.DATE))
            days = now.get(Calendar.DATE) - birthDay.get(Calendar.DATE);
        else if (now.get(Calendar.DATE) < birthDay.get(Calendar.DATE)) {
            int today = now.get(Calendar.DAY_OF_MONTH);
            now.add(Calendar.MONTH, -1);
            days = now.getActualMaximum(Calendar.DAY_OF_MONTH) - birthDay.get(Calendar.DAY_OF_MONTH) + today;
        } else {
            days = 0;
            if (months == 12) {
                years++;
                months = 0;
            }
        }
        //Create new Age object
        //return new Age(days, months, years);
        return years;
    }

    private AlertDialog AskOption() {

        AlertDialog myQuittingDialogBox = new AlertDialog.Builder(this)
                .setTitle("Flag Inappropriate")
                .setMessage("Do you want to flag inappropriate?")
                .setIcon(R.drawable.icon_delete)

                .setPositiveButton("Flag", new DialogInterface.OnClickListener() {
                    ArrayList<String> flaglist = new ArrayList<>();


                    public void onClick(DialogInterface dialog, int whichButton) {
                        if (getList().size() > 0)
                            flaglist.addAll(getList());
                        flaglist.add(arrayList.get(0).getUserId());
                        setList("KEY_flag", flaglist);
                        Intent i = new Intent(Full_Profile.this, DashBoardActivity.class);
// set the new task and clear flags
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(i);
                        Utility.displayToast(Full_Profile.this, "User flag inappropriate successfully");

                    }
                })
                .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create();
        return myQuittingDialogBox;

    }

    public <T> void setList(String key, List<T> list) {
        Gson gson = new Gson();
        String json = gson.toJson(list);

        set(key, json);
    }

    public static void set(String key, String value) {
        editor.putString(key, value);
        editor.commit();
    }

    public List<String> getList() {
        List<String> arrayItems = new ArrayList<>();
        String serializedObject = sharedPreferences.getString("KEY_flag", null);
        if (serializedObject != null) {
            Gson gson = new Gson();
            Type type = new TypeToken<List<String>>() {
            }.getType();
            arrayItems = gson.fromJson(serializedObject, type);
        }
        return arrayItems;
    }
}
