package com.dies.suunis4marriage.activity;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dies.suunis4marriage.R;
import com.dies.suunis4marriage.adapter.ProfileAdapter;
import com.dies.suunis4marriage.apiservice.ApiService;
import com.dies.suunis4marriage.apiservice.ApiServiceCreator;
import com.dies.suunis4marriage.application.SessionManager;
import com.dies.suunis4marriage.model.ProfileSearch;

import java.net.SocketTimeoutException;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observable;
import rx.Observer;
import rx.schedulers.Schedulers;

public class SearchBeforeSuggestion extends AppCompatActivity {



    @BindView(R.id.rcv_sugestion)
    RecyclerView rcv_sugestion;

    @BindView(R.id.txt_advance_search)
    TextView txt_advance_search;

    @BindView(R.id.lnr_adv_search)
    LinearLayout lnr_adv_search;

    SessionManager sessionManager;
    ApiService apiservice;
    ProgressDialog pDialog;
    RecyclerView.LayoutManager layoutManager;
    int statusCode;
    ProfileAdapter exampleAdapter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_before_suggession);
        ButterKnife.bind(this);
        sessionManager = new SessionManager(this);
        apiservice = ApiServiceCreator.createService("latest");

        //txt_advance_search.setBackground(ContextCompat.getDrawable(this, R.drawable.chat_background_edittext));

//        final Animation animation = new AlphaAnimation(1f, 0.6f);
//        animation.setDuration(700); // duration - half a second
//        animation.setInterpolator(new LinearInterpolator()); // do not alter animation rate
//        animation.setRepeatCount(Animation.INFINITE); // Repeat animation infinitely
//        animation.setRepeatMode(Animation.REVERSE); // Reverse animation at the end so the button will fade back in
//        txt_advance_search.startAnimation(animation);
      //  manageBlinkEffect();

        txt_advance_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(SearchBeforeSuggestion.this,ProfileActivity.class);
                startActivity(intent);
            }
        });

        getSuggesiton();
    }


    @SuppressLint("WrongConstant")
    private void manageBlinkEffect() {
        ObjectAnimator anim = ObjectAnimator.ofInt(lnr_adv_search, "backgroundColor", getResources().getColor(R.color.green),  getResources().getColor(R.color.actionbar_color),
                Color.WHITE);
        anim.setDuration(700);
        anim.setEvaluator(new ArgbEvaluator());
        anim.setRepeatMode(Animation.REVERSE);
        anim.setRepeatCount(Animation.INFINITE);
        anim.start();
    }

    private void getSuggesiton() {

        rcv_sugestion.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        rcv_sugestion.setLayoutManager(layoutManager);
        pDialog = new ProgressDialog(this);
        pDialog.setTitle("Checking Data");
        pDialog.setMessage("Please Wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        Observable<ProfileSearch> responseObservable = apiservice.getProfileData(sessionManager.getKeyId());

        responseObservable.subscribeOn(Schedulers.newThread())
                .observeOn(rx.android.schedulers.AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> {
                    if (throwable instanceof retrofit2.HttpException) {
                        retrofit2.HttpException ex = (retrofit2.HttpException) throwable;
                        statusCode = ex.code();
                        Log.e("error", ex.getLocalizedMessage());
                    } else if (throwable instanceof SocketTimeoutException) {
                        statusCode = 1000;
                    }
                    return Observable.empty();
                })
                .subscribe(new Observer<ProfileSearch>() {
                    @Override
                    public void onCompleted() {
                        pDialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("error", "" + e.getMessage());
                    }

                    @Override
                    public void onNext(ProfileSearch profileSearch) {
                        statusCode = profileSearch.getStatusCode();
                        if (statusCode == 200) {
                            exampleAdapter=new ProfileAdapter(SearchBeforeSuggestion.this,profileSearch.getData(),"search_friend","suggesion");
                            rcv_sugestion.setAdapter(exampleAdapter);
                        }
                    }
                });

    }
}
