package com.dies.suunis4marriage.activity;

import android.app.ProgressDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.dies.suunis4marriage.R;
import com.dies.suunis4marriage.apiservice.ApiService;
import com.dies.suunis4marriage.apiservice.ApiServiceCreator;
import com.dies.suunis4marriage.application.SessionManager;
import com.dies.suunis4marriage.application.Utility;
import com.dies.suunis4marriage.model.UserDetail;

import java.net.SocketTimeoutException;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observable;
import rx.Observer;
import rx.schedulers.Schedulers;

public class ChangePasswordActivity extends AppCompatActivity {

    @BindView(R.id.et_old_password)
    EditText et_old_password;
    @BindView(R.id.et_new_password)
    EditText et_new_password;
    @BindView(R.id.et_confirm_new_password)
    EditText et_confirm_new_password;
    @BindView(R.id.btn_submit)
    Button btn_submit;
    @BindView(R.id.back_icon)
    ImageView back_icon;


    SessionManager sessionManager;
    ApiService apiservice;
    ProgressDialog pDialog;
    int statusCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        ButterKnife.bind(this);
        sessionManager = new SessionManager(this);
        apiservice = ApiServiceCreator.createService("latest");

        back_icon.setOnClickListener(view -> {
            finish();
        });

        btn_submit.setOnClickListener(view -> {
             if (et_old_password.getText().toString().equals("")){
                Utility.displayToast(this,"Please fill old Password");
            }else if (et_old_password.getText().toString().equals("")){
                Utility.displayToast(this,"Please fill New Password");
            }else  if (!et_new_password.getText().toString().equals(et_confirm_new_password.getText().toString())){
                Utility.displayToast(this,"Please check Old Password and New Password");
            }else {
                ChangePassword();
            }

        });


    }

    public void ChangePassword(){
        pDialog = new ProgressDialog(this);
        pDialog.setTitle("Checking Data");
        pDialog.setMessage("Please Wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        Observable<UserDetail> responseObservable = apiservice.ChangePassword(et_old_password.getText().toString()
                ,et_new_password.getText().toString()
                ,sessionManager.getKeyId());

        responseObservable.subscribeOn(Schedulers.newThread())
                .observeOn(rx.android.schedulers.AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> {
                    if (throwable instanceof retrofit2.HttpException) {
                        retrofit2.HttpException ex = (retrofit2.HttpException) throwable;
                        statusCode = ex.code();
                        Log.e("error", ex.getLocalizedMessage());
                    } else if (throwable instanceof SocketTimeoutException) {
                        statusCode = 1000;
                    }
                    return Observable.empty();
                })
                .subscribe(new Observer<UserDetail>() {
                    @Override
                    public void onCompleted() {
                        pDialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("error", "" + e.getMessage());
                    }

                    @Override
                    public void onNext(UserDetail userDetail) {
                        statusCode = userDetail.getStatusCode();
                        if (statusCode == 200) {
                            Utility.displayToast(ChangePasswordActivity.this,userDetail.getMessage());
                            et_old_password.getText().clear();
                            et_new_password.getText().clear();
                            et_confirm_new_password.getText().clear();
                        }else {
                            Utility.displayToast(ChangePasswordActivity.this,userDetail.getMessage());
                        }
                    }
                });

    }





}
