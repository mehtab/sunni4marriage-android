package com.dies.suunis4marriage.activity;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.cardview.widget.CardView;
import android.widget.ImageView;
import android.widget.TextView;

import com.dies.suunis4marriage.R;
import com.dies.suunis4marriage.activity.profile.AboutYouActivityStep2;
import com.dies.suunis4marriage.activity.profile.CharacterActivityStep3;
import com.dies.suunis4marriage.activity.profile.FamilyActivityStep5;
import com.dies.suunis4marriage.activity.profile.PersonalActivityStep1;
import com.dies.suunis4marriage.activity.profile.PreferencesActiityStep6;
import com.dies.suunis4marriage.activity.profile.PrimaryQuestion;
import com.dies.suunis4marriage.activity.profile.ReligionActivityStep4;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EditProfile extends AppCompatActivity {

    @BindView(R.id.toolbar_Title)
    TextView toolbar_Title;
    @BindView(R.id.back_icon)
    ImageView back_icon;
    @BindView(R.id.card_personal)
    CardView card_personal;
    @BindView(R.id.card_aboutyou)
    CardView card_aboutyou;
    @BindView(R.id.card_character)
    CardView card_character;
    @BindView(R.id.card_religion)
    CardView card_religion;
    @BindView(R.id.card_family)
    CardView card_family;
    @BindView(R.id.card_preferences)
    CardView card_preferences;
    @BindView(R.id.card_question)
    CardView card_question;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        ButterKnife.bind(this);
        toolbar_Title.setText("Edit Profile");
        back_icon.setOnClickListener(view -> {
            finish();
        });

        card_personal.setOnClickListener(view -> {
            Intent intent=new Intent(this, PersonalActivityStep1.class);
            startActivity(intent);
        });
        card_aboutyou.setOnClickListener(view -> {
            Intent intent=new Intent(this, AboutYouActivityStep2.class);
            startActivity(intent);
        });
        card_character.setOnClickListener(view -> {
            Intent intent=new Intent(this, CharacterActivityStep3.class);
            startActivity(intent);
        });
        card_religion.setOnClickListener(view -> {
            Intent intent=new Intent(this, ReligionActivityStep4.class);
            startActivity(intent);
        });
        card_family.setOnClickListener(view -> {
            Intent intent=new Intent(this, FamilyActivityStep5.class);
            startActivity(intent);
        });

        card_preferences.setOnClickListener(view -> {
            Intent intent=new Intent(this, PreferencesActiityStep6.class);
            startActivity(intent);
        });

        card_question.setOnClickListener(view -> {
            Intent intent=new Intent(this, PrimaryQuestion.class);
            startActivity(intent);
        });
    }
}
