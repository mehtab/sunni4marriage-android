package com.dies.suunis4marriage.activity.profile;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.dies.suunis4marriage.R;
import com.dies.suunis4marriage.apiservice.ApiService;
import com.dies.suunis4marriage.apiservice.ApiServiceCreator;
import com.dies.suunis4marriage.application.SessionManager;
import com.dies.suunis4marriage.application.Utility;
import com.dies.suunis4marriage.model.AboutYouDataModel;
import com.dies.suunis4marriage.model.ProfileBuild;
import com.thomashaertel.widget.MultiSpinner;

import java.net.SocketTimeoutException;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observable;
import rx.Observer;
import rx.schedulers.Schedulers;

public class AboutYouActivityStep2 extends AppCompatActivity {


    AlertDialog alertDialog1;
    String[] values = {" High School ", " GCSES ", " A-Levels ", " GNVQ/NVQ ", " Post Graduate ", " Master's Degree ", " Doctorate Degree "," Bachelor's degree ", " Others "};
    String [] occupation, Income, body_type,Height;
    String[] language;
    String Selection, Occupation, body, Income_,height;
    @BindView(R.id.et_dob)
    EditText et_dob;
    @BindView(R.id.et_tall)
    EditText et_tall;
    @BindView(R.id.rg_marital_status)
    RadioGroup rg_marital_status;
    @BindView(R.id.rg_children)
    RadioGroup rg_children;
    @BindView(R.id.spin_lang_type)
    MultiSpinner spin_lang_type;
    @BindView(R.id.et_education)
    EditText et_education;
    @BindView(R.id.et_ocuupetion)
    EditText et_ocuupetion;
    @BindView(R.id.et_income)
    EditText et_income;
    @BindView(R.id.et_body_type)
    EditText et_body_type;
    @BindView(R.id.et_describe_your_self)
    EditText et_describe_your_self;
    @BindView(R.id.et_searching_for_spose)
    EditText et_searching_for_spose;
    @BindView(R.id.et_question)
    EditText et_question;
    @BindView(R.id.btn_next)
    Button btn_next;
    @BindView(R.id.radioButton1)
    RadioButton radioButton1;
    @BindView(R.id.radioButton2)
    RadioButton radioButton2;
    @BindView(R.id.radioButton3)
    RadioButton radioButton3;
    @BindView(R.id.radioButton4)
    RadioButton radioButton4;
    @BindView(R.id.ch0)
    RadioButton ch0;
    @BindView(R.id.ch1)
    RadioButton ch1;
    @BindView(R.id.ch2)
    RadioButton ch2;
    @BindView(R.id.ch3)
    RadioButton ch3;
    @BindView(R.id.ch5)
    RadioButton ch5;

    @BindView(R.id.lnr_children)
    LinearLayout lnr_children;
    RadioButton merital_sts, child;
    SessionManager sessionManager;
    ApiService apiservice;
    ProgressDialog pDialog;
    int statusCode;
    String Lang, radio1,radio2;
    boolean[] selectedItemsFriend;
    String[] selected_lang;

    private MultiSpinner spinner;
    private ArrayAdapter<String> spin_lang_typeadapter;
    private MultiSpinner.MultiSpinnerListener onSelectedlang = new MultiSpinner.MultiSpinnerListener() {
        public void onItemsSelected(boolean[] selected) {
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < selected.length; i++) {
                if (selected[i]) {
                    builder.append(spin_lang_typeadapter.getItem(i)).append(",");
                }
            }
            Lang = builder.toString();
           // Toast.makeText(AboutYouActivityStep2.this, builder.toString(), Toast.LENGTH_SHORT).show();
            // Do something here with the selected items
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_about_you_fragment_step2);
        ButterKnife.bind(this);
        sessionManager = new SessionManager(this);
        apiservice = ApiServiceCreator.createService("latest");


        getAboutYouData();

        occupation = getResources().getStringArray(R.array.occupetion);
        Income = getResources().getStringArray(R.array.annual_income);
        language = getResources().getStringArray(R.array.language);
        body_type = getResources().getStringArray(R.array.body_type);
        Height = getResources().getStringArray(R.array.Height);

        et_body_type.setOnClickListener(view1 -> {
            Body_type();
        });
        et_education.setOnClickListener(view1 -> {
            CreateAlertDialogWithRadioButtonGroup();
        });
//        et_ocuupetion.setOnClickListener(view1 -> {
//            CreateOccupation();
//        });
        et_income.setOnClickListener(view1 -> {
            AnnualIncome();
        });
        et_tall.setOnClickListener(view1 -> {
            CreateAlertDialogWithRadioHeight();
        });
        btn_next.setOnClickListener(view -> {

            int merital_status = rg_marital_status.getCheckedRadioButtonId();
            merital_sts = (RadioButton) findViewById(merital_status);
            if (merital_sts != null) {
                radio1 = merital_sts.getText().toString();
            }


            int children = rg_children.getCheckedRadioButtonId();
            child = (RadioButton) findViewById(children);
            if (child!=null){
                radio2 = child.getText().toString();
            }

            if (et_dob.getText().toString().equals("")) {
                Utility.displayToast(this, "Please enter BirthDate");
            } else if (et_tall.getText().toString().equals("")) {
                Utility.displayToast(this, "Please enter tall are you");
            }
            if (et_body_type.getText().toString().equals("")) {
                Utility.displayToast(this, "Please Select BodyType");
            }  else if (radio1 == null) {
                Utility.displayToast(this, "Please Select Marital Status");
            } else if (et_education.getText().toString().equals("")) {
                Utility.displayToast(this, "Please Select Education");
            } else if (et_ocuupetion.getText().toString().equals("")) {
                Utility.displayToast(this, "Please Select Occupation");
            } else if (et_describe_your_self.getText().toString().equals("")) {
                Utility.displayToast(this, "Please enter Desscribe Your Self");
            } else if (et_searching_for_spose.getText().toString().equals("")) {
                Utility.displayToast(this, "Please enter Searching For Spouse");
            } else {
                AboutYouData();
//                Intent intent = new Intent(this, CharacterActivityStep3.class);
//                startActivity(intent);
//                Intent intent=new Intent(AboutYouActivityStep2.this, DashBoardActivity.class);
//                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                startActivity(intent);
                finish();
            }


        });


        rg_marital_status.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (i){
                    case R.id.radioButton1:
                        lnr_children.setVisibility(View.GONE);
                        break;
                    case R.id.radioButton2:
                        lnr_children.setVisibility(View.VISIBLE);
                        break;
                    case R.id.radioButton3:
                        lnr_children.setVisibility(View.VISIBLE);
                        break;
                    case R.id.radioButton4:
                        lnr_children.setVisibility(View.GONE);
                        break;
                }
            }
        });


        spin_lang_typeadapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, language);
        spin_lang_type.setAdapter(spin_lang_typeadapter, false, onSelectedlang);
        spin_lang_type.setHint("Select Language's");

    }


    public void CreateAlertDialogWithRadioHeight() {

        int pos=-1;
        if (!et_tall.getText().toString().equals("")){
            String data=et_tall.getText().toString();

            for(int i=0; i < Height.length; i++)
            {
                if(Height[i].contains(data)){
                    pos = i;
                }

            }

        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Select Your Height");
        builder.setSingleChoiceItems(Height, pos, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                height = (String) Height[item];
            }
        });

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Toast.makeText(AboutYouActivityStep2.this, height, Toast.LENGTH_SHORT).show();
                et_tall.setText(height);
                //alertDialog1.dismiss();
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                alertDialog1.dismiss();
            }
        });
        alertDialog1 = builder.create();
        alertDialog1.show();

    }



    public void CreateAlertDialogWithRadioButtonGroup() {

        int pos=-1;
        if (!et_education.getText().toString().equals("")){
            String data=et_education.getText().toString();

            for(int i=0; i < values.length; i++)
            {
                if(values[i].contains(data)){
                    pos = i;
                }
            }
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Select Your Choice");
        builder.setSingleChoiceItems(values, pos, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                Selection = (String) values[item];
            }
        });

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Toast.makeText(AboutYouActivityStep2.this, Selection, Toast.LENGTH_SHORT).show();
                et_education.setText(Selection);
                //alertDialog1.dismiss();
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                alertDialog1.dismiss();
            }
        });
        alertDialog1 = builder.create();
        alertDialog1.show();

    }

    public void CreateOccupation() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Select Your Occupation");
        builder.setSingleChoiceItems(occupation, -1, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                Occupation = (String) occupation[item];
            }
        });

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Toast.makeText(AboutYouActivityStep2.this, Selection, Toast.LENGTH_SHORT).show();
                et_ocuupetion.setText(Occupation);
                //alertDialog1.dismiss();
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                alertDialog1.dismiss();
            }
        });
        alertDialog1 = builder.create();
        alertDialog1.show();

    }

    public void AnnualIncome() {

        int pos=-1;
        if (!et_income.getText().toString().equals("")){
            String data=et_income.getText().toString();

            for(int i=0; i < Income.length; i++)
            {
                if(Income[i].contains(data)){
                    pos = i;
                }

            }

        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Select Your Annual Income");
        builder.setSingleChoiceItems(Income, pos, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                Income_ = (String) Income[item];
            }
        });

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //Toast.makeText(AboutYouActivityStep2.this, Selection, Toast.LENGTH_SHORT).show();
                et_income.setText(Income_);
                //alertDialog1.dismiss();
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                alertDialog1.dismiss();
            }
        });
        alertDialog1 = builder.create();
        alertDialog1.show();

    }


    public void Body_type() {
        int pos=-1;
        if (!et_body_type.getText().toString().equals("")){
            String data=et_body_type.getText().toString();

            for(int i=0; i < body_type.length; i++)
            {
                if(body_type[i].contains(data)){
                    pos = i;
                }
            }
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Select Your Body Type");
        builder.setSingleChoiceItems(body_type, pos, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                body = (String) body_type[item];
            }
        });

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Toast.makeText(AboutYouActivityStep2.this, Selection, Toast.LENGTH_SHORT).show();
                et_body_type.setText(body);
                //alertDialog1.dismiss();
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                alertDialog1.dismiss();
            }
        });
        alertDialog1 = builder.create();
        alertDialog1.show();

    }

    public void AboutYouData() {

        pDialog = new ProgressDialog(this);
        pDialog.setTitle("Checking Data");
        pDialog.setMessage("Please Wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        Observable<ProfileBuild> responseObservable = apiservice.aboutyou(
                et_dob.getText().toString(),
                sessionManager.getKeyId(),
                et_tall.getText().toString(),
                et_body_type.getText().toString(),
                Lang,
                radio1,
                radio2,
                et_education.getText().toString(),
                et_ocuupetion.getText().toString(),
                et_income.getText().toString(),
                et_describe_your_self.getText().toString(),
                et_searching_for_spose.getText().toString(),
                et_question.getText().toString(),
                sessionManager.getKeyId());

        responseObservable.subscribeOn(Schedulers.newThread())
                .observeOn(rx.android.schedulers.AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> {
                    if (throwable instanceof retrofit2.HttpException) {
                        retrofit2.HttpException ex = (retrofit2.HttpException) throwable;
                        statusCode = ex.code();
                        Log.e("error", ex.getLocalizedMessage());
                    } else if (throwable instanceof SocketTimeoutException) {
                        statusCode = 1000;
                    }
                    return Observable.empty();
                })
                .subscribe(new Observer<ProfileBuild>() {
                    @Override
                    public void onCompleted() {
                        pDialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("error", "" + e.getMessage());
                    }

                    @Override
                    public void onNext(ProfileBuild profileBuild) {
                        statusCode = profileBuild.getStatusCode();
                        if (statusCode == 200) {
                            Utility.displayToast(AboutYouActivityStep2.this, profileBuild.getMessage());
                        } else {
                            Utility.displayToast(AboutYouActivityStep2.this, profileBuild.getMessage());
                        }
                    }
                });

    }


    public void getAboutYouData() {

        pDialog = new ProgressDialog(this);
        pDialog.setTitle("Checking Data");
        pDialog.setMessage("Please Wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        Observable<AboutYouDataModel> responseObservable = apiservice.getAboutYouData(sessionManager.getKeyId());

        responseObservable.subscribeOn(Schedulers.newThread())
                .observeOn(rx.android.schedulers.AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> {
                    if (throwable instanceof retrofit2.HttpException) {
                        retrofit2.HttpException ex = (retrofit2.HttpException) throwable;
                        statusCode = ex.code();
                        Log.e("error", ex.getLocalizedMessage());
                    } else if (throwable instanceof SocketTimeoutException) {
                        statusCode = 1000;
                    }
                    return Observable.empty();
                })
                .subscribe(new Observer<AboutYouDataModel>() {
                    @Override
                    public void onCompleted() {
                        pDialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("error", "" + e.getMessage());
                    }

                    @Override
                    public void onNext(AboutYouDataModel aboutYouDataModel) {
                        statusCode = aboutYouDataModel.getStatusCode();
                        if (statusCode == 200) {

                            if (aboutYouDataModel.getData().get(0).getMaritalstatus() != null) {

                                if (aboutYouDataModel.getData().get(0).getMaritalstatus().equals("Never Married")) {
                                    radioButton1.setChecked(true);
                                } else if (aboutYouDataModel.getData().get(0).getMaritalstatus().equals("Divorced")) {
                                    radioButton2.setChecked(true);
                                } else if (aboutYouDataModel.getData().get(0).getMaritalstatus().equals("Widowed")) {
                                    radioButton3.setChecked(true);
                                } else if (aboutYouDataModel.getData().get(0).getMaritalstatus().equals("Annuled")) {
                                    radioButton4.setChecked(true);
                                }
                            }

                            if (!aboutYouDataModel.getData().get(0).getChildren().equals("")) {

                                if (aboutYouDataModel.getData().get(0).getChildren().equals("0")) {
                                    ch0.setChecked(true);
                                } else if (aboutYouDataModel.getData().get(0).getChildren().equals("1")) {
                                    ch1.setChecked(true);
                                } else if (aboutYouDataModel.getData().get(0).getChildren().equals("2")) {
                                    ch2.setChecked(true);
                                } else if (aboutYouDataModel.getData().get(0).getChildren().equals("3")) {
                                    ch3.setChecked(true);
                                }else if (aboutYouDataModel.getData().get(0).getChildren().equals("More then 3")) {
                                    ch5.setChecked(true);
                                }
                            }


//                            if (aboutYouDataModel.getData().get(0).getBodytype().equals("Never Merried")){
//                                radioButton1.setChecked(true);
//                            }else if (aboutYouDataModel.getData().get(0).getBodytype().equals("Divorced")){
//                                radioButton2.setChecked(true);
//                            }else if (aboutYouDataModel.getData().get(0).getBodytype().equals("Widowed")){
//                                radioButton3.setChecked(true);
//                            }else if (aboutYouDataModel.getData().get(0).getBodytype().equals("Annuled")){
//                                radioButton4.setChecked(true);
//                            }


                            if(aboutYouDataModel.getData().get(0).getLanguagesspeak()!=null) {
                                String str = aboutYouDataModel.getData().get(0).getLanguagesspeak();
                                List<String> items = Arrays.asList(str.split("\\s*,\\s*"));
                                selected_lang = items.toArray(new String[items.size()]);


                                Integer array_length = language.length;
                                selectedItemsFriend = new boolean[language.length];

                                for (int i = 0; i < selected_lang.length; i++) {

                                    for (int j = 0; j < language.length; j++) {
                                        if ((selected_lang[i].equals(language[j]))) {
                                            selectedItemsFriend[j] = true;
                                            break;
                                        }
                                    }
                                }


                                if (selected_lang.length > 0) {
                                    spin_lang_type.setSelected(selectedItemsFriend);
                                }

                                spin_lang_type.setText(aboutYouDataModel.getData().get(0).getLanguagesspeak());
                                Lang = aboutYouDataModel.getData().get(0).getLanguagesspeak();
                            }

                                et_dob.setText(aboutYouDataModel.getData().get(0).getDob());
                                et_tall.setText(aboutYouDataModel.getData().get(0).getTall());
                                et_body_type.setText(aboutYouDataModel.getData().get(0).getBodytype());
                                et_education.setText(aboutYouDataModel.getData().get(0).getEducation());
                                et_ocuupetion.setText(aboutYouDataModel.getData().get(0).getOccupation());
                                et_income.setText(aboutYouDataModel.getData().get(0).getAnnualincome());
                                et_describe_your_self.setText(aboutYouDataModel.getData().get(0).getYourself());
                                et_searching_for_spose.setText(aboutYouDataModel.getData().get(0).getSearchingspouse());
                                et_question.setText(aboutYouDataModel.getData().get(0).getThreequestionspotential());


                            }
                        }
                    });

                }
    }
