package com.dies.suunis4marriage.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.chaos.view.PinView;
import com.dies.suunis4marriage.R;
import com.dies.suunis4marriage.apiservice.ApiService;
import com.dies.suunis4marriage.apiservice.ApiServiceCreator;
import com.dies.suunis4marriage.application.SessionManager;
import com.dies.suunis4marriage.application.Utility;
import com.dies.suunis4marriage.model.UserDataResponse;
import com.dies.suunis4marriage.model.UserDetail;
import com.dies.suunis4marriage.model.UserdetailResponse;
import com.google.gson.Gson;

import java.net.SocketTimeoutException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.et_email)
    EditText et_email;
    @BindView(R.id.et_pass)
    EditText et_pass;
    @BindView(R.id.btn_signin)
    Button btn_signin;
    @BindView(R.id.btn_signup)
    Button btn_signup;
    @BindView(R.id.txt_forgot_pass)
    TextView txt_forgot_pass;

    SessionManager sessionManager;
    ApiService apiservice;
    ProgressDialog pDialog;
    RecyclerView.LayoutManager layoutManager;
    int statusCode;
    EditText edt_email_d, edt_new_pwd_d, edt_confirm_pwd_d;
    ImageView btn_cancel_forgot, btn_cancel_change_d;
    Button btn_submit_forgot_d, btn_new_pwd_d;
    String forgot_email = "";
    private Dialog Mdialog, Odialog, Ndialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        sessionManager = new SessionManager(LoginActivity.this);
        apiservice = ApiServiceCreator.createService("latest");


        btn_signin.setOnClickListener(view -> {
            if (et_email.getText().toString().equals("")) {
                Utility.displayToast(this, "Please enter Email");
            } else if (et_pass.getText().toString().equals("")) {
                Utility.displayToast(this, "Please enter Password");
            } else {
                LoginUser();
            }
        });
        btn_signup.setOnClickListener(view -> {
            Intent intent = new Intent(this, SignUpActivity.class);
            startActivity(intent);
        });

      /*  if (sessionManager.isLoggedIn()) {
            Updatelatlong();
            getUserDetail();
//            Intent intent = new Intent(this, DashBoardActivity.class);
//            startActivity(intent);
        }*/


    }

    public void LoginUser() {
        pDialog = new ProgressDialog(this);
        pDialog.setTitle("Checking Data");
        pDialog.setMessage("Please Wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        Observable<UserDetail> responseObservable = apiservice.LoginUserDetails(
                et_email.getText().toString(),
                et_pass.getText().toString(),
                sessionManager.getFcmId());

        responseObservable.subscribeOn(Schedulers.newThread())
                .observeOn(rx.android.schedulers.AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> {
                    if (throwable instanceof retrofit2.HttpException) {
                        retrofit2.HttpException ex = (retrofit2.HttpException) throwable;
                        statusCode = ex.code();
                        Log.e("error", ex.getLocalizedMessage());
                    } else if (throwable instanceof SocketTimeoutException) {
                        statusCode = 1000;
                    }
                    return Observable.empty();
                })
                .subscribe(new Observer<UserDetail>() {
                    @Override
                    public void onCompleted() {
                        pDialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("error", "" + e.getMessage());
                    }

                    @Override
                    public void onNext(UserDetail userDetail) {
                        statusCode = userDetail.getStatusCode();
                        if (statusCode == 200) {
                            Utility.displayToast(LoginActivity.this, userDetail.getMessage());
                            sessionManager.setKeyId(userDetail.getData().get(0).getUserId());
                            sessionManager.setKeyEmail(et_email.getText().toString());
                            sessionManager.setLoginType("normal");
                            Updatelatlong();
                            getUserDetail();

                        } else if (statusCode == 200) {
                            Utility.displayToast(LoginActivity.this, userDetail.getMessage());
                        } else {
                            Utility.displayToast(LoginActivity.this, userDetail.getMessage());
                        }
                    }
                });

    }


    private void getUserDetail() {

        pDialog = new ProgressDialog(LoginActivity.this);
        pDialog.setTitle("Checking Data");
        pDialog.setMessage("Please Wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        Observable<UserDataResponse> responseObservable = apiservice.getUserDetails(sessionManager.getKeyEmail());
        responseObservable.subscribeOn(Schedulers.newThread())
                .observeOn(rx.android.schedulers.AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> {
                    if (throwable instanceof retrofit2.HttpException) {
                        retrofit2.HttpException ex = (retrofit2.HttpException) throwable;
                        statusCode = ex.code();
                        Log.e("error", ex.getLocalizedMessage());
                    } else if (throwable instanceof SocketTimeoutException) {
                        statusCode = 1000;
                    }
                    return Observable.empty();
                })
                .subscribe(new Observer<UserDataResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("error", e.getLocalizedMessage());
                    }

                    @Override
                    public void onNext(UserDataResponse userDataResponse) {
                        statusCode = userDataResponse.getStatusCode();
                        if (statusCode == 200) {
                            sessionManager.setKeyUserDetails(new Gson().toJson(userDataResponse.getData()));
                            sessionManager.createLoginSession();
                            sessionManager.setKeyImage(userDataResponse.getData().get(0).getProfilepic());
                            sessionManager.setKeyId(userDataResponse.getData().get(0).getUserId());
                            sessionManager.setCustomerId(userDataResponse.getData().get(0).getCustomerID());
                            if (userDataResponse.getData().get(0).getPayment_status().equals("1")) {
                                sessionManager.createPaymentSession();
                            } else {
                                sessionManager.destroyPaymentSession();
                            }
                            if (userDataResponse.getData().get(0).getPlan_status() != null) {
                                if (userDataResponse.getData().get(0).getPlan_status().equals("1")) {
                                    sessionManager.createPlanSession();
                                }
                            }
                            pDialog.dismiss();
                            Utility.getAppcon().getSession().Image = userDataResponse.getData().get(0).getMultiple_image();
                            //  Updatelatlong();
                            startActivity(new Intent(LoginActivity.this, DashBoardActivity.class));
                            finish();
                        } else {
                            pDialog.dismiss();
                        }
                    }
                });
    }


    public void Updatelatlong() {

        pDialog = new ProgressDialog(this);
        pDialog.setTitle("Checking Data");
        pDialog.setMessage("Please Wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        Observable<UserDetail> responseObservable = apiservice.UpdateLatLong(
                sessionManager.getKeyId(),
                Utility.getAppcon().getSession().lattitude,
                Utility.getAppcon().getSession().longitude);

        responseObservable.subscribeOn(Schedulers.newThread())
                .observeOn(rx.android.schedulers.AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> {
                    if (throwable instanceof retrofit2.HttpException) {
                        retrofit2.HttpException ex = (retrofit2.HttpException) throwable;
                        statusCode = ex.code();
                        Log.e("error", ex.getLocalizedMessage());
                    } else if (throwable instanceof SocketTimeoutException) {
                        statusCode = 1000;
                    }
                    return Observable.empty();
                })
                .subscribe(new Observer<UserDetail>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("error", "" + e.getMessage());
                    }

                    @Override
                    public void onNext(UserDetail userDetail) {
                        statusCode = userDetail.getStatusCode();
                        if (statusCode == 200) {
                            pDialog.dismiss();
                        } else {
                            pDialog.dismiss();
                        }

                    }
                });

    }


    @OnClick(R.id.txt_forgot_pass)
    public void txt_forgot_pwd() {
        Mdialog = new Dialog(LoginActivity.this);
        Mdialog.getWindow();
        Mdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Mdialog.setContentView(R.layout.custom_layout_forgot_pwd);
        edt_email_d = Mdialog.findViewById(R.id.edt_email_d);
        btn_submit_forgot_d = Mdialog.findViewById(R.id.btn_submit_forgot_d);
        btn_cancel_forgot = Mdialog.findViewById(R.id.btn_cancel_forgot);

        Mdialog.show();
        edt_email_d.setText("");

        Odialog = new Dialog(LoginActivity.this);
        Odialog.getWindow();
        Odialog.setCancelable(false);
        Odialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Odialog.setContentView(R.layout.dialog_otp);

        final PinView pinView = Odialog.findViewById(R.id.pinView);
        Button btn_submit_otp = Odialog.findViewById(R.id.btn_submit_otp);
        ImageView img_cancel_dialog = Odialog.findViewById(R.id.img_cancel_dialog);

        img_cancel_dialog.setOnClickListener(view -> Odialog.dismiss());
        btn_submit_otp.setOnClickListener(view -> verifyOTP(pinView.getText().toString()));

        btn_cancel_forgot.setOnClickListener(view -> Mdialog.dismiss());

        btn_submit_forgot_d.setOnClickListener(view -> {
            forgot_email = edt_email_d.getText().toString();
            getOTP();
        });
    }

    private void getOTP() {
        pDialog = new ProgressDialog(LoginActivity.this);
        pDialog.setTitle("Checking Data");
        pDialog.setMessage("Please Wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        Observable<UserdetailResponse> responseObservable = apiservice.getOTP(forgot_email);

        responseObservable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> {
                    if (throwable instanceof retrofit2.HttpException) {
                        retrofit2.HttpException ex = (retrofit2.HttpException) throwable;
                        statusCode = ex.code();
                        Log.e("error", ex.getLocalizedMessage());
                    } else if (throwable instanceof SocketTimeoutException) {
                        statusCode = 1000;
                    }
                    return Observable.empty();
                })
                .subscribe(new Observer<UserdetailResponse>() {
                    @Override
                    public void onCompleted() {
                        pDialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("error", "" + e.getMessage());
                    }

                    @Override
                    public void onNext(UserdetailResponse loginResponse) {
                        statusCode = loginResponse.getStatusCode();
                        if (statusCode == 201) {
                            pDialog.dismiss();
                            Mdialog.dismiss();
                            Odialog.show();
                        } else if (statusCode == 500) {
                            pDialog.dismiss();
                            Utility.displayToast(getApplicationContext(), loginResponse.getMessage());
                        }
                    }
                });
    }

    private void verifyOTP(String otp) {

        Ndialog = new Dialog(LoginActivity.this);
        Ndialog.getWindow();
        Ndialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Ndialog.setContentView(R.layout.custom_layout_new_password);
        edt_new_pwd_d = Ndialog.findViewById(R.id.edt_new_pwd_d);
        edt_confirm_pwd_d = Ndialog.findViewById(R.id.edt_confirm_pwd_d);
        btn_new_pwd_d = Ndialog.findViewById(R.id.btn_new_pwd_d);
        btn_cancel_change_d = Ndialog.findViewById(R.id.btn_cancel_change_d);
        edt_new_pwd_d.setText("");
        edt_confirm_pwd_d.setText("");

        btn_cancel_change_d.setOnClickListener(view -> Ndialog.dismiss());
        btn_new_pwd_d.setOnClickListener(view -> updateInfo());

        pDialog = new ProgressDialog(LoginActivity.this);
        pDialog.setTitle("Checking Data");
        pDialog.setMessage("Please Wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        Observable<UserdetailResponse> responseObservable = apiservice.check_otp_mail(forgot_email, otp);

        responseObservable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> {
                    if (throwable instanceof retrofit2.HttpException) {
                        retrofit2.HttpException ex = (retrofit2.HttpException) throwable;
                        statusCode = ex.code();
                        Log.e("error", ex.getLocalizedMessage());
                    } else if (throwable instanceof SocketTimeoutException) {
                        statusCode = 1000;
                    }
                    return Observable.empty();
                })
                .subscribe(new Observer<UserdetailResponse>() {
                    @Override
                    public void onCompleted() {
                        pDialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("error", "" + e.getMessage());
                    }

                    @Override
                    public void onNext(UserdetailResponse loginResponse) {
                        statusCode = loginResponse.getStatusCode();
                        if (statusCode == 201) {
                            Odialog.dismiss();
                            pDialog.dismiss();
                            Ndialog.show();
                        } else if (statusCode == 500) {
                            pDialog.dismiss();
                            Utility.displayToast(getApplicationContext(), loginResponse.getMessage());
                        }
                    }
                });
    }


    private void updateInfo() {

        pDialog = new ProgressDialog(LoginActivity.this);
        pDialog.setTitle("Checking Data");
        pDialog.setMessage("Please Wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        if (edt_new_pwd_d.getText().toString().equals("") && edt_confirm_pwd_d.getText().toString().equals("")) {
            pDialog.dismiss();
            Utility.displayToast(getApplicationContext(), "Please fill all the fields");
        } else if (!edt_new_pwd_d.getText().toString().equals(edt_confirm_pwd_d.getText().toString())) {
            pDialog.dismiss();
            Utility.displayToast(getApplicationContext(), "Both password must be same");
        } else {

            Observable<UserdetailResponse> responseObservable = apiservice.UpdatePwd(forgot_email, edt_new_pwd_d.getText().toString());

            responseObservable.subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .onErrorResumeNext(throwable -> {
                        if (throwable instanceof retrofit2.HttpException) {
                            retrofit2.HttpException ex = (retrofit2.HttpException) throwable;
                            statusCode = ex.code();
                            Log.e("error", ex.getLocalizedMessage());
                        } else if (throwable instanceof SocketTimeoutException) {
                            statusCode = 1000;
                        }
                        return Observable.empty();
                    })
                    .subscribe(new Observer<UserdetailResponse>() {
                        @Override
                        public void onCompleted() {
                            pDialog.dismiss();
                        }

                        @Override
                        public void onError(Throwable e) {
                            Log.e("error", "" + e.getMessage());
                        }

                        @Override
                        public void onNext(UserdetailResponse loginResponse) {
                            statusCode = loginResponse.getStatusCode();
                            if (statusCode == 200) {
                                pDialog.dismiss();
                                Ndialog.dismiss();
                                Utility.displayToast(getApplicationContext(), "Your password is successfully updated");
                            } else if (statusCode == 500) {
                                pDialog.dismiss();
                                Utility.displayToast(getApplicationContext(), "Please Try After SomeTime");
                            }
                        }
                    });
        }
    }

}
