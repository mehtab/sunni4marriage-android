package com.dies.suunis4marriage.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.dies.suunis4marriage.R;
import com.dies.suunis4marriage.adapter.ProfileAdapter;
import com.dies.suunis4marriage.apiservice.ApiService;
import com.dies.suunis4marriage.apiservice.ApiServiceCreator;
import com.dies.suunis4marriage.application.SessionManager;
import com.dies.suunis4marriage.application.Utility;
import com.dies.suunis4marriage.model.ProfileSearch;

import java.net.SocketTimeoutException;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observable;
import rx.Observer;
import rx.schedulers.Schedulers;


public class CurrentLocation extends AppCompatActivity {

    @BindView(R.id.rcv_profile)
    RecyclerView rcv_profile;

    SessionManager sessionManager;
    ApiService apiservice;
    ProgressDialog pDialog;
    RecyclerView.LayoutManager layoutManager;
    int statusCode;
    ProfileAdapter profileAdapter;
    String lat, longi,minage="",maxage="",Miles;

    @BindView(R.id.txt_profile_text)
    TextView txt_profile_text;

    @BindView(R.id.back_icon)
    ImageView back_icon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_current_location);
        ButterKnife.bind(this);
        sessionManager = new SessionManager(this);
        apiservice = ApiServiceCreator.createService("latest");
        lat = String.valueOf(Utility.getAppcon().getSession().lattitude);
        longi = String.valueOf(Utility.getAppcon().getSession().longitude);
        if (!Utility.getAppcon().getSession().minage.equals("")){
            minage=Utility.getAppcon().getSession().minage;
        }
        if (!Utility.getAppcon().getSession().maxage.equals("")){
            maxage=Utility.getAppcon().getSession().maxage;
        }
        Miles=Utility.getAppcon().getSession().miles;

        back_icon.setOnClickListener(view -> {
            finish();
        });

        getSearchResult();
    }

    public void getSearchResult() {
        rcv_profile.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        rcv_profile.setLayoutManager(layoutManager);
        pDialog = new ProgressDialog(this);
        pDialog.setTitle("Checking Data");
        pDialog.setMessage("Please Wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        Observable<ProfileSearch> responseObservable = apiservice.getNearme(
                sessionManager.getKeyId(), lat, longi, minage,maxage, Miles);

        responseObservable.subscribeOn(Schedulers.newThread())
                .observeOn(rx.android.schedulers.AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> {
                    if (throwable instanceof retrofit2.HttpException) {
                        retrofit2.HttpException ex = (retrofit2.HttpException) throwable;
                        statusCode = ex.code();
                        Log.e("error", ex.getLocalizedMessage());
                    } else if (throwable instanceof SocketTimeoutException) {
                        statusCode = 1000;
                    }
                    return Observable.empty();
                })
                .subscribe(new Observer<ProfileSearch>() {
                    @Override
                    public void onCompleted() {
                        pDialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("error", "" + e.getMessage());
                    }

                    @Override
                    public void onNext(ProfileSearch profileSearch) {
                        statusCode = profileSearch.getStatusCode();
                        if (statusCode == 200) {
                            if (profileSearch.getData().size()>0){
                                txt_profile_text.setVisibility(View.GONE);
                                profileAdapter = new ProfileAdapter(CurrentLocation.this, profileSearch.getData(), "search_friend","");
                                rcv_profile.setAdapter(profileAdapter);
                            }else {
                                txt_profile_text.setVisibility(View.VISIBLE);
                            }

                        }
                    }
                });

    }
}
