package com.dies.suunis4marriage.activity.profile;

import android.app.ProgressDialog;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.dies.suunis4marriage.R;
import com.dies.suunis4marriage.apiservice.ApiService;
import com.dies.suunis4marriage.apiservice.ApiServiceCreator;
import com.dies.suunis4marriage.application.SessionManager;
import com.dies.suunis4marriage.application.Utility;
import com.dies.suunis4marriage.model.ProfileBuild;
import com.dies.suunis4marriage.model.ReligionDataModel;

import java.net.SocketTimeoutException;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observable;
import rx.Observer;
import rx.schedulers.Schedulers;

public class ReligionActivityStep4 extends AppCompatActivity {


    @BindView(R.id.btn_next)
    Button btn_next;

    @BindView(R.id.rg1)
    RadioGroup rg1;

    @BindView(R.id.rg2)
    RadioGroup rg2;

    @BindView(R.id.rg3)
    RadioGroup rg3;

    @BindView(R.id.rg4)
    RadioGroup rg4;

    @BindView(R.id.rg5)
    RadioGroup rg5;

    @BindView(R.id.rg6)
    RadioGroup rg6;

    @BindView(R.id.rg7)
    RadioGroup rg7;

    @BindView(R.id.rg8)
    RadioGroup rg8;

//    @BindView(R.id.rg9)
//    RadioGroup rg9;

//    @BindView(R.id.rg10)
//    RadioGroup rg10;

    @BindView(R.id.rg11)
    RadioGroup rg11;

    @BindView(R.id.rg12)
    RadioGroup rg12;

    @BindView(R.id.rg13)
    RadioGroup rg13;

    @BindView(R.id.rg14)
    RadioGroup rg14;

    @BindView(R.id.rg15)
    RadioGroup rg15;

    @BindView(R.id.chk_impotnt1)
    CheckBox chk_impotnt1;

    @BindView(R.id.chk_impotnt2)
    CheckBox chk_impotnt2;

    @BindView(R.id.chk_impotnt3)
    CheckBox chk_impotnt3;

    @BindView(R.id.chk_impotnt4)
    CheckBox chk_impotnt4;

    @BindView(R.id.chk_impotnt5)
    CheckBox chk_impotnt5;

    @BindView(R.id.chk_impotnt6)
    CheckBox chk_impotnt6;

    @BindView(R.id.rd1)
    RadioButton rd1;

    @BindView(R.id.rd2)
    RadioButton rd2;

    @BindView(R.id.rd3)
    RadioButton rd3;

    @BindView(R.id.rd4)
    RadioButton rd4;

    @BindView(R.id.rd5)
    RadioButton rd5;

    @BindView(R.id.rd6)
    RadioButton rd6;

    @BindView(R.id.rd7)
    RadioButton rd7;

    @BindView(R.id.rd8)
    RadioButton rd8;

    @BindView(R.id.rd9)
    RadioButton rd9;

    @BindView(R.id.rd10)
    RadioButton rd10;

    @BindView(R.id.rd11)
    RadioButton rd11;

    @BindView(R.id.rd12)
    RadioButton rd12;

    @BindView(R.id.rd13)
    RadioButton rd13;

    @BindView(R.id.rd14)
    RadioButton rd14;

    @BindView(R.id.rd15)
    RadioButton rd15;

    @BindView(R.id.rd16)
    RadioButton rd16;

//    @BindView(R.id.rd17)
//    RadioButton rd17;
//
//    @BindView(R.id.rd18)
//    RadioButton rd18;

//    @BindView(R.id.rd19)
//    RadioButton rd19;

//    @BindView(R.id.rd20)
//    RadioButton rd20;

    @BindView(R.id.rd21)
    RadioButton rd21;

    @BindView(R.id.rd22)
    RadioButton rd22;

    @BindView(R.id.rd23)
    RadioButton rd23;

    @BindView(R.id.rd24)
    RadioButton rd24;

    @BindView(R.id.rd25)
    RadioButton rd25;

    @BindView(R.id.rd26)
    RadioButton rd26;

    @BindView(R.id.rd27)
    RadioButton rd27;

//    @BindView(R.id.rd28)
//    RadioButton rd28;

    @BindView(R.id.rd29)
    RadioButton rd29;

    @BindView(R.id.rd30)
    RadioButton rd30;

    @BindView(R.id.rd31)
    RadioButton rd31;

    @BindView(R.id.rd32)
    RadioButton rd32;

    @BindView(R.id.rd33)
    RadioButton rd33;

    @BindView(R.id.rd34)
    RadioButton rd34;

    @BindView(R.id.rd35)
    RadioButton rd35;

    @BindView(R.id.rd36)
    RadioButton rd36;

    @BindView(R.id.rd99)
    RadioButton rd99;

    @BindView(R.id.rd98)
    RadioButton rd98;





    RadioButton rb1,rb2,rb3,rb4,rb5,rb6,rb7,rb8,rb11,rb12,rb13,rb14,rb15;
    String CheckImportnt1="0",CheckImportnt2="0",CheckImportnt3="0",CheckImportnt4="0",CheckImportnt5="0",CheckImportnt6="0";
    SessionManager sessionManager;
    ApiService apiservice;
    ProgressDialog pDialog;
    int statusCode;
    String rbtn1,rbtn2,rbtn3,rbtn4,rbtn5,rbtn6,rbtn7,rbtn8,rbtn11,rbtn12,rbtn13,rbtn14,rbtn15;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_religion_fragment_step4);
        ButterKnife.bind(this);
        sessionManager=new SessionManager(this);
        apiservice = ApiServiceCreator.createService("latest");

        getAboutYouData();

        chk_impotnt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (((CheckBox) view).isChecked()) {
                    CheckImportnt1= "1";
                } else {
                    CheckImportnt1= "0";
                }

            }
        });

        chk_impotnt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (((CheckBox) view).isChecked()) {
                    CheckImportnt2= "1";
                } else {
                    CheckImportnt2= "0";
                }

            }
        });

        chk_impotnt3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (((CheckBox) view).isChecked()) {
                    CheckImportnt3= "1";
                } else {
                    CheckImportnt3= "0";
                }

            }
        });

        chk_impotnt4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (((CheckBox) view).isChecked()) {
                    CheckImportnt4= "1";
                } else {
                    CheckImportnt4= "0";
                }

            }
        });

        chk_impotnt5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (((CheckBox) view).isChecked()) {
                    CheckImportnt5= "1";
                } else {
                    CheckImportnt5= "0";
                }

            }
        });

        chk_impotnt6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (((CheckBox) view).isChecked()) {
                    CheckImportnt6= "1";
                } else {
                    CheckImportnt6= "0";
                }

            }
        });




        btn_next.setOnClickListener(view -> {

            int rgb1 = rg1.getCheckedRadioButtonId();
            rb1=(RadioButton) findViewById(rgb1);
            if (rb1!=null){
                rbtn1=rb1.getText().toString();
            }

            int rgb2 = rg2.getCheckedRadioButtonId();
            rb2=(RadioButton) findViewById(rgb2);
            if (rb2!=null){
                rbtn2=rb2.getText().toString();
            }

            int rgb3 = rg3.getCheckedRadioButtonId();
            rb3=(RadioButton) findViewById(rgb3);
            if (rb3!=null){
                rbtn3=rb3.getText().toString();
            }

            int rgb4 = rg4.getCheckedRadioButtonId();
            rb4=(RadioButton) findViewById(rgb4);
            if (rb4!=null){
                rbtn4=rb4.getText().toString();
            }

            int rgb5 = rg5.getCheckedRadioButtonId();
            rb5=(RadioButton) findViewById(rgb5);

            if (rb5!=null){
                rbtn5=rb5.getText().toString();
            }
            int rgb6 = rg6.getCheckedRadioButtonId();
            rb6=(RadioButton) findViewById(rgb6);

            if (rb6!=null){
                rbtn6=rb6.getText().toString();
            }

            int rgb7 = rg7.getCheckedRadioButtonId();
            rb7=(RadioButton) findViewById(rgb7);

            if (rb7!=null){
                rbtn7=rb7.getText().toString();
            }

            int rgb8 = rg8.getCheckedRadioButtonId();
            rb8=(RadioButton) findViewById(rgb8);

            if (rb8!=null){
                rbtn8=rb8.getText().toString();
            }
//            int rgb9 = rg9.getCheckedRadioButtonId();
//            rb9=(RadioButton) findViewById(rgb9);


//            if (rb9!=null){
//                rbtn9=rb9.getText().toString();
//            }

//            int rgb10 = rg10.getCheckedRadioButtonId();
//            rb10=(RadioButton) findViewById(rgb10);

//            if (rb10!=null){
//                rbtn10=rb10.getText().toString();
//            }
            int rgb11 = rg11.getCheckedRadioButtonId();
            rb11=(RadioButton) findViewById(rgb11);

            if (rb11!=null){
                rbtn11=rb11.getText().toString();
            }
            int rgb12 = rg12.getCheckedRadioButtonId();
            rb12=(RadioButton) findViewById(rgb12);

            if (rb12!=null){
                rbtn12=rb12.getText().toString();
            }
            int rgb13 = rg13.getCheckedRadioButtonId();
            rb13=(RadioButton) findViewById(rgb13);

            if (rb13!=null){
                rbtn13=rb13.getText().toString();
            }

            int rgb14 = rg14.getCheckedRadioButtonId();
            rb14=(RadioButton) findViewById(rgb14);

            if (rb14!=null){
                rbtn14=rb14.getText().toString();
            }

            int rgb15 = rg15.getCheckedRadioButtonId();
            rb15=(RadioButton) findViewById(rgb15);

            if (rb15!=null){
                rbtn15=rb15.getText().toString();
            }

            Character();

        });



    }

    public void Character(){

        pDialog = new ProgressDialog(this);
        pDialog.setTitle("Checking Data");
        pDialog.setMessage("Please Wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        Observable<ProfileBuild> responseObservable = apiservice.ReligionStep4(
                rbtn1,
                rbtn2,
                rbtn3,
                rbtn4,
                rbtn5,
                rbtn6,
                rbtn7,
                rbtn8,
                rbtn11,
                rbtn12,
                rbtn13,
                CheckImportnt1,
                CheckImportnt2,
                CheckImportnt3,
                CheckImportnt4,
                CheckImportnt5,
                CheckImportnt6,
                sessionManager.getKeyId()
                );

        responseObservable.subscribeOn(Schedulers.newThread())
                .observeOn(rx.android.schedulers.AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> {
                    if (throwable instanceof retrofit2.HttpException) {
                        retrofit2.HttpException ex = (retrofit2.HttpException) throwable;
                        statusCode = ex.code();
                        Log.e("error", ex.getLocalizedMessage());
                    } else if (throwable instanceof SocketTimeoutException) {
                        statusCode = 1000;
                    }
                    return Observable.empty();
                })
                .subscribe(new Observer<ProfileBuild>() {
                    @Override
                    public void onCompleted() {
                        pDialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("error", "" + e.getMessage());
                    }

                    @Override
                    public void onNext(ProfileBuild profileBuild) {
                        statusCode = profileBuild.getStatusCode();
                        if (statusCode == 200) {
                            Utility.displayToast(ReligionActivityStep4.this,profileBuild.getMessage());
                            Intent intent=new Intent(ReligionActivityStep4.this,FamilyActivityStep5.class);
                            startActivity(intent);
                        }else {
                            Intent intent=new Intent(ReligionActivityStep4.this,FamilyActivityStep5.class);
                            startActivity(intent);
                        }
                    }
                });

    }

    public void getAboutYouData(){

        pDialog = new ProgressDialog(this);
        pDialog.setTitle("Checking Data");
        pDialog.setMessage("Please Wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        Observable<ReligionDataModel> responseObservable = apiservice.getReligionData(sessionManager.getKeyId());

        responseObservable.subscribeOn(Schedulers.newThread())
                .observeOn(rx.android.schedulers.AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> {
                    if (throwable instanceof retrofit2.HttpException) {
                        retrofit2.HttpException ex = (retrofit2.HttpException) throwable;
                        statusCode = ex.code();
                        Log.e("error", ex.getLocalizedMessage());
                    } else if (throwable instanceof SocketTimeoutException) {
                        statusCode = 1000;
                    }
                    return Observable.empty();
                })
                .subscribe(new Observer<ReligionDataModel>() {
                    @Override
                    public void onCompleted() {
                        pDialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("error", "" + e.getMessage());
                    }

                    @Override
                    public void onNext(ReligionDataModel religionDataModel) {
                        statusCode = religionDataModel.getStatusCode();
                        if (statusCode == 200) {

                            if (religionDataModel.getData().get(0).getMenandwomen()!=null){

                                if (religionDataModel.getData().get(0).getMenandwomen().equals("Can have friendships with the opposite gender")){
                                    rd1.setChecked(true);
                                }else if (religionDataModel.getData().get(0).getMenandwomen().equals("Can have close friendships only with people of the same gender")){
                                    rd2.setChecked(true);
                                }
                            }


                            if (religionDataModel.getData().get(0).getYouspousewouldsay8()!=null){

                                if (religionDataModel.getData().get(0).getYouspousewouldsay8().equals("Can have friendships with the opposite gender")){
                                    rd3.setChecked(true);
                                }else if (religionDataModel.getData().get(0).getYouspousewouldsay8().equals("Can have close friendships only with people of the same gender")){
                                    rd4.setChecked(true);
                                }
                            }


                            if (religionDataModel.getData().get(0).getIslamis()!=null){

                                if (religionDataModel.getData().get(0).getIslamis().equals("Dear to my heart")){
                                    rd5.setChecked(true);
                                }else if (religionDataModel.getData().get(0).getIslamis().equals("My way of life")){
                                    rd6.setChecked(true);
                                }
                            }


                            if (religionDataModel.getData().get(0).getYouspousewouldsay9()!=null){

                                if (religionDataModel.getData().get(0).getYouspousewouldsay9().equals("Dear to my heart")){
                                    rd7.setChecked(true);
                                }else if (religionDataModel.getData().get(0).getIslamis().equals("My way of life")){
                                    rd8.setChecked(true);
                                }
                            }


                            if (religionDataModel.getData().get(0).getIslamicclasses()!=null){

                                if (religionDataModel.getData().get(0).getIslamicclasses().equals("Often")){
                                    rd9.setChecked(true);
                                }else if (religionDataModel.getData().get(0).getIslamicclasses().equals("Rarely")){
                                    rd10.setChecked(true);
                                }else if (religionDataModel.getData().get(0).getIslamicclasses().equals("sometimes")){
                                    rd99.setChecked(true);
                                }
                            }


                            if (religionDataModel.getData().get(0).getYouspousewouldsay10()!=null){

                                if (religionDataModel.getData().get(0).getYouspousewouldsay10().equals("Often")){
                                    rd11.setChecked(true);
                                }else if (religionDataModel.getData().get(0).getYouspousewouldsay10().equals("Rarely")){
                                    rd12.setChecked(true);
                                }else if (religionDataModel.getData().get(0).getYouspousewouldsay10().equals("sometimes")){
                                    rd98.setChecked(true);
                                }
                            }



                            if (religionDataModel.getData().get(0).getBestdescribesyou11()!=null){

                                if (religionDataModel.getData().get(0).getBestdescribesyou11().equals("Conformist")){
                                    rd13.setChecked(true);
                                }else if (religionDataModel.getData().get(0).getBestdescribesyou11().equals("More Liberal")){
                                    rd14.setChecked(true);
                                }
                            }


                            if (religionDataModel.getData().get(0).getYouspousewouldsay11()!=null){

                                if (religionDataModel.getData().get(0).getYouspousewouldsay11().equals("Conformist")){
                                    rd15.setChecked(true);
                                }else if (religionDataModel.getData().get(0).getYouspousewouldsay11().equals("More Liberal")){
                                    rd16.setChecked(true);
                                }
                            }


//                            if (religionDataModel.getData().get(0).getIslamdoyoufollow12()!=null){
//
//                                if (religionDataModel.getData().get(0).getIslamdoyoufollow12().equals("Sunni")){
//                                    rd17.setChecked(true);
//                                }else if (religionDataModel.getData().get(0).getIslamdoyoufollow12().equals("Shia")){
//                                    rd18.setChecked(true);
//                                }
//                            }



                            if (religionDataModel.getData().get(0).getPerformingprayer13()!=null){

                                if (religionDataModel.getData().get(0).getPerformingprayer13().equals("Sometimes hard for me to perform")){
                                    rd21.setChecked(true);
                                }else if (religionDataModel.getData().get(0).getPerformingprayer13().equals("Something that is I adhere to completely")){
                                    rd22.setChecked(true);
                                }
                            }


                            if (religionDataModel.getData().get(0).getYouspousewouldsay13()!=null){

                                if (religionDataModel.getData().get(0).getYouspousewouldsay13().equals("Sometimes hard for me to perform")){
                                    rd23.setChecked(true);
                                }else if (religionDataModel.getData().get(0).getYouspousewouldsay13().equals("Something that is I adhere to completely")){
                                    rd24.setChecked(true);
                                }

                            }

//
//                            if (religionDataModel.getData().get(0).getYouspousewouldsay13()!=null){
//
//                                if (religionDataModel.getData().get(0).getYouspousewouldsay13().equals("Sometimes hard for me to perform")){
//                                    rd23.setChecked(true);
//                                }else if (religionDataModel.getData().get(0).getYouspousewouldsay13().equals("Something that is I adhere to completely")){
//                                    rd24.setChecked(true);
//                                }
//
//                            }
//
//
//                            if (religionDataModel.getData().get(0).getYouspousewouldsay13()!=null){
//
//                                if (religionDataModel.getData().get(0).getYouspousewouldsay13().equals("Sometimes hard for me to perform")){
//                                    rd23.setChecked(true);
//                                }else if (religionDataModel.getData().get(0).getYouspousewouldsay13().equals("Something that is I adhere to completely")){
//                                    rd24.setChecked(true);
//                                }
//
//                            }

                            if (religionDataModel.getData().get(0).getScholar()!=null){

                                if (religionDataModel.getData().get(0).getScholar().equals("Hanafi")){
                                    rd25.setChecked(true);
                                }else if (religionDataModel.getData().get(0).getScholar().equals("Shafi")){
                                    rd26.setChecked(true);
                                }else if (religionDataModel.getData().get(0).getScholar().equals("Maliki")){
                                    rd27.setChecked(true);
                                }else if (religionDataModel.getData().get(0).getScholar().equals("Hanbali")){
                                    rd29.setChecked(true);
                                }else if (religionDataModel.getData().get(0).getScholar().equals("Jafari")){
                                    rd30.setChecked(true);
                                }else if (religionDataModel.getData().get(0).getScholar().equals("Salafi")){
                                    rd31.setChecked(true);
                                }else if (religionDataModel.getData().get(0).getScholar().equals("Other")){
                                    rd32.setChecked(true);
                                }
                            }



                            if (religionDataModel.getData().get(0).getImportnatqestion8()!=null){

                                if (religionDataModel.getData().get(0).getImportnatqestion8().equals("1")){
                                    chk_impotnt1.setChecked(true);
                                }
                            }


                            if (religionDataModel.getData().get(0).getImportnatqestion9()!=null){

                                if (religionDataModel.getData().get(0).getImportnatqestion9().equals("1")){
                                    chk_impotnt2.setChecked(true);
                                }
                            }


                            if (religionDataModel.getData().get(0).getImportnatqestion10()!=null){

                                if (religionDataModel.getData().get(0).getImportnatqestion10().equals("1")){
                                    chk_impotnt3.setChecked(true);
                                }
                            }


                            if (religionDataModel.getData().get(0).getImportnatqestion11()!=null){

                                if (religionDataModel.getData().get(0).getImportnatqestion11().equals("1")){
                                    chk_impotnt4.setChecked(true);
                                }
                            }


                            if (religionDataModel.getData().get(0).getImportnatqestion12()!=null){

                                if (religionDataModel.getData().get(0).getImportnatqestion12().equals("1")){
                                    chk_impotnt5.setChecked(true);
                                }
                            }


                            if (religionDataModel.getData().get(0).getImportnatqestion13()!=null){

                                if (religionDataModel.getData().get(0).getImportnatqestion13().equals("1")){
                                    chk_impotnt6.setChecked(true);
                                }
                            }


                        }
                    }
                });

    }

}
