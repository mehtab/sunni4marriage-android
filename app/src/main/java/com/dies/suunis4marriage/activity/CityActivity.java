package com.dies.suunis4marriage.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.SearchView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.dies.suunis4marriage.R;
import com.dies.suunis4marriage.apiservice.ApiService;
import com.dies.suunis4marriage.apiservice.ApiServiceCreator;
import com.dies.suunis4marriage.application.SessionManager;
import com.dies.suunis4marriage.model.CountryCityModel;

import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import rx.Observable;
import rx.Observer;
import rx.schedulers.Schedulers;

public class CityActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {


    ListView listView;
    SessionManager sessionManager;
    ApiService apiservice;
    ProgressDialog pDialog;
    int statusCode;
    String[] array;
    ArrayList<String> arrayList_country = new ArrayList<>();
    ArrayAdapter<String> dataAdapter,dataAdaptera;
    ArrayList<String> arrayList=new ArrayList<>();
    List<CountryCityModel.Data> data;
    ArrayList<String> arrayList_city=new ArrayList<>();

    String Stateid;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city);
        ButterKnife.bind(this);
        listView = findViewById(R.id.list_country);
        sessionManager = new SessionManager(this);
        apiservice = ApiServiceCreator.createService("latest");
        Stateid=getIntent().getStringExtra("id");
        SearchView simpleSearchView = (SearchView) findViewById(R.id.simpleSearchView);

        getCity(Stateid);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //  TextView txt = (TextView) view.findViewById(R.id.textView);
                // String str = txt.getText().toString();
                String str = (String) adapterView.getItemAtPosition(i);

                CountryCityModel.Data datalist=data.get(i);
                Stateid=datalist.getId();

                Intent intent = new Intent();
                intent.putExtra("data", str);
                setResult(RESULT_OK, intent);
             //   Toast.makeText(CityActivity.this, Stateid, Toast.LENGTH_SHORT).show();
                finish();
            }
        });

        simpleSearchView.setOnQueryTextListener(this);
    }



    public void getCity(String id){

        Observable<CountryCityModel> responseObservable = apiservice.getCity(id);

        responseObservable.subscribeOn(Schedulers.newThread())
                .observeOn(rx.android.schedulers.AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> {
                    if (throwable instanceof retrofit2.HttpException) {
                        retrofit2.HttpException ex = (retrofit2.HttpException) throwable;
                        statusCode = ex.code();
                        Log.e("error", ex.getLocalizedMessage());
                    } else if (throwable instanceof SocketTimeoutException) {
                        statusCode = 1000;
                    }
                    return Observable.empty();
                })
                .subscribe(new Observer<CountryCityModel>() {
                    @Override
                    public void onCompleted() {
                        //  pDialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("error", "" + e.getMessage());
                    }

                    @Override
                    public void onNext(CountryCityModel countryCityModel) {
                        statusCode = countryCityModel.getStatusCode();
                        if (statusCode == 200) {
                            data=countryCityModel.getData();
                            for (int i=0;i<data.size();i++){
                                arrayList_city.add(data.get(i).getCountry());
                            }

//                            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(PersonalActivityStep1.this, R.layout.spinner_item,arrayList_city);
//                            dataAdapter.add("Select City");
//                            dataAdapter.setDropDownViewResource(R.layout.spinner_item);
                            array = arrayList.toArray(new String[0]);
                            dataAdaptera = new ArrayAdapter<String>(CityActivity.this, R.layout.spinner_item,arrayList_city);
                            listView.setAdapter(dataAdaptera);

                        }
                    }
                });

    }

    @Override
    public void onBackPressed() {
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_CANCELED, returnIntent);
        finish();
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        String text=newText;
        dataAdaptera.getFilter().filter(newText);
        return false;
    }
}
