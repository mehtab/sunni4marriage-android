package com.dies.suunis4marriage.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.widget.Button;
import android.widget.Toast;

import com.dies.suunis4marriage.R;
import com.dies.suunis4marriage.adapter.AnswerAdapter;
import com.dies.suunis4marriage.apiservice.ApiService;
import com.dies.suunis4marriage.apiservice.ApiServiceCreator;
import com.dies.suunis4marriage.application.SessionManager;
import com.dies.suunis4marriage.application.Utility;
import com.dies.suunis4marriage.model.FriendModel;
import com.dies.suunis4marriage.model.QuestionsAnswerModel;
import com.dies.suunis4marriage.model.UserDetail;

import java.net.SocketTimeoutException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observable;
import rx.Observer;
import rx.schedulers.Schedulers;

public class Answer_QuestionsActivity extends AppCompatActivity {


    @BindView(R.id.rcv_id)
    RecyclerView rcv_id;

    SessionManager sessionManager;
    ApiService apiservice;
    ProgressDialog pDialog;
    RecyclerView.LayoutManager layoutManager;
    int statusCode;
    AnswerAdapter answerAdapter;
    String user_id;
    @BindView(R.id.btn_submit)
    Button btn_submit;
//    @BindView(R.id.txt_demo)
//    TextView txt_demo;
    ArrayList<String> list=new ArrayList<>();
    ArrayList<String> listid=new ArrayList<>();
    String status="0";
    String flag="search_friend";
    int pos;
    public ArrayList<QuestionsAnswerModel.Data> editModelArrayList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_answer__questions);
        ButterKnife.bind(this);
        sessionManager = new SessionManager(this);
        apiservice = ApiServiceCreator.createService("latest");


        Intent intent=getIntent();
        user_id=intent.getStringExtra("id");
        pos=getIntent().getIntExtra("pos",0);

        getQuestionsData();


        btn_submit.setOnClickListener(view -> {

            for (int i = 0; i < AnswerAdapter.arrayList.size(); i++) {
                AnswerAdapter.arrayList.get(i).getEditTextValue();

                list.add(AnswerAdapter.arrayList.get(i).getEditTextValue());
                listid.add(AnswerAdapter.arrayList.get(i).getQ_id());
             //   txt_demo.setText(txt_demo.getText() + " " +AnswerAdapter.arrayList.get(i).getEditTextValue()+System.getProperty("line.separator"));
            }

            AnswerQuestions(list,listid);
        });


    }

    public void getQuestionsData(){
        rcv_id.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this    );
        rcv_id.setLayoutManager(layoutManager);
        pDialog = new ProgressDialog(this);
        pDialog.setTitle("Checking Data");
        pDialog.setMessage("Please Wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        Observable<QuestionsAnswerModel> responseObservable = apiservice.getQuestionsData(user_id);

        responseObservable.subscribeOn(Schedulers.newThread())
                .observeOn(rx.android.schedulers.AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> {
                    if (throwable instanceof retrofit2.HttpException) {
                        retrofit2.HttpException ex = (retrofit2.HttpException) throwable;
                        statusCode = ex.code();
                        Log.e("error", ex.getLocalizedMessage());
                    } else if (throwable instanceof SocketTimeoutException) {
                        statusCode = 1000;
                    }
                    return Observable.empty();
                })
                .subscribe(new Observer<QuestionsAnswerModel>() {
                    @Override
                    public void onCompleted() {
                        pDialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("error", "" + e.getMessage());
                    }

                    @Override
                    public void onNext(QuestionsAnswerModel questionsAnswerModel) {
                        statusCode = questionsAnswerModel.getStatusCode();
                        if (statusCode == 200) {
                            answerAdapter=new AnswerAdapter(questionsAnswerModel.getData(),Answer_QuestionsActivity.this);
                            rcv_id.setAdapter(answerAdapter);
                        }else if (statusCode == 405){
                            Toast.makeText(Answer_QuestionsActivity.this, questionsAnswerModel.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                        editModelArrayList = populateList();
                    }
                });

    }

    private ArrayList<QuestionsAnswerModel.Data> populateList(){

        ArrayList<QuestionsAnswerModel.Data> list = new ArrayList<>();

        for(int i = 0; i < answerAdapter.getItemCount(); i++){
            QuestionsAnswerModel.Data editModel = new QuestionsAnswerModel().new Data();
            editModel.setEditTextValue(String.valueOf(i));
            list.add(editModel);
        }

        return list;

}



public void AnswerQuestions(ArrayList<String> anslist,ArrayList<String> idlist){


        Observable<UserDetail> responseObservable = apiservice.getAnswersData(sessionManager.getKeyId(),
                anslist,
                idlist);

        responseObservable.subscribeOn(Schedulers.newThread())
                .observeOn(rx.android.schedulers.AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> {
                    if (throwable instanceof retrofit2.HttpException) {
                        retrofit2.HttpException ex = (retrofit2.HttpException) throwable;
                        statusCode = ex.code();
                        Log.e("error", ex.getLocalizedMessage());
                    } else if (throwable instanceof SocketTimeoutException) {
                        statusCode = 1000;
                    }
                    return Observable.empty();
                })
                .subscribe(new Observer<UserDetail>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("error", "" + e.getMessage());
                    }

                    @Override
                    public void onNext(UserDetail questionsAnswerModel) {
                        statusCode = questionsAnswerModel.getStatusCode();
                        if (statusCode == 200) {
                            Utility.displayToast(Answer_QuestionsActivity.this,questionsAnswerModel.getMessage());
                            UpdateConnectRequest();
                        }else {
                            Utility.displayToast(Answer_QuestionsActivity.this,questionsAnswerModel.getMessage());
                        }
                    }
                });


}



    public void UpdateConnectRequest(){


//        pDialog.setTitle("Checking Data");
//        pDialog.setMessage("Please Wait...");
//        pDialog.setIndeterminate(false);
//        pDialog.setCancelable(false);
//        pDialog.show();

        Observable<FriendModel> responseObservable = apiservice.ChangeFriendStatus(sessionManager.getKeyId(),
                AnswerAdapter.arrayList.get(0).getUser_id(),
                status,
                flag,
                "",
                "");

        responseObservable.subscribeOn(Schedulers.newThread())
                .observeOn(rx.android.schedulers.AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> {
                    if (throwable instanceof retrofit2.HttpException) {
                        retrofit2.HttpException ex = (retrofit2.HttpException) throwable;
                        statusCode = ex.code();
                        Log.e("error", ex.getLocalizedMessage());
                    } else if (throwable instanceof SocketTimeoutException) {
                        statusCode = 1000;
                    }
                    return Observable.empty();
                })
                .subscribe(new Observer<FriendModel>() {
                    @Override
                    public void onCompleted() {
//                        pDialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("error", "" + e.getMessage());
                    }

                    @Override
                    public void onNext(FriendModel friendModel) {
                        statusCode = friendModel.getStatusCode();
                        if (statusCode == 200) {
                            Utility.displayToast(Answer_QuestionsActivity.this,friendModel.getMessage());

                            finish();
                        }else {
                            Utility.displayToast(Answer_QuestionsActivity.this,friendModel.getMessage());
                        }

                    }
                });

    }

}
