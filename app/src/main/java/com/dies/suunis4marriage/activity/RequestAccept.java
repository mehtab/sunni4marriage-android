package com.dies.suunis4marriage.activity;

import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import com.dies.suunis4marriage.R;
import com.dies.suunis4marriage.adapter.FragmentAdapter;
import com.dies.suunis4marriage.fragment.CompatibilitySearch.AcceptedConnectionFragment;
import com.dies.suunis4marriage.fragment.CompatibilitySearch.RequestFragment;

import com.dies.suunis4marriage.fragment.FriendRequest.SentRequestFragment;

public class RequestAccept extends AppCompatActivity {


    ViewPager viewPager;
    TabLayout tabs;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_accept);


        viewPager = (ViewPager)findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        // Set Tabs inside Toolbar
        tabs = (TabLayout)findViewById(R.id.result_tabs);
        tabs.setupWithViewPager(viewPager);
    }


    private void setupViewPager(ViewPager viewPager) {


        FragmentAdapter adapter = new FragmentAdapter(getSupportFragmentManager());
        adapter.addFragment(new RequestFragment(), "Received Request");
        adapter.addFragment(new AcceptedConnectionFragment(), "Accepted Request");
        adapter.addFragment(new SentRequestFragment(), "Sent Request");
        viewPager.setAdapter(adapter);
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(3);
        viewPager.getAdapter().notifyDataSetChanged();
    }
}
