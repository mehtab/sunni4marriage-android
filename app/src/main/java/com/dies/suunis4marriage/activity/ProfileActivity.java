package com.dies.suunis4marriage.activity;

import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import com.dies.suunis4marriage.R;
import com.dies.suunis4marriage.adapter.FragmentAdapter;
import com.dies.suunis4marriage.fragment.CompatibilitySearch.SearchFragment;
import com.dies.suunis4marriage.fragment.CompatibilitySearch.SuggestionFragment;
import com.dies.suunis4marriage.fragment.SearchByUserid;

public class ProfileActivity extends AppCompatActivity {

    ViewPager viewPager;
    TabLayout tabs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);


        viewPager = (ViewPager)findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        // Set Tabs inside Toolbar
        tabs = (TabLayout)findViewById(R.id.result_tabs);
        tabs.setupWithViewPager(viewPager);

    }


    private void setupViewPager(ViewPager viewPager) {

        FragmentAdapter adapter = new FragmentAdapter(getSupportFragmentManager());
        adapter.addFragment(new SearchFragment(), "Search");
        adapter.addFragment(new SearchByUserid(), "Search For ID");
        adapter.addFragment(new SuggestionFragment(), "Suggestion");
        viewPager.setAdapter(adapter);
    }
}
