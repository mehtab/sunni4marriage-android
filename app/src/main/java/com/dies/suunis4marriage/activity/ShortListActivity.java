package com.dies.suunis4marriage.activity;

import android.app.ProgressDialog;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.dies.suunis4marriage.R;
import com.dies.suunis4marriage.adapter.ProfileAdapter;
import com.dies.suunis4marriage.apiservice.ApiService;
import com.dies.suunis4marriage.apiservice.ApiServiceCreator;
import com.dies.suunis4marriage.application.SessionManager;
import com.dies.suunis4marriage.model.ProfileSearch;

import java.net.SocketTimeoutException;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observable;
import rx.Observer;
import rx.schedulers.Schedulers;

public class ShortListActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener{

    @BindView(R.id.rcv_profile)
    RecyclerView rcv_profile;

    @BindView(R.id.txt_nodata)
    TextView txt_nodata;
    @BindView(R.id.back_icon)
    ImageView back_icon;

    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipe_refresh_layout;


    SessionManager sessionManager;
    ApiService apiservice;
    ProgressDialog pDialog;
    RecyclerView.LayoutManager layoutManager;
    int statusCode;
    ProfileAdapter profileAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_short_list);
        ButterKnife.bind(this);
        sessionManager = new SessionManager(this);
        apiservice = ApiServiceCreator.createService("latest");
        back_icon.setOnClickListener(view -> {
            finish();
        });

        swipe_refresh_layout.setOnRefreshListener(this);

        swipe_refresh_layout.post(new Runnable() {
                                      @Override
                                      public void run() {
                                          swipe_refresh_layout.setRefreshing(true);

                                          getShortListData();
                                      }
                                  }
        );

        //getShortListData();

    }

    public void getShortListData(){

        rcv_profile.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        rcv_profile.setLayoutManager(layoutManager);
        pDialog = new ProgressDialog(this);
        pDialog.setTitle("Checking Data");
        pDialog.setMessage("Please Wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        Observable<ProfileSearch> responseObservable = apiservice.ShowShortList(sessionManager.getKeyId(),
                "shortlist_data");

        responseObservable.subscribeOn(Schedulers.newThread())
                .observeOn(rx.android.schedulers.AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> {
                    if (throwable instanceof retrofit2.HttpException) {
                        retrofit2.HttpException ex = (retrofit2.HttpException) throwable;
                        statusCode = ex.code();
                        Log.e("error", ex.getLocalizedMessage());
                    } else if (throwable instanceof SocketTimeoutException) {
                        statusCode = 1000;
                    }
                    return Observable.empty();
                })
                .subscribe(new Observer<ProfileSearch>() {
                    @Override
                    public void onCompleted() {
                        pDialog.dismiss();
                        swipe_refresh_layout.setRefreshing(false);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("error", "" + e.getMessage());
                    }

                    @Override
                    public void onNext(ProfileSearch friendModel) {
                        statusCode = friendModel.getStatusCode();
                        if (statusCode == 200) {
                            if (friendModel.getData().size()>0){
                                txt_nodata.setVisibility(View.GONE);
                                profileAdapter=new ProfileAdapter(ShortListActivity.this,friendModel.getData(),"search_friend","favourite");
                                rcv_profile.setAdapter(profileAdapter);
                            }else {
                                txt_nodata.setVisibility(View.VISIBLE);
                            }

                        }
                    }
                });

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        getShortListData();
    }

    @Override
    public void onRefresh() {
        getShortListData();
    }

}
