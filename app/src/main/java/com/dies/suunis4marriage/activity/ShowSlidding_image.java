package com.dies.suunis4marriage.activity;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import com.dies.suunis4marriage.R;
import com.dies.suunis4marriage.apiservice.ApiConstants;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

public class ShowSlidding_image extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_slidding_image);
        ImageView img =findViewById(R.id.img);

        String img1=getIntent().getStringExtra("img1");
        String img2=getIntent().getStringExtra("img2");
        String img3=getIntent().getStringExtra("img3");
        String img4=getIntent().getStringExtra("img4");

        if (img1!=null && !img1.equals("")){
            Picasso.with(this).load(ApiConstants.IMAGE_URL + img1)
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .networkPolicy(NetworkPolicy.NO_CACHE)
                    .into(img);
        }else if (img2!=null && !img2.equals("")){
            Picasso.with(this).load(ApiConstants.IMAGE_URL + img2)
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .networkPolicy(NetworkPolicy.NO_CACHE)
                    .into(img);
        }else if (img3!=null && !img3.equals("")){
            Picasso.with(this).load(ApiConstants.IMAGE_URL + img3)
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .networkPolicy(NetworkPolicy.NO_CACHE)
                    .into(img);
        }else {
            Picasso.with(this).load(ApiConstants.IMAGE_URL + img4)
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .networkPolicy(NetworkPolicy.NO_CACHE)
                    .into(img);
        }

    }
}
