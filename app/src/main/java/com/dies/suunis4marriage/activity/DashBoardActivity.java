package com.dies.suunis4marriage.activity;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dies.suunis4marriage.R;
import com.dies.suunis4marriage.activity.profile.SettingActivity;
import com.dies.suunis4marriage.apiservice.ApiConstants;
import com.dies.suunis4marriage.apiservice.ApiService;
import com.dies.suunis4marriage.apiservice.ApiServiceCreator;
import com.dies.suunis4marriage.apiservice.FileUtils;
import com.dies.suunis4marriage.application.SessionManager;
import com.dies.suunis4marriage.application.Utility;
import com.dies.suunis4marriage.model.ProfileBuild;
import com.dies.suunis4marriage.model.UserDataResponse;
import com.dies.suunis4marriage.model.UserDetail;
import com.google.gson.Gson;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.Observable;
import rx.Observer;
import rx.schedulers.Schedulers;

public class DashBoardActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    private final String TAG = "data";
    @BindView(R.id.card_appointment)
    CardView card_appointment;
    @BindView(R.id.card_competibilitysearch)
    CardView card_competibilitysearch;
    @BindView(R.id.card_request)
    CardView card_request;
    @BindView(R.id.card_shortlist)
    CardView card_shortlist;
    @BindView(R.id.card_chat)
    CardView card_chat;
    @BindView(R.id.card_logout)
    CardView card_logout;
    @BindView(R.id.card_plan)
    CardView card_plan;
    @BindView(R.id.profile_image)
    ImageView profile_image;
    @BindView(R.id.tv_name)
    TextView tv_name;
    @BindView(R.id.txt_uniq_id)
    TextView txt_uniq_id;
    @BindView(R.id.btn_upgrd)
    Button btn_upgrd;
    @BindView(R.id.card_invite)
    CardView card_invite;
    @BindView(R.id.card_suggetion)
    CardView card_suggetion;
    @BindView(R.id.card_statistics)
    CardView card_statistics;
    @BindView(R.id.card_payment_history)
    CardView card_payment_history;
    @BindView(R.id.card_delete)
    CardView card_delete;
    @BindView(R.id.card_blocklist)
    CardView card_blocklist;
    @BindView(R.id.card_settings)
    CardView card_settings;
    @BindView(R.id.img_premium)
    ImageView img_premium;
    @BindView(R.id.img_upld)
    ImageView img_upld;
    @BindView(R.id.txt_account)
    TextView txt_account;
    @BindView(R.id.btn)
    Button btn;
    @BindView(R.id.notify_icon)
    ImageView notify_icon;
    Uri uri, selectedImage;
    String mediaPath, ImageType;
    SessionManager sessionManager;
    ApiService apiservice;
    int statusCode;
    String DIRECTORY = Environment.getExternalStorageDirectory().getPath() + "/PetsForever/ProfileImage/";
    String pic_name = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
    String StoredPath = DIRECTORY + pic_name + ".png";
    List<String> imagesEncodedList;
    String imageEncoded;
    ProgressDialog pDialog;
    private Dialog dialog;
    private int STORAGE_PERMISSION_CODE = 23;
    private ArrayList<Uri> arrayListuri;
    private Boolean exit = false;

    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board);
        ButterKnife.bind(this);
        apiservice = ApiServiceCreator.createService("latest");
        sessionManager = new SessionManager(this);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);

        swipeRefreshLayout.setOnRefreshListener(this);

        swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        swipeRefreshLayout.setRefreshing(true);
                                        getUserDetail();
                                    }
                                }
        );


        OnlineStatus();


        btn.setOnClickListener(view -> {
            Intent i = new Intent(this, getCurrentLocation.class);
            startActivity(i);
        });

        tv_name.setText(sessionManager.getUserData().get(0).getFirst_name());

        txt_uniq_id.setText(sessionManager.getUserData().get(0).getUnique_user_id());


        btn_upgrd.setOnClickListener(view -> {
//            Utility.getAppcon().getSession().amount = "69.99";
            Utility.getAppcon().getSession().screen_name = "upgrade";
            Intent intent = new Intent(this, PlanActivity.class);
            startActivity(intent);
        });
        card_appointment.setOnClickListener(view -> {
            startActivity(new Intent(this, EditProfile.class));
        });
        card_competibilitysearch.setOnClickListener(view -> {
            if (sessionManager.isPayment()) {
                startActivity(new Intent(this, ProfileActivity.class));
            } else {
                Utility.displayToast(DashBoardActivity.this, "Please Pay First Payment");
            }
            //startActivity(new Intent(this, ProfileActivity.class));
        });

        card_logout.setOnClickListener(view -> {
            sessionManager.logoutUser();

        });

        card_payment_history.setOnClickListener(view -> {
            Intent intent = new Intent(this, PaymentHistoryActivity.class);
            startActivity(intent);
        });

        profile_image.setOnClickListener(view -> {

        });

        img_upld.setOnClickListener(view -> {
            Intent intent = new Intent(this, EditProfileImage.class);
            startActivity(intent);
        });

        card_request.setOnClickListener(view -> {
            if (sessionManager.isPayment()) {
                Intent intent = new Intent(this, RequestAccept.class);
                startActivity(intent);
            } else {
                Utility.displayToast(DashBoardActivity.this, "Please Pay First Payment");
            }


        });

        card_plan.setOnClickListener(view -> {
            Utility.getAppcon().getSession().screen_name = "plan";
            Intent intent = new Intent(this, PlanActivity.class);
            startActivity(intent);
        });


        card_shortlist.setOnClickListener(view -> {
           /* if (sessionManager.isPayment()) {
                Intent intent = new Intent(this, ShortListActivity.class);
                startActivity(intent);
            } else {
                Utility.displayToast(DashBoardActivity.this, "Please Pay First Payment");
            }*/
            /*irfannn*/
            Intent intent = new Intent(this, ShortListActivity.class);
            startActivity(intent);
        });

        card_chat.setOnClickListener(view -> {

            if (sessionManager.isPayment()) {
                Intent intent = new Intent(this, ChatActivity.class);
                startActivity(intent);
            } else {
                Utility.displayToast(DashBoardActivity.this, "Please Pay First Payment");
            }

        });

        card_invite.setOnClickListener(view -> {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, "download Sunnis4Marriage app using this link\n https://play.google.com/store/apps/details?id=com.dies.suunis4marriage&hl=en");
            sendIntent.setType("text/plain");
            startActivity(sendIntent);

        });

        card_suggetion.setOnClickListener(view -> {
            if (sessionManager.isPayment()) {
                Intent intent = new Intent(this, SuggetionActivity.class);
                startActivity(intent);
            } else {
                Utility.displayToast(DashBoardActivity.this, "Please Pay First Payment");
            }


        });


        card_statistics.setOnClickListener(view -> {
            Intent intent = new Intent(this, MyViewedProfileActivity.class);
            startActivity(intent);
        });

        card_delete.setOnClickListener(view -> {
            AlertDialog diaBox = AskOption();
            diaBox.show();
        });

        card_blocklist.setOnClickListener(view -> {
            Intent intent = new Intent(this, BlockActivity.class);
            startActivity(intent);

        });


        card_settings.setOnClickListener(view -> {
            Intent intent = new Intent(this, SettingActivity.class);
            startActivity(intent);
        });

        notify_icon.setOnClickListener(view -> {
            Intent intent = new Intent(this, NotificationActivity.class);
            startActivity(intent);
        });


        Picasso.with(DashBoardActivity.this).load(ApiConstants.IMAGE_URL + sessionManager.getKeyImage())
                .memoryPolicy(MemoryPolicy.NO_CACHE)
                .networkPolicy(NetworkPolicy.NO_CACHE)
                .placeholder(R.drawable.splash_image)
                .into(profile_image);
    }

    @Override
    public void onRefresh() {
        getUserDetail();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            arrayListuri = new ArrayList<>();
            if (requestCode == 100 && resultCode == RESULT_OK && null != data) {

                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                imagesEncodedList = new ArrayList<String>();
                if (data.getData() != null) {
                    Uri selectedImage = data.getData();
                    try {
                        final String path = FileUtils.getPath(this, selectedImage);
                        arrayListuri.add(selectedImage);

                    } catch (Exception e) {
                        Log.e(TAG, "File select error", e);
                    }

                    UploadMultilpeImage();

                } else {
                    if (data.getClipData() != null) {

                        int count = data.getClipData().getItemCount();
                        int currentItem = 0;

                        while (currentItem < count) {
                            Uri imageUri = data.getClipData().getItemAt(currentItem).getUri();

                            //do something with the image (save it to some directory or whatever you need to do with it here)
                            currentItem = currentItem + 1;
                            Log.d("Uri Selected", imageUri.toString());
                            try {
                                // Get the file path from the URI
                                String path = FileUtils.getPath(this, imageUri);
                                Log.d("Multiple File Selected", path);

                                arrayListuri.add(imageUri);

                            } catch (Exception e) {
                                Log.e(TAG, "File select error", e);
                            }
                        }
                        UploadMultilpeImage();
                    }
                }
            }
        } catch (Exception e) {
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG).show();
        }


    }

    private void uploadFile() {

        final ProgressDialog pDialog;
        pDialog = new ProgressDialog(this);
        pDialog.setTitle("Uploading profile image");
        pDialog.setMessage("Please Wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(true);
        pDialog.show();

        // Map is used to multipart the file using okhttp3.RequestBody
        Map<String, RequestBody> map = new HashMap<>();
        if (mediaPath != null) {
            File file = new File(mediaPath);
            RequestBody requestBody = RequestBody.create(MediaType.parse("*/*"), file);
            RequestBody user_id = RequestBody.create(MediaType.parse("text/plain"), sessionManager.getKeyId());
            map.put("file\"; filename=\"" + file.getName() + "\"", requestBody);
            map.put("user_id", user_id);
            Observable<ProfileBuild> responseObservable = apiservice.upload_profile_image(map);

            responseObservable.subscribeOn(Schedulers.newThread())
                    .observeOn(rx.android.schedulers.AndroidSchedulers.mainThread())
                    .onErrorResumeNext(throwable -> {
                        if (throwable instanceof retrofit2.HttpException) {
                            retrofit2.HttpException ex = (retrofit2.HttpException) throwable;
                            statusCode = ex.code();
                            Log.e("error", ex.getLocalizedMessage());
                        } else if (throwable instanceof SocketTimeoutException) {
                            statusCode = 1000;
                        }
                        return Observable.empty();
                    })
                    .subscribe(new Observer<ProfileBuild>() {
                        @Override
                        public void onCompleted() {
                            pDialog.dismiss();
                        }

                        @Override
                        public void onError(Throwable e) {
                            Log.e("error", "" + e.getMessage());
                        }

                        @Override
                        public void onNext(ProfileBuild profileBuild) {
                            statusCode = profileBuild.getStatusCode();
                            if (statusCode == 200) {
                                Toast.makeText(getApplicationContext(), "Your profile image successfully updated", Toast.LENGTH_SHORT).show();
                                Picasso.with(DashBoardActivity.this).load(ApiConstants.IMAGE_URL + profileBuild.getMessage())
                                        .memoryPolicy(MemoryPolicy.NO_CACHE)
                                        .networkPolicy(NetworkPolicy.NO_CACHE)
                                        .fit()
                                        .placeholder(R.drawable.logo)
                                        .into(profile_image);
                                sessionManager.setKeyImage(profileBuild.getMessage());
                            }

                        }
                    });

        }
    }

    public void OnlineStatus() {

        int status = 1;
        Observable<UserDetail> responseObservable = apiservice.getStatus(sessionManager.getKeyId(), status);

        responseObservable.subscribeOn(Schedulers.newThread())
                .observeOn(rx.android.schedulers.AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> {
                    if (throwable instanceof retrofit2.HttpException) {
                        retrofit2.HttpException ex = (retrofit2.HttpException) throwable;
                        statusCode = ex.code();
                        Log.e("error", ex.getLocalizedMessage());
                    } else if (throwable instanceof SocketTimeoutException) {
                        statusCode = 1000;
                    }
                    return Observable.empty();
                })
                .subscribe(new Observer<UserDetail>() {
                    @Override
                    public void onCompleted() {
                        //  pDialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("error", "" + e.getMessage());
                    }

                    @Override
                    public void onNext(UserDetail userDetail) {
                        statusCode = userDetail.getStatusCode();
                        if (statusCode == 200) {
                            Toast.makeText(DashBoardActivity.this, "Online", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (isFinishing()) {
            //    Toast.makeText(this, "Finish your application.........", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (isFinishing()) {
            // Toast.makeText(this, "Finish your application.........", Toast.LENGTH_SHORT).show();
        }
    }

    private void getUserDetail() {

        Observable<UserDataResponse> responseObservable = apiservice.getUserDetails(sessionManager.getKeyEmail());
        responseObservable.subscribeOn(Schedulers.newThread())
                .observeOn(rx.android.schedulers.AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> {
                    if (throwable instanceof retrofit2.HttpException) {
                        retrofit2.HttpException ex = (retrofit2.HttpException) throwable;
                        statusCode = ex.code();
                        Log.e("error", ex.getLocalizedMessage());
                    } else if (throwable instanceof SocketTimeoutException) {
                        statusCode = 1000;
                    }
                    return Observable.empty();
                })
                .subscribe(new Observer<UserDataResponse>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        swipeRefreshLayout.setRefreshing(false);
                        Log.e("error", e.getLocalizedMessage());
                    }

                    @Override
                    public void onNext(UserDataResponse userDataResponse) {
                        statusCode = userDataResponse.getStatusCode();
                        if (statusCode == 200) {
                            swipeRefreshLayout.setRefreshing(false);
                            if (userDataResponse.getData().get(0).getPayment_status().equals("1")) {
                                sessionManager.createPaymentSession();
                                // tv_name.setText(userDataResponse.getData().get(0).getFirstName());
                            }

                            sessionManager.setKeyUserDetails(new Gson().toJson(userDataResponse.getData()));
                            sessionManager.setKeyImage(userDataResponse.getData().get(0).getImage11());
                            tv_name.setText(sessionManager.getUserData().get(0).getFirst_name());

                            if (userDataResponse.getData().get(0).getPayment_status().equals("1")) {
                                sessionManager.createPaymentSession();
                            } else {
                                sessionManager.destroyPaymentSession();
                            }
                            if (userDataResponse.getData().get(0).getPlan_status() != null) {
                                if (userDataResponse.getData().get(0).getPlan_status().equals("1")) {
                                    sessionManager.createPlanSession();
                                }
                            }

                            if (sessionManager.isPayment()) {
                                img_premium.setVisibility(View.VISIBLE);
                                btn_upgrd.setVisibility(View.GONE);
                                txt_account.setText("Account-Paid");
                            } else {
                                txt_account.setText("Account-Free");
                                btn_upgrd.setVisibility(View.VISIBLE);
                                img_premium.setVisibility(View.GONE);
                                displayDialog();
                            }

                        }

                        Picasso.with(DashBoardActivity.this).load(ApiConstants.IMAGE_URL + sessionManager.getKeyImage())
                                .memoryPolicy(MemoryPolicy.NO_CACHE)
                                .networkPolicy(NetworkPolicy.NO_CACHE)
                                .placeholder(R.drawable.splash_image)
                                .into(profile_image);
                    }
                });
    }

    private boolean isReadStorageAllowed() {
        //Getting the permission status
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);

        //If permission is granted returning true
        if (result == PackageManager.PERMISSION_GRANTED)
            return true;

        //If permission is not granted returning false
        return false;
    }

    private void requestStoragePermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {

        }

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == STORAGE_PERMISSION_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "Permission granted now you can read the storage", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(this, "Oops you just denied the permission", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        getUserDetail();
    }

    @Override
    public void onBackPressed() {
        if (exit) {
            finish(); // finish activity
        } else {
            Toast.makeText(this, "Press Back again to Exit.",
                    Toast.LENGTH_SHORT).show();
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 3 * 1000);

        }

    }

    public void UploadMultilpeImage() {

        final ProgressDialog pDialog;
        pDialog = new ProgressDialog(this);
        pDialog.setTitle("Uploading profile image");
        pDialog.setMessage("Please Wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(true);
        pDialog.show();

        List<MultipartBody.Part> parts = new ArrayList<>();

        if (arrayListuri != null) {
            // create part for file (photo, video, ...)
            for (int i = 0; i < arrayListuri.size(); i++) {
                parts.add(prepareFilePart("image[]", arrayListuri.get(i)));
            }
        }

        RequestBody size = createPartFromString("" + parts.size());
        RequestBody user_id = createPartFromString(sessionManager.getKeyId());

        // Map is used to multipart the file using okhttp3.RequestBody
//        Map<String, RequestBody> map = new HashMap<>();
//        if (mediaPath != null) {
//            File file = new File(mediaPath);
//            RequestBody requestBody = RequestBody.create(MediaType.parse("*/*"), file);
//            RequestBody user_id = RequestBody.create(MediaType.parse("text/plain"), sessionManager.getKeyId());
//            map.put("file\"; filename=\"" + file.getName() + "\"", requestBody);
//            map.put("user_id", user_id);
        Observable<ProfileBuild> responseObservable = apiservice.upload_multiple_profile_image(parts, user_id);

        responseObservable.subscribeOn(Schedulers.newThread())
                .observeOn(rx.android.schedulers.AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> {
                    if (throwable instanceof retrofit2.HttpException) {
                        retrofit2.HttpException ex = (retrofit2.HttpException) throwable;
                        statusCode = ex.code();
                        Log.e("error", ex.getLocalizedMessage());
                    } else if (throwable instanceof SocketTimeoutException) {
                        statusCode = 1000;
                    }
                    return Observable.empty();
                })
                .subscribe(new Observer<ProfileBuild>() {
                    @Override
                    public void onCompleted() {
                        pDialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("error", "" + e.getMessage());
                    }

                    @Override
                    public void onNext(ProfileBuild profileBuild) {
                        statusCode = profileBuild.getStatusCode();
                        if (statusCode == 200) {
                            Toast.makeText(getApplicationContext(), "Your profile image successfully updated", Toast.LENGTH_SHORT).show();
                            Picasso.with(DashBoardActivity.this).load(ApiConstants.IMAGE_URL + profileBuild.getMessage())
                                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                                    .networkPolicy(NetworkPolicy.NO_CACHE)
                                    .fit()
                                    .placeholder(R.drawable.logo)
                                    .into(profile_image);
                            sessionManager.setKeyImage(profileBuild.getMessage());
                        }

                    }
                });


    }


    @NonNull
    private MultipartBody.Part prepareFilePart(String partName, Uri fileUri) {
        // https://github.com/iPaulPro/aFileChooser/blob/master/aFileChooser/src/com/ipaulpro/afilechooser/utils/FileUtils.java
        // use the FileUtils to get the actual file by uri
        File file = FileUtils.getFile(this, fileUri);

        // create RequestBody instance from file
        RequestBody requestFile =
                RequestBody.create(
                        MediaType.parse(Objects.requireNonNull(getContentResolver().getType(fileUri))), file
                );

        // MultipartBody.Part is used to send also the actual file name
        return MultipartBody.Part.createFormData(partName, file.getName(), requestFile);
    }

    @NonNull
    private RequestBody createPartFromString(String descriptionString) {
        return RequestBody.create(
                okhttp3.MultipartBody.FORM, descriptionString);
    }

    public void DeleteProfile() {
        pDialog = new ProgressDialog(this);
        pDialog.setTitle("Checking Data");
        pDialog.setMessage("Please Wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        Observable<UserDetail> responseObservable = apiservice.DeleteProfile(sessionManager.getKeyId());

        responseObservable.subscribeOn(Schedulers.newThread())
                .observeOn(rx.android.schedulers.AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> {
                    if (throwable instanceof retrofit2.HttpException) {
                        retrofit2.HttpException ex = (retrofit2.HttpException) throwable;
                        statusCode = ex.code();
                        Log.e("error", ex.getLocalizedMessage());
                    } else if (throwable instanceof SocketTimeoutException) {
                        statusCode = 1000;
                    }
                    return Observable.empty();
                })
                .subscribe(new Observer<UserDetail>() {
                    @Override
                    public void onCompleted() {
                        pDialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("error", "" + e.getMessage());
                    }

                    @Override
                    public void onNext(UserDetail friendModel) {
                        statusCode = friendModel.getStatusCode();
                        if (statusCode == 200) {
                            Utility.displayToast(DashBoardActivity.this, friendModel.getMessage());
                            sessionManager.logoutUser();
                        }
                    }
                });

    }

    private AlertDialog AskOption() {
        AlertDialog myQuittingDialogBox = new AlertDialog.Builder(this)
                .setTitle("Delete")
                .setMessage("Do you want to Delete your profile")
                .setIcon(R.drawable.icon_delete)

                .setPositiveButton("Delete", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        DeleteProfile();
                    }
                })
                .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create();
        return myQuittingDialogBox;

    }


    private void displayDialog() {

        dialog = new Dialog(this);
        dialog.getWindow();
        dialog.setTitle("Upgrade");
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.simple_message);
        dialog.setCancelable(true);

        TextView txt_close = dialog.findViewById(R.id.txt_close);

        txt_close.setOnClickListener(view -> {
            dialog.cancel();
        });


        dialog.show();
        Window window = dialog.getWindow();
        assert window != null;
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

    }


}
