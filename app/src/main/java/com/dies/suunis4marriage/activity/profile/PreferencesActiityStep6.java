package com.dies.suunis4marriage.activity.profile;

import android.app.ProgressDialog;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.dies.suunis4marriage.R;
import com.dies.suunis4marriage.activity.DashBoardActivity;
import com.dies.suunis4marriage.apiservice.ApiService;
import com.dies.suunis4marriage.apiservice.ApiServiceCreator;
import com.dies.suunis4marriage.application.SessionManager;
import com.dies.suunis4marriage.application.Utility;
import com.dies.suunis4marriage.model.PreferenceDataModel;
import com.dies.suunis4marriage.model.ProfileBuild;

import java.net.SocketTimeoutException;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observable;
import rx.Observer;
import rx.schedulers.Schedulers;

public class PreferencesActiityStep6 extends AppCompatActivity {

    @BindView(R.id.btn_next)
    Button btn_next;

    @BindView(R.id.rg1)
    RadioGroup rg1;

    @BindView(R.id.rg2)
    RadioGroup rg2;

    @BindView(R.id.rg3)
    RadioGroup rg3;

    @BindView(R.id.rg4)
    RadioGroup rg4;

    @BindView(R.id.rg5)
    RadioGroup rg5;

    @BindView(R.id.rg6)
    RadioGroup rg6;

    @BindView(R.id.rg7)
    RadioGroup rg7;

    @BindView(R.id.rg8)
    RadioGroup rg8;

    @BindView(R.id.rg9)
    RadioGroup rg9;

    @BindView(R.id.rg10)
    RadioGroup rg10;

    @BindView(R.id.rd1)
    RadioButton rd1;

    @BindView(R.id.rd2)
    RadioButton rd2;

    @BindView(R.id.rd3)
    RadioButton rd3;

    @BindView(R.id.rd4)
    RadioButton rd4;

    @BindView(R.id.rd5)
    RadioButton rd5;

    @BindView(R.id.rd6)
    RadioButton rd6;

    @BindView(R.id.rd7)
    RadioButton rd7;

    @BindView(R.id.rd8)
    RadioButton rd8;

    @BindView(R.id.rd9)
    RadioButton rd9;

    @BindView(R.id.rd10)
    RadioButton rd10;

    @BindView(R.id.rd11)
    RadioButton rd11;

    @BindView(R.id.rd12)
    RadioButton rd12;

    @BindView(R.id.rd13)
    RadioButton rd13;

    @BindView(R.id.rd14)
    RadioButton rd14;

    @BindView(R.id.rd15)
    RadioButton rd15;

    @BindView(R.id.rd16)
    RadioButton rd16;

    @BindView(R.id.rd17)
    RadioButton rd17;

    @BindView(R.id.rd18)
    RadioButton rd18;

    @BindView(R.id.rd19)
    RadioButton rd19;

    @BindView(R.id.rd20)
    RadioButton rd20;

    @BindView(R.id.rd21)
    RadioButton rd21;

    @BindView(R.id.rd22)
    RadioButton rd22;

    @BindView(R.id.rd23)
    RadioButton rd23;

    @BindView(R.id.rd24)
    RadioButton rd24;

    @BindView(R.id.rd25)
    RadioButton rd25;

    @BindView(R.id.rd26)
    RadioButton rd26;

    @BindView(R.id.rd27)
    RadioButton rd27;

    @BindView(R.id.rd28)
    RadioButton rd28;

    @BindView(R.id.rd29)
    RadioButton rd29;

    @BindView(R.id.rd30)
    RadioButton rd30;



    RadioButton rb1,rb2,rb3,rb4,rb5,rb6,rb7,rb8,rb9,rb10;
    SessionManager sessionManager;
    ApiService apiservice;
    ProgressDialog pDialog;
    int statusCode;
    String rbtn1,rbtn2,rbtn3,rbtn4,rbtn5,rbtn6,rbtn7,rbtn8,rbtn9,rbtn10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preferences_actiity_step6);
        ButterKnife.bind(this);
        sessionManager=new SessionManager(this);
        apiservice = ApiServiceCreator.createService("latest");


        getPreferenceData();
        btn_next.setOnClickListener(view -> {

            int rgb1 = rg1.getCheckedRadioButtonId();
            rb1=(RadioButton) findViewById(rgb1);
            if (rb1!=null){
                rbtn1=rb1.getText().toString();
            }

            int rgb2 = rg2.getCheckedRadioButtonId();
            rb2=(RadioButton) findViewById(rgb2);
            if (rb2!=null){
                rbtn2=rb2.getText().toString();
            }

            int rgb3 = rg3.getCheckedRadioButtonId();
            rb3=(RadioButton) findViewById(rgb3);
            if (rb3!=null){
                rbtn3=rb3.getText().toString();
            }

            int rgb4 = rg4.getCheckedRadioButtonId();
            rb4=(RadioButton) findViewById(rgb4);
            if (rb4!=null){
                rbtn4=rb4.getText().toString();
            }

            int rgb5 = rg5.getCheckedRadioButtonId();
            rb5=(RadioButton) findViewById(rgb5);

            if (rb5!=null){
                rbtn5=rb5.getText().toString();
            }
            int rgb6 = rg6.getCheckedRadioButtonId();
            rb6=(RadioButton) findViewById(rgb6);

            if (rb6!=null){
                rbtn6=rb6.getText().toString();
            }

            int rgb7 = rg7.getCheckedRadioButtonId();
            rb7=(RadioButton) findViewById(rgb7);

            if (rb7!=null){
                rbtn7=rb7.getText().toString();
            }

            int rgb8 = rg8.getCheckedRadioButtonId();
            rb8=(RadioButton) findViewById(rgb8);

            if (rb8!=null){
                rbtn8=rb8.getText().toString();
            }
            int rgb9 = rg9.getCheckedRadioButtonId();
            rb9=(RadioButton) findViewById(rgb9);


            if (rb9!=null){
                rbtn9=rb9.getText().toString();
            }

            int rgb10 = rg10.getCheckedRadioButtonId();
            rb10=(RadioButton) findViewById(rgb10);
            if (rb10!=null){
                rbtn10=rb10.getText().toString();
            }


            if (rbtn1==null) {
                Utility.displayToast(this, "All the Field Are Mandatory");
            } else if (rbtn2==null) {
                Utility.displayToast(this, "All the Field Are Mandatory");
            }  if (rbtn3==null) {
                Utility.displayToast(this, "All the Field Are Mandatory");
            } else if (rbtn4==null) {
                Utility.displayToast(this, "All the Field Are Mandatory");
            }else if (rbtn5==null){
                Utility.displayToast(this, "All the Field Are Mandatory");
            }else if (rbtn6==null){
                Utility.displayToast(this, "All the Field Are Mandatory");
            }else if (rbtn7==null){
                Utility.displayToast(this, "All the Field Are Mandatory");
            }else if (rbtn8==null){
                Utility.displayToast(this, "All the Field Are Mandatory");
            }else if (rbtn9==null){
                Utility.displayToast(this, "All the Field Are Mandatory");
            }else if (rbtn10==null){
                Utility.displayToast(this, "All the Field Are Mandatory");
            }else{
                PrefencesStep6();

            }


        });
    }

    public void PrefencesStep6(){

        pDialog = new ProgressDialog(this);
        pDialog.setTitle("Checking Data");
        pDialog.setMessage("Please Wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        Observable<ProfileBuild> responseObservable = apiservice.PrefencesStep6(
                rbtn1,
                rbtn2,
                rbtn3,
                rbtn4,
                rbtn5,
                rbtn6,
                rbtn7,
                rbtn8,
                rbtn9,
                rbtn10,
                sessionManager.getKeyId());

        responseObservable.subscribeOn(Schedulers.newThread())
                .observeOn(rx.android.schedulers.AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> {
                    if (throwable instanceof retrofit2.HttpException) {
                        retrofit2.HttpException ex = (retrofit2.HttpException) throwable;
                        statusCode = ex.code();
                        Log.e("error", ex.getLocalizedMessage());
                    } else if (throwable instanceof SocketTimeoutException) {
                        statusCode = 1000;
                    }
                    return Observable.empty();
                })
                .subscribe(new Observer<ProfileBuild>() {
                    @Override
                    public void onCompleted() {
                        pDialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("error", "" + e.getMessage());
                    }

                    @Override
                    public void onNext(ProfileBuild profileBuild) {
                        statusCode = profileBuild.getStatusCode();
                        if (statusCode == 200) {
                            Utility.displayToast(PreferencesActiityStep6.this,profileBuild.getMessage());
                            Intent intent=new Intent(PreferencesActiityStep6.this, DashBoardActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();
                        }else {
                            Intent intent=new Intent(PreferencesActiityStep6.this, DashBoardActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();
                        }
                    }
                });

    }

    public void getPreferenceData(){

        pDialog = new ProgressDialog(this);
        pDialog.setTitle("Checking Data");
        pDialog.setMessage("Please Wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        Observable<PreferenceDataModel> responseObservable = apiservice.getPreferenceData(sessionManager.getKeyId());

        responseObservable.subscribeOn(Schedulers.newThread())
                .observeOn(rx.android.schedulers.AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> {
                    if (throwable instanceof retrofit2.HttpException) {
                        retrofit2.HttpException ex = (retrofit2.HttpException) throwable;
                        statusCode = ex.code();
                        Log.e("error", ex.getLocalizedMessage());
                    } else if (throwable instanceof SocketTimeoutException) {
                        statusCode = 1000;
                    }
                    return Observable.empty();
                })
                .subscribe(new Observer<PreferenceDataModel>() {
                    @Override
                    public void onCompleted() {
                        pDialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("error", "" + e.getMessage());
                    }

                    @Override
                    public void onNext(PreferenceDataModel preferenceDataModel) {
                        statusCode = preferenceDataModel.getStatusCode();
                        if (statusCode == 200) {

                            if (preferenceDataModel.getData().get(0).getCityformarriage()!=null){

                                if (preferenceDataModel.getData().get(0).getCityformarriage().equals("Yes")){
                                    rd1.setChecked(true);
                                }else if (preferenceDataModel.getData().get(0).getCityformarriage().equals("No")){
                                    rd2.setChecked(true);
                                }else if (preferenceDataModel.getData().get(0).getCityformarriage().equals("Maybe")){
                                    rd3.setChecked(true);
                                }
                            }


                            if (preferenceDataModel.getData().get(0).getDifferentethnicity()!=null){
                                if (preferenceDataModel.getData().get(0).getDifferentethnicity().equals("Yes")){
                                    rd4.setChecked(true);
                                }else if (preferenceDataModel.getData().get(0).getDifferentethnicity().equals("No")){
                                    rd5.setChecked(true);
                                }else if (preferenceDataModel.getData().get(0).getDifferentethnicity().equals("Maybe")){
                                    rd6.setChecked(true);
                                }
                            }


                            if (preferenceDataModel.getData().get(0).getDifferentcaste()!=null){

                                if (preferenceDataModel.getData().get(0).getDifferentcaste().equals("Yes")){
                                    rd7.setChecked(true);
                                }else if (preferenceDataModel.getData().get(0).getDifferentcaste().equals("No")){
                                    rd8.setChecked(true);
                                }else if (preferenceDataModel.getData().get(0).getDifferentcaste().equals("Maybe")){
                                    rd9.setChecked(true);
                                }
                            }



                            if (preferenceDataModel.getData().get(0).getWhohasbeendivorced()!=null){

                                if (preferenceDataModel.getData().get(0).getWhohasbeendivorced().equals("Yes")){
                                    rd10.setChecked(true);
                                }else if (preferenceDataModel.getData().get(0).getWhohasbeendivorced().equals("No")){
                                    rd11.setChecked(true);
                                }else if (preferenceDataModel.getData().get(0).getWhohasbeendivorced().equals("Maybe")){
                                    rd12.setChecked(true);
                                }
                            }


                            if (preferenceDataModel.getData().get(0).getWhohaschildren()!=null){

                                if (preferenceDataModel.getData().get(0).getWhohaschildren().equals("Yes")){
                                    rd13.setChecked(true);
                                }else if (preferenceDataModel.getData().get(0).getWhohaschildren().equals("No")){
                                    rd14.setChecked(true);
                                }else if (preferenceDataModel.getData().get(0).getWhohaschildren().equals("Maybe")){
                                    rd15.setChecked(true);
                                }
                            }


                            if (preferenceDataModel.getData().get(0).getMarrysomeoneyounger()!=null){

                                if (preferenceDataModel.getData().get(0).getMarrysomeoneyounger().equals("Yes")){
                                    rd16.setChecked(true);
                                }else if (preferenceDataModel.getData().get(0).getMarrysomeoneyounger().equals("No")){
                                    rd17.setChecked(true);
                                }else if (preferenceDataModel.getData().get(0).getMarrysomeoneyounger().equals("Maybe")){
                                    rd18.setChecked(true);
                                }
                            }


                            if (preferenceDataModel.getData().get(0).getMarrysomeoneolder()!=null){

                                if (preferenceDataModel.getData().get(0).getMarrysomeoneolder().equals("Yes")){
                                    rd19.setChecked(true);
                                }else if (preferenceDataModel.getData().get(0).getMarrysomeoneolder().equals("No")){
                                    rd20.setChecked(true);
                                }else if (preferenceDataModel.getData().get(0).getMarrysomeoneolder().equals("Maybe")){
                                    rd21.setChecked(true);
                                }
                            }


                            if (preferenceDataModel.getData().get(0).getSamelevelofeducation()!=null){

                                if (preferenceDataModel.getData().get(0).getSamelevelofeducation().equals("Yes")){
                                    rd22.setChecked(true);
                                }else if (preferenceDataModel.getData().get(0).getSamelevelofeducation().equals("No")){
                                    rd23.setChecked(true);
                                }else if (preferenceDataModel.getData().get(0).getSamelevelofeducation().equals("Maybe")){
                                    rd24.setChecked(true);
                                }
                            }



                            if (preferenceDataModel.getData().get(0).getReverttoIslam()!=null){

                                if (preferenceDataModel.getData().get(0).getReverttoIslam().equals("Yes")){
                                    rd25.setChecked(true);
                                }else if (preferenceDataModel.getData().get(0).getReverttoIslam().equals("No")){
                                    rd26.setChecked(true);
                                }else if (preferenceDataModel.getData().get(0).getReverttoIslam().equals("Maybe")){
                                    rd27.setChecked(true);
                                }
                            }


                            if (preferenceDataModel.getData().get(0).getDifferentnationality()!=null){

                                if (preferenceDataModel.getData().get(0).getDifferentnationality().equals("Yes")){
                                    rd28.setChecked(true);
                                }else if (preferenceDataModel.getData().get(0).getDifferentnationality().equals("No")){
                                    rd29.setChecked(true);
                                }else if (preferenceDataModel.getData().get(0).getDifferentnationality().equals("Maybe")){
                                    rd30.setChecked(true);
                                }
                            }



                        }
                    }
                });

    }

}
