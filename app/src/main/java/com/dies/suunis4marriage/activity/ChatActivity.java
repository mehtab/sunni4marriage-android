package com.dies.suunis4marriage.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.widget.ImageView;


import com.dies.suunis4marriage.R;
import com.dies.suunis4marriage.adapter.ChatFriendAdapter;
import com.dies.suunis4marriage.apiservice.ApiService;
import com.dies.suunis4marriage.apiservice.ApiServiceCreator;
import com.dies.suunis4marriage.application.SessionManager;
import com.dies.suunis4marriage.model.FriendModel;

import java.net.SocketTimeoutException;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observable;
import rx.Observer;
import rx.schedulers.Schedulers;

public class ChatActivity extends AppCompatActivity {

    int statusCode;
    private ProgressDialog pDialog;
    RecyclerView.LayoutManager layoutManager;
    SessionManager sessionManager;
    private ApiService apiservice;
    ChatFriendAdapter friendAdapter;
    @BindView(R.id.rcv_firend)
    RecyclerView recyclerView;
    @BindView(R.id.back_icon)
    ImageView back_icon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        ButterKnife.bind(this);

        sessionManager = new SessionManager(this);
        apiservice = ApiServiceCreator.createService("latest");
        back_icon.setOnClickListener(view -> {
            finish();
        });
//        back_icon.setVisibility(View.VISIBLE);
//        back_icon.setOnClickListener(view -> {
//            finish();
//        });
//        branding_icon.setVisibility(View.GONE);
//        notify_icon.setVisibility(View.GONE);
//        toolbar_Title.setText("Chat");
        FriendData();
    }

    public void FriendData(){
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        pDialog = new ProgressDialog(this);
        pDialog.setTitle("Checking Data");
        pDialog.setMessage("Please Wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        Observable<FriendModel> responseObservable = apiservice.getConnectchatData(sessionManager.getKeyId(),
                "friend_list");

        responseObservable.subscribeOn(Schedulers.newThread())
                .observeOn(rx.android.schedulers.AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> {
                    if (throwable instanceof retrofit2.HttpException) {
                        retrofit2.HttpException ex = (retrofit2.HttpException) throwable;
                        statusCode = ex.code();
                        Log.e("error", ex.getLocalizedMessage());
                    } else if (throwable instanceof SocketTimeoutException) {
                        statusCode = 1000;
                    }
                    return Observable.empty();
                })
                .subscribe(new Observer<FriendModel>() {
                    @Override
                    public void onCompleted() {
                        pDialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("error", "" + e.getMessage());
                    }

                    @Override
                    public void onNext(FriendModel friendModel) {
                        statusCode = friendModel.getStatusCode();
                        if (statusCode == 200) {
                            friendAdapter=new ChatFriendAdapter(ChatActivity.this,friendModel.getData());
                            recyclerView.setAdapter(friendAdapter);
                        }
                    }
                });

    }
}
