package com.dies.suunis4marriage.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import androidx.annotation.RequiresApi;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.dies.suunis4marriage.R;
import com.dies.suunis4marriage.adapter.ProfileAdapter;
import com.dies.suunis4marriage.apiservice.ApiService;
import com.dies.suunis4marriage.apiservice.ApiServiceCreator;
import com.dies.suunis4marriage.application.SessionManager;
import com.dies.suunis4marriage.model.ProfileSearch;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observable;
import rx.Observer;
import rx.schedulers.Schedulers;

public class SearchProfile extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.rcv_search)
    RecyclerView rcv_search;
    //    @BindView(R.id.toolbar)
//    Toolbar toolbar;
    @BindView(R.id.toolbar_Title)
    TextView toolbar_Title;
    @BindView(R.id.back_icon)
    ImageView back_icon;

    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipe_refresh_layout;
    @BindView(R.id.dummy_text)
    TextView dummy_text;

    SessionManager sessionManager;
    ApiService apiservice;
    ProgressDialog pDialog;
    RecyclerView.LayoutManager layoutManager;
    int statusCode;
    ProfileAdapter profileAdapter;
    List<ProfileSearch.Data> datalist = new ArrayList<>();

    boolean check = false;
    SharedPreferences sharedPreferences;
    static SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_profile);
        ButterKnife.bind(this);


        back_icon.setOnClickListener(view -> {
            finish();
        });
        sessionManager = new SessionManager(this);
        apiservice = ApiServiceCreator.createService("latest");
        toolbar_Title.setText("Search Result");
        String data = getIntent().getStringExtra("Search");
        swipe_refresh_layout.setOnRefreshListener(this);

        swipe_refresh_layout.post(new Runnable() {
                                      @Override
                                      public void run() {
                                          swipe_refresh_layout.setRefreshing(true);

                                          if (data.equals("multiple_search")) {
                                              getSearchResult();

                                          } else if (data.equals("userid_search")) {
                                              String Search_id = getIntent().getStringExtra("Search_id");
                                              SearchByUderId(Search_id);
                                          }
                                      }
                                  }
        );


    }


    public void getSearchResult() {
        swipe_refresh_layout.setRefreshing(true);
        rcv_search.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        rcv_search.setLayoutManager(layoutManager);
        pDialog = new ProgressDialog(this);
        pDialog.setTitle("Checking Data");
        pDialog.setMessage("Please Wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        Observable<ProfileSearch> responseObservable = apiservice.getSearchableData(
                sessionManager.getEthenicity(),//
                sessionManager.getMinMaxValue(),
                sessionManager.getNationality(),//
                sessionManager.getMerital_status(),
                sessionManager.getIslamic(),
                sessionManager.getEducationLevel(),
                sessionManager.getKeyId(),
                sessionManager.getLocation(),
                sessionManager.getLanguage());

       /* Toast.makeText(this, sessionManager.getEthenicity()
                + "\n" + sessionManager.getMinMaxValue() + "\n" +
                sessionManager.getNationality() + "\n" +
                sessionManager.getIslamic(), Toast.LENGTH_SHORT).show();
*/
        responseObservable.subscribeOn(Schedulers.newThread())
                .observeOn(rx.android.schedulers.AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> {
                    if (throwable instanceof retrofit2.HttpException) {
                        retrofit2.HttpException ex = (retrofit2.HttpException) throwable;
                        statusCode = ex.code();
                        Log.e("error", ex.getLocalizedMessage());
                    } else if (throwable instanceof SocketTimeoutException) {
                        statusCode = 1000;
                    }
                    return Observable.empty();
                })
                .subscribe(new Observer<ProfileSearch>() {
                    @Override
                    public void onCompleted() {
                        pDialog.dismiss();
                        swipe_refresh_layout.setRefreshing(false);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("error", "" + e.getMessage());
                        swipe_refresh_layout.setRefreshing(false);
                    }

                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onNext(ProfileSearch profileSearch) {
                        statusCode = profileSearch.getStatusCode();
                        if (statusCode == 200) {
                            //datalist=profileSearch.getData();

                            dummy_text.setVisibility(View.GONE);
                            /*SearchResult(profileSearch.getData())*/
                          /*  for (int i = 0; i < profileSearch.getData().size(); i++) {
                                if (profileSearch.getData().get(i).getMaritalstatus() != null)
                                    Log.d("MarriedStatus is " + i, profileSearch.getData().get(i).getMaritalstatus());
                            }*/


                            sharedPreferences = getSharedPreferences("STORE_FILE_NAME", Context.MODE_PRIVATE);
                            editor = sharedPreferences.edit();
                            List<String> listfag = getList();
                            List<ProfileSearch.Data> mainList = new ArrayList<>();
                            mainList.addAll(profileSearch.getData());
                            if (getList().size() > 0) {

                                for (int i = 0; i < listfag.size(); i++) {
                                    for (int j = 0; j < profileSearch.getData().size(); j++) {
                                        if (listfag.get(i).equals(mainList.get(j).getUserId())) {
                                            mainList.remove(j);
                                            j--;
                                            break;
                                        }
                                    }
                                }

                                profileAdapter = new ProfileAdapter(SearchProfile.this, mainList, "search_friend", "");
                                Log.d("dta", profileSearch.getData().toString());
                                rcv_search.setAdapter(profileAdapter);
                                check = true;
                            } else {
                                profileAdapter = new ProfileAdapter(SearchProfile.this, profileSearch.getData(), "search_friend", "");
                                Log.d("dta", profileSearch.getData().toString());
                                rcv_search.setAdapter(profileAdapter);
                                check = true;
                            }



                        } else {
                            dummy_text.setVisibility(View.VISIBLE);
                        }
                    }
                });

    }

    public List<ProfileSearch.Data> SearchResult(List<ProfileSearch.Data> data) {
        List<ProfileSearch.Data> dataTwo = new ArrayList<>();
      /*  List<ProfileSearch.Data> result = profileSearch.getData().stream()
                .filter(a -> Objects.equals(a.getNationality().toLowerCase(), sessionManager.getNationality().toLowerCase()))

                .collect(Collectors.toList());*/

        for (int i = 0; i < data.size(); i++) {
            if (sessionManager.getEthenicity() == null &&
                    sessionManager.getNationality() == null &&
                    sessionManager.getMerital_status() == null &&
                    sessionManager.getLanguage() == null &&
                    sessionManager.getIslamic() == null) {

            }
            if (sessionManager.getEthenicity() != null) {
                if (sessionManager.getEthenicity().equals(data.get(i).getEthnicity())) {
                    dataTwo.add(data.get(i));
                    break;
                }
            }
            /**/
            if (sessionManager.getNationality() != null) {
                if (sessionManager.getNationality().equals(data.get(i).getNationality())) {
                    dataTwo.add(data.get(i));
                    break;
                }
            }
            /**/
            if (sessionManager.getMerital_status() != null) {
                if (sessionManager.getMerital_status().equals(data.get(i).getMaritalstatus())) {
                    dataTwo.add(data.get(i));
                    break;
                }
            }
            /**/
            if (sessionManager.getLanguage() != null) {
                if (sessionManager.getLanguage().equals(data.get(i).getLanguagesspeak())) {
                    dataTwo.add(data.get(i));
                    break;
                }
            }
            /**/
            if (sessionManager.getIslamic() != null) {
                if (sessionManager.getIslamic().equals(data.get(i).getIslamic())) {
                    dataTwo.add(data.get(i));
                    break;
                }
            }
            /**/

        }
        return dataTwo;
    }

    public void SearchByUderId(String search_id) {

        rcv_search.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        rcv_search.setLayoutManager(layoutManager);
        pDialog = new ProgressDialog(this);
        pDialog.setTitle("Checking Data");
        pDialog.setMessage("Please Wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        Observable<ProfileSearch> responseObservable = apiservice.SearchByUserId(search_id,
                sessionManager.getKeyId());

        responseObservable.subscribeOn(Schedulers.newThread())
                .observeOn(rx.android.schedulers.AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> {
                    if (throwable instanceof retrofit2.HttpException) {
                        retrofit2.HttpException ex = (retrofit2.HttpException) throwable;
                        statusCode = ex.code();
                        Log.e("error", ex.getLocalizedMessage());
                    } else if (throwable instanceof SocketTimeoutException) {
                        statusCode = 1000;
                    }
                    return Observable.empty();
                })
                .subscribe(new Observer<ProfileSearch>() {
                    @Override
                    public void onCompleted() {
                        pDialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("error", "" + e.getMessage());
                    }

                    @Override
                    public void onNext(ProfileSearch profileSearch) {
                        statusCode = profileSearch.getStatusCode();
                        if (statusCode == 200) {
                            datalist = profileSearch.getData();
                            profileAdapter = new ProfileAdapter(SearchProfile.this, profileSearch.getData(), "search_friend", "search_by_id");
                            rcv_search.setAdapter(profileAdapter);
                        }
                    }
                });


    }


    @Override
    protected void onResume() {
        super.onResume();
        if (check) {
            profileAdapter.notifyDataSetChanged();
        }

    }

    @Override
    public void onRefresh() {
        getSearchResult();
    }

//    @Override
//    protected void onRestart() {
//        super.onRestart();
//        profileAdapter.notifyDataSetChanged();
////        getSearchResult();
//    }

    public <T> void setList(String key, List<T> list) {
        Gson gson = new Gson();
        String json = gson.toJson(list);

        set(key, json);
    }

    public static void set(String key, String value) {
        editor.putString(key, value);
        editor.commit();
    }

    public List<String> getList() {
        List<String> arrayItems = new ArrayList<>();
        String serializedObject = sharedPreferences.getString("KEY_flag", null);
        if (serializedObject != null) {
            Gson gson = new Gson();
            Type type = new TypeToken<List<String>>() {
            }.getType();
            arrayItems = gson.fromJson(serializedObject, type);
        }
        return arrayItems;
    }
}
