package com.dies.suunis4marriage.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.dies.suunis4marriage.R;
import com.dies.suunis4marriage.apiservice.ApiConstants;
import com.dies.suunis4marriage.apiservice.ApiService;
import com.dies.suunis4marriage.apiservice.ApiServiceCreator;
import com.dies.suunis4marriage.application.SessionManager;
import com.dies.suunis4marriage.application.Utility;
import com.dies.suunis4marriage.model.ProfileBuild;
import com.dies.suunis4marriage.model.UserDataResponse;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.net.SocketTimeoutException;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import rx.Observable;
import rx.Observer;
import rx.schedulers.Schedulers;

public class EditProfileImage extends AppCompatActivity {


    @BindView(R.id.img1)
    ImageView img1;
    @BindView(R.id.img2)
    ImageView img2;
    @BindView(R.id.img3)
    ImageView img3;
    @BindView(R.id.img4)
    ImageView img4;
    @BindView(R.id.back_icon)
    ImageView back_icon;

    @BindView(R.id.img_delete)
    ImageView img_delete;

    @BindView(R.id.img1_delete)
    ImageView img1_delete;

    @BindView(R.id.img2_delete)
    ImageView img2_delete;

    @BindView(R.id.img3_delete)
    ImageView img3_delete;


    String file_path_1 = "", file_path_2 = "", file_path_3 = "", file_path_4 = "";
    String mediaPath, ImageType;


    SessionManager sessionManager;
    ApiService apiservice;
    ProgressDialog pDialog;
    RecyclerView.LayoutManager layoutManager;
    int statusCode;

    String image="",image1="",image2="",image3="";

    String imgg1 = "", imgg2 = "", imgg3 = "", imgg4 = "";
    Uri uri, selectedImage;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile_image);
        ButterKnife.bind(this);
        sessionManager = new SessionManager(this);
        apiservice = ApiServiceCreator.createService("latest");
        back_icon.setOnClickListener(view -> {
            finish();
        });

        img1.setOnClickListener(view -> {
            if (!imgg1.equals("")) {
                Intent intent = new Intent(this, ShowSlidding_image.class);
                intent.putExtra("img1", imgg1);
                startActivity(intent);
            }
        });
        img2.setOnClickListener(view -> {
            if (!imgg2.equals("")) {
                Intent intent = new Intent(this, ShowSlidding_image.class);
                intent.putExtra("img2", imgg2);
                startActivity(intent);
            }

        });
        img3.setOnClickListener(view -> {
            if (!imgg3.equals("")) {
                Intent intent = new Intent(this, ShowSlidding_image.class);
                intent.putExtra("img3", imgg3);
                startActivity(intent);
            }
        });
        img4.setOnClickListener(view -> {
            if (!imgg4.equals("")) {
                Intent intent = new Intent(this, ShowSlidding_image.class);
                intent.putExtra("img4", imgg4);
                startActivity(intent);
            }
        });


        img_delete.setOnClickListener(view -> {
            image="image11";
            image1="";
            image2="";
            image3="";
            RemoveImage();
        });

        img1_delete.setOnClickListener(view -> {
            image1="image12";
            image2="";
            image="";
            image3="";
            RemoveImage();
        });

        img2_delete.setOnClickListener(view -> {
            image="";
            image3="";
            image1="";
            image2="image13";
            RemoveImage();
        });

        img3_delete.setOnClickListener(view -> {
            image="";
            image1="";
            image2="";
            image3="image14";
            RemoveImage();
        });

        getUserDetail();
    }


    @OnClick(R.id.img1_upld)
    public void img_1() {
        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, 1);
    }

    @OnClick(R.id.img2_upld)
    public void img_2() {
        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, 2);
    }

    @OnClick(R.id.img3_upld)
    public void img_3() {
        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, 3);
    }

    @OnClick(R.id.img4_upld)
    public void img_4() {
        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, 4);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {
                Uri _uri = data.getData();
                ImageCropFunctionCustom111(_uri);
            } else if (requestCode == 2) {
                Uri _uri = data.getData();
                ImageCropFunctionCustom222(_uri);
            } else if (requestCode == 3) {
                Uri _uri = data.getData();
                ImageCropFunctionCustom333(_uri);
            } else if (requestCode == 4) {
                Uri _uri = data.getData();
                ImageCropFunctionCustom444(_uri);
            } else if (requestCode == 111 && resultCode == RESULT_OK) {
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                Uri _uri = result.getUri();
                if (_uri != null && "content".equals(_uri.getScheme())) {
                    Cursor cursor;
                    cursor = getContentResolver().query(_uri, new String[]{android.provider.MediaStore.Images.Media.DATA}, null, null, null);
                    if (cursor != null) {
                        cursor.moveToFirst();
                        mediaPath = cursor.getString(0);
                        if (mediaPath != null) {
                            ImageType = "image1";
                            uploadFile();
                            img1.setImageBitmap(Utility.getResizedBitmap(BitmapFactory.decodeFile(mediaPath)));

                        }
                        cursor.close();
                    }
                } else {
                    if (_uri != null) {
                        mediaPath = _uri.getPath();
                        ImageType = "image1";
                        uploadFile();
                        img1.setImageBitmap(Utility.getResizedBitmap(BitmapFactory.decodeFile(mediaPath)));
                        Toast.makeText(this, "no upload image", Toast.LENGTH_SHORT).show();
                    }
                }

            } else if (requestCode == 222 && resultCode == RESULT_OK) {
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                Uri _uri = result.getUri();
                if (_uri != null && "content".equals(_uri.getScheme())) {
                    Cursor cursor;
                    cursor = getContentResolver().query(_uri, new String[]{android.provider.MediaStore.Images.Media.DATA}, null, null, null);
                    if (cursor != null) {
                        cursor.moveToFirst();
                        mediaPath = cursor.getString(0);
                        if (mediaPath != null) {
                            ImageType = "image2";
                            uploadFile();
                            img2.setImageBitmap(Utility.getResizedBitmap(BitmapFactory.decodeFile(mediaPath)));
                        }
                        cursor.close();
                    }
                } else {
                    if (_uri != null) {
                        mediaPath = _uri.getPath();
                        ImageType = "image2";
                        uploadFile();
                        img2.setImageBitmap(Utility.getResizedBitmap(BitmapFactory.decodeFile(mediaPath)));
                        Toast.makeText(this, "no upload image", Toast.LENGTH_SHORT).show();
                    }
                }

            } else if (requestCode == 333 && resultCode == RESULT_OK) {
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                Uri _uri = result.getUri();
                if (_uri != null && "content".equals(_uri.getScheme())) {
                    Cursor cursor;
                    cursor = getContentResolver().query(_uri, new String[]{android.provider.MediaStore.Images.Media.DATA}, null, null, null);
                    if (cursor != null) {
                        cursor.moveToFirst();
                        mediaPath = cursor.getString(0);
                        if (mediaPath != null) {
                            ImageType = "image3";
                            uploadFile();
                            img3.setImageBitmap(Utility.getResizedBitmap(BitmapFactory.decodeFile(mediaPath)));
                        }
                        cursor.close();
                    }
                } else {
                    if (_uri != null) {
                        mediaPath = _uri.getPath();
                        ImageType = "image3";
                        uploadFile();
                        img3.setImageBitmap(Utility.getResizedBitmap(BitmapFactory.decodeFile(mediaPath)));
                        Toast.makeText(this, "no upload image", Toast.LENGTH_SHORT).show();
                    }
                }

            } else if (requestCode == 444 && resultCode == RESULT_OK) {
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                Uri _uri = result.getUri();
                if (_uri != null && "content".equals(_uri.getScheme())) {
                    Cursor cursor;
                    cursor = getContentResolver().query(_uri, new String[]{android.provider.MediaStore.Images.Media.DATA}, null, null, null);
                    if (cursor != null) {
                        cursor.moveToFirst();
                        mediaPath = cursor.getString(0);
                        if (mediaPath != null) {
                            ImageType = "image4";
                            uploadFile();
                            img4.setImageBitmap(Utility.getResizedBitmap(BitmapFactory.decodeFile(mediaPath)));
                        }
                        cursor.close();
                    }
                } else {
                    if (_uri != null) {
                        mediaPath = _uri.getPath();
                        ImageType = "image4";
                        uploadFile();
                        img4.setImageBitmap(Utility.getResizedBitmap(BitmapFactory.decodeFile(mediaPath)));
                        Toast.makeText(this, "no upload image", Toast.LENGTH_SHORT).show();
                    }
                }

            }
        }
    }


    public void ImageCropFunctionCustom111(Uri selectedImage) {
        Intent intent = CropImage.activity(selectedImage)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setFixAspectRatio(true)
                .setAspectRatio(1, 1)
                .setAutoZoomEnabled(false)
                .setFixAspectRatio(true)
                .getIntent(this);

        startActivityForResult(intent, 111);
    }

    public void ImageCropFunctionCustom222(Uri selectedImage) {
        Intent intent = CropImage.activity(selectedImage)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setFixAspectRatio(true)
                .setAspectRatio(1, 1)
                .setAutoZoomEnabled(false)
                .setFixAspectRatio(true)
                .getIntent(this);
        startActivityForResult(intent, 222);
    }

    public void ImageCropFunctionCustom333(Uri selectedImage) {
        Intent intent = CropImage.activity(selectedImage)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setFixAspectRatio(true)
                .setAspectRatio(1, 1)
                .setAutoZoomEnabled(false)
                .setFixAspectRatio(true)
                .getIntent(this);
        startActivityForResult(intent, 333);
    }

    public void ImageCropFunctionCustom444(Uri selectedImage) {
        Intent intent = CropImage.activity(selectedImage)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setFixAspectRatio(true)
                .setAspectRatio(1, 1)
                .setAutoZoomEnabled(false)
                .setFixAspectRatio(true)
                .getIntent(this);
        startActivityForResult(intent, 444);
    }


    private void uploadFile() {

        final ProgressDialog pDialog;
        pDialog = new ProgressDialog(this);
        pDialog.setTitle("Uploading profile image");
        pDialog.setMessage("Please Wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(true);
        pDialog.show();

        // Map is used to multipart the file using okhttp3.RequestBody
        Map<String, RequestBody> map = new HashMap<>();
        if (mediaPath != null) {
            Compressor compressedImageFile = new Compressor(this);
            compressedImageFile.setQuality(60);
            try {
                File file = new File(mediaPath);
                File compressfile = compressedImageFile.compressToFile(file);
                //File file = new File(mediaPath);
                RequestBody requestBody = RequestBody.create(MediaType.parse("*/*"), compressfile);
                RequestBody imagetype = RequestBody.create(MediaType.parse("*/*"), ImageType);
                RequestBody user_id = RequestBody.create(MediaType.parse("text/plain"), sessionManager.getKeyId());
                map.put("file\"; filename=\"" + file.getName() + "\"", requestBody);
                map.put("user_id", user_id);
                map.put("image_type", imagetype);
                Observable<ProfileBuild> responseObservable = apiservice.upload_multi_profile_image(sessionManager.getKeyToken(), map);

                responseObservable.subscribeOn(Schedulers.newThread())
                        .observeOn(rx.android.schedulers.AndroidSchedulers.mainThread())
                        .onErrorResumeNext(throwable -> {
                            if (throwable instanceof retrofit2.HttpException) {
                                retrofit2.HttpException ex = (retrofit2.HttpException) throwable;
                                statusCode = ex.code();
                                Log.e("error", ex.getLocalizedMessage());
                            } else if (throwable instanceof SocketTimeoutException) {
                                statusCode = 1000;
                            }
                            return Observable.empty();
                        })
                        .subscribe(new Observer<ProfileBuild>() {
                            @Override
                            public void onCompleted() {
                                pDialog.dismiss();
                                getUserDetail();
                            }

                            @Override
                            public void onError(Throwable e) {
                                Log.e("error", "" + e.getMessage());
                            }

                            @Override
                            public void onNext(ProfileBuild loginResponse) {
                                statusCode = loginResponse.getStatusCode();
                                if (statusCode == 200) {
                                    //getPost();
                                    Toast.makeText(getApplicationContext(), "Your profile image successfully updated", Toast.LENGTH_SHORT).show();
//                                if (!loginResponse.getProfile_image().equals("")) {
//                                    sessionManager.setKeyImage(loginResponse.getProfile_image());
//                                }
                                }

                            }
                        });

            } catch (Exception e) {

            }


        }
    }


    private void getUserDetail() {

        pDialog = new ProgressDialog(EditProfileImage.this);
        pDialog.setTitle("Checking Data");
        pDialog.setMessage("Please Wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        Observable<UserDataResponse> responseObservable = apiservice.getUserDetails(sessionManager.getKeyEmail());
        responseObservable.subscribeOn(Schedulers.newThread())
                .observeOn(rx.android.schedulers.AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> {
                    if (throwable instanceof retrofit2.HttpException) {
                        retrofit2.HttpException ex = (retrofit2.HttpException) throwable;
                        statusCode = ex.code();
                        Log.e("error", ex.getLocalizedMessage());
                    } else if (throwable instanceof SocketTimeoutException) {
                        statusCode = 1000;
                    }
                    return Observable.empty();
                })
                .subscribe(new Observer<UserDataResponse>() {
                    @Override
                    public void onCompleted() {
                        pDialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("error", e.getLocalizedMessage());
                    }

                    @Override
                    public void onNext(UserDataResponse userDataResponse) {
                        statusCode = userDataResponse.getStatusCode();
                        if (statusCode == 200) {

                            if (userDataResponse.getData().get(0).getImage11() != null) {


                                imgg1 = userDataResponse.getData().get(0).getImage11();
                                sessionManager.setKeyImage(userDataResponse.getData().get(0).getImage11());
                                if (sessionManager.getUserData().get(0).getGender().equals("MALE")) {
                                    Picasso.with(EditProfileImage.this).load(ApiConstants.IMAGE_URL + userDataResponse.getData().get(0).getImage11())
                                            .memoryPolicy(MemoryPolicy.NO_CACHE)
                                            .networkPolicy(NetworkPolicy.NO_CACHE)
                                            .fit()
                                            .placeholder(R.drawable.edit_image_back)
                                            .into(img1);
                                } else {
                                    Picasso.with(EditProfileImage.this).load(ApiConstants.IMAGE_URL + userDataResponse.getData().get(0).getImage11())
                                            .memoryPolicy(MemoryPolicy.NO_CACHE)
                                            .networkPolicy(NetworkPolicy.NO_CACHE)
                                            .fit()
                                            .placeholder(R.drawable.edit_image_back)
                                            .into(img1);
                                }


                            }
                            if (userDataResponse.getData().get(0).getImage12() != null) {

                                imgg2 = userDataResponse.getData().get(0).getImage12();
                                if (sessionManager.getUserData().get(0).getGender().equals("MALE")) {

                                    Picasso.with(EditProfileImage.this).load(ApiConstants.IMAGE_URL + userDataResponse.getData().get(0).getImage12())
                                            .memoryPolicy(MemoryPolicy.NO_CACHE)
                                            .networkPolicy(NetworkPolicy.NO_CACHE)
                                            .fit()
                                            .placeholder(R.drawable.edit_image_back)
                                            .into(img2);
                                } else {
                                    Picasso.with(EditProfileImage.this).load(ApiConstants.IMAGE_URL + userDataResponse.getData().get(0).getImage12())
                                            .memoryPolicy(MemoryPolicy.NO_CACHE)
                                            .networkPolicy(NetworkPolicy.NO_CACHE)
                                            .fit()
                                            .placeholder(R.drawable.edit_image_back)
                                            .into(img2);
                                }
                            }
                            if (userDataResponse.getData().get(0).getImage13() != null) {

                                imgg3 = userDataResponse.getData().get(0).getImage13();
                                if (sessionManager.getUserData().get(0).getGender().equals("MALE")) {

                                    Picasso.with(EditProfileImage.this).load(ApiConstants.IMAGE_URL + userDataResponse.getData().get(0).getImage13())
                                            .memoryPolicy(MemoryPolicy.NO_CACHE)
                                            .networkPolicy(NetworkPolicy.NO_CACHE)
                                            .fit()
                                            .placeholder(R.drawable.edit_image_back)
                                            .into(img3);
                                } else {
                                    Picasso.with(EditProfileImage.this).load(ApiConstants.IMAGE_URL + userDataResponse.getData().get(0).getImage13())
                                            .memoryPolicy(MemoryPolicy.NO_CACHE)
                                            .networkPolicy(NetworkPolicy.NO_CACHE)
                                            .fit()
                                            .placeholder(R.drawable.edit_image_back)
                                            .into(img3);
                                }
                            }
                            if (userDataResponse.getData().get(0).getImage14() != null) {
                                imgg4 = userDataResponse.getData().get(0).getImage14();

                                if (sessionManager.getUserData().get(0).getGender().equals("MALE")) {
                                    Picasso.with(EditProfileImage.this).load(ApiConstants.IMAGE_URL + userDataResponse.getData().get(0).getImage14())
                                            .memoryPolicy(MemoryPolicy.NO_CACHE)
                                            .networkPolicy(NetworkPolicy.NO_CACHE)
                                            .fit()
                                            .placeholder(R.drawable.edit_image_back)
                                            .into(img4);
                                } else {
                                    Picasso.with(EditProfileImage.this).load(ApiConstants.IMAGE_URL + userDataResponse.getData().get(0).getImage14())
                                            .memoryPolicy(MemoryPolicy.NO_CACHE)
                                            .networkPolicy(NetworkPolicy.NO_CACHE)
                                            .fit()
                                            .placeholder(R.drawable.edit_image_back)
                                            .into(img4);
                                }
                            }

                        }
                    }
                });
    }


    public void RemoveImage() {
        pDialog = new ProgressDialog(EditProfileImage.this);
        pDialog.setTitle("Checking Data");
        pDialog.setMessage("Please Wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        Observable<UserDataResponse> responseObservable = apiservice.RemoveImage(sessionManager.getKeyId(),image,image1,image2,image3);
        responseObservable.subscribeOn(Schedulers.newThread())
                .observeOn(rx.android.schedulers.AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> {
                    if (throwable instanceof retrofit2.HttpException) {
                        retrofit2.HttpException ex = (retrofit2.HttpException) throwable;
                        statusCode = ex.code();
                        Log.e("error", ex.getLocalizedMessage());
                    } else if (throwable instanceof SocketTimeoutException) {
                        statusCode = 1000;
                    }
                    return Observable.empty();
                })
                .subscribe(new Observer<UserDataResponse>() {
                    @Override
                    public void onCompleted() {
                        pDialog.dismiss();
                        getUserDetail();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("error", e.getLocalizedMessage());
                    }

                    @Override
                    public void onNext(UserDataResponse userDataResponse) {
                        statusCode = userDataResponse.getStatusCode();
                        if (statusCode == 200) {
                            Utility.displayToast(EditProfileImage.this,userDataResponse.getMessage());
                        }else {
                            Utility.displayToast(EditProfileImage.this,userDataResponse.getMessage());
                        }
                    }

                });
    }
}
