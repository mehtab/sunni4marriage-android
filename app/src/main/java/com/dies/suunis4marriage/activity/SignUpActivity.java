package com.dies.suunis4marriage.activity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.dies.suunis4marriage.R;
import com.dies.suunis4marriage.apiservice.ApiService;
import com.dies.suunis4marriage.apiservice.ApiServiceCreator;
import com.dies.suunis4marriage.apiservice.DateInputMask;
import com.dies.suunis4marriage.application.SessionManager;
import com.dies.suunis4marriage.application.Utility;
import com.dies.suunis4marriage.model.UserDetail;

import java.net.SocketTimeoutException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observable;
import rx.Observer;
import rx.schedulers.Schedulers;

public class SignUpActivity extends AppCompatActivity {


    @BindView(R.id.et_email)
    EditText et_email;
    @BindView(R.id.et_pass)
    EditText et_pass;
    @BindView(R.id.txt_login)
    TextView txt_login;
    @BindView(R.id.et_fname)
    EditText et_fname;
    @BindView(R.id.et_lname)
    EditText et_lname;
    @BindView(R.id.et_dob)
    EditText et_dob;
    @BindView(R.id.et_livein)
    EditText et_livein;
    @BindView(R.id.spin_gender)
    Spinner spin_gender;
    @BindView(R.id.btn_submit)
    Button btn_submit;
    @BindView(R.id.et_relationshiplegalrepresent)
    EditText et_relationshiplegalrepresent;
    @BindView(R.id.et_legal_re_email)
    EditText et_legal_re_email;
    @BindView(R.id.et_legal_re_cno)
    EditText et_legal_re_cno;
    @BindView(R.id.et_legal_re)
    EditText et_legal_re;

    @BindView(R.id.et_first_name)
    EditText et_first_name;



    @BindView(R.id.txt_terms)
    TextView txt_terms;

    @BindView(R.id.terms)
    CheckBox terms;

    private Dialog dialog;




    SessionManager sessionManager;
    ApiService apiservice;
    ProgressDialog pDialog;
    RecyclerView.LayoutManager layoutManager;
    int statusCode;
    Calendar myCalendar;
    String gender, language, profile;
    String[] genderValue = {
            "MALE",
            "FEMALE"};
    int year1 = Calendar.YEAR - 18;

    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, -18);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel1();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);
        sessionManager = new SessionManager(SignUpActivity.this);
        apiservice = ApiServiceCreator.createService("latest");
        myCalendar = Calendar.getInstance();
        spinnerAdapter adapter = new spinnerAdapter(SignUpActivity.this, android.R.layout.simple_list_item_1);
        adapter.addAll(genderValue);
        adapter.add("Select Gender");
        spin_gender.setAdapter(adapter);
        spin_gender.setSelection(adapter.getCount());

        txt_terms.setOnClickListener(view -> {
           Intent intent=new Intent(this,TermsActivity.class);
           startActivity(intent);
        });

        new DateInputMask(et_dob);
        txt_login.setOnClickListener(view -> {
            Intent intent = new Intent(SignUpActivity.this, LoginActivity.class);
            startActivity(intent);
        });
        spin_gender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (spin_gender.getSelectedItem() == "Select Gender") {
                    gender = "";
                    //Do nothing.
                } else {
                    gender = spin_gender.getSelectedItem().toString();
                    //Toast.makeText(MainActivity.this, spinner.getSelectedItem().toString(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        btn_submit.setOnClickListener(view -> {
            if (et_email.getText().toString().equals("")) {
                Utility.displayToast(this, "Please check Email Address");
            }else if (et_legal_re.getText().toString().equals("")) {
                Utility.displayToast(this, "Please enter Name of Your Legal Representative");
            }else if (et_relationshiplegalrepresent.getText().toString().equals("")) {
                Utility.displayToast(this, "Relationship To Legal Representative");
            }else if (et_legal_re_email.getText().toString().equals("")) {
                Utility.displayToast(this, "Please enterYour Legal Representative's Email");
            }else if (et_legal_re_cno.getText().toString().equals("")) {
                Utility.displayToast(this, "Please enter Your Legal Representative's Contact No");
            } else if (et_pass.getText().toString().equals("")) {
                Utility.displayToast(this, "Please enter Password");
            }else if (et_first_name.getText().toString().equals("")) {
                Utility.displayToast(this, "Please enter FirstName");
            } else if (et_fname.getText().toString().equals("")) {
                Utility.displayToast(this, "Please enter Nick Name");
            } else if (et_lname.getText().toString().equals("")) {
                Utility.displayToast(this, "Please enter LastName");
            } else if (gender.equals("")) {
                Utility.displayToast(this, "Please Select Gender");
            } else if (et_dob.getText().toString().equals("")) {
                Utility.displayToast(this, "Please enter DateOfBirth");
            } else if (!isValidEmailId(et_email.getText().toString().trim())) {
                Utility.displayToast(this, "Please check Email Address");
            }else if (et_livein.getText().toString().equals("")) {
                Utility.displayToast(this, "Please check Location");
            }else if (!terms.isChecked()) {
                Utility.displayToast(this, "Please check Terms & Condition");
            } else {
                RegisterUser();
            }

        });

    }

    private boolean isValidEmailId(String email) {

        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();
    }

    public void date() {


        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel1();
            }

        };


    }

    private void updateLabel1() {
        String myFormat = "dd/MM/yyyy"; //In which you need put here
        SimpleDateFormat sdf2 = new SimpleDateFormat(myFormat, Locale.US);
        et_dob.setText(sdf2.format(myCalendar.getTime()));
    }


    private int getAge(String dobString) {

        Date date = null;
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        try {
            date = sdf.parse(dobString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (date == null) return 0;

        Calendar dob = Calendar.getInstance();
        Calendar today = Calendar.getInstance();

        dob.setTime(date);

        int year = dob.get(Calendar.YEAR);
        int month = dob.get(Calendar.MONTH);
        int day = dob.get(Calendar.DAY_OF_MONTH);

        dob.set(year, month + 1, day);

        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);

        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)) {
            age--;
        }


        return age;
    }

    public void RegisterUser() {

        pDialog = new ProgressDialog(this);
        pDialog.setTitle("Checking Data");
        pDialog.setMessage("Please Wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        Observable<UserDetail> responseObservable = apiservice.RegisterUser(
                et_email.getText().toString(),
                et_pass.getText().toString(),
                et_fname.getText().toString(),
                et_lname.getText().toString(),
                et_dob.getText().toString(),
                language,
                et_livein.getText().toString(),
                profile,
                gender,
                et_relationshiplegalrepresent.getText().toString(),
                et_legal_re_email.getText().toString(),
                et_legal_re_cno.getText().toString(),
                et_legal_re.getText().toString(),
                sessionManager.getFcmId(),
                et_first_name.getText().toString()
        );

        responseObservable.subscribeOn(Schedulers.newThread())
                .observeOn(rx.android.schedulers.AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> {
                    if (throwable instanceof retrofit2.HttpException) {
                        retrofit2.HttpException ex = (retrofit2.HttpException) throwable;
                        statusCode = ex.code();
                        Log.e("error", ex.getLocalizedMessage());
                    } else if (throwable instanceof SocketTimeoutException) {
                        statusCode = 1000;
                    }
                    return Observable.empty();
                })
                .subscribe(new Observer<UserDetail>() {
                    @Override
                    public void onCompleted() {
                        pDialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("error", "" + e.getMessage());
                    }

                    @Override
                    public void onNext(UserDetail userDetail) {
                        statusCode = userDetail.getStatusCode();
                        if (statusCode == 201) {
                            Utility.displayToast(SignUpActivity.this, userDetail.getMessage());
                            displayDialog();
                        } else {
                            Utility.displayToast(SignUpActivity.this, userDetail.getMessage());
                        }
                    }
                });

    }


    private void displayDialog() {

        dialog = new Dialog(this);
        dialog.getWindow();
        dialog.setTitle("Upgrade");
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.signup_message);
        dialog.setCancelable(false);

        TextView txt_close = dialog.findViewById(R.id.txt_close);

        txt_close.setOnClickListener(view -> {
            dialog.cancel();
            startActivity(new Intent(SignUpActivity.this, LoginActivity.class));
        });


        dialog.show();
        Window window = dialog.getWindow();
        assert window != null;
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

    }


    public class spinnerAdapter extends ArrayAdapter<String> {

        public spinnerAdapter(Context context, int textViewResourceId) {
            super(context, textViewResourceId);
            // TODO Auto-generated constructor stub

        }

        @Override
        public int getCount() {

            // TODO Auto-generated method stub
            int count = super.getCount();

            return count > 0 ? count - 1 : count;


        }


    }
}
