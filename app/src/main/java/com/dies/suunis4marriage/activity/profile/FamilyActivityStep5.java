package com.dies.suunis4marriage.activity.profile;

import android.app.ProgressDialog;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.dies.suunis4marriage.R;
import com.dies.suunis4marriage.apiservice.ApiService;
import com.dies.suunis4marriage.apiservice.ApiServiceCreator;
import com.dies.suunis4marriage.application.SessionManager;
import com.dies.suunis4marriage.application.Utility;
import com.dies.suunis4marriage.model.FamilyDataModel;
import com.dies.suunis4marriage.model.ProfileBuild;

import java.net.SocketTimeoutException;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observable;
import rx.Observer;
import rx.schedulers.Schedulers;

public class FamilyActivityStep5 extends AppCompatActivity {

    @BindView(R.id.btn_next)
    Button btn_next;

    @BindView(R.id.rg1)
    RadioGroup rg1;

    @BindView(R.id.rg2)
    RadioGroup rg2;

    @BindView(R.id.rg3)
    RadioGroup rg3;

    @BindView(R.id.rg4)
    RadioGroup rg4;

    @BindView(R.id.rg5)
    RadioGroup rg5;

    @BindView(R.id.rg6)
    RadioGroup rg6;

    @BindView(R.id.rg7)
    RadioGroup rg7;

    @BindView(R.id.rg8)
    RadioGroup rg8;

    @BindView(R.id.rg9)
    RadioGroup rg9;

    @BindView(R.id.rg10)
    RadioGroup rg10;

    @BindView(R.id.rg11)
    RadioGroup rg11;

    @BindView(R.id.rg12)
    RadioGroup rg12;

    @BindView(R.id.rg13)
    RadioGroup rg13;

    @BindView(R.id.rg14)
    RadioGroup rg14;

    @BindView(R.id.rg15)
    RadioGroup rg15;

    @BindView(R.id.rg16)
    RadioGroup rg16;

    @BindView(R.id.rg17)
    RadioGroup rg17;

    @BindView(R.id.rg18)
    RadioGroup rg18;

    @BindView(R.id.chk_impotnt1)
    CheckBox chk_impotnt1;

    @BindView(R.id.chk_impotnt2)
    CheckBox chk_impotnt2;

    @BindView(R.id.chk_impotnt3)
    CheckBox chk_impotnt3;

    @BindView(R.id.chk_impotnt4)
    CheckBox chk_impotnt4;

    @BindView(R.id.chk_impotnt5)
    CheckBox chk_impotnt5;

    @BindView(R.id.chk_impotnt6)
    CheckBox chk_impotnt6;

    @BindView(R.id.chk_impotnt7)
    CheckBox chk_impotnt7;

    @BindView(R.id.chk_impotnt8)
    CheckBox chk_impotnt8;

    @BindView(R.id.chk_impotnt9)
    CheckBox chk_impotnt9;


    @BindView(R.id.rd1)
    RadioButton rd1;

    @BindView(R.id.rd2)
    RadioButton rd2;

    @BindView(R.id.rd3)
    RadioButton rd3;

    @BindView(R.id.rd4)
    RadioButton rd4;

    @BindView(R.id.rd5)
    RadioButton rd5;

    @BindView(R.id.rd6)
    RadioButton rd6;

    @BindView(R.id.rd7)
    RadioButton rd7;

    @BindView(R.id.rd8)
    RadioButton rd8;

    @BindView(R.id.rd9)
    RadioButton rd9;

    @BindView(R.id.rd10)
    RadioButton rd10;

    @BindView(R.id.rd11)
    RadioButton rd11;

    @BindView(R.id.rd12)
    RadioButton rd12;

    @BindView(R.id.rd13)
    RadioButton rd13;

    @BindView(R.id.rd14)
    RadioButton rd14;

    @BindView(R.id.rd15)
    RadioButton rd15;

    @BindView(R.id.rd16)
    RadioButton rd16;

    @BindView(R.id.rd17)
    RadioButton rd17;

    @BindView(R.id.rd18)
    RadioButton rd18;

    @BindView(R.id.rd19)
    RadioButton rd19;

    @BindView(R.id.rd20)
    RadioButton rd20;

    @BindView(R.id.rd21)
    RadioButton rd21;

    @BindView(R.id.rd22)
    RadioButton rd22;

    @BindView(R.id.rd23)
    RadioButton rd23;

    @BindView(R.id.rd24)
    RadioButton rd24;

    @BindView(R.id.rd25)
    RadioButton rd25;

    @BindView(R.id.rd26)
    RadioButton rd26;

    @BindView(R.id.rd27)
    RadioButton rd27;

    @BindView(R.id.rd28)
    RadioButton rd28;

    @BindView(R.id.rd29)
    RadioButton rd29;

    @BindView(R.id.rd30)
    RadioButton rd30;

    @BindView(R.id.rd31)
    RadioButton rd31;

    @BindView(R.id.rd32)
    RadioButton rd32;

    @BindView(R.id.rd33)
    RadioButton rd33;

    @BindView(R.id.rd34)
    RadioButton rd34;

    @BindView(R.id.rd35)
    RadioButton rd35;

    @BindView(R.id.rd36)
    RadioButton rd36;

    @BindView(R.id.rd37)
    RadioButton rd37;

    @BindView(R.id.rd38)
    RadioButton rd38;

    @BindView(R.id.rd39)
    RadioButton rd39;

    @BindView(R.id.rd40)
    RadioButton rd40;

    @BindView(R.id.rd41)
    RadioButton rd41;

    @BindView(R.id.rd42)
    RadioButton rd42;

    RadioButton rb1,rb2,rb3,rb4,rb5,rb6,rb7,rb8,rb9,rb10,rb11,rb12,rb13,rb14,rb15,rb16,rb17,rb18;
    String CheckImportnt1="0",CheckImportnt2="0",CheckImportnt3="0",CheckImportnt4="0",CheckImportnt5="0",CheckImportnt6="0",CheckImportnt7="0",CheckImportnt8="0",CheckImportnt9="0";
    SessionManager sessionManager;
    ApiService apiservice;
    ProgressDialog pDialog;
    int statusCode;
    String rbtn1,rbtn2,rbtn3,rbtn4,rbtn5,rbtn6,rbtn7,rbtn8,rbtn9,rbtn10,rbtn11,rbtn12,rbtn13,rbtn14,rbtn15,rbtn16,rbtn17,rbtn18;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_family_step5);
        ButterKnife.bind(this);
        sessionManager=new SessionManager(this);
        apiservice = ApiServiceCreator.createService("latest");


        getFamilyData();


        chk_impotnt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (((CheckBox) view).isChecked()) {
                    CheckImportnt1= "1";
                } else {
                    CheckImportnt1= "0";
                }

            }
        });

        chk_impotnt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (((CheckBox) view).isChecked()) {
                    CheckImportnt2= "1";
                } else {
                    CheckImportnt2= "0";
                }

            }
        });

        chk_impotnt3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (((CheckBox) view).isChecked()) {
                    CheckImportnt3= "1";
                } else {
                    CheckImportnt3= "0";
                }

            }
        });

        chk_impotnt4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (((CheckBox) view).isChecked()) {
                    CheckImportnt4= "1";
                } else {
                    CheckImportnt4= "0";
                }

            }
        });

        chk_impotnt5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (((CheckBox) view).isChecked()) {
                    CheckImportnt5= "1";
                } else {
                    CheckImportnt5= "0";
                }

            }
        });

        chk_impotnt6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (((CheckBox) view).isChecked()) {
                    CheckImportnt6= "1";
                } else {
                    CheckImportnt6= "0";
                }

            }
        });

        chk_impotnt7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (((CheckBox) view).isChecked()) {
                    CheckImportnt7= "1";
                } else {
                    CheckImportnt7= "0";
                }

            }
        });

        chk_impotnt8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (((CheckBox) view).isChecked()) {
                    CheckImportnt8= "1";
                } else {
                    CheckImportnt8= "0";
                }

            }
        });

        chk_impotnt9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (((CheckBox) view).isChecked()) {
                    CheckImportnt9= "1";
                } else {
                    CheckImportnt9= "0";
                }

            }
        });




        btn_next.setOnClickListener(view -> {


            int rgb1 = rg1.getCheckedRadioButtonId();
            rb1=(RadioButton) findViewById(rgb1);
            if (rb1!=null){
                rbtn1=rb1.getText().toString();
            }

            int rgb2 = rg2.getCheckedRadioButtonId();
            rb2=(RadioButton) findViewById(rgb2);
            if (rb2!=null){
                rbtn2=rb2.getText().toString();
            }

            int rgb3 = rg3.getCheckedRadioButtonId();
            rb3=(RadioButton) findViewById(rgb3);
            if (rb3!=null){
                rbtn3=rb3.getText().toString();
            }

            int rgb4 = rg4.getCheckedRadioButtonId();
            rb4=(RadioButton) findViewById(rgb4);
            if (rb4!=null){
                rbtn4=rb4.getText().toString();
            }

            int rgb5 = rg5.getCheckedRadioButtonId();
            rb5=(RadioButton) findViewById(rgb5);

            if (rb5!=null){
                rbtn5=rb5.getText().toString();
            }
            int rgb6 = rg6.getCheckedRadioButtonId();
            rb6=(RadioButton) findViewById(rgb6);

            if (rb6!=null){
                rbtn6=rb6.getText().toString();
            }

            int rgb7 = rg7.getCheckedRadioButtonId();
            rb7=(RadioButton) findViewById(rgb7);

            if (rb7!=null){
                rbtn7=rb7.getText().toString();
            }

            int rgb8 = rg8.getCheckedRadioButtonId();
            rb8=(RadioButton) findViewById(rgb8);

            if (rb8!=null){
                rbtn8=rb8.getText().toString();
            }
            int rgb9 = rg9.getCheckedRadioButtonId();
            rb9=(RadioButton) findViewById(rgb9);


            if (rb9!=null){
                rbtn9=rb9.getText().toString();
            }

            int rgb10 = rg10.getCheckedRadioButtonId();
            rb10=(RadioButton) findViewById(rgb10);

            if (rb10!=null){
                rbtn10=rb10.getText().toString();
            }
            int rgb11 = rg11.getCheckedRadioButtonId();
            rb11=(RadioButton) findViewById(rgb11);

            if (rb11!=null){
                rbtn11=rb11.getText().toString();
            }
            int rgb12 = rg12.getCheckedRadioButtonId();
            rb12=(RadioButton) findViewById(rgb12);

            if (rb12!=null){
                rbtn12=rb12.getText().toString();
            }
            int rgb13 = rg13.getCheckedRadioButtonId();
            rb13=(RadioButton) findViewById(rgb13);

            if (rb13!=null){
                rbtn13=rb13.getText().toString();
            }

            int rgb14 = rg14.getCheckedRadioButtonId();
            rb14=(RadioButton) findViewById(rgb14);

            if (rb14!=null){
                rbtn14=rb14.getText().toString();
            }
            int rgb15 = rg15.getCheckedRadioButtonId();
            rb15=(RadioButton) findViewById(rgb15);

            if (rb15!=null){
                rbtn15=rb15.getText().toString();
            }
            int rgb16 = rg16.getCheckedRadioButtonId();
            rb16=(RadioButton) findViewById(rgb16);

            if (rb16!=null){
                rbtn16=rb16.getText().toString();
            }
            int rgb17 = rg17.getCheckedRadioButtonId();
            rb17=(RadioButton) findViewById(rgb17);

            if (rb17!=null){
                rbtn17=rb17.getText().toString();
            }
            int rgb18 = rg18.getCheckedRadioButtonId();
            rb18=(RadioButton) findViewById(rgb18);

            if (rb18!=null){
                rbtn18=rb18.getText().toString();
            }
            FamilyStep5();
        });
    }


    public void FamilyStep5(){

        pDialog = new ProgressDialog(this);
        pDialog.setTitle("Checking Data");
        pDialog.setMessage("Please Wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        Observable<ProfileBuild> responseObservable = apiservice.FamilyStep5(
                rbtn1,
                rbtn2,
                rbtn3,
                rbtn4,
                rbtn5,
                rbtn6,
                rbtn7,
                rbtn8,
                rbtn9,
                rbtn10,
                rbtn11,
                rbtn12,
                rbtn13,
                rbtn14,
                rbtn15,
                rbtn16,
                rbtn17,
                rbtn18,
                CheckImportnt1,
                CheckImportnt2,
                CheckImportnt3,
                CheckImportnt4,
                CheckImportnt5,
                CheckImportnt6,
                CheckImportnt7,
                CheckImportnt8,
                CheckImportnt9,
                sessionManager.getKeyId()
        );

        responseObservable.subscribeOn(Schedulers.newThread())
                .observeOn(rx.android.schedulers.AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> {
                    if (throwable instanceof retrofit2.HttpException) {
                        retrofit2.HttpException ex = (retrofit2.HttpException) throwable;
                        statusCode = ex.code();
                        Log.e("error", ex.getLocalizedMessage());
                    } else if (throwable instanceof SocketTimeoutException) {
                        statusCode = 1000;
                    }
                    return Observable.empty();
                })
                .subscribe(new Observer<ProfileBuild>() {
                    @Override
                    public void onCompleted() {
                        pDialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("error", "" + e.getMessage());
                    }

                    @Override
                    public void onNext(ProfileBuild profileBuild) {
                        statusCode = profileBuild.getStatusCode();
                        if (statusCode == 200) {
                            Utility.displayToast(FamilyActivityStep5.this,profileBuild.getMessage());
                            Intent intent=new Intent(FamilyActivityStep5.this,PreferencesActiityStep6.class);
                            startActivity(intent);
                        }else {
                            Intent intent=new Intent(FamilyActivityStep5.this,PreferencesActiityStep6.class);
                            startActivity(intent);
                        }
                    }
                });

    }


    public void getFamilyData(){

        pDialog = new ProgressDialog(this);
        pDialog.setTitle("Checking Data");
        pDialog.setMessage("Please Wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        Observable<FamilyDataModel> responseObservable = apiservice.getFamilyData(sessionManager.getKeyId());

        responseObservable.subscribeOn(Schedulers.newThread())
                .observeOn(rx.android.schedulers.AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> {
                    if (throwable instanceof retrofit2.HttpException) {
                        retrofit2.HttpException ex = (retrofit2.HttpException) throwable;
                        statusCode = ex.code();
                        Log.e("error", ex.getLocalizedMessage());
                    } else if (throwable instanceof SocketTimeoutException) {
                        statusCode = 1000;
                    }
                    return Observable.empty();
                })
                .subscribe(new Observer<FamilyDataModel>() {
                    @Override
                    public void onCompleted() {
                        pDialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("error", "" + e.getMessage());
                    }

                    @Override
                    public void onNext(FamilyDataModel familyDataModel) {
                        statusCode = familyDataModel.getStatusCode();
                        if (statusCode == 200) {

                            if (familyDataModel.getData().get(0).getCloserelationshipwithyourmother()!=null){

                                if (familyDataModel.getData().get(0).getCloserelationshipwithyourmother().equals("Yes")){
                                    rd1.setChecked(true);
                                }else if (familyDataModel.getData().get(0).getCloserelationshipwithyourmother().equals("No")){
                                    rd2.setChecked(true);
                                }
                            }


                            if (familyDataModel.getData().get(0).getSpousewouldsay16()!=null){

                                if (familyDataModel.getData().get(0).getSpousewouldsay16().equals("Yes")){
                                    rd3.setChecked(true);
                                }else if (familyDataModel.getData().get(0).getSpousewouldsay16().equals("No")){
                                    rd4.setChecked(true);
                                }
                            }


                            if (familyDataModel.getData().get(0).getRelationshipwithyoursiblings()!=null){

                                if (familyDataModel.getData().get(0).getRelationshipwithyoursiblings().equals("Yes")){
                                    rd5.setChecked(true);
                                }else if (familyDataModel.getData().get(0).getRelationshipwithyoursiblings().equals("No")){
                                    rd6.setChecked(true);
                                }
                            }


                            if (familyDataModel.getData().get(0).getSpousewouldsay17()!=null){

                                if (familyDataModel.getData().get(0).getSpousewouldsay17().equals("Yes")){
                                    rd7.setChecked(true);
                                }else if (familyDataModel.getData().get(0).getSpousewouldsay17().equals("No")){
                                    rd8.setChecked(true);
                                }
                            }


                            if (familyDataModel.getData().get(0).getThreeyearsofmarriage()!=null){

                                if (familyDataModel.getData().get(0).getThreeyearsofmarriage().equals("Yes")){
                                    rd9.setChecked(true);
                                }else if (familyDataModel.getData().get(0).getThreeyearsofmarriage().equals("No")){
                                    rd10.setChecked(true);
                                }
                            }


                            if (familyDataModel.getData().get(0).getSpousewouldsay18()!=null){

                                if (familyDataModel.getData().get(0).getSpousewouldsay18().equals("Yes")){
                                    rd11.setChecked(true);
                                }else if (familyDataModel.getData().get(0).getSpousewouldsay18().equals("No")){
                                    rd12.setChecked(true);
                                }
                            }


                            if (familyDataModel.getData().get(0).getChildrentobeeducated()!=null){

                                if (familyDataModel.getData().get(0).getChildrentobeeducated().equals("Islamic")){
                                    rd13.setChecked(true);
                                }else if (familyDataModel.getData().get(0).getChildrentobeeducated().equals("Home School")){
                                    rd14.setChecked(true);
                                }else if (familyDataModel.getData().get(0).getChildrentobeeducated().equals("Public School")){
                                    rd15.setChecked(true);
                                }
                            }


                            if (familyDataModel.getData().get(0).getSpousewouldsay19()!=null){

                                if (familyDataModel.getData().get(0).getSpousewouldsay19().equals("Islamic")){
                                    rd16.setChecked(true);
                                }else if (familyDataModel.getData().get(0).getSpousewouldsay19().equals("Home School")){
                                    rd17.setChecked(true);
                                }else if (familyDataModel.getData().get(0).getSpousewouldsay19().equals("Public School")){
                                    rd18.setChecked(true);
                                }
                            }




                            if (familyDataModel.getData().get(0).getRoleofawifeis()!=null){

                                if (familyDataModel.getData().get(0).getRoleofawifeis().equals("Work and help provide for the family")){
                                    rd19.setChecked(true);
                                }else if (familyDataModel.getData().get(0).getRoleofawifeis().equals("Stay home to raise the kids")){
                                    rd20.setChecked(true);
                                }
                            }


                            if (familyDataModel.getData().get(0).getSpousewouldsay20()!=null){

                                if (familyDataModel.getData().get(0).getSpousewouldsay20().equals("Work and help provide for the family")){
                                    rd21.setChecked(true);
                                }else if (familyDataModel.getData().get(0).getSpousewouldsay20().equals("Stay home to raise the kids")){
                                    rd22.setChecked(true);
                                }
                            }


                            if (familyDataModel.getData().get(0).getTVintheirhome()!=null){

                                if (familyDataModel.getData().get(0).getTVintheirhome().equals("No, TV is time-wasting and a distraction")){
                                    rd23.setChecked(true);
                                }else if (familyDataModel.getData().get(0).getTVintheirhome().equals("Yes, a TV can be used for beneficial things")){
                                    rd24.setChecked(true);
                                }
                            }


                            if (familyDataModel.getData().get(0).getSpousewouldsay21()!=null){

                                if (familyDataModel.getData().get(0).getSpousewouldsay21().equals("No, TV is time-wasting and a distraction")){
                                    rd25.setChecked(true);
                                }else if (familyDataModel.getData().get(0).getSpousewouldsay21().equals("Yes, a TV can be used for beneficial things")){
                                    rd26.setChecked(true);
                                }
                            }


                            if (familyDataModel.getData().get(0).getRoleofahusbandis()!=null){

                                if (familyDataModel.getData().get(0).getRoleofahusbandis().equals("Exclusively support the family and make decisions")){
                                    rd27.setChecked(true);
                                }else if (familyDataModel.getData().get(0).getRoleofahusbandis().equals("Be involved with household chores and share duties")){
                                    rd28.setChecked(true);
                                }
                            }



                            if (familyDataModel.getData().get(0).getSpousewouldsay22()!=null){

                                if (familyDataModel.getData().get(0).getSpousewouldsay22().equals("Exclusively support the family and make decisions")){
                                    rd29.setChecked(true);
                                }else if (familyDataModel.getData().get(0).getSpousewouldsay22().equals("Be involved with household chores and share duties")){
                                    rd30.setChecked(true);
                                }
                            }


                            if (familyDataModel.getData().get(0).getSamehouseasyourparents()!=null){

                                if (familyDataModel.getData().get(0).getSamehouseasyourparents().equals("Yes")){
                                    rd31.setChecked(true);
                                }else if (familyDataModel.getData().get(0).getSamehouseasyourparents().equals("No")){
                                    rd32.setChecked(true);
                                }else if (familyDataModel.getData().get(0).getSamehouseasyourparents().equals("Open for Discussion")){
                                    rd33.setChecked(true);
                                }
                            }



                            if (familyDataModel.getData().get(0).getSpousewouldsay23()!=null){

                                if (familyDataModel.getData().get(0).getSpousewouldsay23().equals("Yes")){
                                    rd34.setChecked(true);
                                }else if (familyDataModel.getData().get(0).getSpousewouldsay23().equals("No")){
                                    rd35.setChecked(true);
                                }else if (familyDataModel.getData().get(0).getSpousewouldsay23().equals("Open for Discussion")){
                                    rd36.setChecked(true);
                                }
                            }



                            if (familyDataModel.getData().get(0).getSamehouseasyourinlaws()!=null){


                                if (familyDataModel.getData().get(0).getSamehouseasyourinlaws().equals("Yes")){
                                    rd37.setChecked(true);
                                }else if (familyDataModel.getData().get(0).getSamehouseasyourinlaws().equals("No")){
                                    rd38.setChecked(true);
                                }else if (familyDataModel.getData().get(0).getSamehouseasyourinlaws().equals("Open for Discussion")){
                                    rd39.setChecked(true);
                                }
                            }


                            if (familyDataModel.getData().get(0).getSpousewouldsay24()!=null){

                                if (familyDataModel.getData().get(0).getSpousewouldsay24().equals("Yes")){
                                    rd40.setChecked(true);
                                }else if (familyDataModel.getData().get(0).getSpousewouldsay24().equals("No")){
                                    rd41.setChecked(true);
                                }else if (familyDataModel.getData().get(0).getSpousewouldsay24().equals("Open for Discussion")){
                                    rd42.setChecked(true);
                                }
                            }


                            if (familyDataModel.getData().get(0).getImportnatqestion17()!=null){

                                if (familyDataModel.getData().get(0).getImportnatqestion17().equals("1")){
                                    chk_impotnt1.setChecked(true);
                                }
                            }


                            if (familyDataModel.getData().get(0).getImportnatqestion18()!=null){

                                if (familyDataModel.getData().get(0).getImportnatqestion18().equals("1")){
                                    chk_impotnt2.setChecked(true);
                                }
                            }


                            if (familyDataModel.getData().get(0).getImportnatqestion19()!=null){

                                if (familyDataModel.getData().get(0).getImportnatqestion19().equals("1")){
                                    chk_impotnt3.setChecked(true);
                                }
                            }


                            if (familyDataModel.getData().get(0).getImportnatqestion20()!=null){

                                if (familyDataModel.getData().get(0).getImportnatqestion20().equals("1")){
                                    chk_impotnt4.setChecked(true);
                                }
                            }


                            if (familyDataModel.getData().get(0).getImportnatqestion21()!=null){

                                if (familyDataModel.getData().get(0).getImportnatqestion21().equals("1")){
                                    chk_impotnt5.setChecked(true);
                                }
                            }


                            if (familyDataModel.getData().get(0).getImportnatqestion22()!=null){

                                if (familyDataModel.getData().get(0).getImportnatqestion22().equals("1")){
                                    chk_impotnt6.setChecked(true);
                                }
                            }


                            if (familyDataModel.getData().get(0).getImportnatqestion23()!=null){

                                if (familyDataModel.getData().get(0).getImportnatqestion23().equals("1")){
                                    chk_impotnt7.setChecked(true);
                                }
                            }


                            if (familyDataModel.getData().get(0).getImportnatqestion24()!=null){

                                if (familyDataModel.getData().get(0).getImportnatqestion24().equals("1")){
                                    chk_impotnt8.setChecked(true);
                                }
                            }







                        }
                    }
                });

    }

}
