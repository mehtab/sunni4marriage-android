package com.dies.suunis4marriage.activity;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;
import android.widget.ImageView;

import com.dies.suunis4marriage.R;

public class TermsActivity extends AppCompatActivity {

    WebView webView;
    ImageView back_icon;


    public String fileName = "simple.html";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms);


        webView = (WebView) findViewById(R.id.webView);
        back_icon = findViewById(R.id.back_icon);

        back_icon.setOnClickListener(view -> {
            finish();
        });
        // displaying content in WebView from html file that stored in assets folder
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl("file:///android_asset/" + fileName);


    }
}
