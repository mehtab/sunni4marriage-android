package com.dies.suunis4marriage.activity.profile;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import com.google.android.material.textfield.TextInputLayout;
import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.dies.suunis4marriage.R;
import com.dies.suunis4marriage.activity.CountryActivity;
import com.dies.suunis4marriage.activity.StateActivity;
import com.dies.suunis4marriage.apiservice.ApiService;
import com.dies.suunis4marriage.apiservice.ApiServiceCreator;
import com.dies.suunis4marriage.application.SessionManager;
import com.dies.suunis4marriage.application.Utility;
import com.dies.suunis4marriage.model.CountryCityModel;
import com.dies.suunis4marriage.model.PersonalDataModel;
import com.dies.suunis4marriage.model.ProfileBuild;

import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observable;
import rx.Observer;
import rx.schedulers.Schedulers;

public class PersonalActivityStep1 extends AppCompatActivity {

    public static final int REQUEST_CODE_COUNTRY = 101;
    public static final int REQUEST_CODE_CITY = 100;
    public static final int REQUEST_CODE_STATE = 102;
    @BindView(R.id.btn_next)
    Button btn_next;
    @BindView(R.id.et_fname)
    EditText et_fname;
    @BindView(R.id.et_lname)
    EditText et_lname;

    AlertDialog alertDialog1;

//    @BindView(R.id.et_country)
//    EditText et_country;
    @BindView(R.id.et_address)
    EditText et_address;

//    @BindView(R.id.et_nationality)
//    EditText et_nationality;
    @BindView(R.id.et_city)
    EditText et_city;
    @BindView(R.id.et_postalcode)
    EditText et_postalcode;
    @BindView(R.id.et_ethnicity)
    EditText et_ethnicity;
//    @BindView(R.id.et_skypeuname)
//    EditText et_skypeuname;
    @BindView(R.id.radioGroup)
    RadioGroup radioGroup;

//    @BindView(R.id.chk_revert)
//    CheckBox chk_revert;
    @BindView(R.id.rb1)
    RadioButton rb1;
    @BindView(R.id.rb2)
    RadioButton rb2;
    @BindView(R.id.et_nameoflegalrepresent)
    EditText et_nameoflegalrepresent;
    @BindView(R.id.et_relationshiplegalrepresent)
    EditText et_relationshiplegalrepresent;
    @BindView(R.id.et_emaillegalrepresent)
    EditText et_emaillegalrepresent;

    @BindView(R.id.et_state)
    EditText et_state;



//    @BindView(R.id.spinner)
//    Spinner spinner;
    @BindView(R.id.et_cnooflegalrepresent)
    EditText et_cnooflegalrepresent;

    @BindView(R.id.input_layout_1)
    TextInputLayout input_layout_1;
    @BindView(R.id.input_layout_2)
    TextInputLayout input_layout_2;
    @BindView(R.id.input_layout_3)
    TextInputLayout input_layout_3;
    @BindView(R.id.input_layout_4)
    TextInputLayout input_layout_4;
    @BindView(R.id.input_layout_5)
    TextInputLayout input_layout_5;
    @BindView(R.id.input_layout_6)
    TextInputLayout input_layout_6;
    @BindView(R.id.input_layout_7)
    TextInputLayout input_layout_7;
    @BindView(R.id.input_layout_8)
    TextInputLayout input_layout_8;
    @BindView(R.id.input_layout_9)
    TextInputLayout input_layout_9;
    @BindView(R.id.input_layout_10)
    TextInputLayout input_layout_10;
    @BindView(R.id.input_layout_11)
    TextInputLayout input_layout_11;
    @BindView(R.id.input_layout_15)
    TextInputLayout input_layout_15;
    @BindView(R.id.input_layout_14)
    TextInputLayout input_layout_14;




    @BindView(R.id.et_first_name)
    EditText et_first_name;







//    @BindView(R.id.spinner_city)
//    Spinner spinner_city;
    @BindView(R.id.et_country)
    EditText et_country;
    @BindView(R.id.spinner_nationality)
    Spinner spinner_nationality;
    //    @BindView(R.id.btn_choose)
//    Button btn_choose;
    RadioButton radioButton;
    SessionManager sessionManager;
    ApiService apiservice;
    ProgressDialog pDialog;
    int statusCode;
    String Selection, spinner_country, spinner_national, spinnercity = "";
    String radio1;
    ArrayAdapter<CharSequence> adapter_nationality;
    String[] All_Ethenicity;
    List<CountryCityModel.Data> data;
    ArrayAdapter<String> adapter;
    String Stateid, country;
    ArrayList<String> arrayList = new ArrayList<>();
    ArrayList<String> arrayList_city = new ArrayList<>();
    ArrayAdapter<String> dataAdapter, dataAdaptera;
    String data1 = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_personal_fragment_step1);
        ButterKnife.bind(this);
        sessionManager = new SessionManager(this);
        apiservice = ApiServiceCreator.createService("latest");


        //getCountry();

        et_country.setOnClickListener(view -> {
            Intent dateintent = new Intent(PersonalActivityStep1.this, CountryActivity.class);
            startActivityForResult(dateintent, REQUEST_CODE_COUNTRY);
        });

//        et_city.setOnClickListener(view -> {
//            if (!et_state.getText().toString().equals("")) {
//                Intent dateintent = new Intent(PersonalActivityStep1.this, CityActivity.class);
//                dateintent.putExtra("id", et_state.getText().toString());
//                startActivityForResult(dateintent, REQUEST_CODE_CITY);
//            } else {
//                Utility.displayToast(this, "First Select State");
//            }
//
//        });

        et_state.setOnClickListener(view -> {
            if (!et_country.getText().toString().equals("")) {
                Intent dateintent = new Intent(PersonalActivityStep1.this, StateActivity.class);
                dateintent.putExtra("id", et_country.getText().toString());
                startActivityForResult(dateintent, REQUEST_CODE_STATE);
            } else {
                Utility.displayToast(this, "First Select Country");
            }
        });


        All_Ethenicity = getResources().getStringArray(R.array.All_Ethenicity);


        adapter_nationality = ArrayAdapter.createFromResource(this,
                R.array.nationality, android.R.layout.simple_spinner_item);
        // Specify layout to be used when list of choices appears
        adapter_nationality.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Applying the adapter to our spinner
        spinner_nationality.setAdapter(adapter_nationality);


        spinner_nationality.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (spinner_nationality.getSelectedItem().equals("Select Nationality")) {
                    spinner_national = "";
                    //Do nothing.
                } else {
                    spinner_national = spinner_nationality.getSelectedItem().toString();

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
//        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                 if(spinner.getSelectedItem() == "Select Country")
//                {
//
//                    spinner_country ="";
//                    //Do nothing.
//                }
//                else{
////                     spinner_country=adapterView.getItemAtPosition(i).toString();
//                    CountryCityModel.Data datalist=data.get(i-1);
//                    Stateid=datalist.getId();
//                     spinner_country=adapterView.getItemAtPosition(i).toString();
//                    Toast.makeText(PersonalActivityStep1.this, Stateid, Toast.LENGTH_LONG).show();
//                    getCity(Stateid);
//                }
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapterView) {
//
//            }
//        });


//        spinner_city.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//
//                if(spinner_city.getSelectedItem() == "Select Country")
//                {
//
//                    spinnercity ="";
//                    //Do nothing.
//                }
//                else{
////                     spinner_country=adapterView.getItemAtPosition(i).toString();
////                    CountryCityModel.Data datalist=data.get(i-1);
////                    Stateid=datalist.getId();
//                    spinnercity=adapterView.getItemAtPosition(i).toString();
//                    Toast.makeText(PersonalActivityStep1.this, spinnercity, Toast.LENGTH_LONG).show();
////                    getCity(Stateid);
//                }
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapterView) {
//
//            }
//        });
        getPersonalData();

        et_ethnicity.setOnClickListener(view -> {
            CreateAlertDialogWithRadioButtonGroup();
        });


        btn_next.setOnClickListener(view -> {

            int selectedId = radioGroup.getCheckedRadioButtonId();
            radioButton = (RadioButton) findViewById(selectedId);
            if (radioButton != null) {
                radio1 = radioButton.getText().toString();
            }
//            Utility.displayToast(this,radioButton.getText().toString());


            if (et_lname.getText().toString().equals("")) {
                Utility.displayToast(this, "Please enter lastname");
            } else if (et_address.getText().toString().equals("")) {
                Utility.displayToast(this, "Please enter Address");
            }else if (et_city.getText().toString().equals("")) {
                Utility.displayToast(this, "Please enter City");
            } else if (et_country.getText().toString().equals("")) {
                Utility.displayToast(this, "Please enter Country");
            } else if (radio1 == null) {
                Utility.displayToast(this, "Please Select Islamic Heritage");
            } else if (et_postalcode.getText().toString().equals("")) {
                Utility.displayToast(this, "Please enter Postalcode");
            } else if (spinner_national.equals("")) {
                Utility.displayToast(this, "Please Select Nationality");
            } else if (et_ethnicity.getText().toString().equals("")) {
                Utility.displayToast(this, "Please select Ethnicity");
            } else {
                BuildProfile();

            }


        });

    }

    public void BuildProfile() {

        pDialog = new ProgressDialog(this);
        pDialog.setTitle("Checking Data");
        pDialog.setMessage("Please Wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        Observable<ProfileBuild> responseObservable = apiservice.Buildprofile(
                et_fname.getText().toString(),
                et_lname.getText().toString(),
                et_first_name.getText().toString(),
                et_address.getText().toString(),
                et_city.getText().toString(),
                et_country.getText().toString(),
                et_state.getText().toString(),
                radio1,
                et_postalcode.getText().toString(),
                spinner_national,
                et_ethnicity.getText().toString(),
                et_nameoflegalrepresent.getText().toString(),
                et_relationshiplegalrepresent.getText().toString(),
                et_emaillegalrepresent.getText().toString(),
                et_cnooflegalrepresent.getText().toString(),
                sessionManager.getKeyId());


        responseObservable.subscribeOn(Schedulers.newThread())
                .observeOn(rx.android.schedulers.AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> {
                    if (throwable instanceof retrofit2.HttpException) {
                        retrofit2.HttpException ex = (retrofit2.HttpException) throwable;
                        statusCode = ex.code();
                        Log.e("error", ex.getLocalizedMessage());
                    } else if (throwable instanceof SocketTimeoutException) {
                        statusCode = 1000;
                    }
                    return Observable.empty();
                })
                .subscribe(new Observer<ProfileBuild>() {
                    @Override
                    public void onCompleted() {
                        pDialog.dismiss();
                        finish();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("error", "" + e.getMessage());
                    }

                    @Override
                    public void onNext(ProfileBuild profileBuild) {
                        statusCode = profileBuild.getStatusCode();
                        if (statusCode == 200) {
                            Utility.displayToast(PersonalActivityStep1.this, profileBuild.getMessage());
                        } else {
                            Utility.displayToast(PersonalActivityStep1.this, profileBuild.getMessage());
                        }
                    }
                });

    }


    public void getPersonalData() {

        pDialog = new ProgressDialog(this);
        pDialog.setTitle("Checking Data");
        pDialog.setMessage("Please Wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        Observable<PersonalDataModel> responseObservable = apiservice.getPersonalData(sessionManager.getKeyId());

        responseObservable.subscribeOn(Schedulers.newThread())
                .observeOn(rx.android.schedulers.AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> {
                    if (throwable instanceof retrofit2.HttpException) {
                        retrofit2.HttpException ex = (retrofit2.HttpException) throwable;
                        statusCode = ex.code();
                        Log.e("error", ex.getLocalizedMessage());
                    } else if (throwable instanceof SocketTimeoutException) {
                        statusCode = 1000;
                    }
                    return Observable.empty();
                })
                .subscribe(new Observer<PersonalDataModel>() {
                    @Override
                    public void onCompleted() {
                        pDialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("error", "" + e.getMessage());
                    }

                    @Override
                    public void onNext(PersonalDataModel personalDataModel) {
                        statusCode = personalDataModel.getStatusCode();
                        if (statusCode == 200) {

                            if (personalDataModel.getData().get(0).getIslamic() != null) {

                                if (personalDataModel.getData().get(0).getIslamic().equals("Born into Islam")) {
                                    rb1.setChecked(true);
                                } else if (personalDataModel.getData().get(0).getIslamic().equals("Converted to Islam")) {
                                    rb2.setChecked(true);
                                }
                            }


                            et_fname.setText(personalDataModel.getData().get(0).getFirstname());
                            et_lname.setText(personalDataModel.getData().get(0).getLastname());
                            et_address.setText(personalDataModel.getData().get(0).getAddress());
                            et_city.setText(personalDataModel.getData().get(0).getTown_city());
                            et_state.setText(personalDataModel.getData().get(0).getState());

                            et_first_name.setText(personalDataModel.getData().get(0).getFirst_name());



                            et_country.setText(personalDataModel.getData().get(0).getCountry());

//                            String compareValue = personalDataModel.getData().get(0).getCountry();
//                            if (compareValue != null) {
//                                int spinnerPosition = adapter.getPosition(compareValue);
//                                spinner.setSelection(spinnerPosition);
//                            }


                            // et_country.setText(personalDataModel.getData().get(0).getCountry());
                            et_postalcode.setText(personalDataModel.getData().get(0).getPostcode());

                            String nationality = personalDataModel.getData().get(0).getNationality();
                            if (nationality != null) {
                                int spinnerPosition = adapter_nationality.getPosition(nationality);
                                spinner_nationality.setSelection(spinnerPosition);
                            }
                            //et_nationality.setText(personalDataModel.getData().get(0).getNationality());
                            et_ethnicity.setText(personalDataModel.getData().get(0).getEthnicity());
                           // et_skypeuname.setText(personalDataModel.getData().get(0).getSkypeid());
                            et_nameoflegalrepresent.setText(personalDataModel.getData().get(0).getLeagalname());
                            et_cnooflegalrepresent.setText(personalDataModel.getData().get(0).getContactnumber());
                            et_relationshiplegalrepresent.setText(personalDataModel.getData().get(0).getRelationship());
                            et_emaillegalrepresent.setText(personalDataModel.getData().get(0).getEmail());

                            //et_emaillegalrepresent.setText(personalDataModel.getData().get(0).getL);


                        }
                    }
                });

    }


    public void CreateAlertDialogWithRadioButtonGroup() {


        int pos=-1;
        if (!et_ethnicity.getText().toString().equals("")){
            String data=et_ethnicity.getText().toString();

            for(int i=0; i < All_Ethenicity.length; i++)
            {
                if(All_Ethenicity[i].contains(data)){
                    pos = i;
                }

            }

        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Select Your Choice");
        builder.setSingleChoiceItems(All_Ethenicity, pos, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                Selection = (String) All_Ethenicity[item];
            }
        });

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Toast.makeText(PersonalActivityStep1.this, Selection, Toast.LENGTH_SHORT).show();
                et_ethnicity.setText(Selection);
                //alertDialog1.dismiss();
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                alertDialog1.dismiss();
            }
        });
        alertDialog1 = builder.create();
        alertDialog1.show();

    }


    public void getCountry() {

//        pDialog = new ProgressDialog(this);
//        pDialog.setTitle("Checking Data");
//        pDialog.setMessage("Please Wait...");
//        pDialog.setIndeterminate(false);
//        pDialog.setCancelable(false);
//        pDialog.show();

        Observable<CountryCityModel> responseObservable = apiservice.getCountry();

        responseObservable.subscribeOn(Schedulers.newThread())
                .observeOn(rx.android.schedulers.AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> {
                    if (throwable instanceof retrofit2.HttpException) {
                        retrofit2.HttpException ex = (retrofit2.HttpException) throwable;
                        statusCode = ex.code();
                        Log.e("error", ex.getLocalizedMessage());
                    } else if (throwable instanceof SocketTimeoutException) {
                        statusCode = 1000;
                    }
                    return Observable.empty();
                })
                .subscribe(new Observer<CountryCityModel>() {
                    @Override
                    public void onCompleted() {
                        // pDialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("error", "" + e.getMessage());
                    }

                    @Override
                    public void onNext(CountryCityModel countryCityModel) {
                        statusCode = countryCityModel.getStatusCode();
                        if (statusCode == 200) {
                            data = countryCityModel.getData();
                            for (int i = 0; i < data.size(); i++) {
                                arrayList.add(data.get(i).getCountry());
                            }

                        }


                    }
                });


        dataAdapter = new ArrayAdapter<String>(PersonalActivityStep1.this, R.layout.spinner_item, arrayList);
        dataAdapter.add("Select Country");
        dataAdapter.setDropDownViewResource(R.layout.spinner_item);
        //spinner.setAdapter(dataAdapter);


//        if (arrayList.size()>2){
//
//
//        }


    }


    public void getCity(String id) {

        Observable<CountryCityModel> responseObservable = apiservice.getCity(id);

        responseObservable.subscribeOn(Schedulers.newThread())
                .observeOn(rx.android.schedulers.AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> {
                    if (throwable instanceof retrofit2.HttpException) {
                        retrofit2.HttpException ex = (retrofit2.HttpException) throwable;
                        statusCode = ex.code();
                        Log.e("error", ex.getLocalizedMessage());
                    } else if (throwable instanceof SocketTimeoutException) {
                        statusCode = 1000;
                    }
                    return Observable.empty();
                })
                .subscribe(new Observer<CountryCityModel>() {
                    @Override
                    public void onCompleted() {
                        //  pDialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("error", "" + e.getMessage());
                    }

                    @Override
                    public void onNext(CountryCityModel countryCityModel) {
                        statusCode = countryCityModel.getStatusCode();
                        if (statusCode == 200) {
                            data = countryCityModel.getData();
                            for (int i = 0; i < data.size(); i++) {
                                arrayList_city.add(data.get(i).getCountry());
                            }

//                            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(PersonalActivityStep1.this, R.layout.spinner_item,arrayList_city);
//                            dataAdapter.add("Select City");
//                            dataAdapter.setDropDownViewResource(R.layout.spinner_item);
                            dataAdaptera = new ArrayAdapter<String>(PersonalActivityStep1.this, R.layout.spinner_item, arrayList_city);
                            dataAdaptera.add("Select City");
                            dataAdaptera.setDropDownViewResource(R.layout.spinner_item);
//                            spinner_city.setAdapter(dataAdaptera);

                        }
                    }
                });

    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//
//        if (requestCode == REQUEST_CODE_COUNTRY) {
//            if(resultCode == Activity.RESULT_OK){
//                Stateid=data.getStringExtra("country_id");
//            }
//            if (resultCode == Activity.RESULT_CANCELED) {
//
//            }
//        }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        data1 = data.getStringExtra("data");

        if (requestCode == REQUEST_CODE_COUNTRY && resultCode == RESULT_OK && data1 != null) {
            et_country.setText(data1);
        }
        if (requestCode == REQUEST_CODE_CITY && resultCode == RESULT_OK && data1 != null) {
            et_city.setText(data1);
        }
        if (requestCode == REQUEST_CODE_STATE && resultCode == RESULT_OK && data1 != null) {
            et_state.setText(data1);
        }
    }

}
