package com.dies.suunis4marriage.activity;

import android.app.ProgressDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.dies.suunis4marriage.R;
import com.dies.suunis4marriage.adapter.PaymentHistoryAdapter;
import com.dies.suunis4marriage.apiservice.ApiService;
import com.dies.suunis4marriage.apiservice.ApiServiceCreator;
import com.dies.suunis4marriage.application.SessionManager;
import com.dies.suunis4marriage.model.PaymentModel;

import java.net.SocketTimeoutException;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observable;
import rx.Observer;
import rx.schedulers.Schedulers;

public class PaymentHistoryActivity extends AppCompatActivity {


    SessionManager sessionManager;
    ApiService apiservice;
    ProgressDialog pDialog;
    RecyclerView.LayoutManager layoutManager;
    int statusCode;

    @BindView(R.id.rcv_history)
    RecyclerView rcv_history;
    @BindView(R.id.txt_nodata)
    TextView txt_nodata;

    @BindView(R.id.back_icon)
    ImageView back_icon;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_history);
        ButterKnife.bind(this);
        sessionManager = new SessionManager(this);
        apiservice = ApiServiceCreator.createService("latest");
        back_icon.setOnClickListener(view -> {
            finish();
        });
        getPaymentHistory();
    }



    public void getPaymentHistory(){
        rcv_history.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        rcv_history.setLayoutManager(layoutManager);
        pDialog = new ProgressDialog(this);
        pDialog.setTitle("Checking Data");
        pDialog.setMessage("Please Wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        Observable<PaymentModel> responseObservable = apiservice.getPaymentHistory(sessionManager.getKeyId());

        responseObservable.subscribeOn(Schedulers.newThread())
                .observeOn(rx.android.schedulers.AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> {
                    if (throwable instanceof retrofit2.HttpException) {
                        retrofit2.HttpException ex = (retrofit2.HttpException) throwable;
                        statusCode = ex.code();
                        Log.e("error", ex.getLocalizedMessage());
                    } else if (throwable instanceof SocketTimeoutException) {
                        statusCode = 1000;
                    }
                    return Observable.empty();
                })
                .subscribe(new Observer<PaymentModel>() {
                    @Override
                    public void onCompleted() {
                        pDialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("error", "" + e.getMessage());
                    }

                    @Override
                    public void onNext(PaymentModel paymentModel) {
                        statusCode = paymentModel.getStatusCode();
                        if (statusCode == 200) {
                            if (paymentModel.getData().size()>0){
                                txt_nodata.setVisibility(View.GONE);
                                PaymentHistoryAdapter adapter=new PaymentHistoryAdapter(PaymentHistoryActivity.this,paymentModel.getData());
                                rcv_history.setAdapter(adapter);
                            }else {
                                txt_nodata.setVisibility(View.VISIBLE);
                            }

                        }
                    }
                });

    }

}
