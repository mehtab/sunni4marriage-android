package com.dies.suunis4marriage.activity.profile;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.cardview.widget.CardView;
import android.util.Log;
import android.widget.ImageView;

import com.dies.suunis4marriage.R;
import com.dies.suunis4marriage.activity.ChangePasswordActivity;
import com.dies.suunis4marriage.activity.ContactActivity;
import com.dies.suunis4marriage.apiservice.ApiService;
import com.dies.suunis4marriage.apiservice.ApiServiceCreator;
import com.dies.suunis4marriage.application.SessionManager;
import com.dies.suunis4marriage.application.Utility;
import com.dies.suunis4marriage.model.UserDetail;

import java.net.SocketTimeoutException;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observable;
import rx.Observer;
import rx.schedulers.Schedulers;

public class SettingActivity extends AppCompatActivity {


    @BindView(R.id.back_icon)
    ImageView back_icon;

    ProgressDialog pDialog;
    SessionManager sessionManager;
    ApiService apiservice;
    int statusCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        apiservice = ApiServiceCreator.createService("latest");
        sessionManager = new SessionManager(this);
        ButterKnife.bind(this);
        back_icon.setOnClickListener(view -> {
            finish();
        });
        CardView card_change_password=findViewById(R.id.card_change_password);
        CardView card_delete=findViewById(R.id.card_delete);
        CardView card_contact_us=findViewById(R.id.card_contact_us);

        card_change_password.setOnClickListener(view -> {

            Intent intent=new Intent(this, ChangePasswordActivity.class);
            startActivity(intent);

        });

        card_delete.setOnClickListener(view -> {
            AlertDialog diaBox = AskOption();
            diaBox.show();
        });

        card_contact_us.setOnClickListener(view -> {
            Intent intent=new Intent(this, ContactActivity.class);
            startActivity(intent);
        });




    }

    private AlertDialog AskOption() {
        AlertDialog myQuittingDialogBox = new AlertDialog.Builder(this)
                .setTitle("Delete")
                .setMessage("Do you want to Delete your profile")
                .setIcon(R.drawable.icon_delete)

                .setPositiveButton("Delete", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        DeleteProfile();
                    }
                })
                .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create();
        return myQuittingDialogBox;

    }

    public void DeleteProfile() {
        pDialog = new ProgressDialog(this);
        pDialog.setTitle("Checking Data");
        pDialog.setMessage("Please Wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        Observable<UserDetail> responseObservable = apiservice.DeleteProfile(sessionManager.getKeyId());

        responseObservable.subscribeOn(Schedulers.newThread())
                .observeOn(rx.android.schedulers.AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> {
                    if (throwable instanceof retrofit2.HttpException) {
                        retrofit2.HttpException ex = (retrofit2.HttpException) throwable;
                        statusCode = ex.code();
                        Log.e("error", ex.getLocalizedMessage());
                    } else if (throwable instanceof SocketTimeoutException) {
                        statusCode = 1000;
                    }
                    return Observable.empty();
                })
                .subscribe(new Observer<UserDetail>() {
                    @Override
                    public void onCompleted() {
                        pDialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("error", "" + e.getMessage());
                    }

                    @Override
                    public void onNext(UserDetail friendModel) {
                        statusCode = friendModel.getStatusCode();
                        if (statusCode == 200) {
                            Utility.displayToast(SettingActivity.this, friendModel.getMessage());
                            sessionManager.logoutUser();
                        }
                    }
                });

    }
}
