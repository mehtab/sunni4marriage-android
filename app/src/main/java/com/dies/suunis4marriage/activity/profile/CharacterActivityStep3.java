package com.dies.suunis4marriage.activity.profile;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.dies.suunis4marriage.R;
import com.dies.suunis4marriage.apiservice.ApiService;
import com.dies.suunis4marriage.apiservice.ApiServiceCreator;
import com.dies.suunis4marriage.application.SessionManager;
import com.dies.suunis4marriage.application.Utility;
import com.dies.suunis4marriage.model.CharacterDataModel;
import com.dies.suunis4marriage.model.ProfileBuild;
import com.thomashaertel.widget.MultiSpinner;

import java.net.SocketTimeoutException;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observable;
import rx.Observer;
import rx.schedulers.Schedulers;

public class CharacterActivityStep3 extends AppCompatActivity {

    @BindView(R.id.btn_next)
    Button btn_next;

//    @BindView(R.id.et_yourself)
//    TextView et_yourself;

    @BindView(R.id.et_spuse)
    TextView et_spuse;

//    @BindView(R.id.et_bad_day)
//    TextView et_bad_day;

//    @BindView(R.id.et_spuse_say)
//    TextView et_spuse_say;

    @BindView(R.id.chk_impotnt1)
    CheckBox chk_impotnt1;

    @BindView(R.id.rg1)
    RadioGroup rg1;

    @BindView(R.id.rg2)
    RadioGroup rg2;

    @BindView(R.id.chk_impotnt2)
    CheckBox chk_impotnt2;

    @BindView(R.id.rg3)
    RadioGroup rg3;

    @BindView(R.id.rg4)
    RadioGroup rg4;

    @BindView(R.id.chk_impotnt3)
    CheckBox chk_impotnt3;

    @BindView(R.id.rg5)
    RadioGroup rg5;

    @BindView(R.id.rg6)
    RadioGroup rg6;

    @BindView(R.id.chk_impotnt4)
    CheckBox chk_impotnt4;

    @BindView(R.id.rg7)
    RadioGroup rg7;

    @BindView(R.id.rg8)
    RadioGroup rg8;


    @BindView(R.id.chk_impotnt5)
    CheckBox chk_impotnt5;

    @BindView(R.id.rg9)
    RadioGroup rg9;

    @BindView(R.id.rg10)
    RadioGroup rg10;

    @BindView(R.id.rg11)
    RadioGroup rg11;

    @BindView(R.id.rd1)
    RadioButton rd1;

    @BindView(R.id.rd2)
    RadioButton rd2;

    @BindView(R.id.rd3)
    RadioButton rd3;

    @BindView(R.id.rd4)
    RadioButton rd4;

    @BindView(R.id.rd5)
    RadioButton rd5;

    @BindView(R.id.rd6)
    RadioButton rd6;

    @BindView(R.id.rd7)
    RadioButton rd7;

    @BindView(R.id.rd8)
    RadioButton rd8;

    @BindView(R.id.rd9)
    RadioButton rd9;

    @BindView(R.id.rd10)
    RadioButton rd10;

    @BindView(R.id.rd11)
    RadioButton rd11;

    @BindView(R.id.rd12)
    RadioButton rd12;

    @BindView(R.id.rd13)
    RadioButton rd13;

    @BindView(R.id.rd14)
    RadioButton rd14;

    @BindView(R.id.rd15)
    RadioButton rd15;

    @BindView(R.id.rd16)
    RadioButton rd16;

    @BindView(R.id.rd17)
    RadioButton rd17;

    @BindView(R.id.rd18)
    RadioButton rd18;

    @BindView(R.id.rd19)
    RadioButton rd19;

    @BindView(R.id.rd20)
    RadioButton rd20;

    @BindView(R.id.rd21)
    RadioButton rd21;

    @BindView(R.id.rd22)
    RadioButton rd22;

    @BindView(R.id.rd23)
    RadioButton rd23;

    @BindView(R.id.rd24)
    RadioButton rd24;

    @BindView(R.id.rd25)
    RadioButton rd25;

    @BindView(R.id.rd26)
    RadioButton rd26;

    AlertDialog alertDialog1;
    CharSequence[] bad_day;
    SessionManager sessionManager;
    ApiService apiservice;
    ProgressDialog pDialog;
    int statusCode;
    RadioButton rb1,rb2,rb3,rb4,rb5,rb6,rb7,rb8,rb9,rb10,rb11;
    String CheckImportnt1="0",CheckImportnt2="0",CheckImportnt3="0",CheckImportnt4="0",CheckImportnt5="0";
    String rbtn1,rbtn2,rbtn3,rbtn4,rbtn5,rbtn6,rbtn7,rbtn8,rbtn9,rbtn10,rbtn11;
    String bad_day_work;
    private ArrayAdapter<String> spin_lang_typeadapter;
    String AboutYou;
    @BindView(R.id.spin_aboutyou)
    MultiSpinner spin_aboutyou;
    String[] aboutyou;
    String[] selected_aboutyou;
    boolean[] selectedItemsFriend;





    private MultiSpinner.MultiSpinnerListener onSelectedlang = new MultiSpinner.MultiSpinnerListener() {
        public void onItemsSelected(boolean[] selected) {
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < selected.length; i++) {
                if (selected[i]) {
                    builder.append(spin_lang_typeadapter.getItem(i)).append(",");
                }
            }
            AboutYou = builder.toString();
            // Toast.makeText(AboutYouActivityStep2.this, builder.toString(), Toast.LENGTH_SHORT).show();
            // Do something here with the selected items
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_character_fragment_step3);
        ButterKnife.bind(this);
        sessionManager=new SessionManager(this);
        apiservice = ApiServiceCreator.createService("latest");


        getCharacterData();

        aboutyou = getResources().getStringArray(R.array.aboutyou);

        chk_impotnt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (((CheckBox) view).isChecked()) {
                    CheckImportnt1= "1";
                } else {
                    CheckImportnt1= "0";
                }

            }
        });

        chk_impotnt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (((CheckBox) view).isChecked()) {
                    CheckImportnt2= "1";
                } else {
                    CheckImportnt2= "0";
                }

            }
        });

        chk_impotnt3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (((CheckBox) view).isChecked()) {
                    CheckImportnt3= "1";
                } else {
                    CheckImportnt3= "0";
                }

            }
        });

        chk_impotnt4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (((CheckBox) view).isChecked()) {
                    CheckImportnt4= "1";
                } else {
                    CheckImportnt4= "0";
                }

            }
        });

        chk_impotnt5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (((CheckBox) view).isChecked()) {
                    CheckImportnt5= "1";
                } else {
                    CheckImportnt5= "0";
                }

            }
        });


//
//        et_bad_day.setOnClickListener(view1 -> {
//            bad_day_working();
//        });

        btn_next.setOnClickListener(view -> {

//            Utility.displayToast(CharacterActivityStep3.this,CheckImportnt1+"  "
//                    +CheckImportnt2+"  "
//                    +CheckImportnt3+"  "
//                    +CheckImportnt4+"  "
//                    +CheckImportnt5);

            int rgb1 = rg1.getCheckedRadioButtonId();
            rb1=(RadioButton) findViewById(rgb1);
            if (rb1!=null){
                rbtn1=rb1.getText().toString();
            }

            int rgb2 = rg2.getCheckedRadioButtonId();
            rb2=(RadioButton) findViewById(rgb2);

            if (rb2!=null){
                rbtn2=rb2.getText().toString();
            }
            int rgb3 = rg3.getCheckedRadioButtonId();
            rb3=(RadioButton) findViewById(rgb3);

            if (rb3!=null){
                rbtn3=rb3.getText().toString();
            }
            int rgb4 = rg4.getCheckedRadioButtonId();
            rb4=(RadioButton) findViewById(rgb4);

            if (rb4!=null){
                rbtn4=rb4.getText().toString();
            }
            int rgb5 = rg5.getCheckedRadioButtonId();
            rb5=(RadioButton) findViewById(rgb5);

            if (rb5!=null){
                rbtn5=rb5.getText().toString();
            }
            int rgb6 = rg6.getCheckedRadioButtonId();
            rb6=(RadioButton) findViewById(rgb6);

            if (rb6!=null){
                rbtn6=rb6.getText().toString();
            }
            int rgb7 = rg7.getCheckedRadioButtonId();
            rb7=(RadioButton) findViewById(rgb7);

            if (rb7!=null){
                rbtn7=rb7.getText().toString();
            }
            int rgb8 = rg8.getCheckedRadioButtonId();
            rb8=(RadioButton) findViewById(rgb8);

            if (rb8!=null){
                rbtn8=rb8.getText().toString();
            }
            int rgb9 = rg9.getCheckedRadioButtonId();
            rb9=(RadioButton) findViewById(rgb9);

            if (rb9!=null){
                rbtn9=rb9.getText().toString();
            }

            int rgb10 = rg10.getCheckedRadioButtonId();
            rb10=(RadioButton) findViewById(rgb10);

            if (rb10!=null){
                rbtn10=rb10.getText().toString();
            }

            int rgb11 = rg11.getCheckedRadioButtonId();
            rb11=(RadioButton) findViewById(rgb11);

            if (rb11!=null){
                rbtn11=rb11.getText().toString();
            }


            Character();

        });



        spin_lang_typeadapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, aboutyou);
        spin_aboutyou.setAdapter(spin_lang_typeadapter, false, onSelectedlang);
        spin_aboutyou.setHint("Select your answer");
    }



    public void Character(){

        pDialog = new ProgressDialog(this);
        pDialog.setTitle("Checking Data");
        pDialog.setMessage("Please Wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        Observable<ProfileBuild> responseObservable = apiservice.CharacterStep3(
                AboutYou,
                et_spuse.getText().toString(),
                CheckImportnt1,
                rbtn1,
                rbtn2,
                CheckImportnt2,
                rbtn3,
                rbtn4,
                CheckImportnt3,
                rbtn5,
                rbtn6,
                CheckImportnt4,
                rbtn7,
                rbtn8,
                CheckImportnt5,
                rbtn9,
                rbtn10,
                rbtn11,
                sessionManager.getKeyId());

        responseObservable.subscribeOn(Schedulers.newThread())
                .observeOn(rx.android.schedulers.AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> {
                    if (throwable instanceof retrofit2.HttpException) {
                        retrofit2.HttpException ex = (retrofit2.HttpException) throwable;
                        statusCode = ex.code();
                        Log.e("error", ex.getLocalizedMessage());
                    } else if (throwable instanceof SocketTimeoutException) {
                        statusCode = 1000;
                    }
                    return Observable.empty();
                })
                .subscribe(new Observer<ProfileBuild>() {
                    @Override
                    public void onCompleted() {
                        pDialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("error", "" + e.getMessage());
                    }

                    @Override
                    public void onNext(ProfileBuild profileBuild) {
                        statusCode = profileBuild.getStatusCode();
                        if (statusCode == 200) {
                            Utility.displayToast(CharacterActivityStep3.this,profileBuild.getMessage());
                            Intent intent = new Intent(CharacterActivityStep3.this, ReligionActivityStep4.class);
                            startActivity(intent);
                        }else {
                            Intent intent = new Intent(CharacterActivityStep3.this, ReligionActivityStep4.class);
                            startActivity(intent);
                        }
                    }
                });

    }



    public void getCharacterData(){

        pDialog = new ProgressDialog(this);
        pDialog.setTitle("Checking Data");
        pDialog.setMessage("Please Wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        Observable<CharacterDataModel> responseObservable = apiservice.getCharacterData(sessionManager.getKeyId());

        responseObservable.subscribeOn(Schedulers.newThread())
                .observeOn(rx.android.schedulers.AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> {
                    if (throwable instanceof retrofit2.HttpException) {
                        retrofit2.HttpException ex = (retrofit2.HttpException) throwable;
                        statusCode = ex.code();
                        Log.e("error", ex.getLocalizedMessage());
                    } else if (throwable instanceof SocketTimeoutException) {
                        statusCode = 1000;
                    }
                    return Observable.empty();
                })
                .subscribe(new Observer<CharacterDataModel>() {
                    @Override
                    public void onCompleted() {
                        pDialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("error", "" + e.getMessage());
                    }

                    @Override
                    public void onNext(CharacterDataModel characterDataModel) {
                        statusCode = characterDataModel.getStatusCode();
                        if (statusCode == 200) {

                          //  et_yourself.setText(characterDataModel.getData().get(0).getHowbestwould());
                            et_spuse.setText(characterDataModel.getData().get(0).getTraitswouldyouwant());
//                            et_bad_day.setText(characterDataModel.getData().get(0).getHadabaddayatwork1());
//                            et_spuse_say.setText(characterDataModel.getData().get(0).getYourspousewouldsay1());



                            if (characterDataModel.getData().get(0).getYouexpectyourspousetodothings()!=null){

                                if (characterDataModel.getData().get(0).getYouexpectyourspousetodothings().equals("Yes")){
                                    rd1.setChecked(true);
                                }else if (characterDataModel.getData().get(0).getYouexpectyourspousetodothings().equals("No")){
                                    rd2.setChecked(true);
                                }else if (characterDataModel.getData().get(0).getYouexpectyourspousetodothings().equals("Maybe")){
                                    rd3.setChecked(true);
                                }
                            }


                            if (characterDataModel.getData().get(0).getYourspousewouldsay2()!=null){

                                if (characterDataModel.getData().get(0).getYourspousewouldsay2().equals("Yes")){
                                    rd4.setChecked(true);
                                }else if (characterDataModel.getData().get(0).getYourspousewouldsay2().equals("No")){
                                    rd5.setChecked(true);
                                }else if (characterDataModel.getData().get(0).getYourspousewouldsay2().equals("Maybe")){
                                    rd6.setChecked(true);
                                }
                            }


                            if (characterDataModel.getData().get(0).getHowimportantisittoyou()!=null){

                                if (characterDataModel.getData().get(0).getHowimportantisittoyou().equals("Very")){
                                    rd7.setChecked(true);
                                }else if (characterDataModel.getData().get(0).getHowimportantisittoyou().equals("Somewhat")){
                                    rd8.setChecked(true);
                                }else if (characterDataModel.getData().get(0).getHowimportantisittoyou().equals("Not at all")){
                                    rd9.setChecked(true);
                                }
                            }


                            if (characterDataModel.getData().get(0).getYourspousewouldsay3()!=null){

                                if (characterDataModel.getData().get(0).getYourspousewouldsay3().equals("Very")){
                                    rd10.setChecked(true);
                                }else if (characterDataModel.getData().get(0).getYourspousewouldsay3().equals("Somewhat")){
                                    rd11.setChecked(true);
                                }else if (characterDataModel.getData().get(0).getYourspousewouldsay3().equals("Not at all")){
                                    rd12.setChecked(true);
                                }
                            }



                            if(characterDataModel.getData().get(0).getHowbestwould()!=null) {
                                String str = characterDataModel.getData().get(0).getHowbestwould();
                                List<String> items = Arrays.asList(str.split("\\s*,\\s*"));
                                selected_aboutyou = items.toArray(new String[items.size()]);


                                Integer array_length = aboutyou.length;
                                selectedItemsFriend = new boolean[aboutyou.length];

                                for (int i = 0; i < selected_aboutyou.length; i++) {

                                    for (int j = 0; j < aboutyou.length; j++) {
                                        if ((selected_aboutyou[i].equals(aboutyou[j]))) {
                                            selectedItemsFriend[j] = true;
                                            break;
                                        }
                                    }
                                }


                                if (selected_aboutyou.length > 0) {
                                    spin_aboutyou.setSelected(selectedItemsFriend);
                                }

                                spin_aboutyou.setText(characterDataModel.getData().get(0).getHowbestwould());
                                AboutYou = characterDataModel.getData().get(0).getHowbestwould();
                            }




//                            if (characterDataModel.getData().get(0).getWhichdescribes().equals("Yes")){
//                                rd13.setChecked(true);
//                            }else if (characterDataModel.getData().get(0).getWhichdescribes().equals("No")){
//                                rd14.setChecked(true);
//                            }
//
                            if (characterDataModel.getData().get(0).getYourspousewouldsay4()!=null){

                                if (characterDataModel.getData().get(0).getYourspousewouldsay4().equals("Yes")){
                                    rd15.setChecked(true);
                                }else if (characterDataModel.getData().get(0).getYourspousewouldsay4().equals("No")){
                                    rd16.setChecked(true);
                                }
                            }




                            if (characterDataModel.getData().get(0).getDescribesyoubest5()!=null){

                                if (characterDataModel.getData().get(0).getDescribesyoubest5().equals("Ambitious")){
                                    rd17.setChecked(true);
                                }else if (characterDataModel.getData().get(0).getDescribesyoubest5().equals("Easy Going")){
                                    rd18.setChecked(true);
                                }
                            }


                            if (characterDataModel.getData().get(0).getYourspousewouldsay5()!=null){

                                if (characterDataModel.getData().get(0).getYourspousewouldsay5().equals("Ambitious")){
                                    rd19.setChecked(true);
                                }else if (characterDataModel.getData().get(0).getYourspousewouldsay5().equals("Easy Going")){
                                    rd20.setChecked(true);
                                }
                            }


                            if (characterDataModel.getData().get(0).getAreyou6()!=null){

                                if (characterDataModel.getData().get(0).getAreyou6().equals("Shy and nervous with new people")){
                                    rd21.setChecked(true);
                                }else if (characterDataModel.getData().get(0).getAreyou6().equals("Open and unreserved with new people")){
                                    rd22.setChecked(true);
                                }
                            }


                            if (characterDataModel.getData().get(0).getAreyou7()!=null){

                                if (characterDataModel.getData().get(0).getAreyou7().equals("Conservative")){
                                    rd23.setChecked(true);
                                }else if (characterDataModel.getData().get(0).getAreyou7().equals("Progressive")){
                                    rd24.setChecked(true);
                                }
                            }

                            if (characterDataModel.getData().get(0).getYourspousewouldsay7()!=null){

                                if (characterDataModel.getData().get(0).getYourspousewouldsay7().equals("Conservative")){
                                    rd25.setChecked(true);
                                }else if (characterDataModel.getData().get(0).getYourspousewouldsay7().equals("Progressive")){
                                    rd26.setChecked(true);
                                }
                            }



                            if (characterDataModel.getData().get(0).getQuestionisimportant1()!=null){

                                if (characterDataModel.getData().get(0).getQuestionisimportant1().equals("1")){
                                    chk_impotnt1.setChecked(true);
                                }
                            }

                            if (characterDataModel.getData().get(0).getQuestionisimportant2()!=null){

                                if (characterDataModel.getData().get(0).getQuestionisimportant2().equals("1")){
                                    chk_impotnt2.setChecked(true);
                                }
                            }

                            if (characterDataModel.getData().get(0).getQuestionisimportant3()!=null){

                                if (characterDataModel.getData().get(0).getQuestionisimportant3().equals("1")){
                                    chk_impotnt3.setChecked(true);
                                }
                            }

                            if (characterDataModel.getData().get(0).getQuestionisimportant4()!=null){

                                if (characterDataModel.getData().get(0).getQuestionisimportant4().equals("1")){
                                    chk_impotnt4.setChecked(true);
                                }
                            }




                        }
                    }
                });

    }





    public void bad_day_working() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Select Your Annual Income");
        builder.setSingleChoiceItems(bad_day, -1, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                bad_day_work = (String) bad_day[item];
            }
        });

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
               // Toast.makeText(AboutYouActivityStep2.this, bad_day_work, Toast.LENGTH_SHORT).show();
                //et_bad_day.setText(bad_day_work);
                //alertDialog1.dismiss();
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                alertDialog1.dismiss();
            }
        });
        alertDialog1 = builder.create();
        alertDialog1.show();

    }
}
