package com.dies.suunis4marriage.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.dies.suunis4marriage.R;
import com.dies.suunis4marriage.apiservice.ApiService;
import com.dies.suunis4marriage.apiservice.ApiServiceCreator;
import com.dies.suunis4marriage.application.SessionManager;
import com.dies.suunis4marriage.model.CountryCityModel;

import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import rx.Observable;
import rx.Observer;
import rx.schedulers.Schedulers;

public class CountryActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

    ListView listView;
    SessionManager sessionManager;
    ApiService apiservice;
    ProgressDialog pDialog;
    int statusCode;
    String[] array;
    ArrayList<String> arrayList_country = new ArrayList<>();
    ArrayAdapter<String> dataAdapter,dataAdaptera;
    ArrayList<String> arrayList=new ArrayList<>();
    List<CountryCityModel.Data> data;

    String Stateid;
//    @BindView(R.id.toolbar_Title)
//    TextView toolbar_Title;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country);
        ButterKnife.bind(this);
        listView = findViewById(R.id.list_country);
        sessionManager = new SessionManager(this);
        apiservice = ApiServiceCreator.createService("latest");
        SearchView simpleSearchView = (SearchView) findViewById(R.id.simpleSearchView);
//        toolbar_Title.setText("Country");
//        backIcon.setOnClickListener(view -> {
//            Intent intent = new Intent(CountryActivity.this, PersonalActivityStep1.class);
//            startActivity(intent);
//            finish();
//        });
        getCountry();
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //  TextView txt = (TextView) view.findViewById(R.id.textView);
                // String str = txt.getText().toString();
                String str = (String) adapterView.getItemAtPosition(i);

                CountryCityModel.Data datalist=data.get(i);
                    Stateid=datalist.getId();

                Intent intent = new Intent();
                intent.putExtra("data", str);
                setResult(RESULT_OK, intent);
               // Toast.makeText(CountryActivity.this, Stateid, Toast.LENGTH_SHORT).show();
                finish();
            }
        });


        simpleSearchView.setOnQueryTextListener(this);
    }






    public void getCountry(){

//        pDialog = new ProgressDialog(this);
//        pDialog.setTitle("Checking Data");
//        pDialog.setMessage("Please Wait...");
//        pDialog.setIndeterminate(false);
//        pDialog.setCancelable(false);
//        pDialog.show();

        Observable<CountryCityModel> responseObservable = apiservice.getCountry();

        responseObservable.subscribeOn(Schedulers.newThread())
                .observeOn(rx.android.schedulers.AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> {
                    if (throwable instanceof retrofit2.HttpException) {
                        retrofit2.HttpException ex = (retrofit2.HttpException) throwable;
                        statusCode = ex.code();
                        Log.e("error", ex.getLocalizedMessage());
                    } else if (throwable instanceof SocketTimeoutException) {
                        statusCode = 1000;
                    }
                    return Observable.empty();
                })
                .subscribe(new Observer<CountryCityModel>() {
                    @Override
                    public void onCompleted() {
                        // pDialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("error", "" + e.getMessage());
                    }

                    @Override
                    public void onNext(CountryCityModel countryCityModel) {
                        statusCode = countryCityModel.getStatusCode();
                        if (statusCode == 200) {
                            data=countryCityModel.getData();
                            for (int i=0;i<data.size();i++){
                                arrayList.add(data.get(i).getCountry());
                            }


                        }

                        array = arrayList.toArray(new String[0]);
                        dataAdapter = new ArrayAdapter<String>(CountryActivity.this, R.layout.spinner_item,array);
                        listView.setAdapter(dataAdapter);
                    }

                });


    }

    @Override
    public void onBackPressed() {
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_CANCELED, returnIntent);
        finish();
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        String text=newText;
        dataAdapter.getFilter().filter(newText);
        return false;
    }
}
