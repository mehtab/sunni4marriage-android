package com.dies.suunis4marriage.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PaymentModel {

    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("message")
    @Expose
    private  String message;
    @SerializedName("data")
    @Expose
    private List<PaymentModel.Data> data = null;


    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<PaymentModel.Data> getData() {
        return data;
    }

    public void setData(List<PaymentModel.Data> data) {
        this.data = data;
    }

    public class Data {

        @SerializedName("transaction_id")
        @Expose
        private String transaction_id;
        @SerializedName("currency")
        @Expose
        private String currency;
        @SerializedName("amount")
        @Expose
        private String amount;
        @SerializedName("user_id")
        @Expose
        private String user_id;

        @SerializedName("merchant_account_id")
        @Expose
        private String merchant_account_id;

        @SerializedName("submerchant_account_id")
        @Expose
        private String submerchant_account_id;

        @SerializedName("mastermerchant_account_id")
        @Expose
        private String mastermerchant_account_id;

        @SerializedName("processor_response_code")
        @Expose
        private String processor_response_code;

        @SerializedName("processor_response_type")
        @Expose
        private String processor_response_type;

        @SerializedName("payment_date")
        @Expose
        private String payment_date;

        @SerializedName("start_date")
        @Expose
        private String start_date;

        @SerializedName("end_date")
        @Expose
        private String end_date;

        @SerializedName("payment_type")
        @Expose
        private String payment_type;





        public String getTransaction_id() {
            return transaction_id;
        }

        public void setTransaction_id(String transaction_id) {
            this.transaction_id = transaction_id;
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getMerchant_account_id() {
            return merchant_account_id;
        }

        public void setMerchant_account_id(String merchant_account_id) {
            this.merchant_account_id = merchant_account_id;
        }

        public String getSubmerchant_account_id() {
            return submerchant_account_id;
        }

        public void setSubmerchant_account_id(String submerchant_account_id) {
            this.submerchant_account_id = submerchant_account_id;
        }

        public String getMastermerchant_account_id() {
            return mastermerchant_account_id;
        }

        public void setMastermerchant_account_id(String mastermerchant_account_id) {
            this.mastermerchant_account_id = mastermerchant_account_id;
        }

        public String getProcessor_response_code() {
            return processor_response_code;
        }

        public void setProcessor_response_code(String processor_response_code) {
            this.processor_response_code = processor_response_code;
        }

        public String getProcessor_response_type() {
            return processor_response_type;
        }

        public void setProcessor_response_type(String processor_response_type) {
            this.processor_response_type = processor_response_type;
        }

        public String getPayment_date() {
            return payment_date;
        }

        public void setPayment_date(String payment_date) {
            this.payment_date = payment_date;
        }

        public String getStart_date() {
            return start_date;
        }

        public void setStart_date(String start_date) {
            this.start_date = start_date;
        }

        public String getEnd_date() {
            return end_date;
        }

        public void setEnd_date(String end_date) {
            this.end_date = end_date;
        }

        public String getPayment_type() {
            return payment_type;
        }

        public void setPayment_type(String payment_type) {
            this.payment_type = payment_type;
        }
    }
}
