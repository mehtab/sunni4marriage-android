package com.dies.suunis4marriage.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FamilyDataModel {

    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("data")
    @Expose
    private List<FamilyDataModel.Data> data = null;
    @SerializedName("message")
    @Expose
    private String message;

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public List<FamilyDataModel.Data> getData() {
        return data;
    }

    public void setData(List<FamilyDataModel.Data> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public class Data{

        @SerializedName("closerelationshipwithyourmother")
        @Expose
        private String closerelationshipwithyourmother;

        @SerializedName("roleofahusbandis")
        @Expose
        private String roleofahusbandis;

        @SerializedName("TVintheirhome")
        @Expose
        private String TVintheirhome;

        @SerializedName("spousewouldsay21")
        @Expose
        private String spousewouldsay21;

        @SerializedName("spousewouldsay22")
        @Expose
        private String spousewouldsay22;

        @SerializedName("roleofawifeis")
        @Expose
        private String roleofawifeis;

        @SerializedName("spousewouldsay20")
        @Expose
        private String spousewouldsay20;

        @SerializedName("importnatqestion18")
        @Expose
        private String importnatqestion18;

        @SerializedName("importnatqestion19")
        @Expose
        private String importnatqestion19;

        @SerializedName("importnatqestion16")
        @Expose
        private String importnatqestion16;

        @SerializedName("spousewouldsay23")
        @Expose
        private String spousewouldsay23;

        @SerializedName("importnatqestion17")
        @Expose
        private String importnatqestion17;

        @SerializedName("threeyearsofmarriage")
        @Expose
        private String threeyearsofmarriage;

        @SerializedName("spousewouldsay24")
        @Expose
        private String spousewouldsay24;

        @SerializedName("yearsofmarriage")
        @Expose
        private String yearsofmarriage;

        @SerializedName("samehouseasyourinlaws")
        @Expose
        private String samehouseasyourinlaws;

        @SerializedName("childrentobeeducated")
        @Expose
        private String childrentobeeducated;

        @SerializedName("relationshipwithyoursiblings")
        @Expose
        private String relationshipwithyoursiblings;

        @SerializedName("spousewouldsay18")
        @Expose
        private String spousewouldsay18;

        @SerializedName("spousewouldsay19")
        @Expose
        private String spousewouldsay19;

        @SerializedName("spousewouldsay16")
        @Expose
        private String spousewouldsay16;

        @SerializedName("spousewouldsay17")
        @Expose
        private String spousewouldsay17;

        @SerializedName("importnatqestion23")
        @Expose
        private String importnatqestion23;

        @SerializedName("importnatqestion24")
        @Expose
        private String importnatqestion24;

        @SerializedName("importnatqestion21")
        @Expose
        private String importnatqestion21;

        @SerializedName("importnatqestion22")
        @Expose
        private String importnatqestion22;

        @SerializedName("importnatqestion20")
        @Expose
        private String importnatqestion20;

        @SerializedName("samehouseasyourparents")
        @Expose
        private String samehouseasyourparents;


        public String getCloserelationshipwithyourmother() {
            return closerelationshipwithyourmother;
        }

        public void setCloserelationshipwithyourmother(String closerelationshipwithyourmother) {
            this.closerelationshipwithyourmother = closerelationshipwithyourmother;
        }

        public String getRoleofahusbandis() {
            return roleofahusbandis;
        }

        public void setRoleofahusbandis(String roleofahusbandis) {
            this.roleofahusbandis = roleofahusbandis;
        }

        public String getTVintheirhome() {
            return TVintheirhome;
        }

        public void setTVintheirhome(String TVintheirhome) {
            this.TVintheirhome = TVintheirhome;
        }

        public String getSpousewouldsay21() {
            return spousewouldsay21;
        }

        public void setSpousewouldsay21(String spousewouldsay21) {
            this.spousewouldsay21 = spousewouldsay21;
        }

        public String getSpousewouldsay22() {
            return spousewouldsay22;
        }

        public void setSpousewouldsay22(String spousewouldsay22) {
            this.spousewouldsay22 = spousewouldsay22;
        }

        public String getRoleofawifeis() {
            return roleofawifeis;
        }

        public void setRoleofawifeis(String roleofawifeis) {
            this.roleofawifeis = roleofawifeis;
        }

        public String getSpousewouldsay20() {
            return spousewouldsay20;
        }

        public void setSpousewouldsay20(String spousewouldsay20) {
            this.spousewouldsay20 = spousewouldsay20;
        }

        public String getImportnatqestion18() {
            return importnatqestion18;
        }

        public void setImportnatqestion18(String importnatqestion18) {
            this.importnatqestion18 = importnatqestion18;
        }

        public String getImportnatqestion19() {
            return importnatqestion19;
        }

        public void setImportnatqestion19(String importnatqestion19) {
            this.importnatqestion19 = importnatqestion19;
        }

        public String getImportnatqestion16() {
            return importnatqestion16;
        }

        public void setImportnatqestion16(String importnatqestion16) {
            this.importnatqestion16 = importnatqestion16;
        }

        public String getSpousewouldsay23() {
            return spousewouldsay23;
        }

        public void setSpousewouldsay23(String spousewouldsay23) {
            this.spousewouldsay23 = spousewouldsay23;
        }

        public String getImportnatqestion17() {
            return importnatqestion17;
        }

        public void setImportnatqestion17(String importnatqestion17) {
            this.importnatqestion17 = importnatqestion17;
        }

        public String getThreeyearsofmarriage() {
            return threeyearsofmarriage;
        }

        public void setThreeyearsofmarriage(String threeyearsofmarriage) {
            this.threeyearsofmarriage = threeyearsofmarriage;
        }

        public String getSpousewouldsay24() {
            return spousewouldsay24;
        }

        public void setSpousewouldsay24(String spousewouldsay24) {
            this.spousewouldsay24 = spousewouldsay24;
        }

        public String getYearsofmarriage() {
            return yearsofmarriage;
        }

        public void setYearsofmarriage(String yearsofmarriage) {
            this.yearsofmarriage = yearsofmarriage;
        }

        public String getSamehouseasyourinlaws() {
            return samehouseasyourinlaws;
        }

        public void setSamehouseasyourinlaws(String samehouseasyourinlaws) {
            this.samehouseasyourinlaws = samehouseasyourinlaws;
        }

        public String getChildrentobeeducated() {
            return childrentobeeducated;
        }

        public void setChildrentobeeducated(String childrentobeeducated) {
            this.childrentobeeducated = childrentobeeducated;
        }

        public String getRelationshipwithyoursiblings() {
            return relationshipwithyoursiblings;
        }

        public void setRelationshipwithyoursiblings(String relationshipwithyoursiblings) {
            this.relationshipwithyoursiblings = relationshipwithyoursiblings;
        }

        public String getSpousewouldsay18() {
            return spousewouldsay18;
        }

        public void setSpousewouldsay18(String spousewouldsay18) {
            this.spousewouldsay18 = spousewouldsay18;
        }

        public String getSpousewouldsay19() {
            return spousewouldsay19;
        }

        public void setSpousewouldsay19(String spousewouldsay19) {
            this.spousewouldsay19 = spousewouldsay19;
        }

        public String getSpousewouldsay16() {
            return spousewouldsay16;
        }

        public void setSpousewouldsay16(String spousewouldsay16) {
            this.spousewouldsay16 = spousewouldsay16;
        }

        public String getSpousewouldsay17() {
            return spousewouldsay17;
        }

        public void setSpousewouldsay17(String spousewouldsay17) {
            this.spousewouldsay17 = spousewouldsay17;
        }

        public String getImportnatqestion23() {
            return importnatqestion23;
        }

        public void setImportnatqestion23(String importnatqestion23) {
            this.importnatqestion23 = importnatqestion23;
        }

        public String getImportnatqestion24() {
            return importnatqestion24;
        }

        public void setImportnatqestion24(String importnatqestion24) {
            this.importnatqestion24 = importnatqestion24;
        }

        public String getImportnatqestion21() {
            return importnatqestion21;
        }

        public void setImportnatqestion21(String importnatqestion21) {
            this.importnatqestion21 = importnatqestion21;
        }

        public String getImportnatqestion22() {
            return importnatqestion22;
        }

        public void setImportnatqestion22(String importnatqestion22) {
            this.importnatqestion22 = importnatqestion22;
        }

        public String getImportnatqestion20() {
            return importnatqestion20;
        }

        public void setImportnatqestion20(String importnatqestion20) {
            this.importnatqestion20 = importnatqestion20;
        }

        public String getSamehouseasyourparents() {
            return samehouseasyourparents;
        }

        public void setSamehouseasyourparents(String samehouseasyourparents) {
            this.samehouseasyourparents = samehouseasyourparents;
        }
    }

}
