package com.dies.suunis4marriage.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FriendModel {
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("data")
    @Expose
    private List<Data> data = null;
    @SerializedName("message")
    @Expose
    private String message;

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public class Data {

        @SerializedName("userId")
        @Expose
        private String friendid;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("email")
        @Expose
        private String email;

        @SerializedName("friend_request_id")
        @Expose
        private String friend_request_id;

        @SerializedName("profilepic")
        @Expose
        private String profile_image;

        @SerializedName("occupation")
        @Expose
        private String occupation;

        @SerializedName("town_city")
        @Expose
        private String town_city;

        @SerializedName("country")
        @Expose
        private String country;

        @SerializedName("dob")
        @Expose
        private String dob;

        @SerializedName("connect_request_id")
        @Expose
        private String connect_request_id;

        @SerializedName("message")
        @Expose
        private String message;





        public String getFriendid() {
                return friendid;
        }

        public void setFriendid(String friendid) {
            this.friendid = friendid;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getFriend_request_id() {
            return friend_request_id;
        }

        public void setFriend_request_id(String friend_request_id) {
            this.friend_request_id = friend_request_id;
        }

        public String getProfile_image() {
            return profile_image;
        }

        public void setProfile_image(String profile_image) {
            this.profile_image = profile_image;
        }

        public String getOccupation() {
            return occupation;
        }

        public void setOccupation(String occupation) {
            this.occupation = occupation;
        }

        public String getTown_city() {
            return town_city;
        }

        public void setTown_city(String town_city) {
            this.town_city = town_city;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getDob() {
            return dob;
        }

        public void setDob(String dob) {
            this.dob = dob;
        }

        public String getConnect_request_id() {
            return connect_request_id;
        }

        public void setConnect_request_id(String connect_request_id) {
            this.connect_request_id = connect_request_id;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }
}
