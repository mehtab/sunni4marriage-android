
package com.dies.suunis4marriage.model;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PromoCode {

    @SerializedName("data")
    @Expose
    private List<Object> data = null;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("PromoCode")
    @Expose
    private String promoCode;
    @SerializedName("discount")
    @Expose
    private String discount;
    @SerializedName("Expirydate")
    @Expose
    private String expirydate;
    @SerializedName("PromoUser")
    @Expose
    private String promoUser;
    @SerializedName("ExpCount")
    @Expose
    private String expCount;
    @SerializedName("CurrentCount")
    @Expose
    private String currentCount;

    public List<Object> getData() {
        return data;
    }

    public void setData(List<Object> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getExpirydate() {
        return expirydate;
    }

    public void setExpirydate(String expirydate) {
        this.expirydate = expirydate;
    }

    public String getPromoUser() {
        return promoUser;
    }

    public void setPromoUser(String promoUser) {
        this.promoUser = promoUser;
    }

    public String getExpCount() {
        return expCount;
    }

    public void setExpCount(String expCount) {
        this.expCount = expCount;
    }

    public String getCurrentCount() {
        return currentCount;
    }

    public void setCurrentCount(String currentCount) {
        this.currentCount = currentCount;
    }

}
