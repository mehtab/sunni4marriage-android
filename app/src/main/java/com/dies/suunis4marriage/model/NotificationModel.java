package com.dies.suunis4marriage.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NotificationModel {

    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("data")
    @Expose
    private List<Data> data = null;
    @SerializedName("message")
    @Expose
    private String message;

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public class Data {
        @SerializedName("nf_id")
        @Expose
        private String nf_id;
        @SerializedName("nf_user_id")
        @Expose
        private String nf_user_id;
        @SerializedName("seen")
        @Expose
        private String seen;
        @SerializedName("created_by")
        @Expose
        private String created_by;
        @SerializedName("nf_status")
        @Expose
        private String nf_status;
        @SerializedName("nf_receiver_id")
        @Expose
        private String nf_receiver_id;

        @SerializedName("nf_message")
        @Expose
        private String nf_message;
        @SerializedName("created_time")
        @Expose
        private String created_time;
        @SerializedName("profile_image")
        @Expose
        private String profile_image;
        @SerializedName("nf_created")
        @Expose
        private String nf_created;

        @SerializedName("redirect_status")
        @Expose
        private String redirect_status;


        public String getNf_id() {
            return nf_id;
        }

        public void setNf_id(String nf_id) {
            this.nf_id = nf_id;
        }

        public String getNf_user_id() {
            return nf_user_id;
        }

        public void setNf_user_id(String nf_user_id) {
            this.nf_user_id = nf_user_id;
        }

        public String getSeen() {
            return seen;
        }

        public void setSeen(String seen) {
            this.seen = seen;
        }

        public String getCreated_by() {
            return created_by;
        }

        public void setCreated_by(String created_by) {
            this.created_by = created_by;
        }

        public String getNf_status() {
            return nf_status;
        }

        public void setNf_status(String nf_status) {
            this.nf_status = nf_status;
        }

        public String getNf_receiver_id() {
            return nf_receiver_id;
        }

        public void setNf_receiver_id(String nf_receiver_id) {
            this.nf_receiver_id = nf_receiver_id;
        }

        public String getNf_message() {
            return nf_message;
        }

        public void setNf_message(String nf_message) {
            this.nf_message = nf_message;
        }

        public String getCreated_time() {
            return created_time;
        }

        public void setCreated_time(String created_time) {
            this.created_time = created_time;
        }

        public String getProfile_image() {
            return profile_image;
        }

        public void setProfile_image(String profile_image) {
            this.profile_image = profile_image;
        }

        public String getNf_created() {
            return nf_created;
        }

        public void setNf_created(String nf_created) {
            this.nf_created = nf_created;
        }

        public String getRedirect_status() {
            return redirect_status;
        }

        public void setRedirect_status(String redirect_status) {
            this.redirect_status = redirect_status;
        }
    }


}
