package com.dies.suunis4marriage.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PreferenceDataModel {

    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("data")
    @Expose
    private List<PreferenceDataModel.Data> data = null;
    @SerializedName("message")
    @Expose
    private String message;

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public List<PreferenceDataModel.Data> getData() {
        return data;
    }

    public void setData(List<PreferenceDataModel.Data> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public class Data{

        @SerializedName("cityformarriage")
        @Expose
        private String cityformarriage;

        @SerializedName("differentethnicity")
        @Expose
        private String differentethnicity;


        @SerializedName("differentcaste")
        @Expose
        private String differentcaste;


        @SerializedName("whohasbeendivorced")
        @Expose
        private String whohasbeendivorced;


        @SerializedName("whohaschildren")
        @Expose
        private String whohaschildren;

        @SerializedName("marrysomeoneyounger")
        @Expose
        private String marrysomeoneyounger;


        @SerializedName("marrysomeoneolder")
        @Expose
        private String marrysomeoneolder;


        @SerializedName("samelevelofeducation")
        @Expose
        private String samelevelofeducation;


        @SerializedName("reverttoIslam")
        @Expose
        private String reverttoIslam;


        @SerializedName("differentnationality")
        @Expose
        private String differentnationality;


        public String getCityformarriage() {
            return cityformarriage;
        }

        public void setCityformarriage(String cityformarriage) {
            this.cityformarriage = cityformarriage;
        }

        public String getDifferentethnicity() {
            return differentethnicity;
        }

        public void setDifferentethnicity(String differentethnicity) {
            this.differentethnicity = differentethnicity;
        }

        public String getDifferentcaste() {
            return differentcaste;
        }

        public void setDifferentcaste(String differentcaste) {
            this.differentcaste = differentcaste;
        }

        public String getWhohasbeendivorced() {
            return whohasbeendivorced;
        }

        public void setWhohasbeendivorced(String whohasbeendivorced) {
            this.whohasbeendivorced = whohasbeendivorced;
        }

        public String getWhohaschildren() {
            return whohaschildren;
        }

        public void setWhohaschildren(String whohaschildren) {
            this.whohaschildren = whohaschildren;
        }

        public String getMarrysomeoneyounger() {
            return marrysomeoneyounger;
        }

        public void setMarrysomeoneyounger(String marrysomeoneyounger) {
            this.marrysomeoneyounger = marrysomeoneyounger;
        }

        public String getMarrysomeoneolder() {
            return marrysomeoneolder;
        }

        public void setMarrysomeoneolder(String marrysomeoneolder) {
            this.marrysomeoneolder = marrysomeoneolder;
        }

        public String getSamelevelofeducation() {
            return samelevelofeducation;
        }

        public void setSamelevelofeducation(String samelevelofeducation) {
            this.samelevelofeducation = samelevelofeducation;
        }

        public String getReverttoIslam() {
            return reverttoIslam;
        }

        public void setReverttoIslam(String reverttoIslam) {
            this.reverttoIslam = reverttoIslam;
        }

        public String getDifferentnationality() {
            return differentnationality;
        }

        public void setDifferentnationality(String differentnationality) {
            this.differentnationality = differentnationality;
        }
    }

}
