package com.dies.suunis4marriage.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ReligionDataModel {

    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("data")
    @Expose
    private List<ReligionDataModel.Data> data = null;
    @SerializedName("message")
    @Expose
    private String message;

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public List<ReligionDataModel.Data> getData() {
        return data;
    }

    public void setData(List<ReligionDataModel.Data> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public class Data{

        @SerializedName("userId")
        @Expose
        private String userId;

        @SerializedName("menandwomen")
        @Expose
        private String menandwomen;

        @SerializedName("importnatqestion8")
        @Expose
        private String importnatqestion8;

        @SerializedName("islamis")
        @Expose
        private String islamis;


        @SerializedName("youspousewouldsay8")
        @Expose
        private String youspousewouldsay8;

        @SerializedName("youspousewouldsay9")
        @Expose
        private String youspousewouldsay9;


        @SerializedName("importnatqestion9")
        @Expose
        private String importnatqestion9;


        @SerializedName("islamicclasses")
        @Expose
        private String islamicclasses;

        @SerializedName("youspousewouldsay10")
        @Expose
        private String youspousewouldsay10;

        @SerializedName("importnatqestion10")
        @Expose
        private String importnatqestion10;

        @SerializedName("bestdescribesyou11")
        @Expose
        private String bestdescribesyou11;


        @SerializedName("youspousewouldsay11")
        @Expose
        private String youspousewouldsay11;

        @SerializedName("importnatqestion11")
        @Expose
        private String importnatqestion11;

        @SerializedName("islamdoyoufollow12")
        @Expose
        private String islamdoyoufollow12;

        @SerializedName("youspousewouldsay12")
        @Expose
        private String youspousewouldsay12;

        @SerializedName("importnatqestion12")
        @Expose
        private String importnatqestion12;

        @SerializedName("performingprayer13")
        @Expose
        private String performingprayer13;

        @SerializedName("youspousewouldsay13")
        @Expose
        private String youspousewouldsay13;

        @SerializedName("importnatqestion13")
        @Expose
        private String importnatqestion13;

        @SerializedName("schoolofthought")
        @Expose
        private String scholar;

        @SerializedName("whichdescribes2")
        @Expose
        private String whichdescribes2;

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getMenandwomen() {
            return menandwomen;
        }

        public void setMenandwomen(String menandwomen) {
            this.menandwomen = menandwomen;
        }

        public String getImportnatqestion8() {
            return importnatqestion8;
        }

        public void setImportnatqestion8(String importnatqestion8) {
            this.importnatqestion8 = importnatqestion8;
        }

        public String getIslamis() {
            return islamis;
        }

        public void setIslamis(String islamis) {
            this.islamis = islamis;
        }

        public String getYouspousewouldsay9() {
            return youspousewouldsay9;
        }

        public void setYouspousewouldsay9(String youspousewouldsay9) {
            this.youspousewouldsay9 = youspousewouldsay9;
        }

        public String getImportnatqestion9() {
            return importnatqestion9;
        }

        public void setImportnatqestion9(String importnatqestion9) {
            this.importnatqestion9 = importnatqestion9;
        }

        public String getIslamicclasses() {
            return islamicclasses;
        }

        public void setIslamicclasses(String islamicclasses) {
            this.islamicclasses = islamicclasses;
        }

        public String getYouspousewouldsay10() {
            return youspousewouldsay10;
        }

        public void setYouspousewouldsay10(String youspousewouldsay10) {
            this.youspousewouldsay10 = youspousewouldsay10;
        }

        public String getImportnatqestion10() {
            return importnatqestion10;
        }

        public void setImportnatqestion10(String importnatqestion10) {
            this.importnatqestion10 = importnatqestion10;
        }

        public String getBestdescribesyou11() {
            return bestdescribesyou11;
        }

        public void setBestdescribesyou11(String bestdescribesyou11) {
            this.bestdescribesyou11 = bestdescribesyou11;
        }

        public String getYouspousewouldsay11() {
            return youspousewouldsay11;
        }

        public void setYouspousewouldsay11(String youspousewouldsay11) {
            this.youspousewouldsay11 = youspousewouldsay11;
        }

        public String getImportnatqestion11() {
            return importnatqestion11;
        }

        public void setImportnatqestion11(String importnatqestion11) {
            this.importnatqestion11 = importnatqestion11;
        }

        public String getIslamdoyoufollow12() {
            return islamdoyoufollow12;
        }

        public void setIslamdoyoufollow12(String islamdoyoufollow12) {
            this.islamdoyoufollow12 = islamdoyoufollow12;
        }

        public String getYouspousewouldsay12() {
            return youspousewouldsay12;
        }

        public void setYouspousewouldsay12(String youspousewouldsay12) {
            this.youspousewouldsay12 = youspousewouldsay12;
        }

        public String getImportnatqestion12() {
            return importnatqestion12;
        }

        public void setImportnatqestion12(String importnatqestion12) {
            this.importnatqestion12 = importnatqestion12;
        }

        public String getPerformingprayer13() {
            return performingprayer13;
        }

        public void setPerformingprayer13(String performingprayer13) {
            this.performingprayer13 = performingprayer13;
        }

        public String getYouspousewouldsay13() {
            return youspousewouldsay13;
        }

        public void setYouspousewouldsay13(String youspousewouldsay13) {
            this.youspousewouldsay13 = youspousewouldsay13;
        }

        public String getImportnatqestion13() {
            return importnatqestion13;
        }

        public void setImportnatqestion13(String importnatqestion13) {
            this.importnatqestion13 = importnatqestion13;
        }

        public String getScholar() {
            return scholar;
        }

        public void setScholar(String scholar) {
            this.scholar = scholar;
        }

        public String getWhichdescribes2() {
            return whichdescribes2;
        }

        public void setWhichdescribes2(String whichdescribes2) {
            this.whichdescribes2 = whichdescribes2;
        }

        public String getYouspousewouldsay8() {
            return youspousewouldsay8;
        }

        public void setYouspousewouldsay8(String youspousewouldsay8) {
            this.youspousewouldsay8 = youspousewouldsay8;
        }
    }

}
