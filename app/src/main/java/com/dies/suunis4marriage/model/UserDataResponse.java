package com.dies.suunis4marriage.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UserDataResponse {

    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("data")
    @Expose
    private List<Data> data = null;
    @SerializedName("message")
    @Expose
    private String message;

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public class Data {

        @SerializedName("userId")
        @Expose
        private String userId;

        @SerializedName("name")
        @Expose
        private String firstName;

        @SerializedName("first_name")
        @Expose
        private String first_name;

        @SerializedName("last_name")
        @Expose
        private String lastName;

        @SerializedName("email")
        @Expose
        private String email;

        @SerializedName("unique_user_id")
        @Expose
        private String unique_user_id;

        @SerializedName("user_name")
        @Expose
        private String userName;

        @SerializedName("contactnumber")
        @Expose
        private String mobile;

        @SerializedName("profilepic")
        @Expose
        private String profilepic;

        @SerializedName("multiple_image")
        @Expose
        private String multiple_image;

        @SerializedName("payment_status")
        @Expose
        private String payment_status;

        @SerializedName("customer_id")
        @Expose
        private String customerID;

        @SerializedName("plan_status")
        @Expose
        private String plan_status;

        @SerializedName("plan_type")
        @Expose
        private String plan_type;

        @SerializedName("plan_end_date")
        @Expose
        private String plan_end_date;

        @SerializedName("plan_start_date")
        @Expose
        private String plan_start_date;

        @SerializedName("plan_days")
        @Expose
        private String plan_days;

        @SerializedName("amount")
        @Expose
        private String plan_price;

        @SerializedName("image11")
        @Expose
        private String image11;

        @SerializedName("image12")
        @Expose
        private String image12;

        @SerializedName("image13")
        @Expose
        private String image13;

        @SerializedName("image14")
        @Expose
        private String image14;

        @SerializedName("gender")
        @Expose
        private String gender;

        public String getFirst_name() {
            return first_name;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getEmail() {
            return email;
        }

        public String getUnique_user_id() {
            return unique_user_id;
        }

        public void setUnique_user_id(String unique_user_id) {
            this.unique_user_id = unique_user_id;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getProfilepic() {
            return profilepic;
        }

        public void setProfilepic(String profilepic) {
            this.profilepic = profilepic;
        }

        public String getPayment_status() {
            return payment_status;
        }

        public String getCustomerID() {
            return customerID;
        }

        public void setCustomerID(String customerID) {
            this.customerID = customerID;
        }

        public void setPayment_status(String payment_status) {
            this.payment_status = payment_status;
        }



        public String getMultiple_image() {
            return multiple_image;
        }

        public void setMultiple_image(String multiple_image) {
            this.multiple_image = multiple_image;
        }

        public String getPlan_status() {
            return plan_status;
        }

        public void setPlan_status(String plan_status) {
            this.plan_status = plan_status;
        }

        public String getPlan_type() {
            return plan_type;
        }

        public void setPlan_type(String plan_type) {
            this.plan_type = plan_type;
        }

        public String getPlan_end_date() {
            return plan_end_date;
        }

        public void setPlan_end_date(String plan_end_date) {
            this.plan_end_date = plan_end_date;
        }

        public String getPlan_start_date() {
            return plan_start_date;
        }

        public void setPlan_start_date(String plan_start_date) {
            this.plan_start_date = plan_start_date;
        }

        public String getPlan_days() {
            return plan_days;
        }

        public void setPlan_days(String plan_days) {
            this.plan_days = plan_days;
        }

        public String getPlan_price() {
            return plan_price;
        }

        public void setPlan_price(String plan_price) {
            this.plan_price = plan_price;
        }

        public String getImage11() {
            return image11;
        }

        public void setImage11(String image11) {
            this.image11 = image11;
        }

        public String getImage12() {
            return image12;
        }

        public void setImage12(String image12) {
            this.image12 = image12;
        }

        public String getImage13() {
            return image13;
        }

        public void setImage13(String image13) {
            this.image13 = image13;
        }

        public String getImage14() {
            return image14;
        }

        public void setImage14(String image14) {
            this.image14 = image14;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }
    }
}
