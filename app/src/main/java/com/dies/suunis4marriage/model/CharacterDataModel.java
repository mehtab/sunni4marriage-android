package com.dies.suunis4marriage.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CharacterDataModel {

    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("data")
    @Expose
    private List<CharacterDataModel.Data> data = null;
    @SerializedName("message")
    @Expose
    private String message;

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public List<CharacterDataModel.Data> getData() {
        return data;
    }

    public void setData(List<CharacterDataModel.Data> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public class Data{

        @SerializedName("userId")
        @Expose
        private String userId;

        @SerializedName("howbestwould")
        @Expose
        private String howbestwould;

        @SerializedName("traitswouldyouwant")
        @Expose
        private String traitswouldyouwant;

        @SerializedName("hadabaddayatwork1")
        @Expose
        private String hadabaddayatwork1;

        @SerializedName("Yourspousewouldsay1")
        @Expose
        private String Yourspousewouldsay1;

        @SerializedName("questionisimportant1")
        @Expose
        private String questionisimportant1;

        @SerializedName("youexpectyourspousetodothings")
        @Expose
        private String youexpectyourspousetodothings;

        @SerializedName("Yourspousewouldsay2")
        @Expose
        private String Yourspousewouldsay2;

        @SerializedName("questionisimportant2")
        @Expose
        private String questionisimportant2;

        @SerializedName("howimportantisittoyou")
        @Expose
        private String howimportantisittoyou;
        @SerializedName("Yourspousewouldsay3")
        @Expose
        private String Yourspousewouldsay3;

        @SerializedName("questionisimportant3")
        @Expose
        private String questionisimportant3;

        @SerializedName("whichdescribes2")
        @Expose
        private String whichdescribes2;

        @SerializedName("whichdescribes")
        @Expose
        private String whichdescribes;

        @SerializedName("Yourspousewouldsay4")
        @Expose
        private String Yourspousewouldsay4;

        @SerializedName("questionisimportant4")
        @Expose
        private String questionisimportant4;

        @SerializedName("describesyoubest5")
        @Expose
        private String describesyoubest5;

        @SerializedName("Yourspousewouldsay5")
        @Expose
        private String Yourspousewouldsay5;

        @SerializedName("areyou6")
        @Expose
        private String areyou6;

        @SerializedName("Yourspousewouldsay6")
        @Expose
        private String Yourspousewouldsay6;

        @SerializedName("questionisimportant6")
        @Expose
        private String questionisimportant6;

        @SerializedName("areyou7")
        @Expose
        private String areyou7;

        @SerializedName("Yourspousewouldsay7")
        @Expose
        private String Yourspousewouldsay7;


        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getHowbestwould() {
            return howbestwould;
        }

        public void setHowbestwould(String howbestwould) {
            this.howbestwould = howbestwould;
        }

        public String getTraitswouldyouwant() {
            return traitswouldyouwant;
        }

        public void setTraitswouldyouwant(String traitswouldyouwant) {
            this.traitswouldyouwant = traitswouldyouwant;
        }

        public String getHadabaddayatwork1() {
            return hadabaddayatwork1;
        }

        public void setHadabaddayatwork1(String hadabaddayatwork1) {
            this.hadabaddayatwork1 = hadabaddayatwork1;
        }

        public String getYourspousewouldsay1() {
            return Yourspousewouldsay1;
        }

        public void setYourspousewouldsay1(String yourspousewouldsay1) {
            Yourspousewouldsay1 = yourspousewouldsay1;
        }

        public String getQuestionisimportant1() {
            return questionisimportant1;
        }

        public void setQuestionisimportant1(String questionisimportant1) {
            this.questionisimportant1 = questionisimportant1;
        }

        public String getYouexpectyourspousetodothings() {
            return youexpectyourspousetodothings;
        }

        public void setYouexpectyourspousetodothings(String youexpectyourspousetodothings) {
            this.youexpectyourspousetodothings = youexpectyourspousetodothings;
        }

        public String getYourspousewouldsay2() {
            return Yourspousewouldsay2;
        }

        public void setYourspousewouldsay2(String yourspousewouldsay2) {
            Yourspousewouldsay2 = yourspousewouldsay2;
        }

        public String getQuestionisimportant2() {
            return questionisimportant2;
        }

        public void setQuestionisimportant2(String questionisimportant2) {
            this.questionisimportant2 = questionisimportant2;
        }

        public String getHowimportantisittoyou() {
            return howimportantisittoyou;
        }

        public void setHowimportantisittoyou(String howimportantisittoyou) {
            this.howimportantisittoyou = howimportantisittoyou;
        }

        public String getYourspousewouldsay3() {
            return Yourspousewouldsay3;
        }

        public void setYourspousewouldsay3(String yourspousewouldsay3) {
            Yourspousewouldsay3 = yourspousewouldsay3;
        }

        public String getQuestionisimportant3() {
            return questionisimportant3;
        }

        public void setQuestionisimportant3(String questionisimportant3) {
            this.questionisimportant3 = questionisimportant3;
        }

        public String getWhichdescribes2() {
            return whichdescribes2;
        }

        public void setWhichdescribes2(String whichdescribes2) {
            this.whichdescribes2 = whichdescribes2;
        }

        public String getWhichdescribes() {
            return whichdescribes;
        }

        public void setWhichdescribes(String whichdescribes) {
            this.whichdescribes = whichdescribes;
        }

        public String getYourspousewouldsay4() {
            return Yourspousewouldsay4;
        }

        public void setYourspousewouldsay4(String yourspousewouldsay4) {
            Yourspousewouldsay4 = yourspousewouldsay4;
        }

        public String getQuestionisimportant4() {
            return questionisimportant4;
        }

        public void setQuestionisimportant4(String questionisimportant4) {
            this.questionisimportant4 = questionisimportant4;
        }

        public String getAreyou6() {
            return areyou6;
        }

        public void setAreyou6(String areyou6) {
            this.areyou6 = areyou6;
        }

        public String getYourspousewouldsay6() {
            return Yourspousewouldsay6;
        }

        public void setYourspousewouldsay6(String yourspousewouldsay6) {
            Yourspousewouldsay6 = yourspousewouldsay6;
        }

        public String getQuestionisimportant6() {
            return questionisimportant6;
        }

        public void setQuestionisimportant6(String questionisimportant6) {
            this.questionisimportant6 = questionisimportant6;
        }

        public String getAreyou7() {
            return areyou7;
        }

        public void setAreyou7(String areyou7) {
            this.areyou7 = areyou7;
        }

        public String getYourspousewouldsay7() {
            return Yourspousewouldsay7;
        }

        public void setYourspousewouldsay7(String yourspousewouldsay7) {
            Yourspousewouldsay7 = yourspousewouldsay7;
        }

        public String getDescribesyoubest5() {
            return describesyoubest5;
        }

        public void setDescribesyoubest5(String describesyoubest5) {
            this.describesyoubest5 = describesyoubest5;
        }

        public String getYourspousewouldsay5() {
            return Yourspousewouldsay5;
        }

        public void setYourspousewouldsay5(String yourspousewouldsay5) {
            Yourspousewouldsay5 = yourspousewouldsay5;
        }
    }

}
