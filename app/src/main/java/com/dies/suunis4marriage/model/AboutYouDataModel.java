package com.dies.suunis4marriage.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AboutYouDataModel {

    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("data")
    @Expose
    private List<AboutYouDataModel.Data> data = null;
    @SerializedName("message")
    @Expose
    private String message;

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public List<AboutYouDataModel.Data> getData() {
        return data;
    }

    public void setData(List<AboutYouDataModel.Data> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public class Data{

        @SerializedName("userId")
        @Expose
        private String userId;

        @SerializedName("dob")
        @Expose
        private String dob;
        @SerializedName("tall")
        @Expose
        private String tall;

        @SerializedName("bodytype")
        @Expose
        private String bodytype;

        @SerializedName("maritalstatus")
        @Expose
        private String maritalstatus;

        @SerializedName("children")
        @Expose
        private String children;
        @SerializedName("languagesspeak")
        @Expose
        private String languagesspeak;

        @SerializedName("education")
        @Expose
        private String education;

        @SerializedName("occupation")
        @Expose
        private String occupation;
        @SerializedName("annualincome")
        @Expose
        private String annualincome;
        @SerializedName("yourself")
        @Expose
        private String yourself;

        @SerializedName("searchingspouse")
        @Expose
        private String searchingspouse;

        @SerializedName("threequestionspotential")
        @Expose
        private String threequestionspotential;

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getDob() {
            return dob;
        }

        public void setDob(String dob) {
            this.dob = dob;
        }

        public String getTall() {
            return tall;
        }

        public void setTall(String tall) {
            this.tall = tall;
        }

        public String getBodytype() {
            return bodytype;
        }

        public void setBodytype(String bodytype) {
            this.bodytype = bodytype;
        }

        public String getMaritalstatus() {
            return maritalstatus;
        }

        public void setMaritalstatus(String maritalstatus) {
            this.maritalstatus = maritalstatus;
        }

        public String getLanguagesspeak() {
            return languagesspeak;
        }

        public void setLanguagesspeak(String languagesspeak) {
            this.languagesspeak = languagesspeak;
        }

        public String getEducation() {
            return education;
        }

        public void setEducation(String education) {
            this.education = education;
        }

        public String getOccupation() {
            return occupation;
        }

        public void setOccupation(String occupation) {
            this.occupation = occupation;
        }

        public String getAnnualincome() {
            return annualincome;
        }

        public void setAnnualincome(String annualincome) {
            this.annualincome = annualincome;
        }

        public String getYourself() {
            return yourself;
        }

        public void setYourself(String yourself) {
            this.yourself = yourself;
        }

        public String getSearchingspouse() {
            return searchingspouse;
        }

        public void setSearchingspouse(String searchingspouse) {
            this.searchingspouse = searchingspouse;
        }

        public String getThreequestionspotential() {
            return threequestionspotential;
        }

        public void setThreequestionspotential(String threequestionspotential) {
            this.threequestionspotential = threequestionspotential;
        }

        public String getChildren() {
            return children;
        }

        public void setChildren(String children) {
            this.children = children;
        }
    }

}
