package com.dies.suunis4marriage.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PersonalDataModel {

    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("data")
    @Expose
    private List<PersonalDataModel.Data> data = null;
    @SerializedName("message")
    @Expose
    private String message;

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public List<PersonalDataModel.Data> getData() {
        return data;
    }

    public void setData(List<PersonalDataModel.Data> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public class Data{

        @SerializedName("userId")
        @Expose
        private String userId;

        @SerializedName("firstname")
        @Expose
        private String firstname;

        @SerializedName("first_name")
        @Expose
        private String first_name;
        @SerializedName("lastname")
        @Expose
        private String lastname;

        @SerializedName("username")
        @Expose
        private String username;

        @SerializedName("skypeid")
        @Expose
        private String skypeid;
        @SerializedName("address")
        @Expose
        private String address;

        @SerializedName("town_city")
        @Expose
        private String town_city;

        @SerializedName("education_text")
        @Expose
        private String education_text;
        @SerializedName("occupation_text")
        @Expose
        private String occupation_text;
        @SerializedName("languagesspeak_text")
        @Expose
        private String languagesspeak_text;

        @SerializedName("country")
        @Expose
        private String country;
        @SerializedName("state")
        @Expose
        private String state;

        @SerializedName("postcode")
        @Expose
        private String postcode;

        @SerializedName("nationality")
        @Expose
        private String nationality;

        @SerializedName("ethnicity")
        @Expose
        private String ethnicity;


        @SerializedName("islamic")
        @Expose
        private String islamic;


        @SerializedName("leagalname")
        @Expose
        private String leagalname;

        @SerializedName("contactnumber")
        @Expose
        private String contactnumber;

        @SerializedName("email")
        @Expose
        private String email;

        @SerializedName("relationship")
        @Expose
        private String relationship;

        @SerializedName("profilepic")
        @Expose
        private String profilepic;


        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getFirstname() {
            return firstname;
        }

        public void setFirstname(String firstname) {
            this.firstname = firstname;
        }

        public String getLastname() {
            return lastname;
        }

        public void setLastname(String lastname) {
            this.lastname = lastname;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getSkypeid() {
            return skypeid;
        }

        public void setSkypeid(String skypeid) {
            this.skypeid = skypeid;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getTown_city() {
            return town_city;
        }

        public void setTown_city(String town_city) {
            this.town_city = town_city;
        }

        public String getEducation_text() {
            return education_text;
        }

        public void setEducation_text(String education_text) {
            this.education_text = education_text;
        }

        public String getOccupation_text() {
            return occupation_text;
        }

        public void setOccupation_text(String occupation_text) {
            this.occupation_text = occupation_text;
        }

        public String getLanguagesspeak_text() {
            return languagesspeak_text;
        }

        public void setLanguagesspeak_text(String languagesspeak_text) {
            this.languagesspeak_text = languagesspeak_text;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getPostcode() {
            return postcode;
        }

        public void setPostcode(String postcode) {
            this.postcode = postcode;
        }

        public String getNationality() {
            return nationality;
        }

        public void setNationality(String nationality) {
            this.nationality = nationality;
        }

        public String getEthnicity() {
            return ethnicity;
        }

        public void setEthnicity(String ethnicity) {
            this.ethnicity = ethnicity;
        }

        public String getIslamic() {
            return islamic;
        }

        public void setIslamic(String islamic) {
            this.islamic = islamic;
        }

        public String getLeagalname() {
            return leagalname;
        }

        public void setLeagalname(String leagalname) {
            this.leagalname = leagalname;
        }

        public String getContactnumber() {
            return contactnumber;
        }

        public void setContactnumber(String contactnumber) {
            this.contactnumber = contactnumber;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getRelationship() {
            return relationship;
        }

        public void setRelationship(String relationship) {
            this.relationship = relationship;
        }

        public String getProfilepic() {
            return profilepic;
        }

        public void setProfilepic(String profilepic) {
            this.profilepic = profilepic;
        }

        public String getFirst_name() {
            return first_name;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }
    }

}
