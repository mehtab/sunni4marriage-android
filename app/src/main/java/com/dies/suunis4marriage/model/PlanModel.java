package com.dies.suunis4marriage.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PlanModel {
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("data")
    @Expose
    private List<Data> data = null;
    @SerializedName("message")
    @Expose
    private String message;

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public class Data {

        @SerializedName("plan_id")
        @Expose
        private String plan_id;
        @SerializedName("plan_days")
        @Expose
        private String plan_days;
        @SerializedName("plan_name")
        @Expose
        private String plan_name;

        @SerializedName("plan_description")
        @Expose
        private String plan_description;

        @SerializedName("plan_amount")
        @Expose
        private String plan_amount;

        @SerializedName("plan_type")
        @Expose
        private String plan_type;

        @SerializedName("plan_sell_amount")
        @Expose
        private String plan_sell_amount;




        public String getPlan_id() {
            return plan_id;
        }

        public void setPlan_id(String plan_id) {
            this.plan_id = plan_id;
        }

        public String getPlan_days() {
            return plan_days;
        }

        public void setPlan_days(String plan_days) {
            this.plan_days = plan_days;
        }

        public String getPlan_name() {
            return plan_name;
        }

        public void setPlan_name(String plan_name) {
            this.plan_name = plan_name;
        }

        public String getPlan_description() {
            return plan_description;
        }

        public void setPlan_description(String plan_description) {
            this.plan_description = plan_description;
        }

        public String getPlan_amount() {
            return plan_amount;
        }

        public void setPlan_amount(String plan_amount) {
            this.plan_amount = plan_amount;
        }

        public String getPlan_type() {
            return plan_type;
        }

        public void setPlan_type(String plan_type) {
            this.plan_type = plan_type;
        }

        public String getPlan_sell_amount() {
            return plan_sell_amount;
        }

        public void setPlan_sell_amount(String plan_sell_amount) {
            this.plan_sell_amount = plan_sell_amount;
        }
    }
}
