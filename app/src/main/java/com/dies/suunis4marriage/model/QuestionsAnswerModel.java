package com.dies.suunis4marriage.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class QuestionsAnswerModel {

    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("data")
    @Expose
    private List<QuestionsAnswerModel.Data> data = null;
    @SerializedName("message")
    @Expose
    private String message;

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public List<QuestionsAnswerModel.Data> getData() {
        return data;
    }

    public void setData(List<QuestionsAnswerModel.Data> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public class Data{

        @SerializedName("q_id")
        @Expose
        private String q_id;
        @SerializedName("questions")
        @Expose
        private String questions;

        @SerializedName("questions1")
        @Expose
        private String questions1;

        @SerializedName("questions2")
        @Expose
        private String questions2;

        @SerializedName("status")
        @Expose
        private String status;

        @SerializedName("qm_id")
        @Expose
        private String qm_id;

        @SerializedName("user_id")
        @Expose
        private String user_id;

        @SerializedName("answers")
        @Expose
        private String answer;

        private String editTextValue,editTextValue2,editTextValue3;





        public String getQ_id() {
            return q_id;
        }

        public void setQ_id(String q_id) {
            this.q_id = q_id;
        }

        public String getQuestions() {
            return questions;
        }

        public void setQuestions(String questions) {
            this.questions = questions;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getQm_id() {
            return qm_id;
        }

        public void setQm_id(String qm_id) {
            this.qm_id = qm_id;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getEditTextValue() {
            return editTextValue;
        }

        public void setEditTextValue(String editTextValue) {
            this.editTextValue = editTextValue;
        }

        public String getAnswer() {
            return answer;
        }

        public void setAnswer(String answer) {
            this.answer = answer;
        }

        public String getQuestions1() {
            return questions1;
        }

        public void setQuestions1(String questions1) {
            this.questions1 = questions1;
        }

        public String getQuestions2() {
            return questions2;
        }

        public void setQuestions2(String questions2) {
            this.questions2 = questions2;
        }

        public String getEditTextValue2() {
            return editTextValue2;
        }

        public void setEditTextValue2(String editTextValue2) {
            this.editTextValue2 = editTextValue2;
        }

        public String getEditTextValue3() {
            return editTextValue3;
        }

        public void setEditTextValue3(String editTextValue3) {
            this.editTextValue3 = editTextValue3;
        }
    }

}
