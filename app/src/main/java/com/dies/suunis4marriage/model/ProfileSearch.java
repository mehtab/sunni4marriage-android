package com.dies.suunis4marriage.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProfileSearch {

    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("data")
    @Expose
    private List<ProfileSearch.Data> data = null;
    @SerializedName("message")
    @Expose
    private String message;


    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public class Data {

        @SerializedName("userId")
        @Expose
        private String userId;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("firstname")
        @Expose
        private String firstname;

        @SerializedName("first_name")
        @Expose
        private String first_name;
        @SerializedName("lastname")
        @Expose
        private String lastname;
        @SerializedName("contactnumber")
        @Expose
        private String contactnumber;
        @SerializedName("profilepic")
        @Expose
        private String profilepic;
        @SerializedName("gender")
        @Expose
        private String gender;
        @SerializedName("dob")
        @Expose
        private String dateofbirth;
        @SerializedName("mothertongue")
        @Expose
        private String mothertongue;
        @SerializedName("maritalstatus")
        @Expose
        private String maritalstatus;
        @SerializedName("country")
        @Expose
        private String country;
        @SerializedName("town_city")
        @Expose
        private String town_city;
        @SerializedName("nationality")
        @Expose
        private String nationality;
        @SerializedName("ethnicity")
        @Expose
        private String ethnicity;
        @SerializedName("islamic")
        @Expose
        private String islamic;
        @SerializedName("height")
        @Expose
        private String height;
        @SerializedName("bodytype")
        @Expose
        private String bodytype;
        @SerializedName("languagesspeak")
        @Expose
        private String languagesspeak;
        @SerializedName("occupation")
        @Expose
        private String occupation;
        @SerializedName("annualincome")
        @Expose
        private String annualincome;
        @SerializedName("yourself")
        @Expose
        private String aboutyourself;
        @SerializedName("searchingspouse")
        @Expose
        private String threequestionspotential;
        @SerializedName("unique_user_id")
        @Expose
        private String unique_user_id;
        @SerializedName("us_status")
        @Expose
        private String us_status;
        @SerializedName("us_datetime")
        @Expose
        private String us_datetime;
        @SerializedName("question_status")
        @Expose
        private String question_status;
        @SerializedName("request_status")
        @Expose
        private String request_status;
        @SerializedName("sl_id")
        @Expose
        private String sl_id;
        @SerializedName("connect_request_id")
        @Expose
        private String connect_request_id;
        @SerializedName("payment_status")
        @Expose
        private String payment_status;
        @SerializedName("plan_status")
        @Expose
        private String plan_status;
        @SerializedName("image11")
        @Expose
        private String multiple_image;
        @SerializedName("image12")
        @Expose
        private String image12;
        @SerializedName("image13")
        @Expose
        private String image13;
        @SerializedName("image14")
        @Expose
        private String image14;
        @SerializedName("block_status")
        @Expose
        private String block_status;
        @SerializedName("shortlist_status")
        @Expose
        private String shortlist_status;
        @SerializedName("children")
        @Expose
        private String children;
        @SerializedName("email_rep")
        @Expose
        private String email_rep;
        @SerializedName("leagalname")
        @Expose
        private String leagalname;
        @SerializedName("friend_request_id")
        @Expose
        private String friend_request_id;
        @SerializedName("message")
        @Expose
        private String messageData;
        @SerializedName("question")
        @Expose
        private String question;


        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getFirstname() {
            return firstname;
        }

        public void setFirstname(String firstname) {
            this.firstname = firstname;
        }

        public String getLastname() {
            return lastname;
        }

        public void setLastname(String lastname) {
            this.lastname = lastname;
        }

        public String getContactnumber() {
            return contactnumber;
        }

        public void setContactnumber(String contactnumber) {
            this.contactnumber = contactnumber;
        }

        public String getProfilepic() {
            return profilepic;
        }

        public void setProfilepic(String profilepic) {
            this.profilepic = profilepic;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getDateofbirth() {
            return dateofbirth;
        }

        public void setDateofbirth(String dateofbirth) {
            this.dateofbirth = dateofbirth;
        }

        public String getMothertongue() {
            return mothertongue;
        }

        public void setMothertongue(String mothertongue) {
            this.mothertongue = mothertongue;
        }

        public String getMaritalstatus() {
            return maritalstatus;
        }

        public void setMaritalstatus(String maritalstatus) {
            this.maritalstatus = maritalstatus;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getTown_city() {
            return town_city;
        }

        public void setTown_city(String town_city) {
            this.town_city = town_city;
        }

        public String getNationality() {

            if (nationality == null || nationality.equals(null) || nationality.equals("null") || nationality == "null") {
                return "";
            } else
                return nationality;

        }

        public void setNationality(String nationality) {
            if (nationality == null || nationality.equals(null) || nationality.equals("null") || nationality == "null") {
                this.nationality = "";
            } else
                this.nationality = nationality;
        }

        public String getEthnicity() {
            return ethnicity;
        }

        public void setEthnicity(String ethnicity) {
            this.ethnicity = ethnicity;
        }

        public String getIslamic() {
            return islamic;
        }

        public void setIslamic(String islamic) {
            this.islamic = islamic;
        }

        public String getHeight() {
            return height;
        }

        public void setHeight(String height) {
            this.height = height;
        }

        public String getBodytype() {
            return bodytype;
        }

        public void setBodytype(String bodytype) {
            this.bodytype = bodytype;
        }

        public String getLanguagesspeak() {
            return languagesspeak;
        }

        public void setLanguagesspeak(String languagesspeak) {
            this.languagesspeak = languagesspeak;
        }

        public String getOccupation() {
            return occupation;
        }

        public void setOccupation(String occupation) {
            this.occupation = occupation;
        }

        public String getAnnualincome() {
            return annualincome;
        }

        public void setAnnualincome(String annualincome) {
            this.annualincome = annualincome;
        }

        public String getAboutyourself() {
            return aboutyourself;
        }

        public void setAboutyourself(String aboutyourself) {
            this.aboutyourself = aboutyourself;
        }

        public String getThreequestionspotential() {
            return threequestionspotential;
        }

        public void setThreequestionspotential(String threequestionspotential) {
            this.threequestionspotential = threequestionspotential;
        }

        public String getUnique_user_id() {
            return unique_user_id;
        }

        public void setUnique_user_id(String unique_user_id) {
            this.unique_user_id = unique_user_id;
        }

        public String getUs_status() {
            return us_status;
        }

        public void setUs_status(String us_status) {
            this.us_status = us_status;
        }

        public String getUs_datetime() {
            return us_datetime;
        }

        public void setUs_datetime(String us_datetime) {
            this.us_datetime = us_datetime;
        }

        public String getQuestion_status() {
            return question_status;
        }

        public void setQuestion_status(String question_status) {
            this.question_status = question_status;
        }

        public String getRequest_status() {
            return request_status;
        }

        public void setRequest_status(String request_status) {
            this.request_status = request_status;
        }

        public String getSl_id() {
            return sl_id;
        }

        public void setSl_id(String sl_id) {
            this.sl_id = sl_id;
        }

        public String getConnect_request_id() {
            return connect_request_id;
        }

        public void setConnect_request_id(String connect_request_id) {
            this.connect_request_id = connect_request_id;
        }

        public String getPayment_status() {
            return payment_status;
        }

        public void setPayment_status(String payment_status) {
            this.payment_status = payment_status;
        }

        public String getPlan_status() {
            return plan_status;
        }

        public void setPlan_status(String plan_status) {
            this.plan_status = plan_status;
        }

        public String getMultiple_image() {
            return multiple_image;
        }

        public void setMultiple_image(String multiple_image) {
            this.multiple_image = multiple_image;
        }

        public String getImage12() {
            return image12;
        }

        public void setImage12(String image12) {
            this.image12 = image12;
        }

        public String getImage13() {
            return image13;
        }

        public void setImage13(String image13) {
            this.image13 = image13;
        }

        public String getImage14() {
            return image14;
        }

        public void setImage14(String image14) {
            this.image14 = image14;
        }

        public String getBlock_status() {
            return block_status;
        }

        public void setBlock_status(String block_status) {
            this.block_status = block_status;
        }

        public String getShortlist_status() {
            return shortlist_status;
        }

        public void setShortlist_status(String shortlist_status) {
            this.shortlist_status = shortlist_status;
        }

        public String getChildren() {
            return children;
        }

        public void setChildren(String children) {
            this.children = children;
        }

        public String getEmail_rep() {
            return email_rep;
        }

        public void setEmail_rep(String email_rep) {
            this.email_rep = email_rep;
        }

        public String getLeagalname() {
            return leagalname;
        }

        public void setLeagalname(String leagalname) {
            this.leagalname = leagalname;
        }

        public String getFirst_name() {
            return first_name;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }

        public String getFriend_request_id() {
            return friend_request_id;
        }

        public void setFriend_request_id(String friend_request_id) {
            this.friend_request_id = friend_request_id;
        }

        public String getMessageData() {
            return messageData;
        }

        public void setMessageData(String messageData) {
            this.messageData = messageData;
        }

        public String getQuestion() {
            return question;
        }

        public void setQuestion(String question) {
            this.question = question;
        }
    }
}
