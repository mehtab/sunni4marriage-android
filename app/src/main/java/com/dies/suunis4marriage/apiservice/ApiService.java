package com.dies.suunis4marriage.apiservice;

import com.dies.suunis4marriage.application.Root;
import com.dies.suunis4marriage.model.AboutYouDataModel;
import com.dies.suunis4marriage.model.CharacterDataModel;
import com.dies.suunis4marriage.model.CountryCityModel;
import com.dies.suunis4marriage.model.FamilyDataModel;
import com.dies.suunis4marriage.model.FriendModel;
import com.dies.suunis4marriage.model.NotificationModel;
import com.dies.suunis4marriage.model.PaymentModel;
import com.dies.suunis4marriage.model.PersonalDataModel;
import com.dies.suunis4marriage.model.PlanModel;
import com.dies.suunis4marriage.model.PreferenceDataModel;
import com.dies.suunis4marriage.model.ProfileBuild;
import com.dies.suunis4marriage.model.ProfileSearch;
import com.dies.suunis4marriage.model.PromoCode;
import com.dies.suunis4marriage.model.QuestionsAnswerModel;
import com.dies.suunis4marriage.model.ReligionDataModel;
import com.dies.suunis4marriage.model.UserDataResponse;
import com.dies.suunis4marriage.model.UserDetail;
import com.dies.suunis4marriage.model.UserdetailResponse;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by Kunal
 * on 5/5/2017.
 */

public interface ApiService {


    @FormUrlEncoded
    @POST("UserData/login")
    public Observable<UserDetail> LoginUserDetails(@Field("email") String email,
                                                   @Field("password") String pass,
                                                   @Field("fcm_key") String fcm_key);

    @FormUrlEncoded
    @POST("UserData/registernew")
    public Observable<UserDetail> RegisterUser(@Field("email") String email,
                                               @Field("password") String pass,
                                               @Field("fname") String fname,
                                               @Field("lastname") String lname,
                                               @Field("dob") String dob,
                                               @Field("mothertongue") String mothertongue,
                                               @Field("country") String country,
                                               @Field("profilefor") String profilefor,
                                               @Field("gender") String gender,
                                               @Field("relationship") String et_relationshiplegalrepresent,
                                               @Field("email_rep") String et_legal_re_email,
                                               @Field("contactnumber") String et_legal_re_cno,
                                               @Field("leagalname") String et_legal_re,
                                               @Field("device_id") String device_id,
                                               @Field("first_name") String first_name);

    @FormUrlEncoded
    @POST("UserData/profileupdatestep1")
    public Observable<ProfileBuild> Buildprofile(@Field("firstname") String et_fname,
                                                 @Field("lastname") String et_lname,
                                                 @Field("first_name") String first_name,
                                                 @Field("address") String et_address,
                                                 @Field("town_city") String et_city,
                                                 @Field("country") String et_country,
                                                 @Field("state") String et_state,
                                                 @Field("islamic") String islamic,
                                                 @Field("postcode") String et_postalcode,
                                                 @Field("nationality") String et_nationality,
                                                 @Field("ethnicity") String et_ethnicity,
                                                 @Field("leagalname") String et_nameoflegalrepresent,
                                                 @Field("relationship") String et_relationshiplegalrepresent,
                                                 @Field("emailaddress") String et_emaillegalrepresent,
                                                 @Field("contactnumber") String et_cnooflegalrepresent,
                                                 @Field("userId") String usrr_id);

    @FormUrlEncoded
    @POST("UserData/profileupdatestep2")
    public Observable<ProfileBuild> aboutyou(@Field("dob") String et_dob,
                                             @Field("userId") String userId,
                                             @Field("tall") String et_tall,
                                             @Field("bodytype") String et_body_type,
                                             @Field("languagesspeak") String languagesspeak,
                                             @Field("maritalstatus") String merital_sts,
                                             @Field("children") String child,
                                             @Field("education") String et_education,
                                             @Field("occupation") String et_ocuupetion,
                                             @Field("annualincome") String et_income,
                                             @Field("yourself") String et_describe_your_self,
                                             @Field("searchingspouse") String et_searching_for_spose,
                                             @Field("threequestionspotential") String et_question,
                                             @Field("userId") String usrr_id);


    @FormUrlEncoded
    @POST("UserData/profileupdatestep3")
    public Observable<ProfileBuild> CharacterStep3(@Field("howbestwould") String et_yourself,
                                                   @Field("traitswouldyouwant") String et_spuse,
                                                   @Field("questionisimportant1") String chk_impotnt1,
                                                   @Field("youexpectyourspousetodothings") String rb1,
                                                   @Field("Yourspousewouldsay2") String rb2,
                                                   @Field("questionisimportant2") String chk_impotnt2,
                                                   @Field("howimportantisittoyou") String rb3,
                                                   @Field("Yourspousewouldsay3") String rb4,
                                                   @Field("questionisimportant3") String chk_impotnt3,
                                                   @Field("whichdescribes") String rb5,
                                                   @Field("Yourspousewouldsay4") String rb6,
                                                   @Field("questionisimportant4") String chk_impotnt4,
                                                   @Field("describesyoubest5") String rb7,
                                                   @Field("Yourspousewouldsay5") String rb8,
                                                   @Field("questionisimportant5") String chk_impotnt5,
                                                   @Field("areyou6") String rb9,
                                                   @Field("areyou7") String rb10,
                                                   @Field("Yourspousewouldsay7") String rb11,
                                                   @Field("userId") String user_id);

    @FormUrlEncoded
    @POST("UserData/profileupdatestep4")
    public Observable<ProfileBuild> ReligionStep4(@Field("menandwomen") String rb1,
                                                  @Field("youspousewouldsay8") String rb2,
                                                  @Field("islamis") String rb3,
                                                  @Field("youspousewouldsay9") String rb4,
                                                  @Field("islamicclasses") String rb5,
                                                  @Field("youspousewouldsay10") String rb6,
                                                  @Field("bestdescribesyou11") String rb7,
                                                  @Field("youspousewouldsay11") String rb8,
                                                  @Field("performingprayer13") String rb11,
                                                  @Field("youspousewouldsay13") String rb12,
                                                  @Field("schoolofthought") String rb13,
                                                  @Field("importnatqestion8") String CheckImportnt1,
                                                  @Field("importnatqestion9") String CheckImportnt2,
                                                  @Field("importnatqestion10") String CheckImportnt3,
                                                  @Field("importnatqestion11") String CheckImportnt4,
                                                  @Field("importnatqestion12") String CheckImportnt5,
                                                  @Field("importnatqestion13") String CheckImportnt6,
                                                  @Field("userId") String user_id);

    @FormUrlEncoded
    @POST("UserData/profileupdatestep5")
    public Observable<ProfileBuild> FamilyStep5(@Field("closerelationshipwithyourmother") String rb1,
                                                @Field("spousewouldsay16") String rb2,
                                                @Field("relationshipwithyoursiblings") String rb3,
                                                @Field("spousewouldsay17") String rb4,
                                                @Field("3yearsofmarriage") String rb5,
                                                @Field("spousewouldsay18") String rb6,
                                                @Field("childrentobeeducated") String rb7,
                                                @Field("spousewouldsay19") String rb8,
                                                @Field("roleofawifeis") String rb9,
                                                @Field("spousewouldsay20") String rb10,
                                                @Field("TVintheirhome") String rb11,
                                                @Field("spousewouldsay21") String rb12,
                                                @Field("roleofahusbandis") String rb13,
                                                @Field("spousewouldsay22") String rb14,
                                                @Field("samehouseasyourparents") String rb15,
                                                @Field("spousewouldsay23") String rb16,
                                                @Field("samehouseasyourinlaws") String rb17,
                                                @Field("spousewouldsay24") String rb18,
                                                @Field("importnatqestion16") String CheckImportnt1,
                                                @Field("importnatqestion17") String CheckImportnt2,
                                                @Field("importnatqestion18") String CheckImportnt3,
                                                @Field("importnatqestion19") String CheckImportnt4,
                                                @Field("importnatqestion20") String CheckImportnt5,
                                                @Field("importnatqestion21") String CheckImportnt6,
                                                @Field("importnatqestion22") String CheckImportnt7,
                                                @Field("importnatqestion23") String CheckImportnt8,
                                                @Field("importnatqestion24") String CheckImportnt9,
                                                @Field("userId") String user_id);

    @FormUrlEncoded
    @POST("UserData/profileupdatestep6")
    public Observable<ProfileBuild> PrefencesStep6(@Field("cityformarriage") String rb1,
                                                   @Field("differentethnicity") String rb2,
                                                   @Field("differentcaste") String rb3,
                                                   @Field("whohasbeendivorced") String rb4,
                                                   @Field("whohaschildren") String rb5,
                                                   @Field("marrysomeoneyounger") String rb6,
                                                   @Field("marrysomeoneolder") String rb7,
                                                   @Field("samelevelofeducation") String rb8,
                                                   @Field("reverttoIslam") String rb9,
                                                   @Field("differentnationality") String rb10,
                                                   @Field("userId") String user_id);


    @FormUrlEncoded
    @POST("UserData/getUserDataForEdit")
    public Observable<PersonalDataModel> getPersonalData(@Field("userId") String user_id);

    @FormUrlEncoded
    @POST("UserData/getUserDataForStep2")
    public Observable<AboutYouDataModel> getAboutYouData(@Field("userId") String user_id);

    @FormUrlEncoded
    @POST("UserData/getUserDataForStepthree")
    public Observable<CharacterDataModel> getCharacterData(@Field("userId") String user_id);

    @FormUrlEncoded
    @POST("UserData/getUserDataForStepfour")
    public Observable<ReligionDataModel> getReligionData(@Field("userId") String user_id);

    @FormUrlEncoded
    @POST("UserData/getUserDataForStepfive")
    public Observable<FamilyDataModel> getFamilyData(@Field("userId") String user_id);

    @FormUrlEncoded
    @POST("UserData/getUserDataForStepsix")
    public Observable<PreferenceDataModel> getPreferenceData(@Field("userId") String user_id);

    @Multipart
    @POST("UserData/UploadProfileImage")
    Observable<ProfileBuild> upload_profile_image(@PartMap Map<String, RequestBody> map);

    @Multipart
    @POST("UserData/UploadProfileImage")
    Observable<ProfileBuild> upload_multiple_profile_image(@Part List<MultipartBody.Part> files,
                                                           @Part("user_id") RequestBody user_id);


    @FormUrlEncoded
    @POST("UserData/getUserData")
    public Observable<UserDataResponse> getUserDetails(@Field("email") String email);

    @FormUrlEncoded
    @POST("UserData/CheckFilterData")
    public Observable<ProfileSearch> getSearchableData(@Field("ethnicity") String ethenicity,
                                                       @Field("age") String age,
                                                       @Field("nationality") String nationality,
                                                       @Field("maritalstatus") String merital_status,
                                                       @Field("islamic") String islamic,
                                                       @Field("education") String education,
                                                       @Field("user_id") String user_id,
                                                       @Field("town_city") String town_city,
                                                       @Field("languagesspeak") String language);

    @FormUrlEncoded
    @POST("connect/UpdateFriendData")
    public Observable<FriendModel> ChangeFriendStatus(@Field("user_id") String user_id,
                                                      @Field("connect_request_user_id") String friend_id,
                                                      @Field("status") String status,
                                                      @Field("flag") String flag,
                                                      @Field("message") String message,
                                                      @Field("connect_request_id") String connect_request_id);

    @FormUrlEncoded
    @POST("connect/FriendData")
    public Observable<ProfileSearch> getConnectData(@Field("user_id") String user_id,
                                                    @Field("flag") String flag);

    @FormUrlEncoded
    @POST("connect/FriendData")
    public Observable<FriendModel> getConnectchatData(@Field("user_id") String user_id,
                                                      @Field("flag") String flag);

    @FormUrlEncoded
    @POST("connect/UpdateFriendData")
    public Observable<FriendModel> updatestatus(@Field("user_id") String user_id,
                                                @Field("connect_request_id") String request_id,
                                                @Field("flag") String flag,
                                                @Field("status") String status);

    @FormUrlEncoded
    @POST("connect/FriendData")
    public Observable<ProfileSearch> ShowFriendList(@Field("user_id") String user_id,
                                                    @Field("flag") String flag);


    @FormUrlEncoded
    @POST("connect/ShortList")
    public Observable<ProfileSearch> shortlist(@Field("sl_user_id") String sl_user_id,
                                               @Field("sl_profile_id") String sl_profile_id);

    @FormUrlEncoded
    @POST("connect/BlockList")
    public Observable<ProfileSearch> blocklist(@Field("bl_user_id") String sl_user_id,
                                               @Field("bl_profile_id") String sl_profile_id);


    @FormUrlEncoded
    @POST("connect/ShortList")
    public Observable<ProfileSearch> getshortlist(@Field("sl_user_id") String sl_user_id);

    @FormUrlEncoded
    @POST("connect/FriendData")
    public Observable<ProfileSearch> ShowShortList(@Field("user_id") String user_id,
                                                   @Field("flag") String flag);

    @FormUrlEncoded
    @POST("connect/FriendData")
    public Observable<ProfileSearch> ShowBlockList(@Field("user_id") String user_id,
                                                   @Field("flag") String flag);

    @FormUrlEncoded
    @POST("UserData/getDistance")
    public Observable<ProfileSearch> NearBy(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("UserData/CheckIdWiseFilterData")
    public Observable<ProfileSearch> SearchByUserId(@Field("uniq_id") String uniq_id,
                                                    @Field("user_id") String user_id);


    //    @FormUrlEncoded
//    @POST("UserData/suggestionFilterDataByAge")
//    Observable<ProfileSearch> getProfileData(@Field("user_id") String user_id,
//                                             @Query("index") int index);
    @FormUrlEncoded
    @POST("UserData/suggestionFilterDataByAge")
    Observable<ProfileSearch> getProfileData(@Field("user_id") String user_id);


//    http://diestechnology.com.au/projects/sunnis4marriage/app_controller/UserData/suggestionFilterDataByAge
//    suggestionFilterData

    @GET("UserData/userStatus")
    Observable<UserDetail> getStatus(@Query("user_id") String user_id,
                                     @Query("action") int index);

    //Promo/GetProfomInfo?Promo=Ali123
    @GET("Promo/GetProfomInfo")
    Observable<PromoCode> getPromoCode(@Query("Promo") String Promo);


    @FormUrlEncoded
    @POST("UserData/getQuestins")
    Observable<QuestionsAnswerModel> getQuestions(@Field("user_id") String user_id);


    @FormUrlEncoded
    @POST("UserData/addQuestion")
    Observable<QuestionsAnswerModel> setQuestions(@Field("user_id") String user_id,
                                                  @Field("questions") String id1,
                                                  @Field("questions1") String id2,
                                                  @Field("questions2") String id3,
                                                  @Field("q_id") String q_id);

    @FormUrlEncoded
    @POST("UserData/getQuestionsByUserId")
    Observable<QuestionsAnswerModel> getQuestionsData(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("UserData/submitAnswer")
    Observable<UserDetail> getAnswersData(@Field("user_id") String user_id,
                                          @Field("answer[]") ArrayList<String> answer,
                                          @Field("id[]") ArrayList<String> id);

    @FormUrlEncoded
    @POST("UserData/submitAnswer")
    Observable<QuestionsAnswerModel> setMarkQuestions(@Field("user_id") String user_id);


    @FormUrlEncoded
    @POST("braintree/checkout.php")
    Observable<PaymentModel> setpayment(@Field("amount") String amount,
                                        @Field("nonce") String nonce,
                                        @Field("user_id") String user_id);


    @FormUrlEncoded
    @POST("UserData/setPaymentInfo")
    Observable<PaymentModel> SavePaymentDetail(@Field("transaction_id") String transaction_id,
                                               @Field("currency") String currency,
                                               @Field("amount") String amount,
                                               @Field("user_id") String user_id,
                                               @Field("custom") String id,
                                               @Field("merchant_account_id") String merchant_account_id,
                                               @Field("submerchant_account_id") String s_merchant_acc_id,
                                               @Field("mastermerchant_account_id") String m_merchant_acc_id,
                                               @Field("processor_response_code") String processor_response_code,
                                               @Field("processor_response_type") String processor_response_type,
                                               @Field("payment_date") String date,
                                               @Field("payment_type") String payment_type,
                                               @Field("plan_type") String plan_type);


    @FormUrlEncoded
    @POST("connect/RemoveShortlist")
    public Observable<FriendModel> RemoveShortlist(@Field("profile_id") String profile_id,
                                                   @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("connect/RemoveBlocklist")
    public Observable<FriendModel> RemoveBlocklist(@Field("profile_id") String profile_id,
                                                   @Field("user_id") String user_id);


    @FormUrlEncoded
    @POST("UserData/getQueAnsByUserId")
    Observable<QuestionsAnswerModel> showQueAns(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("UserData/viewProfile")
    public Observable<ProfileSearch> ViewProfile(@Field("user_id") String user_id,
                                                 @Field("view_id") String flag);

    @FormUrlEncoded
    @POST("UserData/getVisitedUsers")
    public Observable<ProfileSearch> ViewedProfile(@Field("user_id") String user_id);


    @POST("UserData/getCountries")
    public Observable<CountryCityModel> getCountry();

    @FormUrlEncoded
    @POST("UserData/getCities")
    public Observable<CountryCityModel> getCity(@Field("country_id") String user_id);

    @FormUrlEncoded
    @POST("UserData/getStates")
    public Observable<CountryCityModel> getStates(@Field("country_id") String user_id);

    @FormUrlEncoded
    @POST("UserData/getCitiesByCountryName")
    public Observable<UserDetail> getImage(@Field("country_name") String user_id);

    @FormUrlEncoded
    @POST("UserData/getPlanStatusView")
    public Observable<UserDataResponse> getActivatePlan(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("UserData/getPaymentHistory")
    public Observable<PaymentModel> getPaymentHistory(@Field("user_id") String user_id);

    // @FormUrlEncoded
    @POST("UserData/getPlan")
    public Observable<PlanModel> getPlan();

    @FormUrlEncoded
    @POST("UserData/user_delete_profile")
    public Observable<UserDetail> DeleteProfile(@Field("user_id") String user_id);


    @Multipart
    @POST("UserData/Upload_ProfileImage")
    Observable<ProfileBuild> upload_multi_profile_image(
            @Header("Authorization") String authorization,
            @PartMap Map<String, RequestBody> map);


    @FormUrlEncoded
    @POST("UserData/getnewmiles")
    public Observable<ProfileSearch> getNearme(@Field("user_id") String user_id,
                                               @Field("start_latitude") String start_latitude,
                                               @Field("start_longitude") String start_longitude,
                                               @Field("minage") String minage,
                                               @Field("maxage") String maxage,
                                               @Field("miles") String miles);

    @FormUrlEncoded
    @POST("UserData/Updatelatlong")
    public Observable<UserDetail> UpdateLatLong(@Field("user_id") String user_id,
                                                @Field("latitude") double start_latitude,
                                                @Field("longitude") double start_longitude);

    @FormUrlEncoded
    @POST("UserData/changePassword")
    public Observable<UserDetail> ChangePassword(@Field("oldPassword") String old_password,
                                                 @Field("newPassword") String new_password,
                                                 @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("UserData/deleteQuestion")
    Observable<QuestionsAnswerModel> deleteQuestions(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("UserData/getOTP")
    public Observable<UserdetailResponse> getOTP(@Field("mail") String mail);

    @FormUrlEncoded
    @POST("UserData/CheckOTPMail")
    public Observable<UserdetailResponse> check_otp_mail(@Field("email") String email,
                                                         @Field("otp") String otp);

    @FormUrlEncoded
    @POST("UserData/updatePwd")
    public Observable<UserdetailResponse> UpdatePwd(@Field("email") String email,
                                                    @Field("password") String password);

    @FormUrlEncoded
    @POST("UserData/getNotificationData")
    public Observable<NotificationModel> getNotification(@Field("user_id") String user_id);


    @FormUrlEncoded
    @POST("UserData/RemoveImage")
    public Observable<UserDataResponse> RemoveImage(@Field("user_id") String user_id,
                                                    @Field("image11") String img,
                                                    @Field("image12") String img1,
                                                    @Field("image13") String img2,
                                                    @Field("image14") String img3);

    @FormUrlEncoded
    @POST("UserData/createPaymentIntent")
    public Observable<Root> paymentIntent(@Field("customer_id") String customerID,
                                          @Field("amount") int amount,
                                          @Field("currency") String currency,
                                          @Field("payment_method_types") String method);
//    http://diestechnology.com.au/projects/sunnis4marriage/app_controller/UserData/getCitiesByCountryName

}
