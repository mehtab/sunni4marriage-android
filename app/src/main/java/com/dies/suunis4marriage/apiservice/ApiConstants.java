package com.dies.suunis4marriage.apiservice;

/**
 * Created by Harsh
 * on 4/2/2019.
 */

public class ApiConstants {

    //public static final String IMAGE_URL ="http://diestechnology.com.au/projects/sunnis4marriage/";
    public static final String IMAGE_URL = "https://sunnis4marriage.com/Appadmin/";
    //public static final String BASE_URL = "http://diestechnology.com.au/projects/sunnis4marriage/app_controller/";
    public static final String BASE_URL = "https://sunnis4marriage.com/Appadmin/app_controller/";
    public static final String BASE_URLNEW = "https://diestechnology.com.au/projects/";

}
