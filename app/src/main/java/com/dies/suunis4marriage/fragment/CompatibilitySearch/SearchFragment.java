package com.dies.suunis4marriage.fragment.CompatibilitySearch;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarFinalValueListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar;
import com.dies.suunis4marriage.R;
import com.dies.suunis4marriage.activity.SearchProfile;
import com.dies.suunis4marriage.apiservice.ApiService;
import com.dies.suunis4marriage.apiservice.ApiServiceCreator;
import com.dies.suunis4marriage.application.SessionManager;
import com.thomashaertel.widget.MultiSpinner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class SearchFragment extends Fragment {


    @BindView(R.id.et_ethnicities)
    EditText et_ethnicities;
    @BindView(R.id.et_location)
    EditText et_location;
    @BindView(R.id.et_lang)
    EditText et_lang;
    @BindView(R.id.et_education_level)
    EditText et_education_level;
    @BindView(R.id.et_nationality)
    EditText et_nationality;
    @BindView(R.id.et_merital_status)
    EditText et_merital_status;
    @BindView(R.id.et_children)
    EditText et_children;
    @BindView(R.id.et_allislamic)
    EditText et_allislamic;
    @BindView(R.id.btn_search)
    Button btn_search;
    @BindView(R.id.btn_clear)
    Button btn_clear;
    @BindView(R.id.spin_ethnicities)
    MultiSpinner spin_ethnicities;
    @BindView(R.id.spin_language)
    MultiSpinner spin_language;

    SessionManager sessionManager;
    ApiService apiservice;
    ProgressDialog pDialog;
    RecyclerView.LayoutManager layoutManager;
    int statusCode;
    String minvalue, maxvalue;
    String result;
    CrystalRangeSeekbar rangeSeekbar;
    String selected_city = "";
    String selected_ethenicity = "", selected_language = "";
    String[] City;
    String[] selected_aboutyou;
    boolean[] selectedItemsFriend;
    String[] All_Ethenicity, Location, Nationality, Merital_status, Children, EducationLevel, language, Islamic;
    String Selection, location, merital_status, nationality, e_level, lang, children, islamic;
    AlertDialog alertDialog1;
    private ArrayAdapter<String> spin_city_adapter, spin_lang_adapter;
    private MultiSpinner.MultiSpinnerListener onSelected_eath = new MultiSpinner.MultiSpinnerListener() {
        public void onItemsSelected(boolean[] selected) {
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < selected.length; i++) {
                if (selected[i]) {
                    builder.append(spin_city_adapter.getItem(i)).append(",");
                }
            }
            selected_ethenicity = builder.toString();


        }
    };

    private MultiSpinner.MultiSpinnerListener onSelected_language = new MultiSpinner.MultiSpinnerListener() {
        public void onItemsSelected(boolean[] selected) {
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < selected.length; i++) {
                if (selected[i]) {
                    builder.append(spin_lang_adapter.getItem(i)).append(",");
                }
            }
            selected_language = builder.toString();
            selected_language = selected_language.replaceAll(",$", "");
        }
    };

    public SearchFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_search, container, false);
        ButterKnife.bind(this, view);
        sessionManager = new SessionManager(getActivity());
        apiservice = ApiServiceCreator.createService("latest");
        rangeSeekbar = (CrystalRangeSeekbar) view.findViewById(R.id.rangeSeekbar1);
        final TextView tvMin = (TextView) view.findViewById(R.id.textMin1);
        final TextView tvMax = (TextView) view.findViewById(R.id.textMax1);
        //

        City = getResources().getStringArray(R.array.india_top_places);
        All_Ethenicity = getResources().getStringArray(R.array.All_Ethenicity);
        Location = getResources().getStringArray(R.array.Location);
        Nationality = getResources().getStringArray(R.array.Nationality);
        Merital_status = getResources().getStringArray(R.array.Merital_status);
        Children = getResources().getStringArray(R.array.Children);
        EducationLevel = getResources().getStringArray(R.array.Education_level);
        language = getResources().getStringArray(R.array.language);
        Islamic = getResources().getStringArray(R.array.Islamic);


        et_ethnicities.setOnClickListener(view1 -> {
            CreateAlertDialogWithRadioButtonGroup();
        });
        et_location.setOnClickListener(view1 -> {
            CreateAlertDialogWithRadioButtonGroupLocation();
        });
        et_education_level.setOnClickListener(view1 -> {
            CreateAlertDialogWithRadioButtonGroupEducationLevel();
        });
        et_allislamic.setOnClickListener(view1 -> {
            CreateAlertDialogWithRadioButtonGroupLanguageIslamic();
        });
        et_lang.setOnClickListener(view1 -> {
            CreateAlertDialogWithRadioButtonGroupLanguage();
        });
        et_merital_status.setOnClickListener(view1 -> {
            CreateAlertDialogWithRadioButtonGroupMeritalStatus();
        });
        et_nationality.setOnClickListener(view1 -> {
            CreateAlertDialogWithRadioButtonGroupNationality();
        });
        et_children.setOnClickListener(view1 -> {
            CreateAlertDialogWithRadioButtonGroupChildren();
        });
        btn_clear.setOnClickListener(view1 -> {
            clearData();
        });
        et_merital_status.setText(sessionManager.getMerital_status());
        et_ethnicities.setText(sessionManager.getEthenicity());
        et_nationality.setText(sessionManager.getNationality());
        et_allislamic.setText(sessionManager.getIslamic());
        

        rangeSeekbar.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {
                tvMin.setText("Min " + String.valueOf(minValue) + " Yrs");
                tvMax.setText("Max " + String.valueOf(maxValue) + " Yrs");
            }
        });


// set final value listener
        rangeSeekbar.setOnRangeSeekbarFinalValueListener(new OnRangeSeekbarFinalValueListener() {
            @Override
            public void finalValue(Number minValue, Number maxValue) {
                Log.d("CRS=>", String.valueOf(minValue) + " : " + String.valueOf(maxValue));
                minvalue = String.valueOf(minValue);
                maxvalue = String.valueOf(maxValue);
            }
        });

        btn_search.setOnClickListener(view1 -> {
            getValue();
            Intent intent = new Intent(getActivity(), SearchProfile.class);
            intent.putExtra("Search", "multiple_search");
            startActivity(intent);
        });

        spin_lang_adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, language);
        spin_language.setAdapter(spin_lang_adapter, false, onSelected_language);
        spin_language.setHint("Select Language's");

        spin_city_adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, All_Ethenicity);
        spin_ethnicities.setAdapter(spin_city_adapter, false, onSelected_eath);
        spin_ethnicities.setHint("Select Ethnicity");


        // clearData();
        SetValue();
        return view;


    }

    public void clearData() {
        sessionManager.ClearSearchData();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.detach(SearchFragment.this).attach(SearchFragment.this).commit();
        et_allislamic.getText().clear();
        et_education_level.getText().clear();
        et_merital_status.getText().clear();
        et_nationality.getText().clear();
        et_location.getText().clear();
        et_ethnicities.getText().clear();
        et_lang.getText().clear();
        et_children.getText().clear();
        minvalue = null;
        maxvalue = null;
        selected_city = "";
    }

    public void CreateAlertDialogWithRadioButtonGroup() {
        int pos = -1;
        if (!et_ethnicities.getText().toString().equals("")) {
            String data = et_ethnicities.getText().toString();

            for (int i = 0; i < All_Ethenicity.length; i++) {
                if (All_Ethenicity[i].contains(data)) {
                    pos = i;
                }

            }

        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Select Your Choice");
        builder.setSingleChoiceItems(All_Ethenicity, pos, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                Selection = (String) All_Ethenicity[item];
            }
        });

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Toast.makeText(getActivity(), Selection, Toast.LENGTH_SHORT).show();
                et_ethnicities.setText(Selection);
                //alertDialog1.dismiss();
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                alertDialog1.dismiss();
            }
        });

        builder.setNeutralButton("clear", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                et_ethnicities.getText().clear();
                sessionManager.setEthenicity(null);

            }
        });
        alertDialog1 = builder.create();
        alertDialog1.show();

    }


    public void CreateAlertDialogWithRadioButtonGroupLocation() {
        int pos = -1;
        if (!et_location.getText().toString().equals("")) {
            String data = et_location.getText().toString();

            for (int i = 0; i < Location.length; i++) {
                if (Location[i].contains(data)) {
                    pos = i;
                }

            }

        }
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Select Location");
        builder.setSingleChoiceItems(Location, pos, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                location = (String) Location[item];
            }
        });

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //Toast.makeText(getActivity(), location, Toast.LENGTH_SHORT).show();
                et_location.setText(location);
                //alertDialog1.dismiss();
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                alertDialog1.dismiss();
            }
        });


        builder.setNeutralButton("clear", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                et_location.getText().clear();
                sessionManager.setLocation(null);

            }
        });
        alertDialog1 = builder.create();
        alertDialog1.show();
    }


    public void CreateAlertDialogWithRadioButtonGroupLanguage() {

        int pos = -1;
        if (!et_lang.getText().toString().equals("")) {
            String data = et_lang.getText().toString();

            for (int i = 0; i < language.length; i++) {
                if (language[i].contains(data)) {
                    pos = i;
                }

            }

        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Select Location");
        builder.setSingleChoiceItems(language, pos, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                lang = (String) language[item];
            }
        });

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //  Toast.makeText(getActivity(), lang, Toast.LENGTH_SHORT).show();
                et_lang.setText(lang);
                //alertDialog1.dismiss();
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                alertDialog1.dismiss();
            }
        });


        builder.setNeutralButton("clear", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                et_lang.getText().clear();
                sessionManager.setLanguage(null);

            }
        });

        alertDialog1 = builder.create();
        alertDialog1.show();
    }

    public void CreateAlertDialogWithRadioButtonGroupEducationLevel() {

        int pos = -1;
        if (!et_education_level.getText().toString().equals("")) {
            String data = et_education_level.getText().toString();
            for (int i = 0; i < EducationLevel.length; i++) {
                if (EducationLevel[i].contains(data)) {
                    pos = i;
                }

            }
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Select Education Level");
        builder.setSingleChoiceItems(EducationLevel, pos, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                e_level = (String) EducationLevel[item];
            }
        });

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //   Toast.makeText(getActivity(), e_level, Toast.LENGTH_SHORT).show();
                et_education_level.setText(e_level);
                //alertDialog1.dismiss();
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                alertDialog1.dismiss();
            }
        });

        builder.setNeutralButton("clear", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                et_education_level.getText().clear();
                sessionManager.setEducationLevel(null);

            }
        });
        alertDialog1 = builder.create();
        alertDialog1.show();

    }


    public void CreateAlertDialogWithRadioButtonGroupNationality() {

        int pos = -1;
        if (!et_nationality.getText().toString().equals("")) {
            String data = et_nationality.getText().toString();

            for (int i = 0; i < Nationality.length; i++) {
                if (Nationality[i].contains(data)) {
                    pos = i;
                }

            }

        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Select Nationality");
        builder.setSingleChoiceItems(Nationality, pos, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                nationality = (String) Nationality[item];
            }
        });

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //  Toast.makeText(getActivity(), nationality, Toast.LENGTH_SHORT).show();
                et_nationality.setText(nationality);
                //alertDialog1.dismiss();
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                alertDialog1.dismiss();
            }
        });

        builder.setNeutralButton("clear", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                et_nationality.getText().clear();
                sessionManager.setNationality(null);

            }
        });
        alertDialog1 = builder.create();
        alertDialog1.show();

    }

    public void CreateAlertDialogWithRadioButtonGroupMeritalStatus() {

        int pos = -1;
        if (!et_merital_status.getText().toString().equals("")) {
            String data = et_merital_status.getText().toString();

            for (int i = 0; i < Merital_status.length; i++) {
                if (Merital_status[i].contains(data)) {
                    pos = i;
                }
            }
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Select Marital Status");
        builder.setSingleChoiceItems(Merital_status, pos, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                merital_status = (String) Merital_status[item];
            }
        });

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                // Toast.makeText(getActivity(), merital_status, Toast.LENGTH_SHORT).show();
                et_merital_status.setText(merital_status);
                //alertDialog1.dismiss();
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                alertDialog1.dismiss();
            }
        });


        builder.setNeutralButton("clear", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                et_merital_status.getText().clear();
                sessionManager.setMerital_status(null);
            }
        });
        alertDialog1 = builder.create();
        alertDialog1.show();

    }

    public void CreateAlertDialogWithRadioButtonGroupChildren() {

        int pos = -1;
        if (!et_children.getText().toString().equals("")) {
            String data = et_children.getText().toString();

            for (int i = 0; i < Children.length; i++) {
                if (Children[i].contains(data)) {
                    pos = i;
                }

            }

        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Select Children");
        builder.setSingleChoiceItems(Children, pos, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                children = (String) Children[item];
            }
        });

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                // Toast.makeText(getActivity(), children, Toast.LENGTH_SHORT).show();
                et_children.setText(children);
                //alertDialog1.dismiss();
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                alertDialog1.dismiss();
            }
        });
        builder.setNeutralButton("clear", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                et_children.getText().clear();
            }
        });

        alertDialog1 = builder.create();
        alertDialog1.show();
    }

    public void CreateAlertDialogWithRadioButtonGroupLanguageIslamic() {

        int pos = -1;
        if (!et_allislamic.getText().toString().equals("")) {
            String data = et_allislamic.getText().toString();

            for (int i = 0; i < Islamic.length; i++) {
                if (Islamic[i].contains(data)) {
                    pos = i;
                }

            }

        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("All Islamic Background");
        builder.setSingleChoiceItems(Islamic, pos, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                islamic = (String) Islamic[item];
            }
        });

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //  Toast.makeText(getActivity(), islamic, Toast.LENGTH_SHORT).show();
                et_allislamic.setText(islamic);
                //alertDialog1.dismiss();
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                alertDialog1.dismiss();
            }
        });

        builder.setNeutralButton("clear", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                et_allislamic.getText().clear();
                sessionManager.setIslamic(null);

            }
        });
        alertDialog1 = builder.create();
        alertDialog1.show();

    }


    public void getValue() {

        if (!selected_ethenicity.equals("")) {
            //   sessionManager.setEthenicity(et_ethnicities.getText().toString());
            sessionManager.setEthenicity(selected_ethenicity);
        } else {
            sessionManager.setEthenicity("");
        }
        if (!selected_language.equals("")) {
            sessionManager.setLanguage(selected_language);
        } else {
            sessionManager.setLanguage("");
        }
        if (!et_location.getText().toString().equals("")) {
            //  if (!selected_city.equals("") || selected_city!=null){
            sessionManager.setLocation(et_location.getText().toString());
            //   sessionManager.setLocation(selected_city);
            // Toast.makeText(getActivity(), selected_city, Toast.LENGTH_SHORT).show();
        }
        if (!et_nationality.getText().toString().equals("")) {
            sessionManager.setNationality(et_nationality.getText().toString());
        }

        if (!et_merital_status.getText().toString().equals("")) {
            sessionManager.setMerital_status(et_merital_status.getText().toString());
        }
        if (!et_allislamic.getText().toString().equals("")) {
            sessionManager.setIslamic(et_allislamic.getText().toString());
        }
        if (!et_education_level.getText().toString().equals("")) {
            sessionManager.setEducationLevel(et_education_level.getText().toString());
        }
        if (minvalue != null && maxvalue != null) {
            sessionManager.setMinValue(minvalue);
            sessionManager.setMaxValue(maxvalue);
            List<String> list = Arrays.asList(minvalue, maxvalue);
            List<String> listdata = new ArrayList<>();
            listdata.add(minvalue);
            listdata.add(maxvalue);
            // if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            //String result = String.join(",", list);
            String result = TextUtils.join(",", list);
            sessionManager.setMinMaxValue(result);
            //   }
        }
//            sessionManager.setMaxValue(maxvalue);
    }

    public void SetValue() {
        if (sessionManager.getEthenicity() != null) {
            //spin_ethnicities.setText(sessionManager.getEthenicity());
            et_ethnicities.setText(sessionManager.getEthenicity());
        }

        if (sessionManager.getLanguage() != null) {
            //   spin_language.setText(sessionManager.getLanguage());
            et_ethnicities.setText(sessionManager.getLanguage());
        }


        if (sessionManager.getLocation() != null) {
            //  et_location.setText(sessionManager.getLocation());
            String str = sessionManager.getLocation();
            List<String> items = Arrays.asList(str.split("\\s*,\\s*"));
            selected_aboutyou = items.toArray(new String[items.size()]);


            Integer array_length = City.length;
            selectedItemsFriend = new boolean[City.length];

            for (int i = 0; i < selected_aboutyou.length; i++) {

                for (int j = 0; j < City.length; j++) {
                    if ((selected_aboutyou[i].equals(City[j]))) {
                        selectedItemsFriend[j] = true;
                        break;
                    }
                }
            }
            if (selected_aboutyou.length > 0) {
                try {
                    //spin_city.setSelected(selectedItemsFriend);
                } catch (Exception e) {
                }
            }


            selected_city = sessionManager.getLocation();
            et_location.setText(sessionManager.getLocation());


        }
        if (sessionManager.getNationality() != null) {
            //et_nationality.setText(sessionManager.getNationality());  mim change
            et_nationality.setText("");
        }
        if (sessionManager.getMerital_status() != null) {
            et_merital_status.setText(sessionManager.getMerital_status());
        }
        if (sessionManager.getEducationLevel() != null) {
            et_education_level.setText(sessionManager.getEducationLevel());
        }
        if (sessionManager.getIslamic() != null) {
            et_allislamic.setText(sessionManager.getIslamic());
        }
        //Toast.makeText(getActivity(), sessionManager.getMinValue()+sessionManager.getMaxValue(), Toast.LENGTH_SHORT).show();
        if (sessionManager.getMinValue() != null && !sessionManager.getMinValue().equals("") && sessionManager.getMaxValue() != null && !sessionManager.getMaxValue().equals("")) {
            rangeSeekbar.setMinStartValue(Float.parseFloat(sessionManager.getMinValue())).apply();
            rangeSeekbar.setMaxStartValue(Float.parseFloat(sessionManager.getMaxValue())).apply();
        }


    }

}
