package com.dies.suunis4marriage.fragment;


import android.app.ProgressDialog;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dies.suunis4marriage.R;
import com.dies.suunis4marriage.apiservice.ApiService;
import com.dies.suunis4marriage.apiservice.ApiServiceCreator;
import com.dies.suunis4marriage.application.SessionManager;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class RecentlyAddFragment extends Fragment {


    public RecentlyAddFragment() {
        // Required empty public constructor
    }

    @BindView(R.id.rcv_recent)
    RecyclerView rcv_recent;

    SessionManager sessionManager;
    ApiService apiservice;
    ProgressDialog pDialog;
    RecyclerView.LayoutManager layoutManager;
    int statusCode;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_recently_add, container, false);
        ButterKnife.bind(this,view);
        sessionManager = new SessionManager(getActivity());
        apiservice = ApiServiceCreator.createService("latest");



        return view;


    }

}
