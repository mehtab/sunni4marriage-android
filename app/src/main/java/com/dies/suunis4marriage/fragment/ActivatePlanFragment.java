package com.dies.suunis4marriage.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dies.suunis4marriage.R;
import com.dies.suunis4marriage.adapter.ActivatePlanAdapter;
import com.dies.suunis4marriage.apiservice.ApiService;
import com.dies.suunis4marriage.apiservice.ApiServiceCreator;
import com.dies.suunis4marriage.application.SessionManager;
import com.dies.suunis4marriage.model.UserDataResponse;

import java.net.SocketTimeoutException;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observable;
import rx.Observer;
import rx.schedulers.Schedulers;

public class ActivatePlanFragment extends Fragment {


    SessionManager sessionManager;
    ApiService apiservice;
    ProgressDialog pDialog;
    RecyclerView.LayoutManager layoutManager;
    int statusCode;

//    @BindView(R.id.txt_plan_type)
//    TextView txt_plan_type;
//    @BindView(R.id.txt_start_plan_date)
//    TextView txt_start_plan_date;
//    @BindView(R.id.txt_end_plan_date)
//    TextView txt_end_plan_date;
//    @BindView(R.id.txt_plan_price)
//    TextView txt_plan_price;
//    @BindView(R.id.txt_days_remaining)
//    TextView txt_days_remaining;
//    @BindView(R.id.txt_days)
//    TextView txt_days;
//
//    @BindView(R.id.lnr_plan)
//    LinearLayout lnr_plan;
//    @BindView(R.id.txt_plan_activate)
//    TextView txt_plan_activate;

    @BindView(R.id.rcv_plan)
    RecyclerView rcv_plan;



    public ActivatePlanFragment() {
        // Required empty public constructor
    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_activate_plan, container, false);
        ButterKnife.bind(this,view);
        sessionManager = new SessionManager(getActivity());
        apiservice = ApiServiceCreator.createService("latest");


//        if (sessionManager.isPlanActivate()){
//            getActivatePlan();
//        }else {
//            txt_plan_activate.setVisibility(View.VISIBLE);
//            lnr_plan.setVisibility(View.GONE);
//        }

        getActivatePlan();

        return view;
    }


    public void getActivatePlan(){

        rcv_plan.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity());
        rcv_plan.setLayoutManager(layoutManager);
        pDialog = new ProgressDialog(getActivity());
        pDialog.setTitle("Checking Data");
        pDialog.setMessage("Please Wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        Observable<UserDataResponse> responseObservable = apiservice.getActivatePlan(sessionManager.getKeyId());

        responseObservable.subscribeOn(Schedulers.newThread())
                .observeOn(rx.android.schedulers.AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> {
                    if (throwable instanceof retrofit2.HttpException) {
                        retrofit2.HttpException ex = (retrofit2.HttpException) throwable;
                        statusCode = ex.code();
                        Log.e("error", ex.getLocalizedMessage());
                    } else if (throwable instanceof SocketTimeoutException) {
                        statusCode = 1000;
                    }
                    return Observable.empty();
                })
                .subscribe(new Observer<UserDataResponse>() {
                    @Override
                    public void onCompleted() {
                        pDialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("error", "" + e.getMessage());
                    }

                    @Override
                    public void onNext(UserDataResponse userDataResponse) {
                        statusCode = userDataResponse.getStatusCode();
                        if (statusCode == 200) {
                            ActivatePlanAdapter adapter=new ActivatePlanAdapter(userDataResponse.getData(),getActivity());
                            rcv_plan.setAdapter(adapter);

//                            lnr_plan.setVisibility(View.VISIBLE);
//                            txt_plan_type.setText(userDataResponse.getData().get(0).getPlan_type());
//                            txt_start_plan_date.setText(parseDateToddMMyyyy(userDataResponse.getData().get(0).getPlan_start_date()));
//                            txt_end_plan_date.setText(parseDateToddMMyyyy(userDataResponse.getData().get(0).getPlan_end_date()));
//                            txt_plan_price.setText("£"+userDataResponse.getData().get(0).getPlan_price());
//                            txt_days.setText(userDataResponse.getData().get(0).getPlan_days()+" Days");


//                            try {
//                                String CurrentDate= parseDateToddMMyyyy(userDataResponse.getData().get(0).getPlan_start_date());
//                                String FinalDate= parseDateToddMMyyyy(userDataResponse.getData().get(0).getPlan_end_date());
//                                Date date1;
//                                Date date2;
//                                SimpleDateFormat dates = new SimpleDateFormat("dd-MM-yyyy");
//                                date1 = dates.parse(CurrentDate);
//                                date2 = dates.parse(FinalDate);
//                                long difference = Math.abs(date1.getTime() - date2.getTime());
//                                long differenceDates = difference / (24 * 60 * 60 * 1000);
//                                String dayDifference = Long.toString(differenceDates);
//                                txt_days_remaining.setText(dayDifference + " days");
//                            } catch (Exception exception) {
//                                Toast.makeText(getActivity(), "Unable to find difference"+exception, Toast.LENGTH_SHORT).show();
//                                Log.e("errorrooror",exception.toString());
//                            }

//                            long msDiff = Calendar.getInstance().getTimeInMillis() - testCalendar.getTimeInMillis();
//                            long daysDiff = TimeUnit.MILLISECONDS.toDays(msDiff);
//                        } else {
//                            txt_plan_activate.setVisibility(View.VISIBLE);
//                            lnr_plan.setVisibility(View.GONE);
//                            Utility.displayToast(getActivity(),userDataResponse.getMessage());
                        }
                    }
                });

    }

//    public String parseDateToddMMyyyy(String time) {
//        String inputPattern = "yyyy-MM-dd HH:mm:ss";
//        String outputPattern = "dd-MM-yyyy";
//        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
//        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
//
//        Date date = null;
//        String str = null;
//
//        try {
//            date = inputFormat.parse(time);
//            str = outputFormat.format(date);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        return str;
//    }

}
