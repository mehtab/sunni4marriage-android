package com.dies.suunis4marriage.fragment;


import android.app.ProgressDialog;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dies.suunis4marriage.R;
import com.dies.suunis4marriage.adapter.PlanAdapter;
import com.dies.suunis4marriage.apiservice.ApiService;
import com.dies.suunis4marriage.apiservice.ApiServiceCreator;
import com.dies.suunis4marriage.application.SessionManager;
import com.dies.suunis4marriage.model.PlanModel;

import java.net.SocketTimeoutException;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observable;
import rx.Observer;
import rx.schedulers.Schedulers;

/**
 * A simple {@link Fragment} subclass.
 */
public class AllPlanFragment extends Fragment {

//    @BindView(R.id.btn_pln)
//    Button btn_pln;
    SessionManager sessionManager;
    ApiService apiservice;
    ProgressDialog pDialog;
    RecyclerView.LayoutManager layoutManager;
    int statusCode;
    @BindView(R.id.rcv_plan)
    RecyclerView rcv_plan;

//    @BindView(R.id.rcv_firend)
//    RecyclerView rcv_plan;


    public AllPlanFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view1=inflater.inflate(R.layout.fragment_all_plan, container, false);
        ButterKnife.bind(this,view1);
        sessionManager = new SessionManager(getActivity());
        apiservice = ApiServiceCreator.createService("latest");


        getPlan();

        return view1;
    }

    public void getPlan(){
        rcv_plan.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity());
        rcv_plan.setLayoutManager(layoutManager);
        pDialog = new ProgressDialog(getActivity());
        pDialog.setTitle("Checking Data");
        pDialog.setMessage("Please Wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        Observable<PlanModel> responseObservable = apiservice.getPlan();

        responseObservable.subscribeOn(Schedulers.newThread())
                .observeOn(rx.android.schedulers.AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> {
                    if (throwable instanceof retrofit2.HttpException) {
                        retrofit2.HttpException ex = (retrofit2.HttpException) throwable;
                        statusCode = ex.code();
                        Log.e("error", ex.getLocalizedMessage());
                    } else if (throwable instanceof SocketTimeoutException) {
                        statusCode = 1000;
                    }
                    return Observable.empty();
                })
                .subscribe(new Observer<PlanModel>() {
                    @Override
                    public void onCompleted() {
                        pDialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("error", "" + e.getMessage());
                    }

                    @Override
                    public void onNext(PlanModel planModel) {
                        statusCode = planModel.getStatusCode();
                        if (statusCode == 200) {
                            PlanAdapter planAdapter=new PlanAdapter(planModel.getData(),getActivity());
                            rcv_plan.setAdapter(planAdapter);
                        }
                    }
                });

    }
}
