//package com.dies.suunis4marriage.fragment.CompatibilitySearch;
//
//
//import android.app.ProgressDialog;
//import android.os.Bundle;
//import android.os.Handler;
//import android.support.v4.app.Fragment;
//import android.support.v7.widget.DefaultItemAnimator;
//import android.support.v7.widget.LinearLayoutManager;
//import android.support.v7.widget.RecyclerView;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ProgressBar;
//
//import com.dies.suunis4marriage.PaginationScrollListener;
//import com.dies.suunis4marriage.R;
//import com.dies.suunis4marriage.adapter.ExampleAdapter;
//import com.dies.suunis4marriage.adapter.ProfileAdapter;
//import com.dies.suunis4marriage.apiservice.ApiService;
//import com.dies.suunis4marriage.apiservice.ApiServiceCreator;
//import com.dies.suunis4marriage.application.SessionManager;
//import com.dies.suunis4marriage.model.ProfileBuild;
//import com.dies.suunis4marriage.model.ProfileSearch;
//
//import java.net.SocketTimeoutException;
//import java.util.ArrayList;
//import java.util.List;
//
//import butterknife.BindView;
//import butterknife.ButterKnife;
//import rx.Observable;
//import rx.Observer;
//import rx.schedulers.Schedulers;
//
///**
// * A simple {@link Fragment} subclass.
// */
//public class TodaystaticFragment extends Fragment {
//
//
//
//    SessionManager sessionManager;
//    ApiService apiservice;
//    ProgressDialog pDialog;
//    //RecyclerView.LayoutManager layoutManager;
//    LinearLayoutManager layoutManager;
//    @BindView(R.id.rcv_profile)
//    RecyclerView rcv_profile;
//    int statusCode;
//    int index=1;
//    ExampleAdapter adapter;
//    String fragment_name="search_friend";
//    List<ProfileSearch.Data> profile;
//    ProfileSearch profileSearch;
//
//    List<ProfileSearch> movies;
//
//    private int TOTAL_PAGES = 1;
//    private static final int PAGE_START = 1;
//    private int currentPage = PAGE_START;
//    private boolean isLoading = false;
//    private boolean isLastPage = false;
//    ProgressBar progressBar;
//
//
//    public TodaystaticFragment() {
//        // Required empty public constructor
//    }
//
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
//        // Inflate the layout for this fragment
//        View view=inflater.inflate(R.layout.fragment_todaystatic, container, false);
//
//        ButterKnife.bind(this,view);
//        sessionManager = new SessionManager(getActivity());
//        apiservice = ApiServiceCreator.createService("latest");
//
//        adapter=new ExampleAdapter(getActivity(),"search_friend");
//
//        layoutManager = new LinearLayoutManager(getActivity());
//        rcv_profile.setLayoutManager(layoutManager);
//
//
//        rcv_profile.setItemAnimator(new DefaultItemAnimator());
//
//        rcv_profile.setAdapter(adapter);
//        rcv_profile.addOnScrollListener(new PaginationScrollListener(layoutManager) {
//            @Override
//            protected void loadMoreItems() {
//                isLoading = true;
//                currentPage += 1;
//
//                // mocking network delay for API call
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        loadNextPage();
//                    }
//                }, 1000);
//            }
//
//            @Override
//            public int getTotalPageCount() {
//                return TOTAL_PAGES;
//            }
//
//            @Override
//            public boolean isLastPage() {
//                return isLastPage;
//            }
//
//            @Override
//            public boolean isLoading() {
//                return isLoading;
//            }
//        });
//
//        loadFirstPage();
//
//
//        return view;
//    }
//
//    private void loadFirstPage() {
//        //Log.d(TAG, "loadFirstPage: ");
//      //  currentPage = PAGE_START;
//        Observable<ProfileSearch> responseObservable = apiservice.getProfileData(sessionManager.getKeyId(),1);
//
//        responseObservable.subscribeOn(Schedulers.newThread())
//                .observeOn(rx.android.schedulers.AndroidSchedulers.mainThread())
//                .onErrorResumeNext(throwable -> {
//                    if (throwable instanceof retrofit2.HttpException) {
//                        retrofit2.HttpException ex = (retrofit2.HttpException) throwable;
//                        statusCode = ex.code();
//                        Log.e("error", ex.getLocalizedMessage());
//                    } else if (throwable instanceof SocketTimeoutException) {
//                        statusCode = 1000;
//                    }
//                    return Observable.empty();
//                })
//                .subscribe(new Observer<ProfileSearch>() {
//                    @Override
//                    public void onCompleted() {
//                        //  pDialog.dismiss();
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                        Log.e("error", "" + e.getMessage());
//                    }
//
//                    @Override
//                    public void onNext(ProfileSearch profileSearch) {
//                        statusCode = profileSearch.getStatusCode();
//                        if (statusCode == 200) {
//
//                            List<ProfileSearch.Data> list=profileSearch.getData();
//                            adapter.addAll(list);
//                            if (currentPage <= TOTAL_PAGES) adapter.addLoadingFooter();
//                            else isLastPage = true;
//                        }
//                    }
//                });
//
//    }
//
//    private void loadNextPage() {
//        // Log.d(TAG, "loadNextPage: " + currentPage);
//
//        Observable<ProfileSearch> responseObservable = apiservice.getProfileData(sessionManager.getKeyId(),currentPage);
//
//        responseObservable.subscribeOn(Schedulers.newThread())
//                .observeOn(rx.android.schedulers.AndroidSchedulers.mainThread())
//                .onErrorResumeNext(throwable -> {
//                    if (throwable instanceof retrofit2.HttpException) {
//                        retrofit2.HttpException ex = (retrofit2.HttpException) throwable;
//                        statusCode = ex.code();
//                        Log.e("error", ex.getLocalizedMessage());
//                    } else if (throwable instanceof SocketTimeoutException) {
//                        statusCode = 1000;
//                    }
//                    return Observable.empty();
//                })
//                .subscribe(new Observer<ProfileSearch>() {
//                    @Override
//                    public void onCompleted() {
//                        //  pDialog.dismiss();
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                        Log.e("error", "" + e.getMessage());
//                    }
//
//                    @Override
//                    public void onNext(ProfileSearch petDetailResponse) {
//                        statusCode = petDetailResponse.getStatusCode();
//                        if (statusCode == 200) {
//                            adapter.removeLoadingFooter();
//                            isLoading = false;
//                            List<ProfileSearch.Data> list=petDetailResponse.getData();
//                            adapter.addAll(list);
//                            if (list.size()>0) adapter.addLoadingFooter();
//                            else isLastPage = true;
//                        }
//                    }
//                });
//    }
//
//
//}
