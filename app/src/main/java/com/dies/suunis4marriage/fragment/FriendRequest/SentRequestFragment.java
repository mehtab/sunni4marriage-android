package com.dies.suunis4marriage.fragment.FriendRequest;


import android.app.ProgressDialog;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dies.suunis4marriage.R;
import com.dies.suunis4marriage.adapter.ProfileAdapter;
import com.dies.suunis4marriage.apiservice.ApiService;
import com.dies.suunis4marriage.apiservice.ApiServiceCreator;
import com.dies.suunis4marriage.application.SessionManager;
import com.dies.suunis4marriage.model.ProfileSearch;

import java.net.SocketTimeoutException;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observable;
import rx.Observer;
import rx.schedulers.Schedulers;

/**
 * A simple {@link Fragment} subclass.
 */
public class SentRequestFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener{


    SessionManager sessionManager;
    ApiService apiservice;
    ProgressDialog pDialog;
    RecyclerView.LayoutManager layoutManager;
    int statusCode;
    @BindView(R.id.rcv_sentprofile)
    RecyclerView rcv_profile;
    ProfileAdapter profileAdapter;
    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipe_refresh_layout;
    private boolean isBackFromB;

    public SentRequestFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_sent_request, container, false);
        ButterKnife.bind(this,view);
        sessionManager = new SessionManager(getActivity());
        apiservice = ApiServiceCreator.createService("latest");
        isBackFromB=false;

       // getConnectFriend();

        swipe_refresh_layout.setOnRefreshListener(this);

        swipe_refresh_layout.post(new Runnable() {
                                      @Override
                                      public void run() {
                                          swipe_refresh_layout.setRefreshing(true);

                                          getConnectFriend();
                                      }
                                  }
        );
        return view;
    }

    public void getConnectFriend(){

        swipe_refresh_layout.setRefreshing(true);
        rcv_profile.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity());
        rcv_profile.setLayoutManager(layoutManager);
        pDialog = new ProgressDialog(getActivity());
        pDialog.setTitle("Checking Data");
        pDialog.setMessage("Please Wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        Observable<ProfileSearch> responseObservable = apiservice.ShowFriendList(sessionManager.getKeyId(),
                "sent_request");

        responseObservable.subscribeOn(Schedulers.newThread())
                .observeOn(rx.android.schedulers.AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> {
                    if (throwable instanceof retrofit2.HttpException) {
                        retrofit2.HttpException ex = (retrofit2.HttpException) throwable;
                        statusCode = ex.code();
                        Log.e("error", ex.getLocalizedMessage());
                    } else if (throwable instanceof SocketTimeoutException) {
                        statusCode = 1000;
                    }
                    return Observable.empty();
                })
                .subscribe(new Observer<ProfileSearch>() {
                    @Override
                    public void onCompleted() {
                        pDialog.dismiss();
                        swipe_refresh_layout.setRefreshing(false);
                    }

                    @Override
                    public void onError(Throwable e) {
                        swipe_refresh_layout.setRefreshing(false);
                        Log.e("error", "" + e.getMessage());
                    }

                    @Override
                    public void onNext(ProfileSearch friendModel) {
                        statusCode = friendModel.getStatusCode();
                        if (statusCode == 200) {
                            profileAdapter=new ProfileAdapter(getActivity(),friendModel.getData(),"sent_request","");
                            rcv_profile.setAdapter(profileAdapter);
                        }
                    }
                });

    }

    @Override
    public void onRefresh() {
        getConnectFriend();
    }


    @Override
    public void onResume() {
        super.onResume();
        isBackFromB = true;
    }

    @Override
    public void onPause() {
        super.onPause();
        if (isBackFromB){
            getConnectFriend();
        }
    }
}
