package com.dies.suunis4marriage.fragment.CompatibilitySearch;


import android.app.ProgressDialog;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dies.suunis4marriage.R;
import com.dies.suunis4marriage.adapter.ExampleAdapter;
import com.dies.suunis4marriage.adapter.ProfileAdapter;
import com.dies.suunis4marriage.apiservice.ApiService;
import com.dies.suunis4marriage.apiservice.ApiServiceCreator;
import com.dies.suunis4marriage.application.SessionManager;
import com.dies.suunis4marriage.model.ProfileSearch;

import java.net.SocketTimeoutException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observable;
import rx.Observer;
import rx.schedulers.Schedulers;

/**
 * A simple {@link Fragment} subclass.
 */
public class SuggestionFragment extends Fragment {


    SessionManager sessionManager;
    ApiService apiservice;
    ProgressDialog pDialog;
    //RecyclerView.LayoutManager layoutManager;
    LinearLayoutManager layoutManager;
    @BindView(R.id.rcv_profile)
    RecyclerView rcv_profile;
    @BindView(R.id.txt_profile)
    TextView txt_profile;


    int statusCode;
    int index=1;
    ExampleAdapter adapter;
    String fragment_name="search_friend";
    List<ProfileSearch.Data> profile;
    ProfileSearch profileSearch;
    ProfileAdapter exampleAdapter;


    public SuggestionFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_todaystatic, container, false);

        ButterKnife.bind(this,view);
        sessionManager = new SessionManager(getActivity());
        apiservice = ApiServiceCreator.createService("latest");


        getFriend();
        return view;
    }


    public void getFriend(){
        rcv_profile.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity());
        rcv_profile.setLayoutManager(layoutManager);
        pDialog = new ProgressDialog(getActivity());
        pDialog.setTitle("Checking Data");
        pDialog.setMessage("Please Wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        Observable<ProfileSearch> responseObservable = apiservice.getProfileData(sessionManager.getKeyId());

        responseObservable.subscribeOn(Schedulers.newThread())
                .observeOn(rx.android.schedulers.AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> {
                    if (throwable instanceof retrofit2.HttpException) {
                        retrofit2.HttpException ex = (retrofit2.HttpException) throwable;
                        statusCode = ex.code();
                        Log.e("error", ex.getLocalizedMessage());
                    } else if (throwable instanceof SocketTimeoutException) {
                        statusCode = 1000;
                    }
                    return Observable.empty();
                })
                .subscribe(new Observer<ProfileSearch>() {
                    @Override
                    public void onCompleted() {
                        pDialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("error", "" + e.getMessage());
                    }

                    @Override
                    public void onNext(ProfileSearch profileSearch) {
                        statusCode = profileSearch.getStatusCode();
                        if (statusCode == 200) {
                            if (profileSearch.getData().size()>0){
                                txt_profile.setVisibility(View.GONE);
                                exampleAdapter=new ProfileAdapter(getActivity(),profileSearch.getData(),"search_friend","suggesion");
                                rcv_profile.setAdapter(exampleAdapter);
                            }else {
                                txt_profile.setVisibility(View.VISIBLE);
                            }

                        }
                    }
                });

    }

}
