package com.dies.suunis4marriage.fragment;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.dies.suunis4marriage.R;
import com.dies.suunis4marriage.activity.SearchByUderIdActivity;
import com.dies.suunis4marriage.apiservice.ApiService;
import com.dies.suunis4marriage.apiservice.ApiServiceCreator;
import com.dies.suunis4marriage.application.SessionManager;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class SearchByUserid extends Fragment {


    @BindView(R.id.et_serach_id)
    EditText et_serach_id;

    @BindView(R.id.btn_search)
    Button btn_search;

    SessionManager sessionManager;
    ApiService apiservice;
    ProgressDialog pDialog;
    RecyclerView.LayoutManager layoutManager;
    int statusCode;

    public SearchByUserid() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_search_by_userid, container, false);
        ButterKnife.bind(this,view);
        sessionManager = new SessionManager(getActivity());
        apiservice = ApiServiceCreator.createService("latest");


        btn_search.setOnClickListener(view1 -> {
            String Search_id=et_serach_id.getText().toString();
            Intent intent=new Intent(getActivity(), SearchByUderIdActivity.class);
            intent.putExtra("Search","userid_search");
            intent.putExtra("Search_id",Search_id);

            startActivity(intent);
        });

        return view;
    }




}
