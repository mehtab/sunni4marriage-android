package com.dies.suunis4marriage.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dies.suunis4marriage.R;
import com.dies.suunis4marriage.model.UserDataResponse;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivatePlanAdapter extends RecyclerView.Adapter<ActivatePlanAdapter.MyViewHolder> {

    List<UserDataResponse.Data> arrayList;
    Context context;

    public ActivatePlanAdapter(List<UserDataResponse.Data> arrayList, Context context) {
        this.arrayList = arrayList;
        this.context = context;
    }

    @NonNull
    @Override
    public ActivatePlanAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.activate_plan_layout,parent,false);
        return new ActivatePlanAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ActivatePlanAdapter.MyViewHolder holder, int position) {

        holder.lnr_plan.setVisibility(View.VISIBLE);
        holder.txt_plan_type.setText(arrayList.get(position).getPlan_type());
        holder.txt_start_plan_date.setText(parseDateToddMMyyyy(arrayList.get(position).getPlan_start_date()));
        holder.txt_end_plan_date.setText(parseDateToddMMyyyy(arrayList.get(position).getPlan_end_date()));
        holder.txt_plan_price.setText("£"+arrayList.get(position).getPlan_price());
        holder.txt_days.setText(arrayList.get(position).getPlan_days()+" Days");


        Date today = new Date();
        //SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        String dateToStr = format.format(today);
        try {
            //String CurrentDate= parseDateToddMMyyyy(arrayList.get(position).getPlan_start_date());
            String CurrentDate = dateToStr;
            String FinalDate= parseDateToddMMyyyy(arrayList.get(position).getPlan_end_date());
            Date date1;
            Date date2;
            SimpleDateFormat dates = new SimpleDateFormat("dd-MM-yyyy");
            date1 = dates.parse(CurrentDate);
            date2 = dates.parse(FinalDate);
            long difference = Math.abs(date1.getTime() - date2.getTime());
            long differenceDates = difference / (24 * 60 * 60 * 1000);
            String dayDifference = Long.toString(differenceDates);
            holder.txt_days_remaining.setText(dayDifference + " days");
        } catch (Exception exception) {
            Toast.makeText(context, "Unable to find difference"+exception, Toast.LENGTH_SHORT).show();
            Log.e("errorrooror",exception.toString());
        }


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.txt_plan_type)
        TextView txt_plan_type;
        @BindView(R.id.txt_start_plan_date)
        TextView txt_start_plan_date;
        @BindView(R.id.txt_end_plan_date)
        TextView txt_end_plan_date;
        @BindView(R.id.txt_plan_price)
        TextView txt_plan_price;
        @BindView(R.id.txt_days_remaining)
        TextView txt_days_remaining;
        @BindView(R.id.txt_days)
        TextView txt_days;
        @BindView(R.id.lnr_plan)
        LinearLayout lnr_plan;
        @BindView(R.id.txt_plan_activate)
        TextView txt_plan_activate;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }

    public String parseDateToddMMyyyy(String time) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "dd-MM-yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }
}
