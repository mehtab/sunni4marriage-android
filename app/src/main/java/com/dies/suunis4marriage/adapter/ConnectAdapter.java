package com.dies.suunis4marriage.adapter;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dies.suunis4marriage.R;
import com.dies.suunis4marriage.activity.Full_Profile;
import com.dies.suunis4marriage.apiservice.ApiService;
import com.dies.suunis4marriage.apiservice.ApiServiceCreator;
import com.dies.suunis4marriage.apiservice.MySpannable;
import com.dies.suunis4marriage.application.SessionManager;
import com.dies.suunis4marriage.application.Utility;
import com.dies.suunis4marriage.model.FriendModel;
import com.dies.suunis4marriage.model.ProfileSearch;
import com.dies.suunis4marriage.model.QuestionsAnswerModel;

import java.net.SocketTimeoutException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observable;
import rx.Observer;
import rx.schedulers.Schedulers;

public class ConnectAdapter extends RecyclerView.Adapter<ConnectAdapter.MyViewHolder> {

    Context context;
    List<ProfileSearch.Data> arrayList;
    SessionManager sessionManager;
    ApiService apiservice;
    ProgressDialog pDialog;
    RecyclerView.LayoutManager layoutManager;
    int statusCode;
    String status;
    private Dialog dialog;
    String fragment_name;


    public ConnectAdapter(Context context, List<ProfileSearch.Data> arrayList,String fragment_name) {
        this.context = context;
        this.arrayList = arrayList;
        this.fragment_name=fragment_name;
        sessionManager = new SessionManager(context);
        apiservice = ApiServiceCreator.createService("latest");
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.friendlist_layout, parent, false);
        return new ConnectAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        holder.txt_name.setText(arrayList.get(position).getName());

        String year = arrayList.get(position).getDateofbirth();
        String age = String.valueOf(getAge(year));

        holder.txt_working.setText(arrayList.get(position).getOccupation());

        holder.txt_age.setText(age + " Year");

        if (!arrayList.get(position).getMessageData().equals("")){
            holder.lnr_msg.setVisibility(View.VISIBLE);
            holder.txt_message.setText(arrayList.get(position).getMessageData());
            makeTextViewResizable( holder.txt_message, 3, "See More", true);
        }else {
            holder.lnr_msg.setVisibility(View.GONE);
        }

//        holder.card_data.setOnClickListener(view -> {
//            Utility.getAppcon().getSession().arrayListProfile = new ArrayList<>();
//            Utility.getAppcon().getSession().arrayListProfile.add(arrayList.get(position));
//            Intent intent = new Intent(context, Full_Profile.class);
//            intent.putExtra("fr_name", fragment_name);
//            context.startActivity(intent);
//        });



        holder.txt_Country.setText(arrayList.get(position).getTown_city() + "," + arrayList.get(position).getCountry());

        holder.txt_accept.setOnClickListener(view -> {
            status = "1";
            statusupdate(arrayList.get(position).getConnect_request_id(), position, status);
        });
        holder.txt_reject.setOnClickListener(view -> {
            status = "2";
            statusupdate(arrayList.get(position).getConnect_request_id(), position, status);
        });

        holder.btn_view_profile.setOnClickListener(view -> {
            Utility.getAppcon().getSession().arrayListProfile = new ArrayList<>();
            Utility.getAppcon().getSession().arrayListProfile.add(arrayList.get(position));
            Intent intent = new Intent(context, Full_Profile.class);
            intent.putExtra("fr_name", fragment_name);
            context.startActivity(intent);
        });

        holder.btn_view.setOnClickListener(view -> {
            DisplayDialog(position);
        });

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    private int getAge(String dobString) {

        Date date = null;
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        try {
            date = sdf.parse(dobString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (date == null) return 0;

        Calendar dob = Calendar.getInstance();
        Calendar today = Calendar.getInstance();

        dob.setTime(date);

        int year = dob.get(Calendar.YEAR);
        int month = dob.get(Calendar.MONTH);
        int day = dob.get(Calendar.DAY_OF_MONTH);

        dob.set(year, month + 1, day);

        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);

        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)) {
            age--;
        }
        return age;
    }

    public void statusupdate(String request_id, int position, String sts) {
        layoutManager = new LinearLayoutManager(context);
        pDialog = new ProgressDialog(context);
        pDialog.setTitle("Checking Data");
        pDialog.setMessage("Please Wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        Observable<FriendModel> responseObservable = apiservice.updatestatus(sessionManager.getKeyId(), request_id,
                "pending_request", sts);

        responseObservable.subscribeOn(Schedulers.newThread())
                .observeOn(rx.android.schedulers.AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> {
                    if (throwable instanceof retrofit2.HttpException) {
                        retrofit2.HttpException ex = (retrofit2.HttpException) throwable;
                        statusCode = ex.code();
                        Log.e("error", ex.getLocalizedMessage());
                    } else if (throwable instanceof SocketTimeoutException) {
                        statusCode = 1000;
                    }
                    return Observable.empty();
                })
                .subscribe(new Observer<FriendModel>() {
                    @Override
                    public void onCompleted() {
                        pDialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("error", "" + e.getMessage());
                    }

                    @Override
                    public void onNext(FriendModel friendModel) {
                        statusCode = friendModel.getStatusCode();
                        if (statusCode == 200) {
                            Utility.displayToast(context, friendModel.getMessage());
                            arrayList.remove(position);
                            notifyDataSetChanged();
                        } else {
                            Utility.displayToast(context, friendModel.getMessage());
                        }
                    }
                });

    }

    public void DisplayDialog(int pos) {
        dialog = new Dialog(context);
        dialog.getWindow();
        dialog.setTitle("Address");
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.questions_recycleview);
        dialog.setCancelable(true);

        RecyclerView rcv_questions;
        rcv_questions = dialog.findViewById(R.id.rcv_questions);
        getMarkedQuestions(rcv_questions,pos);

        dialog.show();
        Window window = dialog.getWindow();
        assert window != null;
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

    }

    public void getMarkedQuestions(RecyclerView rcv_questions,int pos) {
        rcv_questions.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(context);
        rcv_questions.setLayoutManager(layoutManager);
        pDialog = new ProgressDialog(context);
        pDialog.setTitle("Checking Data");
        pDialog.setMessage("Please Wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();


        Observable<QuestionsAnswerModel> responseObservable = apiservice.showQueAns(arrayList.get(pos).getUserId());

        responseObservable.subscribeOn(Schedulers.newThread())
                .observeOn(rx.android.schedulers.AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> {
                    if (throwable instanceof retrofit2.HttpException) {
                        retrofit2.HttpException ex = (retrofit2.HttpException) throwable;
                        statusCode = ex.code();
                        Log.e("error", ex.getLocalizedMessage());
                    } else if (throwable instanceof SocketTimeoutException) {
                        statusCode = 1000;
                    }
                    return Observable.empty();
                })
                .subscribe(new Observer<QuestionsAnswerModel>() {
                    @Override
                    public void onCompleted() {
                        pDialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("error", "" + e.getMessage());
                    }

                    @Override
                    public void onNext(QuestionsAnswerModel questionsAnswerModel) {
                        statusCode = questionsAnswerModel.getStatusCode();
                        if (statusCode == 200) {
                            QuestionsShowinDialogAdapter adapter = new QuestionsShowinDialogAdapter(context, questionsAnswerModel.getData());
                            rcv_questions.setAdapter(adapter);
                        }else {
                            Utility.displayToast(context,"No questions available");
                        }

                    }
                });


    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_name)
        TextView txt_name;
        @BindView(R.id.txt_age)
        TextView txt_age;
        @BindView(R.id.txt_working)
        TextView txt_working;
        @BindView(R.id.txt_message)
        TextView txt_message;
        @BindView(R.id.txt_accept)
        TextView txt_accept;
        @BindView(R.id.txt_reject)
        TextView txt_reject;
        @BindView(R.id.txt_Country)
        TextView txt_Country;
        @BindView(R.id.btn_view)
        Button btn_view;
        @BindView(R.id.btn_view_profile)
        Button btn_view_profile;
        @BindView(R.id.lnr_msg)
        LinearLayout lnr_msg;
        @BindView(R.id.card_data)
        CardView card_data;





        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


    public static void makeTextViewResizable(final TextView tv, final int maxLine, final String expandText, final boolean viewMore) {

        if (tv.getTag() == null) {
            tv.setTag(tv.getText());
        }
        ViewTreeObserver vto = tv.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @SuppressWarnings("deprecation")
            @Override
            public void onGlobalLayout() {

                ViewTreeObserver obs = tv.getViewTreeObserver();
                obs.removeGlobalOnLayoutListener(this);
                if (maxLine == 0) {
                    int lineEndIndex = tv.getLayout().getLineEnd(0);
                    String text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(
                            addClickablePartTextViewResizable(Html.fromHtml(tv.getText().toString()), tv, maxLine, expandText,
                                    viewMore), TextView.BufferType.SPANNABLE);
                } else if (maxLine > 0 && tv.getLineCount() >= maxLine) {
                    int lineEndIndex = tv.getLayout().getLineEnd(maxLine - 1);
                    String text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(
                            addClickablePartTextViewResizable(Html.fromHtml(tv.getText().toString()), tv, maxLine, expandText,
                                    viewMore), TextView.BufferType.SPANNABLE);
                } else {
                    int lineEndIndex = tv.getLayout().getLineEnd(tv.getLayout().getLineCount() - 1);
                    String text = tv.getText().subSequence(0, lineEndIndex) + " " + expandText;
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(
                            addClickablePartTextViewResizable(Html.fromHtml(tv.getText().toString()), tv, lineEndIndex, expandText,
                                    viewMore), TextView.BufferType.SPANNABLE);
                }
            }
        });

    }

    private static SpannableStringBuilder addClickablePartTextViewResizable(final Spanned strSpanned, final TextView tv,
                                                                            final int maxLine, final String spanableText, final boolean viewMore) {
        String str = strSpanned.toString();
        SpannableStringBuilder ssb = new SpannableStringBuilder(strSpanned);

        if (str.contains(spanableText)) {


            ssb.setSpan(new MySpannable(false){
                @Override
                public void onClick(View widget) {
                    if (viewMore) {
                        tv.setLayoutParams(tv.getLayoutParams());
                        tv.setText(tv.getTag().toString(), TextView.BufferType.SPANNABLE);
                        tv.invalidate();
                        makeTextViewResizable(tv, -1, "See Less", false);
                    } else {
                        tv.setLayoutParams(tv.getLayoutParams());
                        tv.setText(tv.getTag().toString(), TextView.BufferType.SPANNABLE);
                        tv.invalidate();
                        makeTextViewResizable(tv, 3, ".. See More", true);
                    }
                }
            }, str.indexOf(spanableText), str.indexOf(spanableText) + spanableText.length(), 0);

        }
        return ssb;

    }


}
