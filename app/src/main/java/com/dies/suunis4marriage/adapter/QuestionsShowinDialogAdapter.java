package com.dies.suunis4marriage.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dies.suunis4marriage.R;
import com.dies.suunis4marriage.model.QuestionsAnswerModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class QuestionsShowinDialogAdapter extends RecyclerView.Adapter<QuestionsShowinDialogAdapter.MyViewHolder> {

    List<QuestionsAnswerModel.Data> arrayList;
    Context context;


    public QuestionsShowinDialogAdapter( Context context,List<QuestionsAnswerModel.Data> arrayList) {
        this.arrayList = arrayList;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.questio_answer_dialog,parent,false);
        return new QuestionsShowinDialogAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        holder.txt_queston.setText(arrayList.get(position).getQuestions());
        holder.txt_answer.setText(arrayList.get(position).getAnswer());

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_queston)
        TextView txt_queston;

        @BindView(R.id.txt_answer)
        TextView txt_answer;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
