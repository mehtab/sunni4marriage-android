//package com.dies.suunis4marriage.adapter;
//
//import android.app.ProgressDialog;
//import android.content.Context;
//import android.content.Intent;
//import android.support.annotation.NonNull;
//import android.support.v7.widget.RecyclerView;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.Button;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.dies.suunis4marriage.R;
//import com.dies.suunis4marriage.apiservice.ApiConstants;
//import com.dies.suunis4marriage.apiservice.ApiService;
//import com.dies.suunis4marriage.apiservice.ApiServiceCreator;
//import com.dies.suunis4marriage.application.SessionManager;
//import com.dies.suunis4marriage.model.FriendModel;
//import com.squareup.picasso.MemoryPolicy;
//import com.squareup.picasso.NetworkPolicy;
//import com.squareup.picasso.Picasso;
//
//import java.net.SocketTimeoutException;
//import java.util.ArrayList;
//import java.util.List;
//
//import butterknife.BindView;
//import butterknife.ButterKnife;
//import de.hdodenhof.circleimageview.CircleImageView;
//import rx.Observable;
//import rx.Observer;
//import rx.schedulers.Schedulers;
//
//public class FriendAdapter extends RecyclerView.Adapter<FriendAdapter.MyViewHolder> {
//
//    Context context;
//    List<FriendModel.Data> arrayList;
//    private String fragment_name;
//    SessionManager sessionManager;
//    private ApiService apiservice;
//    private ProgressDialog pDialog;
//    int statusCode;
//
//    public FriendAdapter(Context context, List<FriendModel.Data> arrayList, String fragment_name) {
//
//        this.context=context;
//        this.arrayList=arrayList;
//        this.fragment_name=fragment_name;
//        sessionManager=new SessionManager(context);
//        apiservice = ApiServiceCreator.createService("latest");
//    }
//
//    @NonNull
//    @Override
//    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.friendlist_layout,parent,false);
//        return new FriendAdapter.MyViewHolder(view);
//    }
//
//    @Override
//    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
//
//        holder.setListeners();
//        holder.name.setText(arrayList.get(position).getName());
////        holder.btn2.setOnClickListener(view -> {
////            Utility.getAppcon().getSession().arrayListfriendResponse= new ArrayList<>();
////            Utility.getAppcon().getSession().arrayListfriendResponse.add(arrayList.get(position));
////            Intent intent=new Intent(context,FriendDetailActivity.class);
////            context.startActivity(intent);
////
////        });
//
//        Picasso.with(context).load(ApiConstants.IMAGE_URL + arrayList.get(position).getProfile_image())
//                .memoryPolicy(MemoryPolicy.NO_CACHE)
//                .networkPolicy(NetworkPolicy.NO_CACHE)
//                .fit()
//                //.placeholder(R.drawable.no_image)
//                .into(holder.imageView);
//
//        switch (fragment_name) {
//                case "friend_list":
//                    holder.btn1.setText(context.getString(R.string.str_unfriend));
//                    holder.btn2.setText(context.getString(R.string.str_view));
//                    break;
//                case "pending_request":
//                    holder.btn1.setText(context.getString(R.string.str_accept));
//                    holder.btn2.setText(context.getString(R.string.str_reject));
//                    break;
//                case "search_friend":
//                    holder.btn1.setText(context.getString(R.string.addfriend));
//                    holder.btn2.setText(context.getString(R.string.viewfriend));
//                    break;
//                case  "sent_request":
//                    holder.btn1.setText(context.getString(R.string.str_cancel_request));
//                    holder.btn2.setText(context.getString(R.string.str_view));
//                    break;
//        }
//    }
//
//    @Override
//    public int getItemCount() {
//        return arrayList.size();
//    }
//
//    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
//
//        @BindView(R.id.btn_firend)
//        Button btn1;
//        @BindView(R.id.btn_view)
//        Button btn2;
//        @BindView(R.id.txt_name)
//        TextView name;
//        @BindView(R.id.txt_bio)
//        TextView Bio;
//        @BindView(R.id.profile_image)
//        CircleImageView imageView;
//
//
//        public MyViewHolder(View itemView) {
//            super(itemView);
//            ButterKnife.bind(this, itemView);
//
//        }
//
//        void setListeners() {
//            btn1.setOnClickListener(MyViewHolder.this);
//            btn2.setOnClickListener(MyViewHolder.this);
//        }
//
//        @Override
//        public void onClick(View view) {
//            Integer status;
//            switch (view.getId()) {
//                case R.id.btn_firend:
//                    if (fragment_name.equals("pending_request")) {
//                        status = 2;
//                        updateList(getAdapterPosition(), status);
//                    }
//                    if (fragment_name.equals("search_friend")) {
//                        status = 1;
//                        updateList(getAdapterPosition(), status);
//                    }
//                    if (fragment_name.equals("friend_list")) {
//                        status = 1;
//                        updateList(getAdapterPosition(), status);
//                    }
//                    if (fragment_name.equals("sent_request")) {
//                        status = 1;
//                        updateList(getAdapterPosition(), status);
//                    }
//                    break;
//
//                case R.id.btn_view:
//                    if (fragment_name.equals("pending_request")) {
//                        status = 1;
//                        updateList(getAdapterPosition(), status);
//                    }
//                    if (fragment_name.equals("search_friend")) {
//                        Utility.getAppcon().getSession().screen_name = "frienddata";
//                        Utility.getAppcon().getSession().arrayListFriendDetail = new ArrayList<>();
//                        Utility.getAppcon().getSession().arrayListFriendDetail.add(arrayList.get(getAdapterPosition()));
//                        Intent intent = new Intent(context, UserProfileActivity.class);
//                        context.startActivity(intent);
//                    }
//                    if (fragment_name.equals("friend_list")) {
//                        Utility.getAppcon().getSession().screen_name = "frienddata";
//                        Utility.getAppcon().getSession().arrayListFriendDetail = new ArrayList<>();
//                        Utility.getAppcon().getSession().arrayListFriendDetail.add(arrayList.get(getAdapterPosition()));
//                        Intent intent = new Intent(context, UserProfileActivity.class);
//                        context.startActivity(intent);
//                    }
//                    if (fragment_name.equals("sent_request")) {
//                        Utility.getAppcon().getSession().screen_name = "frienddata";
//                        Utility.getAppcon().getSession().arrayListFriendDetail = new ArrayList<>();
//                        Utility.getAppcon().getSession().arrayListFriendDetail.add(arrayList.get(getAdapterPosition()));
//                        Intent intent = new Intent(context, UserProfileActivity.class);
//                        context.startActivity(intent);
//                    }
//                    break;
//            }
//        }
//
//
//
//        private void updateList(Integer position,Integer status){
//
//                pDialog = new ProgressDialog(context);
//                pDialog.setTitle("Checking Data");
//                pDialog.setMessage("Please Wait...");
//                pDialog.setIndeterminate(false);
//                pDialog.setCancelable(false);
//                pDialog.show();
//
//                Observable<UserDataResponse> responseObservable = apiservice.update_friend_detail(arrayList.get(position).getFriend_request_id(),
//                        arrayList.get(position).getFriendid(),
//                        sessionManager.getKeyId(),
//                        status,
//                        fragment_name);
//
//                responseObservable.subscribeOn(Schedulers.newThread())
//                        .observeOn(rx.android.schedulers.AndroidSchedulers.mainThread())
//                        .onErrorResumeNext(throwable -> {
//                            if (throwable instanceof retrofit2.HttpException) {
//                                retrofit2.HttpException ex = (retrofit2.HttpException) throwable;
//                                statusCode = ex.code();
//                                Log.e("error", ex.getLocalizedMessage());
//                            } else if (throwable instanceof SocketTimeoutException) {
//                                statusCode = 1000;
//                            }
//                            return Observable.empty();
//                        })
//                        .subscribe(new Observer<UserDataResponse>() {
//                            @Override
//                            public void onCompleted() {
//                                pDialog.dismiss();
//                            }
//
//                            @Override
//                            public void onError(Throwable e) {
//                                Log.e("error", "" + e.getMessage());
//                            }
//
//                            @Override
//                            public void onNext(UserDataResponse userDataResponse) {
//                                statusCode = userDataResponse.getStatusCode();
//                                if (statusCode == 200) {
//                                        Toast.makeText(context, "your data is updated", Toast.LENGTH_SHORT).show();
//                                        arrayList.remove(getAdapterPosition());
//                                        notifyDataSetChanged();
//                                    }else {
//                                        Toast.makeText(context, "Server Problem", Toast.LENGTH_SHORT).show();
//                                    }
//
//
//                            }
//                        });
//
//            }
//
//    }
//}
