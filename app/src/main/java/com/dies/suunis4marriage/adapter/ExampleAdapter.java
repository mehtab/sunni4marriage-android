package com.dies.suunis4marriage.adapter;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import com.google.android.material.textfield.TextInputEditText;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dies.suunis4marriage.R;
import com.dies.suunis4marriage.activity.Full_Profile;
import com.dies.suunis4marriage.apiservice.ApiConstants;
import com.dies.suunis4marriage.apiservice.ApiService;
import com.dies.suunis4marriage.apiservice.ApiServiceCreator;
import com.dies.suunis4marriage.application.SessionManager;
import com.dies.suunis4marriage.application.Utility;
import com.dies.suunis4marriage.model.FriendModel;
import com.dies.suunis4marriage.model.ProfileSearch;
import com.squareup.picasso.Picasso;

import java.net.SocketTimeoutException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.ButterKnife;
import io.reactivex.annotations.Nullable;
import rx.Observable;
import rx.Observer;
import rx.schedulers.Schedulers;

public class ExampleAdapter extends RecyclerView.Adapter<ExampleAdapter.MyViewHolder> {

    Context context;
    List<ProfileSearch.Data> arrayList;
    int statusCode;
    ProgressDialog pDialog;
    SessionManager sessionManager;
    ApiService apiservice;
    String fragment_name;
    String status;
    private Dialog dialog;
    TextInputEditText etText;
    Button btn_submit;

    private static final int ITEM = 0;
    private static final int LOADING = 1;
    ProfileSearch.Data data = new ProfileSearch().new Data();
    private boolean isLoadingAdded = false;



    public ExampleAdapter(Context context,String fragment_name) {
        this.context = context;
        this.arrayList = arrayList;
        this.fragment_name=fragment_name;
        sessionManager=new SessionManager(context);
        apiservice = ApiServiceCreator.createService("latest");
        arrayList=new ArrayList<>();
    }

    @NonNull
    @Override
    public ExampleAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        ExampleAdapter.MyViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case ITEM:
                viewHolder = getViewHolder(parent, inflater);
                break;
            case LOADING:
                View v2 = inflater.inflate(R.layout.item_progress, parent, false);
                viewHolder = new LoadingVH(v2);
                break;
        }


     //   View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.list_profile_layout,parent,false);
    //    return new ExampleAdapter.MyViewHolder(view);
        return viewHolder;
    }

    @Nullable
    private ExampleAdapter.MyViewHolder getViewHolder(ViewGroup parent, LayoutInflater inflater) {
        ExampleAdapter.MyViewHolder viewHolder;
        View v1 = inflater.inflate(R.layout.list_profile_layout, parent, false);
        viewHolder = new MyViewHolder(v1);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {


        ProfileSearch.Data result = arrayList.get(position);
        switch (getItemViewType(position)) {
            case ITEM:


        holder.txt_name.setText(arrayList.get(position).getName());


        Picasso.with(context).load(ApiConstants.IMAGE_URL + arrayList.get(position).getProfilepic())
//                .memoryPolicy(MemoryPolicy.NO_CACHE)
//                .networkPolicy(NetworkPolicy.NO_CACHE)
                .fit()
                .into(holder.img_profile);
        String year=arrayList.get(position).getDateofbirth();
        String age=String.valueOf(getAge(year));
        holder.txt_year.setText(age+" Year");
        holder.txt_city.setText(arrayList.get(position).getTown_city()+","+arrayList.get(position).getCountry());

        holder.lnr_profile.setOnClickListener(view -> {
            Utility.getAppcon().getSession().arrayListProfile=new ArrayList<>();
            Utility.getAppcon().getSession().arrayListProfile.add(arrayList.get(position));
            Intent intent=new Intent(context, Full_Profile.class);
            context.startActivity(intent);


            switch (fragment_name) {
                case "friend_list":
                    holder.btn_conect.setText("Remove");
                    Utility.getAppcon().getSession().fragment_name = "friend_list";
                    break;
                case "search_friend":
                    holder.btn_conect.setText("Connect Now");
                    Utility.getAppcon().getSession().fragment_name = "search_friend";
                    break;
                case "shortlist_data":
                    //holder.btn_conect.setText("Connect Now");
                    Utility.getAppcon().getSession().fragment_name = "shortlist_data";
                    break;
            }



            holder.btn_conect.setOnClickListener(view1 -> {
                if (fragment_name.equals("friend_list")) {
                    status="1";
                    UpdateConnectRequest(position,status,fragment_name,"");
                }
                if (fragment_name.equals("search_friend")){
                    status="0";
                    displayDialog(position,fragment_name);
                    //UpdateConnectRequest(position,status);
                }
            });

        });
        if (fragment_name.equals("friend_list")){

            holder.btn_conect.setText("Remove");

        }

                break;

            case LOADING:
//                Do nothing
                break;
        }


    }

    @Override
    public int getItemCount() {
        return arrayList == null ? 0 : arrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return (position == arrayList.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        //@BindView(R.id.txt_name)
        TextView txt_name;
        //@BindView(R.id.txt_year)
        TextView txt_year;
        //@BindView(R.id.txt_work)
        TextView txt_work;
        //@BindView(R.id.txt_city)
        TextView txt_city;
        //@BindView(R.id.img_profile)
        ImageView img_profile;
        //@BindView(R.id.lnr_profile)
        LinearLayout lnr_profile;
        //@BindView(R.id.btn_conect)
        Button btn_conect;


        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            txt_name=itemView.findViewById(R.id.txt_name);
            txt_year=itemView.findViewById(R.id.txt_year);
//            txt_work=itemView.findViewById(R.id.txt_work);
            txt_city=itemView.findViewById(R.id.txt_city);
            img_profile=itemView.findViewById(R.id.img_profile);
            lnr_profile=itemView.findViewById(R.id.lnr_profile);
            btn_conect=itemView.findViewById(R.id.btn_conect);
        }
    }

    private int getAge(String dobString){

        Date date = null;
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        try {
            date = sdf.parse(dobString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if(date == null) return 0;

        Calendar dob = Calendar.getInstance();
        Calendar today = Calendar.getInstance();

        dob.setTime(date);

        int year = dob.get(Calendar.YEAR);
        int month = dob.get(Calendar.MONTH);
        int day = dob.get(Calendar.DAY_OF_MONTH);

        dob.set(year, month+1, day);

        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);

        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)){
            age--;
        }



        return age;
    }



    public void UpdateConnectRequest(int position,String status,String flag,String message){

        pDialog = new ProgressDialog(context);
        pDialog.setTitle("Checking Data");
        pDialog.setMessage("Please Wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        Observable<FriendModel> responseObservable = apiservice.ChangeFriendStatus(sessionManager.getKeyId(),
                arrayList.get(position).getUserId(),
                status,
                flag,
                message,
                arrayList.get(position).getConnect_request_id());

        responseObservable.subscribeOn(Schedulers.newThread())
                .observeOn(rx.android.schedulers.AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> {
                    if (throwable instanceof retrofit2.HttpException) {
                        retrofit2.HttpException ex = (retrofit2.HttpException) throwable;
                        statusCode = ex.code();
                        Log.e("error", ex.getLocalizedMessage());
                    } else if (throwable instanceof SocketTimeoutException) {
                        statusCode = 1000;
                    }
                    return Observable.empty();
                })
                .subscribe(new Observer<FriendModel>() {
                    @Override
                    public void onCompleted() {
                        pDialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("error", "" + e.getMessage());
                    }

                    @Override
                    public void onNext(FriendModel friendModel) {
                        statusCode = friendModel.getStatusCode();
                        if (statusCode == 200) {
                            Utility.displayToast(context,friendModel.getMessage());
                            arrayList.remove(position);
                            notifyDataSetChanged();

                        }else {
                            Utility.displayToast(context,friendModel.getMessage());
                        }

                    }
                });

    }



    private void displayDialog(int position,String flag) {

        dialog = new Dialog(context);
        dialog.getWindow();
        dialog.setTitle("Address");
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.add_message_layout);
        dialog.setCancelable(true);


        // spinner = dialog.findViewById(R.id.spin_year);
        etText = dialog.findViewById(R.id.etText);
        btn_submit=dialog.findViewById(R.id.btn_submit);

        btn_submit.setOnClickListener(view -> {

            if (etText.getText().toString().equals("")){
                Utility.displayToast(context,"Enter Your Message");
            }else {
                UpdateConnectRequest(position,status,flag,etText.getText().toString());
                dialog.dismiss();
            }

        });



        dialog.show();
        Window window = dialog.getWindow();
        assert window != null;
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);


    }


    public void add(ProfileSearch.Data r) {
        arrayList.add(r);
        notifyItemInserted(arrayList.size() - 1);
    }

    public void addAll(List<ProfileSearch.Data> moveResults) {
        for (ProfileSearch.Data result : moveResults) {
            add(result);
        }
    }

    public void remove(ProfileSearch.Data r) {
        int position = arrayList.indexOf(r);
        if (position > -1) {
            arrayList.remove(position);
            notifyItemRemoved(position);
        }
    }


    public void clear() {
        isLoadingAdded = false;
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }

    public boolean isEmpty() {
        return getItemCount() == 0;
    }

    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new ProfileSearch().new Data());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = arrayList.size() - 1;
        ProfileSearch.Data result = getItem(position);

        if (result != null) {
            arrayList.remove(position);
            notifyItemRemoved(position);
        }
    }

    public ProfileSearch.Data getItem(int position) {
        return arrayList.get(position);
    }


    protected class LoadingVH extends ExampleAdapter.MyViewHolder{

        public LoadingVH(View itemView) {
            super(itemView);
        }
    }
}
