package com.dies.suunis4marriage.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dies.suunis4marriage.R;
import com.dies.suunis4marriage.application.SessionManager;
import com.dies.suunis4marriage.model.PaymentModel;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PaymentHistoryAdapter extends RecyclerView.Adapter<PaymentHistoryAdapter.MyViewHolder> {

    Context context;
    List<PaymentModel.Data> arrayList;
    DecimalFormat format = new DecimalFormat("0.00");
    SessionManager sessionManager;

    public PaymentHistoryAdapter(Context context, List<PaymentModel.Data> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
        sessionManager=new SessionManager(context);

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.paymenthistory_layout,parent,false);
        return new PaymentHistoryAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.txt_id.setText(arrayList.get(position).getTransaction_id());

        holder.txt_start_date.setText(parseDateToddMMyyyy(arrayList.get(position).getPayment_date()));

        holder.txt_payment_plan.setText("£"+format.format(Double.parseDouble(arrayList.get(position).getAmount())));

        holder.txt_time.setText(parseDateTohhmmss(arrayList.get(position).getPayment_date()));

        holder.txt_name.setText(sessionManager.getUserData().get(0).getFirstName());

        if (arrayList.get(position).getPayment_type().equals("upgrade")){
            holder.txt_membership.setText("membership plan");
        }else {
            holder.txt_membership.setText("advertisement plan");
        }

        if (arrayList.get(position).getEnd_date()!=null){
            holder.txt_end_date.setText(parseDateToddMMyyyy(arrayList.get(position).getEnd_date()));
        }

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_name)
        TextView txt_name;
        @BindView(R.id.txt_id)
        TextView txt_id;
        @BindView(R.id.txt_start_date)
        TextView txt_start_date;
        @BindView(R.id.txt_end_date)
        TextView txt_end_date;
        @BindView(R.id.txt_payment_plan)
        TextView txt_payment_plan;
        @BindView(R.id.txt_time)
        TextView txt_time;

        @BindView(R.id.txt_membership)
        TextView txt_membership;


        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }

    public String parseDateTohhmmss(String time) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "HH:mm:ss";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public String parseDateToddMMyyyy(String time) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "yyyy-MM-dd";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }
}
