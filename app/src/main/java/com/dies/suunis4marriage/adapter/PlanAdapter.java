package com.dies.suunis4marriage.adapter;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.dies.suunis4marriage.R;
import com.dies.suunis4marriage.activity.UpgradeAccountActivity;
import com.dies.suunis4marriage.apiservice.ApiService;
import com.dies.suunis4marriage.apiservice.ApiServiceCreator;
import com.dies.suunis4marriage.application.SessionManager;
import com.dies.suunis4marriage.application.Utility;
import com.dies.suunis4marriage.model.PlanModel;
import com.dies.suunis4marriage.model.PromoCode;

import java.net.SocketTimeoutException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observable;
import rx.Observer;
import rx.schedulers.Schedulers;

public class PlanAdapter extends RecyclerView.Adapter<PlanAdapter.MyViewHolder> {

    List<PlanModel.Data> arrayList;
    Context context;
    SessionManager sessionManager;
    int statusCode;

    public PlanAdapter(List<PlanModel.Data> arrayList, Context context) {
        this.arrayList = arrayList;
        this.context = context;
        sessionManager = new SessionManager(context);

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.plan_layout, parent, false);
        return new PlanAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        if (sessionManager.isPlanActivate()) {
            if (sessionManager.isPayment()) {
                //   holder.btn_pln.setVisibility(View.GONE);
                Utility.displayToast(context, "Your Plan already activated");
            } else {
                Utility.displayToast(context, "Please upgrade your account");
            }

//        }else {

//            if (arrayList.get(position).getPlan_type().equals("membership")){
//                //Utility.getAppcon().getSession().amount = "69.99";
//                Utility.getAppcon().getSession().amount=arrayList.get(position).getPlan_sell_amount();
//                Utility.getAppcon().getSession().payment_type = "upgrade";
//                Utility.getAppcon().getSession().plan_type=arrayList.get(position).getPlan_id();
//            }else {
//                Utility.getAppcon().getSession().payment_type="plan";
//                Utility.getAppcon().getSession().amount=arrayList.get(position).getPlan_sell_amount();
//                Utility.getAppcon().getSession().plan_type=arrayList.get(position).getPlan_id();
//            }

        }

        holder.btn_pln.setOnClickListener(view -> {
            /**/

            if (holder.promo_code.getText().toString().length() > 0) {
                checkPromoCode(holder.promo_code, position);
            } else {
                /**/

                //   Utility.displayToast(context,"plan");
//                Utility.getAppcon().getSession().payment_type="plan";
//                Utility.getAppcon().getSession().amount=arrayList.get(position).getPlan_amount();
                if (arrayList.get(position).getPlan_type().equals("membership")) {
                    if (sessionManager.isPayment()) {
                        Utility.displayToast(context, "Your plan is already upgraded");
                    } else {
                        Utility.getAppcon().getSession().amount = arrayList.get(position).getPlan_amount();
                        Utility.getAppcon().getSession().payment_type = "upgrade";
                        Utility.getAppcon().getSession().plan_type = arrayList.get(position).getPlan_id();
                        Intent intent = new Intent(context, UpgradeAccountActivity.class);
                        context.startActivity(intent);
                    }
                } else {
                    Utility.getAppcon().getSession().payment_type = "plan";
                    Utility.getAppcon().getSession().amount = arrayList.get(position).getPlan_sell_amount();
                    Utility.getAppcon().getSession().plan_type = arrayList.get(position).getPlan_id();
                    Intent intent = new Intent(context, UpgradeAccountActivity.class);
                    context.startActivity(intent);
                }
            }
        });

        //amount=Utility.getAppcon().getSession().amount;

        holder.txt_amount.setText("£" + arrayList.get(position).getPlan_amount());
        holder.txt_desc.setText(Html.fromHtml(arrayList.get(position).getPlan_description()));
        holder.txt_plna_days.setText(arrayList.get(position).getPlan_days() + " Days");
        holder.txt_regular.setPaintFlags(holder.txt_regular.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        holder.txt_regular.setText("£" + arrayList.get(position).getPlan_amount());


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_amount)
        TextView txt_amount;
        @BindView(R.id.txt_desc)
        TextView txt_desc;
        @BindView(R.id.txt_plna_days)
        TextView txt_plna_days;

        @BindView(R.id.txt_regular)
        TextView txt_regular;
        @BindView(R.id.promo_code)
        EditText promo_code;


        @BindView(R.id.btn_pln)
        Button btn_pln;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    private void displayDialog() {
        Dialog dialog;
        dialog = new Dialog(context);
        dialog.getWindow();
        dialog.setTitle(context.getResources().getString(R.string.congrats));
        dialog.setContentView(R.layout.message_promo);
        dialog.setCancelable(true);

        TextView txt_ok = dialog.findViewById(R.id.txt_ok);
        TextView discount_is = dialog.findViewById(R.id.discount_is);
        discount_is.setText("You get 10% Discount in payment");
        txt_ok.setOnClickListener(view -> {
            dialog.cancel();
        });


        dialog.show();
        Window window = dialog.getWindow();
        assert window != null;
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

    }

    private void checkPromoCode(EditText promo_code, int position) {
        ProgressDialog pDialog;
        pDialog = new ProgressDialog(context);
        pDialog.setTitle("Checking Promo code");
        pDialog.setMessage("Please Wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();
        ApiService apiservice;
        apiservice = ApiServiceCreator.createService("latest");
        Observable<PromoCode> responseObservable = apiservice.getPromoCode(promo_code.getText().toString());
        responseObservable.subscribeOn(Schedulers.newThread()).observeOn(
                rx.android.schedulers.AndroidSchedulers.mainThread()
        ).onErrorResumeNext(throwable -> {
            if (throwable instanceof retrofit2.HttpException) {
                retrofit2.HttpException ex = (retrofit2.HttpException) throwable;
                statusCode = ex.code();
                Log.e("error", ex.getLocalizedMessage());
            } else if (throwable instanceof SocketTimeoutException) {
                statusCode = 1000;
            }
            return Observable.empty();
        }).subscribe(new Observer<PromoCode>() {
            @Override
            public void onCompleted() {
                pDialog.dismiss();
            }

            @Override
            public void onError(Throwable e) {
                Log.e("error", "" + e.getMessage());

            }

            @Override
            public void onNext(PromoCode promoCode) {
                // statusCode = promoCode.getStatusCode();
                //  if (statusCode == 200) {
                if (promoCode.getMessage().toLowerCase().equals("success")) {
                    Toast.makeText(context, "Your promo successfully updated", Toast.LENGTH_SHORT).show();
                    /*irfan if success then discount amount move to next*/

                    SuccessPromoCode(position, Integer.parseInt(promoCode.getDiscount()));
                    promo_code.setText("");
                } else if (promoCode.getMessage().toLowerCase().equals("expired")) {

                    Toast toast = Toast.makeText(context, "Promotion code has expired", Toast.LENGTH_LONG);
                    View view = toast.getView();
                    view.setBackgroundResource(R.color.white);
                    TextView text = (TextView) view.findViewById(android.R.id.message);
                    /*Here you can do anything with above textview like text.setTextColor(Color.parseColor("#000000"));*/
                    text.setTextColor(Color.parseColor("#ff0000"));
                    toast.show();


                    promo_code.setText("");

                } else if (promoCode.getMessage().toLowerCase().equals("not found")) {

                    Toast toast = Toast.makeText(context, "Promotion code is invalid", Toast.LENGTH_LONG);
                    View view = toast.getView();
                    view.setBackgroundResource(R.color.white);
                    TextView text = (TextView) view.findViewById(android.R.id.message);
                    /*Here you can do anything with above textview like text.setTextColor(Color.parseColor("#000000"));*/
                    text.setTextColor(Color.parseColor("#ff0000"));
                    toast.show();
                    //wrong promocode
                    promo_code.setText("");
                }
                //   } else {
                //      Toast.makeText(context, "Error with status code" + statusCode, Toast.LENGTH_SHORT).show();
                //   }

            }


        });
    }

    private void SuccessPromoCode(int position, Integer discount) {
        if (arrayList.get(position).getPlan_type().equals("membership")) {
            if (sessionManager.isPayment()) {
                Utility.displayToast(context, "Your plan is already upgraded");
            } else {

                float origional_amount = Float.parseFloat(arrayList.get(position).getPlan_amount());

                int s = 100 - discount;
                float newAmount = (s * origional_amount) / 100;

                Utility.getAppcon().getSession().amount = String.format("%.2f", newAmount) + "";
                Utility.getAppcon().getSession().payment_type = "upgrade";
                Utility.getAppcon().getSession().plan_type = arrayList.get(position).getPlan_id();
                Intent intent = new Intent(context, UpgradeAccountActivity.class);
                context.startActivity(intent);
            }
        } else {
            Utility.getAppcon().getSession().payment_type = "plan";

            int origional_amount = Integer.parseInt(arrayList.get(position).getPlan_sell_amount());
            Integer newAmount = origional_amount - (origional_amount * (discount / 100));

            Utility.getAppcon().getSession().amount = newAmount.toString();
            Utility.getAppcon().getSession().plan_type = arrayList.get(position).getPlan_id();
            Intent intent = new Intent(context, UpgradeAccountActivity.class);
            context.startActivity(intent);
        }
    }
}
