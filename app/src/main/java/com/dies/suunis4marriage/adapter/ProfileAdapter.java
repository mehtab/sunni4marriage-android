package com.dies.suunis4marriage.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dies.suunis4marriage.R;
import com.dies.suunis4marriage.activity.Full_Profile;
import com.dies.suunis4marriage.apiservice.ApiConstants;
import com.dies.suunis4marriage.apiservice.ApiService;
import com.dies.suunis4marriage.apiservice.ApiServiceCreator;
import com.dies.suunis4marriage.application.SessionManager;
import com.dies.suunis4marriage.application.Utility;
import com.dies.suunis4marriage.model.FriendModel;
import com.dies.suunis4marriage.model.ProfileSearch;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;

import java.lang.reflect.Type;
import java.net.SocketTimeoutException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observable;
import rx.Observer;
import rx.schedulers.Schedulers;

public class ProfileAdapter extends RecyclerView.Adapter<ProfileAdapter.MyViewHolder> {
    Context context;
    List<ProfileSearch.Data> arrayList;
    int statusCode;
    ProgressDialog pDialog;
    SessionManager sessionManager;
    ApiService apiservice;
    String fragment_name, fr_name;
    String status;
    EditText etText;
    TextView txt_que, txt_que1, txt_que2;
    Button btn_submit;
    private Dialog dialog;
    private ArrayList<String> arrayListchk;
    SharedPreferences sharedPreferences;
    static SharedPreferences.Editor editor;

    public ProfileAdapter(Context context, List<ProfileSearch.Data> arrayList, String fragment_name, String fr_name) {
        this.context = context;
        sharedPreferences = context.getSharedPreferences("STORE_FILE_NAME", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();

        this.arrayList = arrayList;
        this.fragment_name = fragment_name;
        this.fr_name = fr_name;
        arrayListchk = new ArrayList<>();
        sessionManager = new SessionManager(context);
        apiservice = ApiServiceCreator.createService("latest");

    }

    @NonNull
    @Override
    public ProfileAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_profile_layout, parent, false);
        return new ProfileAdapter.MyViewHolder(view);
    }
//    View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.list_profile_layout,parent,false);

    @Override
    public void onBindViewHolder(@NonNull ProfileAdapter.MyViewHolder holder, int position) {

        holder.txt_name.setText(arrayList.get(position).getName());


        if (sessionManager.getUserData().get(0).getGender().equals("MALE")) {
            Picasso.with(context).load(ApiConstants.IMAGE_URL + arrayList.get(position).getProfilepic())
//                .memoryPolicy(MemoryPolicy.NO_CACHE)
//                .networkPolicy(NetworkPolicy.NO_CACHE)
                    .placeholder(R.drawable.ic_female_avatar)
                    .fit()
                    .into(holder.img_profile);
        } else {
            Picasso.with(context).load(ApiConstants.IMAGE_URL + arrayList.get(position).getProfilepic())
//                .memoryPolicy(MemoryPolicy.NO_CACHE)
//                .networkPolicy(NetworkPolicy.NO_CACHE)
                    .placeholder(R.drawable.ic_male_avtar)
                    .fit()
                    .into(holder.img_profile);
        }
        holder.lnr_flag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog diaBox = AskOption(position);
                diaBox.show();
            }
        });

        String year = arrayList.get(position).getDateofbirth();
        if (year.equals("DD/MM/YYYY")) {
        } else {
            String age = String.valueOf(calculateAge(year));
            holder.txt_year.setText(age + " Year");
        }
        holder.txt_city.setText(arrayList.get(position).getTown_city() + "," + arrayList.get(position).getCountry());


//            if (arrayList.get(position).getUs_status().equals("1")) {
//                holder.lnr_online.setVisibility(View.VISIBLE);
//            } else if (arrayList.get(position).getUs_status().equals("0")) {
//                            holder.lnr_online.setVisibility(View.VISIBLE);
//                String date = covertTimeToText(arrayList.get(position).getUs_datetime());
//                holder.txt_online.setText(date);
//        }

        if (arrayList.get(position).getPayment_status().equals("1")) {
            holder.img_primium.setVisibility(View.VISIBLE);
        }

        if (arrayList.get(position).getPlan_status() != null) {
            if (arrayList.get(position).getPlan_status().equals("1")) {
                holder.img_plan.setVisibility(View.VISIBLE);
            }
        }


        holder.lnr_profile.setOnClickListener(view -> {
            Utility.getAppcon().getSession().arrayListProfile = new ArrayList<>();
            Utility.getAppcon().getSession().arrayListProfile.add(arrayList.get(position));
            Utility.getAppcon().getSession().arrayListProfileall = arrayList;
            Intent intent = new Intent(context, Full_Profile.class);
            intent.putExtra("fr_name", fragment_name);
            intent.putExtra("pos", position);
            context.startActivity(intent);
        });

        switch (fragment_name) {
            case "friend_list":
                holder.btn_conect.setText("Remove");
                Utility.getAppcon().getSession().fragment_name = "friend_list";
                //   holder.btn_conect.setEnabled(true);
                break;
            case "search_friend":
                holder.btn_conect.setText("Send Request");
                Utility.getAppcon().getSession().fragment_name = "search_friend";
                break;
//                case "shortlist_data":
//                    holder.image_shortlist.setImageResource(R.drawable.ic_like_fill);
//                    holder.btn_conect.setText("Remove Shortlist");
//                    Utility.getAppcon().getSession().fragment_name = "shortlist_data";
//                    break;
            case "sent_request":
                holder.btn_conect.setText("Cancel");
                //   holder.btn_conect.setEnabled(true);
                Utility.getAppcon().getSession().fragment_name = "sent_request";
                break;
        }


        if (fr_name.equals("search_by_id") && arrayList.get(position).getBlock_status().equals("1")) {
            holder.btn_conect.setVisibility(View.GONE);
            holder.lnr_profile.setOnClickListener(null);
        } else {
            holder.btn_conect.setVisibility(View.VISIBLE);
        }


        holder.btn_conect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Toast.makeText(context, "btn_pressed........", Toast.LENGTH_SHORT).show();
                if (arrayList.get(position).getRequest_status().equals("1")) {
                    status = "1";
                    fragment_name = "friend_list";
                    if (fr_name.equals("favourite")) {
                        Utility.displayToast(context, "Please change favourite status first");
                    } else {
                        UpdateConnectRequest(position, status, fragment_name, "");
                    }

                } else if (fragment_name.equals("search_friend") && arrayList.get(position).getRequest_status().equals("3") || fragment_name.equals("search_friend") && arrayList.get(position).getRequest_status().equals("")) {
                    status = "0";
                    Log.e("Question_status", arrayList.get(position).getQuestion_status());
//                    if (arrayList.get(position).getQuestion_status().equals("1")) {
//                        holder.btn_conect.setText("Cancel Request");
//                        holder.btn_conect.setEnabled(false);
//                        Intent intent1 = new Intent(context, Answer_QuestionsActivity.class);
//                        intent1.putExtra("id", arrayList.get(position).getUserId());
//                        intent1.putExtra("pos", position);
//                        context.startActivity(intent1);
//                    } else {
                    displayDialog(position, fragment_name);
//                    }
                    //UpdateConnectRequest(position,status);
                } else if (arrayList.get(position).getRequest_status().equals("0")) {
                    fragment_name = "sent_request";
                    UpdateConnectRequest(position, status, fragment_name, "");
                }
            }
        });


//        if (fragment_name.equals("shortlist_data")){
//            holder.image_shortlist.setOnClickListener(view -> {
//                if (arrayList.get(position).getShortlist_status().equals("1")){
//                    RemoveShortlist(arrayList.get(position).getSl_id(),position);
//                    holder.image_shortlist.setImageResource(R.drawable.ic_like);
//                }
//            });
//        }


        if (arrayList.get(position).getShortlist_status().equals("1")) {
            holder.image_shortlist.setImageResource(R.drawable.ic_like_fill);
        } else {
            holder.image_shortlist.setImageResource(R.drawable.ic_like);
        }


        if (arrayList.get(position).getRequest_status().equals("0") && fragment_name.equals("sent_request")) {
            holder.btn_conect.setText("Cancel Request");
            holder.btn_conect.setEnabled(true);
        } else if (arrayList.get(position).getRequest_status().equals("2")) {
            holder.btn_conect.setText("Pending Request");
            holder.btn_conect.setEnabled(false);
        } else if (arrayList.get(position).getRequest_status().equals("1")) {
            holder.btn_conect.setText("Remove");
            holder.btn_conect.setEnabled(true);
        } else if (arrayList.get(position).getRequest_status().equals("0")) {
            holder.btn_conect.setText("Cancel Request");
            holder.btn_conect.setEnabled(true);
        } else {
            holder.btn_conect.setEnabled(true);
        }

        holder.image_shortlist.setOnClickListener(view -> {
            if (arrayList.get(position).getShortlist_status().equals("1")) {
                RemoveShortlist(arrayList.get(position).getUserId(), sessionManager.getKeyId(), position);
                holder.image_shortlist.setImageResource(R.drawable.ic_like);

            } else {
                if (arrayList.get(position).getBlock_status().equals("1")) {
                    Utility.displayToast(context, "Please remove from blocklist first");
                } else {
                    ShortList(position);
                    holder.image_shortlist.setImageResource(R.drawable.ic_like_fill);
                }
            }
        });

        if (arrayList.get(position).getBlock_status().equals("1")) {
            holder.img_block.setImageResource(R.drawable.ic_block_fill);
        } else {
            holder.img_block.setImageResource(R.drawable.ic_block);
        }
        holder.img_block.setOnClickListener(view -> {

            if (arrayList.get(position).getBlock_status().equals("1")) {
                RemoveBlocklist(arrayList.get(position).getUserId(), sessionManager.getKeyId(), position);
                holder.img_block.setImageResource(R.drawable.ic_block);
            } else {
                if (arrayList.get(position).getRequest_status().equals("1")) {
                    Utility.displayToast(context, "Please remove from friendlist first");
                } else {
                    if (arrayList.get(position).getShortlist_status().equals("1")) {
                        Utility.displayToast(context, "Please remove from favourite first");
                    } else {
                        BlockList(position);
                        holder.img_block.setImageResource(R.drawable.ic_block_fill);
                    }

                }


            }

        });


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    private int getAge(String dobString) {

        Date date = null;
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        try {
            date = sdf.parse(dobString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (date == null) return 0;

        Calendar dob = Calendar.getInstance();
        Calendar today = Calendar.getInstance();

        dob.setTime(date);

        int year = dob.get(Calendar.YEAR);
        int month = dob.get(Calendar.MONTH) + 1;
        int day = dob.get(Calendar.DAY_OF_MONTH);

        dob.set(year, month + 1, day);

        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);

        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)) {
            age--;
        }


        return age;
    }


    private static int calculateAge(String sbirthDate) {
        int years = 0;
        int months = 0;
        int days = 0;

        Date birthDate = null;
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        try {
            birthDate = sdf.parse(sbirthDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        //create calendar object for birth day
        Calendar birthDay = Calendar.getInstance();
        birthDay.setTimeInMillis(birthDate.getTime());

        //create calendar object for current day
        long currentTime = System.currentTimeMillis();
        Calendar now = Calendar.getInstance();
        now.setTimeInMillis(currentTime);

        //Get difference between years
        years = now.get(Calendar.YEAR) - birthDay.get(Calendar.YEAR);
        int currMonth = now.get(Calendar.MONTH) + 1;
        int birthMonth = birthDay.get(Calendar.MONTH) + 1;

        //Get difference between months
        months = currMonth - birthMonth;

        //if month difference is in negative then reduce years by one
        //and calculate the number of months.
        if (months < 0) {
            years--;
            months = 12 - birthMonth + currMonth;
            if (now.get(Calendar.DATE) < birthDay.get(Calendar.DATE))
                months--;
        } else if (months == 0 && now.get(Calendar.DATE) < birthDay.get(Calendar.DATE)) {
            years--;
            months = 11;
        }

        //Calculate the days
        if (now.get(Calendar.DATE) > birthDay.get(Calendar.DATE))
            days = now.get(Calendar.DATE) - birthDay.get(Calendar.DATE);
        else if (now.get(Calendar.DATE) < birthDay.get(Calendar.DATE)) {
            int today = now.get(Calendar.DAY_OF_MONTH);
            now.add(Calendar.MONTH, -1);
            days = now.getActualMaximum(Calendar.DAY_OF_MONTH) - birthDay.get(Calendar.DAY_OF_MONTH) + today;
        } else {
            days = 0;
            if (months == 12) {
                years++;
                months = 0;
            }
        }
        //Create new Age object
        //return new Age(days, months, years);
        return years;
    }

    public void UpdateConnectRequest(int position, String status, String flag, String message) {
        pDialog = new ProgressDialog(context);
        pDialog.setTitle("Checking Data");
        pDialog.setMessage("Please Wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        Observable<FriendModel> responseObservable = apiservice.ChangeFriendStatus(sessionManager.getKeyId(),
                arrayList.get(position).getUserId(),
                status,
                flag,
                message,
                arrayList.get(position).getConnect_request_id());

        responseObservable.subscribeOn(Schedulers.newThread())
                .observeOn(rx.android.schedulers.AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> {
                    if (throwable instanceof retrofit2.HttpException) {
                        retrofit2.HttpException ex = (retrofit2.HttpException) throwable;
                        statusCode = ex.code();
                        Log.e("error", ex.getLocalizedMessage());
                    } else if (throwable instanceof SocketTimeoutException) {
                        statusCode = 1000;
                    }
                    return Observable.empty();
                })
                .subscribe(new Observer<FriendModel>() {
                    @Override
                    public void onCompleted() {
                        pDialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("error", "" + e.getMessage());
                    }

                    @Override
                    public void onNext(FriendModel friendModel) {
                        statusCode = friendModel.getStatusCode();
                        if (statusCode == 200) {
                            Utility.displayToast(context, friendModel.getMessage());
                            if (fr_name.equals("favourite")) {
                                if (arrayList.get(position).getRequest_status().equals("3")) {
                                    //arrayList.get(position).setRequest_status("0");
                                    notifyItemChanged(position);
                                } else {
                                    notifyItemChanged(position);
                                    //  arrayList.get(position).setRequest_status("0");
                                }

                                //notifyDataSetChanged();
                            } else {
                                arrayList.remove(position);
                                notifyDataSetChanged();
                            }

                        } else {
                            if (fragment_name.equals("sent_request")) {
                                Utility.displayToast(context, "You allready cancel this Request");
                            } else {
                                Utility.displayToast(context, friendModel.getMessage());
                            }

                            //Utility.displayToast(context, friendModel.getMessage());
                        }

                    }
                });


    }

    public void RemoveShortlist(String profile_id, String user_id, int posi) {

        pDialog = new ProgressDialog(context);
        pDialog.setTitle("Checking Data");
        pDialog.setMessage("Please Wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        Observable<FriendModel> responseObservable = apiservice.RemoveShortlist(profile_id, user_id);

        responseObservable.subscribeOn(Schedulers.newThread())
                .observeOn(rx.android.schedulers.AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> {
                    if (throwable instanceof retrofit2.HttpException) {
                        retrofit2.HttpException ex = (retrofit2.HttpException) throwable;
                        statusCode = ex.code();
                        Log.e("error", ex.getLocalizedMessage());
                    } else if (throwable instanceof SocketTimeoutException) {
                        statusCode = 1000;
                    }
                    return Observable.empty();
                })
                .subscribe(new Observer<FriendModel>() {
                    @Override
                    public void onCompleted() {
                        pDialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("error", "" + e.getMessage());
                    }

                    @Override
                    public void onNext(FriendModel friendModel) {
                        statusCode = friendModel.getStatusCode();
                        if (statusCode == 200) {
                            Utility.displayToast(context, friendModel.getMessage());
                            arrayList.get(posi).setShortlist_status("0");

                            if (fr_name.equals("favourite")) {
                                removeAt(posi);
                            }

//                            notifyDataSetChanged();

                        } else {
                            Utility.displayToast(context, friendModel.getMessage());
                        }

                    }
                });

    }

    public void removeAt(int position) {
        arrayList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, arrayList.size());
    }

    public void RemoveBlocklist(String profile_id, String user_id, int pos) {

        pDialog = new ProgressDialog(context);
        pDialog.setTitle("Checking Data");
        pDialog.setMessage("Please Wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        Observable<FriendModel> responseObservable = apiservice.RemoveBlocklist(profile_id, user_id);

        responseObservable.subscribeOn(Schedulers.newThread())
                .observeOn(rx.android.schedulers.AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> {
                    if (throwable instanceof retrofit2.HttpException) {
                        retrofit2.HttpException ex = (retrofit2.HttpException) throwable;
                        statusCode = ex.code();
                        Log.e("error", ex.getLocalizedMessage());
                    } else if (throwable instanceof SocketTimeoutException) {
                        statusCode = 1000;
                    }
                    return Observable.empty();
                })
                .subscribe(new Observer<FriendModel>() {
                    @Override
                    public void onCompleted() {
                        pDialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("error", "" + e.getMessage());
                    }

                    @Override
                    public void onNext(FriendModel friendModel) {
                        statusCode = friendModel.getStatusCode();
                        if (statusCode == 200) {

                            //Toast.makeText(context,"Unblock profile",Toast.LENGTH_SHORT).show();
                            Utility.displayToast(context, friendModel.getMessage());
                            arrayList.get(pos).setBlock_status("0");
                            if (fr_name.equals("block")) {
                                removeAt(pos);
                            }

                            // holder.img_block.setImageResource(R.drawable.ic_block);
                        } else {
                            Utility.displayToast(context, friendModel.getMessage());
                        }

                    }
                });


    }


    private void displayDialog(int position, String flag) {

        dialog = new Dialog(context);
        dialog.getWindow();
        dialog.setTitle("Address");
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.add_message_layout);
        dialog.setCancelable(true);


        // spinner = dialog.findViewById(R.id.spin_year);
        etText = dialog.findViewById(R.id.etText);
        txt_que = dialog.findViewById(R.id.txt_que);
        txt_que1 = dialog.findViewById(R.id.txt_que1);
        txt_que2 = dialog.findViewById(R.id.txt_que2);

        btn_submit = dialog.findViewById(R.id.btn_submit);

        String yourString = arrayList.get(position).getQuestion();
        if (!yourString.equals("")) {
            txt_que.setVisibility(View.VISIBLE);
            txt_que1.setVisibility(View.VISIBLE);
            txt_que2.setVisibility(View.VISIBLE);
            List<String> items = Arrays.asList(yourString.split("\\s*,\\s*"));
            String formattedString = yourString.replace(',', '\n');
            String data[] = new String[items.size()];
            String text = "<font color=#cc0029>Question </font>";
            for (int i = 0; i < items.size(); i++) {
                String q = getColoredSpanned("Question " + (i + 1), "#000000");
                data[i] = q + ": " + items.get(i) + "\n";
            }

            String que = Arrays.toString(data);

            txt_que.setText(Html.fromHtml(data[0]));
            txt_que1.setText(Html.fromHtml(data[1]));
            txt_que2.setText(Html.fromHtml(data[2]));
        } else {
            txt_que.setVisibility(View.VISIBLE);
            txt_que.setText("No Questions Available");
            txt_que1.setVisibility(View.GONE);
            txt_que2.setVisibility(View.GONE);
        }
//        txt_que.setText(items.get());

        btn_submit.setOnClickListener(view -> {


            if (etText.getText().toString().equals("")) {
                Utility.displayToast(context, "Enter Your Message");
            } else {
                UpdateConnectRequest(position, status, flag, etText.getText().toString());


                dialog.dismiss();
            }

        });

        dialog.show();
        Window window = dialog.getWindow();
        assert window != null;
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);


    }


    public String covertTimeToText(String dataDate) {

        String convTime = null;

        String prefix = "";
        String suffix = "Ago";

        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date pasTime = dateFormat.parse(dataDate);
            Date nowTime = new Date();
            long dateDiff = nowTime.getTime() - pasTime.getTime();

            long second = TimeUnit.MILLISECONDS.toSeconds(dateDiff);
            long minute = TimeUnit.MILLISECONDS.toMinutes(dateDiff);
            long hour = TimeUnit.MILLISECONDS.toHours(dateDiff);
            long day = TimeUnit.MILLISECONDS.toDays(dateDiff);

            if (second < 60) {
                convTime = second + " Sec " + suffix;
            } else if (minute < 60) {
                convTime = minute + " Min " + suffix;
            } else if (hour < 24) {
                convTime = hour + " Hours " + suffix;
            } else if (day >= 7) {
                if (day > 30) {
                    convTime = (day / 30) + " Months " + suffix;
                } else if (day > 360) {
                    convTime = (day / 360) + " Years " + suffix;
                } else {
                    convTime = (day / 7) + " Week " + suffix;
                }
            } else if (day < 7) {
                convTime = day + " Days " + suffix;
            }

        } catch (ParseException e) {
            e.printStackTrace();
            Log.e("ConvTimeE", e.getMessage());
        }

        return convTime;
    }

    public void updateList(List<ProfileSearch.Data> newList) {
        this.arrayList.clear();
        this.arrayList.addAll(newList);
    }


    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    public void changeButtonStatus() {

    }

    public void ShortList(int posi) {
        pDialog = new ProgressDialog(context);
        pDialog.setTitle("Checking Data");
        pDialog.setMessage("Please Wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        Observable<ProfileSearch> responseObservable = apiservice.shortlist(sessionManager.getKeyId(),
                arrayList.get(posi).getUserId());

        responseObservable.subscribeOn(Schedulers.newThread())
                .observeOn(rx.android.schedulers.AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> {
                    if (throwable instanceof retrofit2.HttpException) {
                        retrofit2.HttpException ex = (retrofit2.HttpException) throwable;
                        statusCode = ex.code();
                        Log.e("error", ex.getLocalizedMessage());
                    } else if (throwable instanceof SocketTimeoutException) {
                        statusCode = 1000;
                    }
                    return Observable.empty();
                })
                .subscribe(new Observer<ProfileSearch>() {
                    @Override
                    public void onCompleted() {
                        pDialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("error", "" + e.getMessage());
                    }

                    @Override
                    public void onNext(ProfileSearch profileSearch) {
                        statusCode = profileSearch.getStatusCode();
                        if (statusCode == 200) {
                            Utility.displayToast(context, profileSearch.getMessage());
                            arrayList.get(posi).setShortlist_status("1");
                        } else {
                            Utility.displayToast(context, profileSearch.getMessage());
                        }

//                            btn_short.setText("ShortListed");
//                            btn_short.setEnabled(false);

                    }
                });
    }

    public void BlockList(int pos) {
        pDialog = new ProgressDialog(context);
        pDialog.setTitle("Checking Data");
        pDialog.setMessage("Please Wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        Observable<ProfileSearch> responseObservable = apiservice.blocklist(sessionManager.getKeyId(),
                arrayList.get(pos).getUserId());

        responseObservable.subscribeOn(Schedulers.newThread())
                .observeOn(rx.android.schedulers.AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> {
                    if (throwable instanceof retrofit2.HttpException) {
                        retrofit2.HttpException ex = (retrofit2.HttpException) throwable;
                        statusCode = ex.code();
                        Log.e("error", ex.getLocalizedMessage());
                    } else if (throwable instanceof SocketTimeoutException) {
                        statusCode = 1000;
                    }
                    return Observable.empty();
                })
                .subscribe(new Observer<ProfileSearch>() {
                    @Override
                    public void onCompleted() {
                        pDialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("error", "" + e.getMessage());
                    }

                    @Override
                    public void onNext(ProfileSearch profileSearch) {
                        statusCode = profileSearch.getStatusCode();
                        if (statusCode == 200) {
                            Utility.displayToast(context, profileSearch.getMessage());

                            arrayList.get(pos).setBlock_status("1");


                            // img_block.setImageResource(R.drawable.ic_block_fill);
                        } else {
                            Utility.displayToast(context, profileSearch.getMessage());
                        }

//                            btn_short.setText("ShortListed");
//                            btn_short.setEnabled(false);

                    }
                });
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_name)
        TextView txt_name;
        @BindView(R.id.lnr_flag)
        LinearLayout lnr_flag;
        @BindView(R.id.txt_year)
        TextView txt_year;
        //        @BindView(R.id.txt_work)
//        TextView txt_work;
        @BindView(R.id.txt_city)
        TextView txt_city;
        @BindView(R.id.img_profile)
        ImageView img_profile;
        @BindView(R.id.lnr_profile)
        LinearLayout lnr_profile;
        @BindView(R.id.btn_conect)
        Button btn_conect;
        //        @BindView(R.id.bbbb)
//        LinearLayout btn_con;
//        @BindView(R.id.lnr_online)
//        LinearLayout lnr_online;
//        @BindView(R.id.txt_online)
//        TextView txt_online;
        @BindView(R.id.img_primium)
        ImageView img_primium;
        @BindView(R.id.img_plan)
        ImageView img_plan;
        @BindView(R.id.image_shortlist)
        ImageView image_shortlist;

        ImageView img_block;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            img_block = itemView.findViewById(R.id.img_block);

        }
    }

    private AlertDialog AskOption(int position) {

        AlertDialog myQuittingDialogBox = new AlertDialog.Builder(context)
                .setTitle("Flag Inappropriate")
                .setMessage("Do you want to flag inappropriate?")
                .setIcon(R.drawable.flag)

                .setPositiveButton("Flag", new DialogInterface.OnClickListener() {
                    ArrayList<String> flaglist = new ArrayList<>();


                    public void onClick(DialogInterface dialog, int whichButton) {


                        if (getList().size() > 0)
                            flaglist.addAll(getList());
                        flaglist.add(arrayList.get(position).getUserId());
                        setList("KEY_flag", flaglist);
                        Utility.displayToast(context, "User flag inappropriate successfully");

                        ((Activity) context).finish();

                    }
                })
                .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create();
        return myQuittingDialogBox;

    }

    public <T> void setList(String key, List<T> list) {
        Gson gson = new Gson();
        String json = gson.toJson(list);

        set(key, json);
    }

    public static void set(String key, String value) {
        editor.putString(key, value);
        editor.commit();
    }

    public List<String> getList() {
        List<String> arrayItems = new ArrayList<>();
        String serializedObject = sharedPreferences.getString("KEY_flag", null);
        if (serializedObject != null) {
            Gson gson = new Gson();
            Type type = new TypeToken<List<String>>() {
            }.getType();
            arrayItems = gson.fromJson(serializedObject, type);
        }
        return arrayItems;
    }
}
