package com.dies.suunis4marriage.adapter;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dies.suunis4marriage.R;
import com.dies.suunis4marriage.activity.ChatRoomActivity;
import com.dies.suunis4marriage.apiservice.ApiConstants;
import com.dies.suunis4marriage.application.SessionManager;
import com.dies.suunis4marriage.application.Utility;
import com.dies.suunis4marriage.model.FriendModel;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class ChatFriendAdapter extends RecyclerView.Adapter<ChatFriendAdapter.MyViewHolder> {
    Context context;
    List<FriendModel.Data> arrayList;
    SessionManager sessionManager;

    public ChatFriendAdapter(Context context, List<FriendModel.Data> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
        sessionManager=new SessionManager(context);

    }

    @NonNull
    @Override
    public ChatFriendAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_friend_layout,parent,false);
        return new ChatFriendAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ChatFriendAdapter.MyViewHolder holder, int position) {
        holder.name.setText(arrayList.get(position).getName());


        if (sessionManager.getUserData().get(0).getGender().equals("MALE")) {
            Picasso.with(context).load(ApiConstants.IMAGE_URL + arrayList.get(position).getProfile_image())
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .networkPolicy(NetworkPolicy.NO_CACHE)
                    .placeholder(R.drawable.ic_female_avatar)
                    .into(holder.imageView);
        }else {
            Picasso.with(context).load(ApiConstants.IMAGE_URL + arrayList.get(position).getProfile_image())
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .networkPolicy(NetworkPolicy.NO_CACHE)
                    .placeholder(R.drawable.ic_male_avtar)
                    .into(holder.imageView);
        }

        holder.card_friend.setOnClickListener(view -> {
            Utility.getAppcon().getSession().arrayListFriendDetail= new ArrayList<>();
            Utility.getAppcon().getSession().arrayListFriendDetail.add(arrayList.get(position));
            context.startActivity(new Intent(context, ChatRoomActivity.class));
        });

//        if (arrayList.getUnreadCount() > 0) {
//            holder.bedge_counter.setText(String.valueOf(chatRoom.getUnreadCount()));
//            holder.bedge_counter.setVisibility(View.VISIBLE);
//        } else {
//            holder.bedge_counter.setVisibility(View.GONE);
//        }

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.txt_name)
        TextView name;
//        @BindView(R.id.txt_bio)
//        TextView Bio;
        @BindView(R.id.profile_image)
        CircleImageView imageView;
        @BindView(R.id.card_friend)
        CardView card_friend;
        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
