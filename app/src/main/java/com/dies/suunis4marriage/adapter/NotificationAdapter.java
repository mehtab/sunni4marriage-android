package com.dies.suunis4marriage.adapter;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.dies.suunis4marriage.R;
import com.dies.suunis4marriage.apiservice.ApiConstants;
import com.dies.suunis4marriage.model.NotificationModel;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.MyViewHolder> {

    Context context;
    List<NotificationModel.Data> arrayList;

    public NotificationAdapter(Context context, List<NotificationModel.Data> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public NotificationAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.notification_layout,parent,false);
        return new NotificationAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NotificationAdapter.MyViewHolder holder, int position) {

        holder.txt_notiname.setText(arrayList.get(position).getNf_message());


//        holder.card_noti.setOnClickListener(view -> {
//
//            if(arrayList.get(position).getRedirect_status().equals("1")){
//
//                Intent intent=new Intent(context, PostViewActivity.class);
//                intent.putExtra("data",arrayList.get(position).getPost_id());
//                context.startActivity(intent);
//
//            }else if (arrayList.get(position).getRedirect_status().equals("2")){
//
//                Utility.getAppcon().getSession().screen="friend_status";
//                Intent intent=new Intent(context, FriendActivity.class);
//                intent.putExtra("data",arrayList.get(position).getRedirect_status());
//                context.startActivity(intent);
//
//
//            }else if (arrayList.get(position).getRedirect_status().equals("3")){
//
//                Intent intent=new Intent(context, FriendActivity.class);
//                intent.putExtra("data",arrayList.get(position).getRedirect_status());
//                context.startActivity(intent);
//            }
//
//        });



//        holder.profile_image.setRadius(15);
//        holder.profile_image.setBorderColor(Color.WHITE);
        Picasso.with(context).load(ApiConstants.IMAGE_URL + arrayList.get(position).getProfile_image())
                .memoryPolicy(MemoryPolicy.NO_CACHE)
                .networkPolicy(NetworkPolicy.NO_CACHE)
                .placeholder(R.drawable.splash_image)
                .into(holder.profile_image);
//
       // holder.txt_date.setText(arrayList.get(position).getNf_created());


        String dateStr=arrayList.get(position).getNf_created();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        df.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = null;
        try {
            date = df.parse(dateStr);
            df.setTimeZone(TimeZone.getDefault());
            String formattedDate = df.format(date);
            holder.txt_date.setText(parseDateToddMMyyyy(formattedDate));
        } catch (ParseException e) {
            e.printStackTrace();
        }

//
//      //  holder.txt_time.setText(arrayList.get(position).getCreated_time());
//
//        holder.txt_notiname.setOnClickListener(view -> {
//
//        });

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.profile_image)
        CircleImageView profile_image;
        @BindView(R.id.txt_notiname)
        TextView txt_notiname;
        @BindView(R.id.txt_date)
        TextView txt_date;
////        @BindView(R.id.txt_time)
////        TextView txt_time;
//        @BindView(R.id.card_noti)
//CardView card_noti;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }

    public String parseDateToddMMyyyy(String time) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "dd-MMM-yyyy h:mm a";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }
}
