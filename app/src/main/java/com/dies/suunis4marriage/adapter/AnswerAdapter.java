package com.dies.suunis4marriage.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.dies.suunis4marriage.R;
import com.dies.suunis4marriage.model.QuestionsAnswerModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AnswerAdapter extends RecyclerView.Adapter<AnswerAdapter.MyViewHolder> {

   public static List<QuestionsAnswerModel.Data> arrayList;
    Context context;
  //  EditText ed[] = new EditText[arrayList.size()];
  String[] escrito;
    ArrayList<String> data;


    public AnswerAdapter(List<QuestionsAnswerModel.Data> arrayList, Context context) {
        this.arrayList = arrayList;
        this.context = context;
        data=new ArrayList<>();
        escrito = new String[arrayList.size()];
    }

    @NonNull
    @Override
    public AnswerAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.answer_layout,parent,false);
        return new AnswerAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AnswerAdapter.MyViewHolder holder, int position) {

        holder.tv_questions.setText("Questions: "+arrayList.get(position).getQuestions());

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public String[] getEscrito() {
        return escrito;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_questions)
        TextView tv_questions;
        @BindView(R.id.et_answer)
        EditText et_answer;



        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            et_answer.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    arrayList.get(getAdapterPosition()).setEditTextValue(et_answer.getText().toString());
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });


        }
    }
}
