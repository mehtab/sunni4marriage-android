package com.dies.suunis4marriage.application;

public class Card {
    private Object installments;
    private Object mandateOptions;
    private Object network;
    private String requestThreeDSecure;

    public Object getInstallments() {
        return installments;
    }

    public void setInstallments(Object installments) {
        this.installments = installments;
    }

    public Object getMandateOptions() {
        return mandateOptions;
    }

    public void setMandateOptions(Object mandateOptions) {
        this.mandateOptions = mandateOptions;
    }

    public Object getNetwork() {
        return network;
    }

    public void setNetwork(Object network) {
        this.network = network;
    }

    public String getRequestThreeDSecure() {
        return requestThreeDSecure;
    }

    public void setRequestThreeDSecure(String requestThreeDSecure) {
        this.requestThreeDSecure = requestThreeDSecure;
    }
}
