 package com.dies.suunis4marriage.application;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.dies.suunis4marriage.activity.WelComeActivity;
import com.dies.suunis4marriage.model.FriendModel;
import com.dies.suunis4marriage.model.ProfileSearch;
import com.dies.suunis4marriage.model.UserDataResponse;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;


public class SessionManager {
    private SharedPreferences pref,pref2,pref3;
    private Editor editor,editor2,editor3,editor_payment;
    private static final String PREF_NAME = "skzlogin";
    private static final String IS_LOGIN = "IsLoggedIn";
    private static final String IS_PAYMENT= "ispayment";
    private static final String IS_PLANACTIVATE= "ispalnactivate";
    private static final String IS_NEW = "IsNew";
    private static String KEY_EMAIL = "id";
    private static String KEY_ID="user_id";
    private static String KEY_TOKEN = "token";
    private static String KEY_PASSWORD = "password";
    private static String KEY_USER_DETAILS = "userdetails";
    private static String KEY_IMAGE = "image_list";
    private static String LOGIN_TYPE = "login_type";
    private static String FCM_ID = "fcm_id";
    private static final String PREF_DEVICE = "device";
    private static final String PREF_DATA = "searchdata";
    private static final String CUSTOMER_ID = "customer_id";

    public List<ProfileSearch.Data> arrayListProfile = new ArrayList<>();
    public List<ProfileSearch.Data> arrayListProfileall = new ArrayList<>();
    public List<FriendModel.Data> arrayListFriendDetail= new ArrayList<>();
    public List<String> arrayListImages= new ArrayList<>();


    public String fragment_name = "";
    public String amount = "";
    public String payment_type = "";
    public String screen_name= "";
    public String plan_type = "";

    public String Image = "";
    public String SearchVIew= "";
    public String minage= "";
    public String maxage= "";
    public String miles= "";
    public double lattitude= 0;
    public double longitude= 0;
    private Context _context;


    public SessionManager(Context context) {
        int PRIVATE_MODE = 0;
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
        pref2 = context.getSharedPreferences(PREF_DEVICE, PRIVATE_MODE);
        editor2 = pref2.edit();
        pref3 = context.getSharedPreferences(PREF_DATA, PRIVATE_MODE);
        editor3 = pref3.edit();
    }

    public String getFcmId() {
        return pref2.getString(FCM_ID, null);
    }

    public  void setFcmId(String fcmId) {
        editor2 = pref2.edit();
        editor2.putString(FCM_ID, fcmId);
        editor2.apply();
    }

    public String getKeyToken() {
        return pref.getString(KEY_TOKEN, null);
    }

    public void setKeyToken(String token) {
        editor.putString(KEY_TOKEN, token);
        editor.commit();
    }

    public String getCustomerId() {
        return pref.getString(CUSTOMER_ID, null);
    }

    public void setCustomerId(String customerId) {
        editor.putString(CUSTOMER_ID, customerId);
        editor.commit();
    }

    public String getKeyPassword() {
        return pref.getString(KEY_PASSWORD, null);
    }

    public void setKeyPassword(String keyPassword) {
        editor.putString(KEY_PASSWORD, keyPassword);
        editor.commit();
    }

    public String getKeyEmail() {
        return pref.getString(KEY_EMAIL, "");
    }

    public void setKeyEmail(String email) {
        editor.putString(KEY_EMAIL, email);
        editor.commit();
    }

    public String getKeyId() {
        return pref.getString(KEY_ID, null);
    }

    public void setKeyId(String keyId) {
        editor.putString(KEY_ID, keyId);
        editor.apply();
    }

    public String getLoginType() {
        return pref.getString(LOGIN_TYPE, "");
    }

    public void setLoginType(String loginType) {
        editor.putString(LOGIN_TYPE, loginType);
        editor.commit();
    }

    public void createLoginSession() {
        editor.putBoolean(IS_LOGIN, true);
        editor.commit();
    }
    public void checkInstall() {
        editor.putBoolean(IS_NEW, true);
        editor.commit();
    }
    public boolean isInstalled() {
        return pref.getBoolean(IS_NEW, false);
    }

    public void setKeyUserDetails(String userData) {
        editor.putString(KEY_USER_DETAILS, userData);
        editor.commit();
    }

    public List<UserDataResponse.Data> getUserData() {
        Type type = new TypeToken<List<UserDataResponse.Data>>() {
        }.getType();
        return new Gson().fromJson(pref.getString(KEY_USER_DETAILS, null), type);
    }

    public void setKeyImage(String keyImage) {
        editor.putString(KEY_IMAGE, keyImage);
        editor.commit();
    }
    public String getKeyImage() {
        return pref.getString(KEY_IMAGE, "");
    }

//    public List<ImagePathResponse.Data> getImageData() {
//        Type type = new TypeToken<List<ImagePathResponse.Data>>() {
//        }.getType();
//        return new Gson().fromJson(pref.getString(KEY_IMAGE, null), type);
//    }

    public void logoutUser() {
        editor.clear();
        editor.commit();

        Intent i = new Intent(_context, WelComeActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY);
        _context.startActivity(i);
    }

    public boolean isLoggedIn() {
        return pref.getBoolean(IS_LOGIN, false);
    }

    public void setEthenicity(String ethnicities) {
        editor3.putString("ethnicities", ethnicities);
        editor3.commit();
    }
//    public String getLanguage() {
//        return pref3.getString("languagedata", null);
//    }

    public String getEthenicity() {
        return pref3.getString("ethnicities", null);
    }

    public void setLocation(String Location) {
        editor3.putString("location", Location);
        editor3.commit();
    }

    public String getLocation() {
        return pref3.getString("location", null);
    }

    public void setNationality(String Nationality) {
        editor3.putString("nationality", Nationality);
        editor3.commit();
    }

    public String getNationality() {
        return pref3.getString("nationality", null);
    }

    public void setMerital_status(String merital_status) {
        editor3.putString("merital_status", merital_status);
        editor3.commit();
    }

    public String getMerital_status() {
        return pref3.getString("merital_status", null);
    }
    public void setEducationLevel(String EducationLevel) {
        editor3.putString("education_level", EducationLevel);
        editor3.commit();
    }
    public String getEducationLevel() {
        return pref3.getString("education_level", null);
    }
    public void setLanguage(String language) {
        editor3.putString("language", language);
        editor3.commit();
    }
    public String getLanguage() {
        return pref3.getString("language", null);
//        editor3.putString("language", language);
//        editor3.commit();
    }
    public void setIslamic(String islamic) {
        editor3.putString("islamic", islamic);
        editor3.commit();
    }
    public String getIslamic() {
        return pref3.getString("islamic", null);
    }

    public void setMinMaxValue(String minvalue) {
        editor3.putString("minvalue", minvalue);
        editor3.commit();
    }
    public String getMinMaxValue() {
        return pref3.getString("minvalue", null);
    }

    public void setMaxValue(String maxvalue) {
        editor3.putString("maxvalue", maxvalue);
        editor3.commit();
    }
    public String getMaxValue() {
        return pref3.getString("maxvalue", null);
    }

    public void setMinValue(String minvalue) {
        editor3.putString("minimumvalue", minvalue);
        editor3.commit();
    }
    public String getMinValue() {
        return pref3.getString("minimumvalue", null);
    }

    public void ClearSearchData() {
        editor3.clear();
        editor3.commit();

//        Intent i = new Intent(_context, WelComeActivity.class);
//        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY);
//        _context.startActivity(i);
    }



    public void setCountryId(String minvalue) {
        editor2.putString("country_id", minvalue);
        editor2.commit();
    }
    public String getCountryId() {
        return pref3.getString("country_id", null);
    }


    public void createPaymentSession() {
        editor.putBoolean(IS_PAYMENT, true);
        editor.commit();
    }

    public void destroyPaymentSession() {
        editor.putBoolean(IS_PAYMENT, false);
        editor.commit();
    }



    public boolean isPayment() {
        return pref.getBoolean(IS_PAYMENT, false);
    }

    public void createPlanSession() {
        editor.putBoolean(IS_PLANACTIVATE, true);
        editor.commit();
    }

    public boolean isPlanActivate() {
        return pref.getBoolean(IS_PLANACTIVATE, false);
    }



}