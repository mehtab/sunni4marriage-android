package com.dies.suunis4marriage.application;

import android.app.ActivityManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;


import com.dies.suunis4marriage.R;
import com.dies.suunis4marriage.activity.LoginActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.List;

/**
 * Created by Kunal on 12/5/2017.
 */

public class FcmMessagingService extends FirebaseMessagingService {

    String title;
    String message;
    String user_id;
    public static final String NOTIFICATION_CHANNEL_ID = "test_notification";
    public static final int NOTIFICATION_ID = 001;
    public static final String FLAG = "flag";
        public static final String FRIEND= "friend";
    public static final String GROUP = "group";
    public static final String LIKE = "like";
    public static final String PUSH_NOTIFICATION = "pushnotification";


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        if (remoteMessage.getData().size() > 0) {
            title = remoteMessage.getData().get("title");
            message = remoteMessage.getData().get("message");
            NotificationUtils notificationHandler = new NotificationUtils(getApplicationContext());

            if (remoteMessage.getData().get("flag").equals(FLAG)){
                user_id = remoteMessage.getData().get("id");

                Intent pushNotification = new Intent(Constant.PUSH_NOTIFICATION);
                //Adding notification data to the intent
                pushNotification.putExtra("message", message);
                pushNotification.putExtra("name", title);
                pushNotification.putExtra("id",user_id);

                if (!isAppIsInBackground(getApplicationContext())) {
                    //Sending a broadcast to the chatroom to add the new message
                    LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);
                }else {
                    notificationHandler.sendNotification(title, message,FLAG);
                }
//                LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);
            }else if (remoteMessage.getData().get("flag").equals(FRIEND)){
                notificationHandler.sendNotification(title, message,FLAG);
            }else if (remoteMessage.getData().get("flag").equals(GROUP)){
                notificationHandler.sendNotification(title, message,FLAG);
            }else if (remoteMessage.getData().get("flag").equals(LIKE)){
                notificationHandler.sendNotification(title, message,FLAG);
            }
        }
      //  sendNotification(title,message);
    }

    private void sendNotification(String titlee,String messagee) {

        createNotificationChannel(messagee);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);
        builder.setSmallIcon(R.mipmap.ic_launcher);
        builder.setContentTitle(titlee);
        builder.setContentText(messagee);
        builder.setPriority(NotificationCompat.PRIORITY_DEFAULT);
        NotificationCompat.BigTextStyle bigTextStyle = new NotificationCompat.BigTextStyle();
        bigTextStyle.bigText(messagee);
        builder.setStyle(bigTextStyle);
        builder.setColor(getResources().getColor(R.color.colorPrimary));

        Intent i = new Intent(this, LoginActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, i, PendingIntent.FLAG_ONE_SHOT);

        Bitmap bm = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
        builder.setLargeIcon(bm);
        builder.setContentIntent(pendingIntent);
        builder.setAutoCancel(true);

        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(this);
        notificationManagerCompat.notify(NOTIFICATION_ID, builder.build());
    }

    private void createNotificationChannel(String description) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "test_notification";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;

            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, name, importance);
            notificationChannel.setDescription(description);

            NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            assert notificationManager != null;
            notificationManager.createNotificationChannel(notificationChannel);
        }
    }

    public static boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }

        return isInBackground;
    }

}
