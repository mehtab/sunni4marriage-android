package com.dies.suunis4marriage.application;

public class PaymentMethodOptions {
    private Card card;

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }
}
