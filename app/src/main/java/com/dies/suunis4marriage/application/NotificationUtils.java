package com.dies.suunis4marriage.application;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.dies.suunis4marriage.R;
import com.dies.suunis4marriage.activity.LoginActivity;

import static android.content.Context.NOTIFICATION_SERVICE;

public class NotificationUtils {
    private Context mContext;
    public static final String NOTIFICATION_CHANNEL_ID = "test_notification";
    public static final int NOTIFICATION_ID = 001;
    PendingIntent pendingIntent;


    public NotificationUtils(Context mContext) {
        this.mContext = mContext;
    }

    public void sendNotification(String titlee,String messagee,String flag) {

        createNotificationChannel(messagee);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(mContext, NOTIFICATION_CHANNEL_ID);
        builder.setSmallIcon(R.mipmap.ic_launcher);
        builder.setContentTitle(titlee);
        builder.setContentText(messagee);
        builder.setPriority(NotificationCompat.PRIORITY_DEFAULT);
        NotificationCompat.BigTextStyle bigTextStyle = new NotificationCompat.BigTextStyle();
        bigTextStyle.bigText(messagee);
        builder.setStyle(bigTextStyle);
        builder.setColor(mContext.getResources().getColor(R.color.colorPrimary));


            Intent i = new Intent(mContext, LoginActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            pendingIntent = PendingIntent.getActivity(mContext, 0, i, PendingIntent.FLAG_ONE_SHOT);




        Bitmap bm = BitmapFactory.decodeResource(mContext.getResources(), R.mipmap.ic_launcher);
        builder.setLargeIcon(bm);
        builder.setContentIntent(pendingIntent);
        builder.setAutoCancel(true);

        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(mContext);
        notificationManagerCompat.notify(NOTIFICATION_ID, builder.build());
    }

    private void createNotificationChannel(String description) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "test_notification";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;

            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, name, importance);
            notificationChannel.setDescription(description);

            NotificationManager notificationManager = (NotificationManager)mContext.getSystemService(NOTIFICATION_SERVICE);
            assert notificationManager != null;
            notificationManager.createNotificationChannel(notificationChannel);
        }
    }


}
