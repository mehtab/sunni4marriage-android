package com.dies.suunis4marriage.application;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by Kunal on 12/5/2017.
 */

public class FcmInstanceIdService extends FirebaseInstanceIdService {

    SessionManager sessionManager = null;

    @Override
    public void onTokenRefresh() {
        String recent_token = FirebaseInstanceId.getInstance().getToken();
        sessionManager = new SessionManager(this);
        sessionManager.setFcmId(recent_token);
        Log.e("token_called", recent_token);
    }

}
